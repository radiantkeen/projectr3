datablock AudioProfile(ShieldsDownSound)
{
   filename    = "fx/weapons/chaingun_off.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ShieldsUpSound)
{
   filename    = "fx/misc/diagnostic_on.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock StaticShapeData(VehicleShieldEmitter)
{
   shapeFile = "turret_sentry.dts";
};

datablock StaticShapeData(VehicleIntegrityField)
{
   shapeFile = "beacon.dts";
};

function VArmorNone::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

VehiclePart.registerVehiclePart($VehiclePartType::Armor, "VArmorNone", "None", "No Additional Armoring", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Omni);

function VModuleNone::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
}

function VModuleNone::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "VModuleNone", "None", "No Module", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Omni);

// VehicleShieldEmitter
function VehicleData::installShield(%data, %obj, %maxStrength, %recDelay, %recRate, %powerDrain, %shieldblock)
{
    %shield = new StaticShape()
    {
        dataBlock = %shieldblock;
        team = %obj.teamBought;
        scale = "1 1 1";
    };

   MissionCleanup.add(%shield);

   %obj.mountObject(%shield, 15);
   %shield.setCloaked(true);
   %obj.shieldSource = %shield;
   %obj.rechargeTick *= 1 - %powerDrain;

   %shield.sourceVehicle = %obj;
   %shield.init = true;
   %shield.maxStrength = %maxStrength;
   %shield.rechargeRate = %recRate;
   %shield.rechargeDelay = %recDelay;
   %shield.getDatablock().startShield(%shield);
}

function VehicleShieldEmitter::startShield(%data, %shield)
{
    if(%shield.init == true)
    {
        %shield.init = false;
        %shield.strength = 0;
        %shield.tickRate = $g_TickTime;
    }

    if(%shield.maxStrength > 0)
        %data.startRecharge(%shield);
    else
        %shield.sourceVehicle.isShielded = false;
}

function VehicleShieldEmitter::startRecharge(%data, %shield)
{
    if(!isObject(%shield.sourceVehicle))
    {
        %shield.schedule(1000, "delete");
        return;
    }

    %shield.sourceVehicle.isShielded = true;
    %shield.sourceVehicle.playAudio(2, ShieldsUpSound);
    
    if(%shield.strength == 0)
        %shield.strength = %shield.maxStrength * 0.25;
        
    %data.tickRecharge(%shield);
}

function VehicleShieldEmitter::restartRecharge(%data, %shield, %bDepleted)
{
    %shield.sourceVehicle.isShielded = (%shield.strength > 0);

    if(%shield.rechargeThread > 0)
        cancel(%shield.rechargeThread);

    if(%bDepleted)
        %shield.sourceVehicle.playAudio(2, ShieldsDownSound);
        
    %shield.rechargeThread = %data.schedule(%shield.rechargeDelay, "startRecharge", %shield);
}

function VehicleShieldEmitter::tickRecharge(%data, %shield)
{
    if(%shield.strength < %shield.maxStrength)
    {
        %shield.strength += %shield.rechargeRate;

        if(%shield.strength > %shield.maxStrength)
            %shield.strength = %shield.maxStrength;
            
        %shield.rechargeThread = %data.schedule(%shield.tickRate, "tickRecharge", %shield);
    }
    else
        %shield.rechargeThread = 0;
}

function VehicleShieldEmitter::onShieldDamage(%data, %shield, %obj, %position, %amount, %damageType, %element)
{
    if(%element == $DamageGroupMask::Sonic)
        return %amount;
        
    if(%obj.lastHitFlags & $Projectile::ArmorOnly)
        %amount = 0;
    else if(%obj.lastHitFlags & $Projectile::ShieldsOnly)
        %amount *= 2;
        
    %energy = %shield.strength;

    // Passthrough if no shields
    if(%energy < 1)
        return %amount;
    
    // shield ignore stuff
    if(%obj.lastHitFlags & $Projectile::IgnoreShields)
        return %amount;
   
    %shieldScale = %obj.getDataBlock().shieldDamageScale[%damageType];

    if(%shieldScale $= "")
        %shieldScale = 1;

    %amount *= %shieldScale;
    %amount *= %obj.shieldDamageFactor[%element];
    %remainder = 0;
    
    if(%energy > %amount)
    {
        %shield.strength = %energy - %amount;
        %obj.playShieldEffect("0.0 0.0 1.0"); // find a new shield effect
        
        %shield.getDatablock().restartRecharge(%shield, false);
    }
    else
    {
        %shield.strength = 0;
        %remainder = %amount - %energy;
        
        if(%remainder < 1)
            %remainder = 0;
        else
            %remainder /= 100;
            
        %shield.getDatablock().restartRecharge(%shield, true);
    }
    
    if(%obj.lastHitFlags & $Projectile::ShieldsOnly)
        %remainder = 0;
    
    return %remainder;
}

// -----------------------------------------------------------------------------

function VehicleIntegrityField::startShield(%data, %shield)
{
    if(%shield.init == true)
    {
        %shield.sourceVehicle.isShielded = false;
        %shield.init = false;
    }
}

function VehicleIntegrityField::onShieldDamage(%data, %shield, %obj, %position, %amount, %damageType)
{
    return %amount * 0.7;
}

function Armor::installShield(%data, %obj, %maxStrength, %recDelay, %recRate, %powerDrain, %shieldblock)
{
    // not needed as module does this
}

function VShieldNone::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function VShieldNone::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 0, 0, 0, 0, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "VShieldNone", "None", "No Shields", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Omni);

datablock TurretImageData(SeekingTurretParam)
{
   mountPoint = 2;
   shapeFile = "turret_muzzlepoint.dts";

   projectile = AssaultChaingunBullet;
   projectileType = TracerProjectile;

   useCapacitor = true;
   usesEnergy = true;

   // Turret parameters
   activationMS      = 1000;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 200;
   degPerSecTheta    = 500;
   degPerSecPhi      = 500;

   attackRadius      = 75;
   
   isSeeker     = true;
   seekRadius   = 300;
   maxSeekAngle = 30;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 10;
};

function XEmptyHardpoint::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
}

function XEmptyHardpoint::installPart(%this, %data, %vehicle, %player)
{

}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "XEmptyHardpoint", "Empty Hardpoint", "Empty Hardpoint", -1, -1, -1);
