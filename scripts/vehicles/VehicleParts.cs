// VehiclePart System
function VehiclePart::__construct(%this)
{
    VehiclePart.Version = 1.0;
}

function VehiclePart::__destruct(%this)
{
   // Thou shalt not spam
}

if(!isObject(VehiclePart))
   System.addClass(VehiclePart);

// Classes
$VehicleClass::Flying               = 0;
$VehicleClass::Hovering             = 1;
$VehicleClass::Ground               = 2;
$VehicleClass::Walker               = 3;

// Vehicle Part Types
$VehiclePartType::Armor             = 0;
$VehiclePartType::Module            = 1;
$VehiclePartType::Shield            = 2;
$VehiclePartType::Weapon            = 3;

// Sizes
$VHardpointSize::Small              = 0;
$VHardpointSize::Medium             = 1;
$VHardpointSize::Large              = 2;
$VHardpointSize::Internal           = 3;
$VHardpointSize::Turret             = 4;
$VHardpointSize::Bay                = 5;

// Types
$VHardpointType::Energy             = 1 << 0;
$VHardpointType::Ballistic          = 1 << 1;
$VHardpointType::Missile            = 1 << 2;
$VHardpointType::Bombs              = 1 << 3;
$VHardpointType::Rack               = 1 << 4;
$VHardpointType::Artillery          = 1 << 5;
$VHardpointType::Standard           = $VHardpointType::Energy | $VHardpointType::Ballistic;
$VHardpointType::AmmoOnly           = $VHardpointType::Ballistic | $VHardpointType::Missile;
$VHardpointType::Omni               = $VHardpointType::Energy | $VHardpointType::Ballistic | $VHardpointType::Missile;
$VHardpointType::Anything           = -1;

// Trigger Types
$VMTrigger::Push = 0;
$VMTrigger::Toggle = 1;
$VMTrigger::Passive = 2;

// Vehicle Firegroups
//----------------------------------------------------------------------------

$VehicleMaxFireGroups = 0;

$VehicleFiregroup::Alpha = 1 << $VehicleMaxFireGroups;
$VehicleFiregroupMask[$VehicleMaxFireGroups] = $VehicleFiregroup::Alpha;
$VehicleFiregroupID[$VehicleFiregroup::Alpha] = $VehicleMaxFireGroups;
$VehicleFiregroupName[$VehicleMaxFireGroups] = "Alpha";
$VehicleMaxFireGroups++;

$VehicleFiregroup::Beta = 1 << $VehicleMaxFireGroups;
$VehicleFiregroupMask[$VehicleMaxFireGroups] = $VehicleFiregroup::Beta;
$VehicleFiregroupID[$VehicleFiregroup::Beta] = $VehicleMaxFireGroups;
$VehicleFiregroupName[$VehicleMaxFireGroups] = "Beta";
$VehicleMaxFireGroups++;

$VehicleFiregroup::Gamma = 1 << $VehicleMaxFireGroups;
$VehicleFiregroupMask[$VehicleMaxFireGroups] = $VehicleFiregroup::Gamma;
$VehicleFiregroupID[$VehicleFiregroup::Gamma] = $VehicleMaxFireGroups;
$VehicleFiregroupName[$VehicleMaxFireGroups] = "Gamma";
$VehicleMaxFireGroups++;

//------------------------------------------------------------------------------
// Vehicles
$VehicleCount = 0;

// Skycutter Interceptor
// -----------------------------------------------------------------------------
$VehicleListID["ScoutFlyer"] = $VehicleCount;
$VehicleList[$VehicleCount] = "ScoutFlyer";
$VehicleListData[$VehicleCount, "block"] = "ScoutFlyer";
$VehicleListData[$VehicleCount, "name"] = "Skycutter Interceptor";
$VehicleListData[$VehicleCount, "desc"] = "Quad wing fast moving scout vehicle";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "hardpoints"] = 2;
$VehicleList::Skycutter = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Skycutter = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[M] Quad Wingtip Mounts";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Medium;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Rack;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[M] Dual Nose Mounts";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Medium;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;

$VehicleMax["ScoutFlyer"] = 3;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "MPumaMissile";
$DefaultVehicleWep[$VehicleCount, 1] = "MBlaster";
$VehicleCount++;

// Outlaw Hoverjeep
// -----------------------------------------------------------------------------
$VehicleListID["Outlaw"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Outlaw";
$VehicleListData[$VehicleCount, "block"] = "Outlaw";
$VehicleListData[$VehicleCount, "name"] = "Outlaw Hovercar";
$VehicleListData[$VehicleCount, "desc"] = "Two-seater fit for guerrilla strike tactics.";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "hardpoints"] = 2;
$VehicleList::Outlaw = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Outlaw = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[M] Pilot Turret Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Medium;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Standard | $VHardpointType::Rack;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[L] Rear Turret";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;

$VehicleMax["Outlaw"] = 2;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "MBolter";
$DefaultVehicleWep[$VehicleCount, 1] = "LMortar";
$VehicleCount++;

// Trebuchet HoverTank
// -----------------------------------------------------------------------------
$VehicleListID["AssaultVehicle"] = $VehicleCount;
$VehicleList[$VehicleCount] = "AssaultVehicle";
$VehicleListData[$VehicleCount, "block"] = "AssaultVehicle";
$VehicleListData[$VehicleCount, "name"] = "Trebuchet HoverTank";
$VehicleListData[$VehicleCount, "desc"] = "Hovering tank with a big gun";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Hovering;
$VehicleListData[$VehicleCount, "hardpoints"] = 3;
$VehicleList::Guardian = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Guardian = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[M] Pilot Turret Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Medium;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni | $VHardpointType::Rack;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[S] Cannon Support Mount";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Small;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Standard;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[L] Cannon Main Barrel";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;

$VehicleMax["AssaultVehicle"] = 2;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "MDisruptor";
$DefaultVehicleWep[$VehicleCount, 1] = "SVulcan";
$DefaultVehicleWep[$VehicleCount, 2] = "LMeteorCannon";

$VehicleCount++;

// Firestorm VTOL Escort
// -----------------------------------------------------------------------------
$VehicleListID["BomberFlyer"] = $VehicleCount;
$VehicleList[$VehicleCount] = "BomberFlyer";
$VehicleListData[$VehicleCount, "block"] = "BomberFlyer";
$VehicleListData[$VehicleCount, "name"] = "Firestorm VTOL Escort";
$VehicleListData[$VehicleCount, "desc"] = "Multi-crewed assault craft";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "hardpoints"] = 3; // 4 - clientside
$VehicleList::Firestorm = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Firestorm = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[M] Pilot Turret Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Medium;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni | $VHardpointType::Rack;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[M] Underbelly Turret";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Turret;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[L] Underbelly Launch Bay";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Bay;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Anything;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[L] Rear Turret";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 11;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 2;

$VehicleMax["BomberFlyer"] = 2;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "MBolter";
$DefaultVehicleWep[$VehicleCount, 1] = "TBlaster";
$DefaultVehicleWep[$VehicleCount, 2] = "BBombBay";
$DefaultVehicleWep[$VehicleCount, 3] = ""; // LDevistatorArray - clientside
$VehicleCount++;

// Imperator Armored Transport
// -----------------------------------------------------------------------------
$VehicleListID["HAPCFlyer"] = $VehicleCount;
$VehicleList[$VehicleCount] = "HAPCFlyer";
$VehicleListData[$VehicleCount, "block"] = "HAPCFlyer";
$VehicleListData[$VehicleCount, "name"] = "Imperator Sky Fortress";
$VehicleListData[$VehicleCount, "desc"] = "Slow moving flying fortress of destruction";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "hardpoints"] = 2; // clientside: 4
$VehicleList::Imperator = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Imperator = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[L] Right Wing Turret";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Standard;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[L] Left Wing Turret";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Standard;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 5;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[L] Underbelly Launch Bay";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Bay;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Anything;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] Underbelly Turret";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Medium;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Standard;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 2;

$VehicleMax["HAPCFlyer"] = 2;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "LPlasma";
$DefaultVehicleWep[$VehicleCount, 1] = "LMortar";
$DefaultVehicleWep[$VehicleCount, 2] = ""; // LBombBay - clientside
$VehicleCount++;

// Bulldog VTOL APC
// -----------------------------------------------------------------------------
$VehicleListID["Bulldog"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Bulldog";
$VehicleListData[$VehicleCount, "block"] = "Bulldog";
$VehicleListData[$VehicleCount, "name"] = "Bulldog VTOL APC";
$VehicleListData[$VehicleCount, "desc"] = "Carrier with built-in stations, press Mine to use";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "hardpoints"] = 0;
$VehicleList::Bulldog = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Bulldog = $VehicleCount;

$VehicleMax["Bulldog"] = 2;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$VehicleCount++;

// Shadow Jetbike
// -----------------------------------------------------------------------------
$VehicleListID["ScoutVehicle"] = $VehicleCount;
$VehicleList[$VehicleCount] = "ScoutVehicle";
$VehicleListData[$VehicleCount, "block"] = "ScoutVehicle";
$VehicleListData[$VehicleCount, "name"] = "Shadow Jetbike";
$VehicleListData[$VehicleCount, "desc"] = "The jetbike built for stealth and speed";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "hardpoints"] = 1;
$VehicleList::Shadow = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Shadow = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[S] Nose Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Small;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;

$VehicleMax["ScoutVehicle"] = 3;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "SVulcan";
$VehicleCount++;

// Bombard Mobile Artillery
// -----------------------------------------------------------------------------
$VehicleListID["MobileBaseVehicle"] = $VehicleCount;
$VehicleList[$VehicleCount] = "MobileBaseVehicle";
$VehicleListData[$VehicleCount, "block"] = "MobileBaseVehicle";
$VehicleListData[$VehicleCount, "name"] = "Bombard Mobile Artillery";
$VehicleListData[$VehicleCount, "desc"] = "Laser-guided heavy ordinance";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Ground;
$VehicleListData[$VehicleCount, "hardpoints"] = 1;
$VehicleList::MPB = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::MPB = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[L] Artillery Turret";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 1;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;

$VehicleMax["MobileBaseVehicle"] = 1;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "SHMPBStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LMissile";
$VehicleCount++;

// Fury Scout Walker
// -----------------------------------------------------------------------------
$VehicleListID["FuryWalkerBase"] = $VehicleCount;
$VehicleList[$VehicleCount] = "FuryWalkerBase";
$VehicleListData[$VehicleCount, "block"] = "FuryWalkerBase";
$VehicleListData[$VehicleCount, "name"] = "Fury Scout Walker";
$VehicleListData[$VehicleCount, "desc"] = "Fast moving, lightly armored and unshielded";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Walker;
$VehicleListData[$VehicleCount, "hardpoints"] = 4;
$VehicleList::Fury = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Fury = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[M] R Main Gun";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Medium;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Standard;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 2; // note: will change in SC to point to actual image mount slots for mechs
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[M] L Main Gun";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Medium;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Standard;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 3;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] R Missile Mount";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Medium;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] L Missile Mount";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Medium;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 5;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;

$VehicleMax["Fury"] = 2;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "MBolter";
$DefaultVehicleWep[$VehicleCount, 1] = "MBolter";
$DefaultVehicleWep[$VehicleCount, 2] = "MHailstorm6";
$DefaultVehicleWep[$VehicleCount, 3] = "MHailstorm6";

$DefaultVehicleEnh[$VehicleCount, 0] = "WalkerArmorPlate";
$DefaultVehicleEnh[$VehicleCount, 1] = "WalkerArmorPlate";
$DefaultVehicleEnh[$VehicleCount, 2] = "WalkerArmorPlate";
$DefaultVehicleEnh[$VehicleCount, 3] = "WalkerArmorPlate";

$VehicleWalkerName[$VehicleCount] = "Fury";
$VehicleWalkerID["Fury"] = $VehicleCount;
$VehicleWalkerData[$VehicleCount, "maxHeat"] = 225;
$VehicleCount++;

// Stormguard Scout Walker
// -----------------------------------------------------------------------------
$VehicleListID["StormguardWalkerBase"] = $VehicleCount;
$VehicleList[$VehicleCount] = "StormguardWalkerBase";
$VehicleListData[$VehicleCount, "block"] = "StormguardWalkerBase";
$VehicleListData[$VehicleCount, "name"] = "Stormguard Assault Walker";
$VehicleListData[$VehicleCount, "desc"] = "Slow and steady destroys the race";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Walker;
$VehicleListData[$VehicleCount, "hardpoints"] = 5;
$VehicleList::Stormguard = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Stormguard = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[L] R Main Gun";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Standard;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 2; // note: will change in SC to point to actual image mount slots for mechs
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[L] L Main Gun";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Standard;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 3;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[L] R Missile Mount";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[L] L Missile Mount";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 5;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[L] Center Mount"; // "[L] Overhead Mount"
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Large;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 6;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 1;

$VehicleMax["Stormguard"] = 2;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone"; //"WShieldDefault";
$DefaultVehicleWep[$VehicleCount, 0] = "LPlasma";
$DefaultVehicleWep[$VehicleCount, 1] = "LPlasma";
$DefaultVehicleWep[$VehicleCount, 2] = "LMissile";
$DefaultVehicleWep[$VehicleCount, 3] = "LMissile";
$DefaultVehicleWep[$VehicleCount, 4] = "LStarHammer";

$DefaultVehicleEnh[$VehicleCount, 0] = "WalkerArmorPlate";
$DefaultVehicleEnh[$VehicleCount, 1] = "WalkerArmorPlate";
$DefaultVehicleEnh[$VehicleCount, 2] = "WalkerArmorPlate";
$DefaultVehicleEnh[$VehicleCount, 3] = "WalkerArmorPlate";
$DefaultVehicleEnh[$VehicleCount, 4] = "WalkerArmorPlate";

$VehicleWalkerName[$VehicleCount] = "Stormguard";
$VehicleWalkerID["Stormguard"] = $VehicleCount;
$VehicleWalkerData[$VehicleCount, "maxHeat"] = 450;
$VehicleCount++;

// Possible use?
$VehicleID::TurretBase = 99;
$VehicleList::TurretBase = 1 << 30;

// Shortcut bitmasks
$VehicleList::All = -1;
$VehicleList::General = -1; // whoa how did this happen, let's get rid of this
$VehicleList::Walkers = $VehicleList::Fury | $VehicleList::Stormguard;
$VehicleList::AllExceptWalkers = $VehicleList::All ^ $VehicleList::Walkers;

// Shield sizes
$VehicleList::Sizes = $VehicleList::Shadow | $VehicleList::Skycutter | $VehicleList::Outlaw | $VehicleList::Firestorm | $VehicleList::Imperator | $VehicleList::Guardian | $VehicleList::Bulldog | $VehicleList::MPB;

// -----------------------------------------------------------------------------

function VehiclePart::registerVehiclePart(%this, %type, %part, %name, %desc, %mask, %size, %slottype)
{
    // No duplication!
    if(isObject(%part))
        return;

    %newPart = new ScriptObject(%part)
    {
        class = %part;
        superClass = VehiclePart;
    };

    if(%size $= "")
        %size = $VHardpointSize::Small;
        
    if(%slottype $= "")
        %slottype = $VHardpointType::Omni;
        
    %newPart.name = %name;
    %newPart.description = %desc;
    %newPart.wearableMask = %mask;
    %newPart.mountSize = %size;
    %newPart.mountType = %slottype;
    %newPart.onInit();

    // Store index of where this object is
    if(%this.vehiclePartCount[%type] $= "")
        %this.vehiclePartCount[%type] = 0;

    %this.vehicleParts[%type, %this.vehiclePartCount[%type]] = %part;
    %this.partID[%type, %part] = %this.vehiclePartCount[%type];
    
    // Next data
    %this.vehiclePartCount[%type]++;
}

function VehiclePart::enumerateClient(%this, %client)
{
    for(%e = 0; %e < 5; %e++)
    {
        for(%i = 0; %i < %this.vehiclePartCount[%e]; %i++)
        {
            %part = %this.vehicleParts[%e, %i];

            commandToClient(%client, 'EnumVehParts', %e, %part.getName(), %part.name, %part.description, %part.wearableMask);
        }
    }
}

function VehiclePart::onInit(%this)
{
    // Set prefs here
}

function VehiclePart::validateUse(%this, %obj)
{
    return true;
}

function VehiclePart::walkerFireSlot(%this, %walker, %slot, %time)
{
    if(!%walker.fireTrigger)
        return;

    if(%walker.slotFireGroup[%walker.slotToIndex[%slot]] & (1 << %walker.selectedWeapon) == 0)
        return;

    %p = 0;
    
    if(%this.validateFire(%walker, %slot, %time))
    {
        if(%this.staggerCount !$= "")
            return %this.staggerFire(%walker, %slot, %this.staggerCount, 0);

        %p = %this.spawnProjectile(%walker, %slot);

        %walker.play3D(%this.fireSound);
        %walker.setImageTrigger(%slot, true);
        %walker.schedule(0, "setImageTrigger", %slot, false);

        if(%this.isSeeker && %p)
        {
            if((%target = %walker.getLockedTarget()))
                %p.setObjectTarget(%target);
            else if(%walker.isLocked())
                %p.setPositionTarget(%walker.getLockedPosition());
            else
                %p.setNoTarget();
        }
    }
    else
    {
        if(%time > %walker.nextFire[%slot])
            %walker.nextFire[%slot] = %time + %this.fireTimeout;
        
        if((%walker.ammoUse > 0 && %walker.ammo[%slot] < 1) || %walker.getEnergyLevel() < %this.fireEnergy)
            %walker.play3D(%this.dryFireSound);
    }
    
    %this.schedule(%this.fireTimeout, "walkerFireSlot", %walker, %slot, (%time + %this.fireTimeout));
    
    return %p;
}

function VehiclePart::validateFire(%this, %walker, %slot, %time)
{
    if(%time < %walker.nextFire[%slot])
        return false;

//    if((%pHeat = %walker.heat + %this.heatPerShot) > %walker.maxHeat)
//        return false;
        
    %energyUse = 0;
    %ammoUse = 0;
    
    if(%this.heatPerShot > 0)
    {
        if(%walker.getEnergyLevel() > %this.heatPerShot)
            %energyUse = %this.heatPerShot;
        else
            return false;
    }

    if(%this.ammoUse > 0)
    {
        if(%walker.ammo[%slot] > 0 && %this.ammoUse <= %walker.ammo[%slot])
            %ammoUse = %this.ammoUse;
        else
            return false;
    }

    if(%energyUse)
        %walker.useEnergy(%energyUse);

    if(%ammoUse)
        %walker.ammo[%slot] -= %ammoUse;

    %walker.nextFire[%slot] = %time + %this.fireTimeout;
//    %walker.heat += %this.heatPerShot;
    
    return true;
}

function VehiclePart::spawnProjectile(%this, %walker, %slot)
{
    if(%this.useForwardVector)
        %vector = %this.projectileSpread > 0 ? vectorSpread(%walker.getEyeVector(), %this.projectileSpread) : %walker.getEyeVector();
    else
        %vector = %this.projectileSpread > 0 ? vectorSpread(%walker.getMuzzleVector(%slot), %this.projectileSpread) : %walker.getMuzzleVector(%slot);

    %proj = createProjectile(%this.projectileType, %this.projectile, %vector, %walker.getMuzzlePoint(%slot), %walker, %slot, %walker.pilotObject);
    %proj.damageBuffFactor = %walker.damageBuffFactor;

    if(%this.deleteLastProjectile)
    {
        if(isObject(%walker.lastProjectile))
            %walker.lastProjectile.delete();

        %walker.deleteLastProjectile = %this.deleteLastProjectile;
    }

    %walker.lastProjectile = %proj;

    if(%this.isLaser && %proj)
        %proj.setEnergyPercentage(%this.laserOpacity);

    // ammo stuff here
//    if(%this.ammoUse > 0)
//        %walker.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo);

    return %proj;
}

function VehiclePart::staggerFire(%this, %walker, %slot, %total, %iteration)
{
    if(%iteration > %total)
        return;
        
    %proj = %this.spawnProjectile(%walker, %slot);
    
    %walker.play3D(%this.fireSound);
    %walker.setImageTrigger(%slot, true);
    %walker.schedule(0, "setImageTrigger", %slot, false);

    if(%this.isSeeker && %proj)
    {
        if((%target = %walker.getLockedTarget()))
            %proj.setObjectTarget(%target);
        else if(%walker.isLocked())
            %proj.setPositionTarget(%walker.getLockedPosition());
        else
            %proj.setNoTarget();
    }
    else if(%this.isLaser && %proj)
        %proj.setEnergyPercentage(%this.laserOpacity);

    %this.schedule(%this.staggerDelay, "staggerFire", %walker, %slot, %total, %iteration++);

    return %proj;
}

// Default setup here
function VehiclePart::installPart(%this, %data, %vehicle, %player, %pid)
{
    // Install parts
}

function VehiclePart::triggerToggle(%this, %data, %vehicle, %player, %state)
{

}

function VehiclePart::triggerPush(%this, %data, %vehicle, %player)
{

}

function VehiclePart::onSwitchWeapon(%this, %vehicle, %client, %wepNum)
{
    %att = $VehicleHardpoints[%vehicle.vid, %pid, "mountPoint"];
    
    for(%i = 0; %i < 8; %i++)
    {
        %vehicle.setImageTrigger(%i, false);
        
        if(%att > 0)
            %vehicle.getMountNodeObject(%att).setImageTrigger(%i, false);
    }
}

function VehicleData::installParts(%data, %obj, %player)
{
    if(%obj.partsInstalled)
        return;
        
    %obj.partsInstalled = true;
    %obj.rechargeTick = %data.rechargeRate;
    %obj.isShielded = false;
    
    %obj.vid = $VehicleListID[%data];
    %obj.nameOverride = %player.client.pendingVehicleName[$VehicleListID[%data]];

    if(%obj.nameOverride !$= "")
    {
        freeTarget(%obj.target);
        %obj.target = createTarget(%obj, addTaggedString(%obj.nameOverride), "", "", %data.targetTypeTag, 0, 0);
    }
    
    // Two pass system - base vehicle enhancements
    for(%i = 0; %i < 3; %i++)
    {
        %partID = 0;
        
        if(%player && %player.client.pendingVehicleMod[%i, $VehicleListID[%data]] !$= "")
            %partID = %player.client.pendingVehicleMod[%i, $VehicleListID[%data]];

        %part = %partID ? VehiclePart.vehicleParts[%i, %partID] : $DefaultVehiclePart[$VehicleListID[%data], %i]; // nameToID($DefaultVehiclePart[$VehicleListID[%data], %i]
        %obj.installedVehiclePart[%i] = %part;

        %part.installPart(%data, %obj, %player, %i);
    }

    // Then weapons
    for(%i = 0; %i < 5; %i++)
    {
        %partID = "";
        %w = %i + 3;
        
        if(%player && %player.client.pendingVehicleMod[%w, $VehicleListID[%data]] !$= "")
            %partID = %player.client.pendingVehicleMod[%w, $VehicleListID[%data]];

        %part = %partID !$= "" ? %partID : $DefaultVehicleWep[$VehicleListID[%data], %i];

        // simply not defined at all
        if(%part $= "")
            continue;
            
        %obj.installedVehiclePart[%w] = %part;
        %tgtPtNum = $VehicleHardpoints[$VehicleListID[%data], %i, "mountPoint"];

        if(%tgtPtNum > 0)
        {
            %tgtPtObj = %obj.getMountNodeObject(%tgtPtNum);

            if(%tgtPtObj.wepCount $= "")
                %tgtPtObj.wepCount = 0;
                
            %tgtPtObj.wepCount++;
            %tgtPtObj.weapon[%tgtPtObj.wepCount] = %part;
        }
        else if(%tgtPtNum == 0)
        {
            if(%obj.wepCount $= "")
                %obj.wepCount = 0;

            %obj.wepCount++;
            %obj.weapon[%obj.wepCount] = %part;
        }
        
        %part.installPart(%data, %obj, %player, %i);
        %part.reArm(%data, %obj, %player, %i);
    }

    // Apply weights
    %weight = %obj.bonusWeight;

    if(%weight > 150)
        %weight = 150;
    if(%weight <= -100)
        %weight = -100;

    %weightImage = %weight ? getWeightName(%data, %obj, %weight) : false;

    if(%weightImage)
    {
        %obj.mountImage(%weightImage, $VehicleWeightSlot);
        %obj.bonusWeight = %weight;
        %obj.totalWeight = %data.mass + %weight;
    }
    else
    {
        %obj.bonusWeight = 0;
        %obj.totalWeight = %data.mass;
    }
}

// For walkers
function Armor::installParts(%data, %obj, %player)
{
    if(%obj.partsInstalled)
        return;

    %obj.partsInstalled = true;
    %vid = $VehicleWalkerID[%obj.shortName];
    %obj.vid = %vid;

    // Two pass system - base vehicle enhancements
    for(%i = 0; %i < 3; %i++)
    {
        %partID = 0;

        if(%player && %player.client.pendingVehicleMod[%i, %vid] !$= "")
            %partID = %player.client.pendingVehicleMod[%i, %vid];

        %part = %partID ? VehiclePart.vehicleParts[%i, %partID] : $DefaultVehiclePart[%vid, %i]; // nameToID($DefaultVehiclePart[$VehicleListID[%data], %i]
        %obj.installedVehiclePart[%i] = %part;

//        echo(%obj SPC %part);
        %part.installPart(%data, %obj, %player, %i);
    }

    // Then weapons
    for(%i = 0; %i < 5; %i++)
    {
        %partID = "";
        %w = %i + 3;

        if(%player && %player.client.pendingVehicleMod[%w, %vid] !$= "")
            %partID = %player.client.pendingVehicleMod[%w, %vid];

        %part = %partID !$= "" ? %partID : $DefaultVehicleWep[%vid, %i];

        // simply not defined at all
        if(%part $= "")
            continue;

        %obj.installedVehiclePart[%w] = %part;
        %tgtPtNum = $VehicleHardpoints[%vid, %i, "mountPoint"];

        if(%tgtPtNum > 0)
        {
            %tgtPtObj = %obj.getMountNodeObject(%tgtPtNum);

            if(%tgtPtObj.wepCount $= "")
                %tgtPtObj.wepCount = 0;

            %tgtPtObj.wepCount++;
            %tgtPtObj.weapon[%tgtPtObj.wepCount] = %part;
        }
        else if(%tgtPtNum == 0)
        {
            if(%obj.wepCount $= "")
                %obj.wepCount = 0;

            %obj.wepCount++;
            %obj.weapon[%obj.wepCount] = %part;
        }

        %part.installPart(%data, %obj, %player, %i);
        %part.reArm(%data, %obj, %player, %i);
        
        // Set up firegroups
        %fg = %player.client.vWepFireGroup[%vid, %w];
        
        if(%fg $= "")
            %fg = %part.defaultFireGroup;
        
        %obj.slotFireGroup[%i] = %fg;
    }
}

function VehiclePart::reArm(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.ammo[$VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]] = %this.ammoAmount * %vehicle.maxAmmoCapacityFactor;
        return;
    }
    
    if(%this.ammo !$= "")
    {
//        %hp = $VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]; // $VehicleListID[%data]
        
//        if(%hp > 0)
//        {
//            %turret = %vehicle.getMountNodeObject(%hp);
//            %turret.setInventory(%this.ammo, %turret.getDatablock().max[%this.ammo] * %vehicle.maxAmmoCapacityFactor, true);
//        }
//        else
//            %vehicle.setInventory(%this.ammo, %data.max[%this.ammo] * %vehicle.maxAmmoCapacityFactor, true);
        %vehicle.ammoCache[%this.ammo] = mCeil(%this.ammoAmount * %vehicle.maxAmmoCapacityFactor);
        
        // Since we're not using ammo objects, set ammo state to true
        %turret = $VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"];
        
        if(%turret > 0)
        {
            %t = %vehicle.getMountNodeObject(%turret);

            for(%i = 0; %i < 8; %i++)
                %t.setImageAmmo(%i, true);
        }
        else
            for(%i = 0; %i < 8; %i++)
                %vehicle.setImageAmmo(%i, true);
    }
}

function VehiclePart::onTrigger(%this, %obj, %trigger, %state)
{
    // Stub function - no console spam
}

// Put these in first to bump 0-indexes
exec("scripts/vehicles/VPartPlaceholders.cs");

execDir("VehicleParts");
// Place empty slot on the last of every list
exec("scripts/vehicles/defaultVehiclePart.cs");
exec("scripts/vehicles/VehicleWeight.cs");
