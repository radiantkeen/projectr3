// These placeholders exist as the vehicle part system 0-indexed exhibit extremely
// strange behavior, such as partial function argument passing and even straight
// out ignoring function calls

function SHAPlaceholder::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHAPlaceholder::installPart(%this, %data, %vehicle, %player)
{
    // Try a function call in here, DO IT! JUST! DO IT!
    %data.installShield(%data, %vehicle, 144, 10000, (28 / $g_TickTime), 0.6, "VehicleShieldEmitter");
    echo("test 123");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHAPlaceholder", "Sanity Check", "If this works I know something", 0, $VHardpointSize::Internal, $VHardpointType::Omni);
VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOAPlaceHolder", "A Placeholder", "Because T2 is retarded sometimes", 0, $VHardpointSize::Internal, $VHardpointType::Omni);
VehiclePart.registerVehiclePart($VehiclePartType::Armor, "APAPlaceHolder", "A Placeholder", "Why does this not work", 0, $VHardpointSize::Internal, $VHardpointType::Omni);
VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "WPAPlaceHolder", "A Placeholder", "Uuuuuuugh", 0, $VHardpointSize::Internal, $VHardpointType::Omni);
