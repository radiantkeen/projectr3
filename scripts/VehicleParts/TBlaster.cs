datablock TracerProjectileData(TBlasterAABolt)
{
   doDynamicClientHits = true;

   projectileShapeName = "energy_bolt.dts";
   directDamage        = 0.3;
   kickBackStrength    = 0;
   directDamageType    = $DamageType::AATurret;
   explosion           = "ExtendedAAExplosion";
   splash              = ChaingunSplash;
   sound               = ShrikeBlasterProjectile;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::BlasterRifle;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 30;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 675.0;
   wetVelocity       = 337.5;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 500;
   lifetimeMS        = 500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 370;

   tracerLength    = 22;
   tracerAlpha     = true;
   tracerMinPixels = 3;
   tracerColor     = "0.1 0.1 1.0 0.75";
	tracerTex[0]  	 = "special/tracer00";//"special/";
	tracerTex[1]  	 = "small_circle";//"special/tracercross";
   tracerWidth     = 0.2;
   crossSize       = 1.9;
   crossViewAng    = 0.7;
   renderCross     = true;
   emap = true;
};

datablock TurretImageData(TBlasterBarrelL)
{
   shapeFile                        = "turret_belly_barrell.dts";
   mountPoint                       = 0;
   fireTimeout                      = 250;
   
   projectile                       = TBlasterAABolt;
   projectileType                   = TracerProjectile;

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;

   minEnergy = 12;
   fireEnergy = 6;

//   projectileSpread = 1.0;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   stateName[0]                     = "Ready";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "Reload";
   stateTimeoutValue[1]             = 0.2;
   stateFire[1]                     = true;
   stateSound[1]                    = BomberTurretFireSound;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";

   stateName[2]                     = "Reload";
   stateTimeoutValue[2]             = 0.05;
   stateTransitionOnTimeout[2]      = "Ready";
};

datablock TurretImageData(TBlasterBarrelR) : TBlasterBarrelL
{
   shapeFile                = "turret_belly_barrelr.dts";
   mountPoint               = 1;
   fireTimeout                      = 250;

   projectile                       = TBlasterAABolt;
   projectileType                   = TracerProjectile;
   
   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
};

function TBlaster::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "";
}

function TBlaster::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);

            %turret.mountImage(TBlasterBarrelL, 2);
            %turret.mountImage(TBlasterBarrelR, 3);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "TBlaster", "Turret: Dual Blasters", "Turret mounted dual blaster array", $VehicleList::General, $VHardpointSize::Turret, $VHardpointType::Energy);
