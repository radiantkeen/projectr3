function SHGuardianStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHGuardianStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 675, 13000, (136 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHGuardianStandard", "Standard Shield", "Strength: 675 HP, Recharge delay: 13 sec, Power drain: 60%", $VehicleList::Guardian, $VHardpointSize::Internal, $VHardpointType::Omni);
