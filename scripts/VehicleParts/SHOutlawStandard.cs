function SHOutlawStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHOutlawStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 386, 10000, (76 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHOutlawStandard", "Standard Shield", "Strength: 386 HP, Recharge delay: 10 sec, Power drain: 60%", $VehicleList::Outlaw, $VHardpointSize::Internal, $VHardpointType::Omni);
