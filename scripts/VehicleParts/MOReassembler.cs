function MOReassembler::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Toggle;
    %this.cooldownTime = 500;
}

function MOReassembler::triggerToggle(%this, %data, %vehicle, %player, %state)
{
    if(%state)
    {
        messageClient(%player.client, 'MsgVReassemblerStart', '\c2Engaging reassembler, please come to a complete stop for full effectiveness.');

        %vehicle.tickHeals = 0;

        if(!%vehicle.reassemblerTicking)
        {
            %vehicle.reassemblerTicking = true;
            %this.tickReassembler(%data, %vehicle, %player);
        }
    }
    else
    {
        %vehicle.reassemblerTicking = false;
        messageClient(%player.client, 'MsgVReassemblerEnd', '\c2Reassembler disengaged.');
    }
}

function MOReassembler::tickReassembler(%this, %data, %vehicle, %player)
{
    if(!isObject(%vehicle))
        return;

    if(!%vehicle.moduleTriggerState)
        return;
        
    %vel = vectorLen(%vehicle.getVelocity());
    %tickTime = 500;
    %repairHP = 8;
    %energyUse = 16;
    
    if(%vel > 0.1)
    {
        if(%vel > 25)
            %vel = 25;

        %tickTime += %vel * 100;
    }

    if(%vehicle.getDamagePercent() == 0)
    {
        messageClient(%player.client, 'MsgVReassemblerFinished', "\c2Armor integrity at 100%, repairs complete.");
        %data.triggerModuleOff(%vehicle);
        return;
    }
    else
    {
        if(%vehicle.getEnergyLevel() < %energyUse)
        {
            %data.triggerModuleOff(%vehicle);
            messageClient(%player.client, 'MsgVReassemblerNoEnergy', "\c2Energy too low, shutting down.");
            return;
        }
        
        %vehicle.useEnergy((%tickTime / 1000) * %energyUse);
    }

    %vehicle.incHP(%repairHP);
    %vehicle.tickHeals++;

    if(%vehicle.tickHeals % 3 == 0)
        zapEffect(%vehicle, "FXRedShift");

    %this.schedule(%tickTime, "tickReassembler", %data, %vehicle, %player);
}

function MOReassembler::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOReassembler", "Reassembler", "Restores armor integrity, drains 16 kw/s", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Omni);
