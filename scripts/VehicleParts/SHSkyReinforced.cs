function SHSkyReinforced::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHSkyReinforced::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 315, 12000, (64 / $g_TickTime), 0.8, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHSkyReinforced", "Reinforced Shield", "Strength: 315 HP, Recharge delay: 12 sec, Power drain: 80%", $VehicleList::Skycutter, $VHardpointSize::Internal, $VHardpointType::Omni);
