function SHOutlawRegenerative::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHOutlawRegenerative::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 230, 4000, (46 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHOutlawRegenerative", "Regenerative Shield", "Strength: 230 HP, Recharge delay: 4 sec, Power drain: 60%", $VehicleList::Outlaw, $VHardpointSize::Internal, $VHardpointType::Omni);
