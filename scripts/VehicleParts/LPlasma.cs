datablock TurretImageData(LPlasmaIMP) : PlasmaBarrelLarge
{
   projectile = PlasmaBarrelBolt;
   projectileType = LinearFlareProjectile;
   fireEnergy = 20;
   minEnergy = 20;
   emap = true;

   useCapacitor = false;
   useMountEnergy = false;
   sharedResourcePool = true;
   updatePilotAmmo = true;
   usesEnergy = false;
   ammo = "VLPlasmaAmmo";
   
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 0";
};

datablock TurretImageData(LPlasmaGUA1) : LPlasmaIMP
{
   mountPoint = 0;
   offset = "0.0 0 0";
   rotation = "0 1 0 0";
   
   useCapacitor = false;
   useMountEnergy = false;
};

datablock TurretImageData(LPlasmaFIR2T) : LPlasmaIMP
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 1;
   offset = "0.1 -1.5 -0.265";
   rotation = "0 1 0 180";

   useCapacitor = false;
   useMountEnergy = false;
};

function LPlasma::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VLPlasmaAmmo";
    %this.refImage = PlasmaBarrelLarge;

    // Walker prefs
    %this.dryFireSound = "PlasmaDryFireSound";
    %this.fireSound = "PBLFireSound";
    %this.projectileType = "LinearFlareProjectile";
    %this.projectile = "PlasmaBarrelBolt";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 75;
    %this.fireTimeout = 2500;
    %this.heatPerShot = 0;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha | $VehicleFiregroup::Beta;
    %this.isSeeker = false;
}

function LPlasma::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Plasma", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]).mountImage(LPlasmaGUA1, 4);

        case $VehicleID::Firestorm:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]).mountImage(LPlasmaFIR2T, 4);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LPlasmaIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LPlasmaIMP, 0);

        case $VehicleID::MPB:
            %vehicle.barrel = "PlasmaBarrelLarge";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LPlasma", "Plasma Cannon Mount", "Turret-level Plasma Cannon mount", $VehicleList::General, $VHardpointSize::Large, $VHardpointType::Ballistic);
