datablock EnergyProjectileData(TSpikeShot) : SpikeSingleShot
{
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Spike;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 45;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   directDamage        = 0.6;
};

datablock TurretImageData(TSpikeBarrelL)
{
   shapeFile                        = "turret_belly_barrell.dts";
   mountPoint                       = 0;
   fireTimeout                      = 500;
   
   projectile                       = TSpikeShot;
   projectileType                   = EnergyProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VTSpikeAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   stateName[0]                     = "Ready";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "Reload";
   stateTimeoutValue[1]             = 0.45;
   stateFire[1]                     = true;
   stateSound[1]                    = OBLFireSound;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";

   stateName[2]                     = "Reload";
   stateTimeoutValue[2]             = 0.05;
   stateTransitionOnTimeout[2]      = "Ready";
};

datablock TurretImageData(TSpikeBarrelR) : TSpikeBarrelL
{
   shapeFile                = "turret_belly_barrelr.dts";
   mountPoint               = 1;
   fireTimeout                      = 500;

   projectile                       = TSpikeShot;
   projectileType                   = EnergyProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VTSpikeAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;
};

function TSpike::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VTSpikeAmmo";
    %this.ammoAmount = 100;
}

function TSpike::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);

            %turret.mountImage(TSpikeBarrelL, 2);
            %turret.mountImage(TSpikeBarrelR, 3);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "TSpike", "Turret: Dual Spike Array", "High velocity precision kinetic justice", $VehicleList::General, $VHardpointSize::Turret, $VHardpointType::Ballistic);
