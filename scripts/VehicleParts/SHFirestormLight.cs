function SHFirestormLight::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHFirestormLight::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 325, 10000, (64 / $g_TickTime), 0.4, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHFirestormLight", "Light Shield", "Strength: 325 HP, Recharge delay: 10 sec, Power drain: 40%", $VehicleList::Firestorm, $VHardpointSize::Internal, $VHardpointType::Omni);
