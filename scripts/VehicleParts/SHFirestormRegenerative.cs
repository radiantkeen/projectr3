function SHFirestormRegenerative::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHFirestormRegenerative::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 325, 5000, (65 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHFirestormRegenerative", "Regenerative Shield", "Strength: 325 HP, Recharge delay: 5 sec, Power drain: 60%", $VehicleList::Firestorm, $VHardpointSize::Internal, $VHardpointType::Omni);
