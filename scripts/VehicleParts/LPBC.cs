datablock TurretImageData(LPBCSTD)
{
   shapeFile = "weapon_missile.dts";
   mountPoint = 0;
   rotation = "0 1 0 90";

   projectile = ParticleBeamV;
   projectileType = SniperProjectile;

   isLaser = true;
   laserOpacity = 0.85;
   
   updatePilotAmmo = true;
   sharedResourcePool = true;
   ammo = "PBCAmmo";

   usesEnergy = true;
   useMountEnergy = true;
   fireEnergy = 150.0;
   minEnergy = 150.0;
   useCapacitor = true;
   sharedResourcePool = true;
   
   // Turret parameters
   activationMS                        = 1000;
   deactivateDelayMS                   = 1500;
   thinkTimeMS                         = 200;
   degPerSecTheta                      = 500;
   degPerSecPhi                        = 500;
   attackRadius                        = 75;

   // State transitions
   stateName[0]                        = "Activate";
   stateTransitionOnNotLoaded[0]       = "Dead";
   stateTransitionOnLoaded[0]          = "ActivateReady";

   stateName[1]                        = "ActivateReady";
   stateSequence[1]                    = "Activate";
   stateSound[1]                       = AssaultTurretActivateSound;
   stateTimeoutValue[1]                = 1.0;
   stateTransitionOnTimeout[1]         = "Ready";
   stateTransitionOnNotLoaded[1]       = "Deactivate";

   stateName[2]                        = "Ready";
   stateTransitionOnNotLoaded[2]       = "Deactivate";
   stateTransitionOnNoAmmo[2]          = "NoAmmo";
   stateTransitionOnTriggerDown[2]     = "Fire";

   stateName[3]                        = "Fire";
   stateSequence[3]                    = "Fire";
   stateTransitionOnTimeout[3]         = "Reload";
   stateTimeoutValue[3]                = 1.0;
   stateFire[3]                        = true;
   stateRecoil[3]                      = LightRecoil;
   stateAllowImageChange[3]            = false;
   stateSound[3]                       = PBCFireSound;
   stateScript[3]                      = "onFire";

   stateName[4]                        = "Reload";
   stateSequence[4]                    = "Reload";
   stateTimeoutValue[4]                = 3.0;
   stateAllowImageChange[4]            = false;
   stateTransitionOnTimeout[4]         = "Ready";
   //stateTransitionOnNoAmmo[4]          = "NoAmmo";
   stateWaitForTimeout[4]              = true;

   stateName[5]                        = "Deactivate";
   stateDirection[5]                   = false;
   stateSequence[5]                    = "Activate";
   stateTimeoutValue[5]                = 1.0;
   stateTransitionOnLoaded[5]          = "ActivateReady";
   stateTransitionOnTimeout[5]         = "Dead";

   stateName[6]                        = "Dead";
   stateTransitionOnLoaded[6]          = "ActivateReady";
   stateTransitionOnTriggerDown[6]     = "DryFire";

   stateName[7]                        = "DryFire";
   stateSound[7]                       = AssaultMortarDryFireSound;
   stateTimeoutValue[7]                = 1.0;
   stateTransitionOnTimeout[7]         = "NoAmmo";

   stateName[8]                        = "NoAmmo";
   stateSequence[8]                    = "NoAmmo";
   stateTransitionOnAmmo[8]            = "Reload";
   stateTransitionOnTriggerDown[8]     = "DryFire";
};

datablock TurretImageData(LPBCIMP) : LPBCSTD
{
   useMountEnergy = false;
   useCapacitor = false;
};

function LPBCSTD::onFire(%data, %obj, %slot)
{
     %p = Parent::onFire(%data, %obj, %slot);

     if(%p)
        createRemoteProjectile("LinearFlareProjectile", "PBCDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);
}

function LPBCIMP::onFire(%data, %obj, %slot)
{
     %p = Parent::onFire(%data, %obj, %slot);

     if(%p)
        createRemoteProjectile("LinearFlareProjectile", "PBCDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);
}

function LPBC::spawnProjectile(%this, %walker, %slot)
{
     %p = Parent::spawnProjectile(%this, %walker, %slot);

     if(%p)
        createRemoteProjectile("LinearFlareProjectile", "PBCDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);

    return %p;
}

function LPBC::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "PBCAmmo";
    %this.refImage = LPBCSTD;

    // Walker prefs
    %this.dryFireSound = "MortarDryFireSound";
    %this.fireSound = "PBCFireSound";
    %this.projectileType = "SniperProjectile";
    %this.projectile = "ParticleBeamV";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 10;
    %this.fireTimeout = 4000;
    %this.heatPerShot = 150;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Beta;
    %this.isSeeker = false;
    
    // Laser
    %this.isLaser = true;
    %this.laserOpacity = 0.85;
}

function LPBC::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "MainCannon", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(LPBCSTD, 4);

        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(LPBCSTD, 4);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LPBCIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LPBCIMP, 0);
            
        case $VehicleID::MPB:
            %vehicle.barrel = "LPBCIMP";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LPBC", "Particle Beam Cannon", "Hit with the force of a lightning strike!", $VehicleList::All, $VHardpointSize::Large, $VHardpointType::Standard);
