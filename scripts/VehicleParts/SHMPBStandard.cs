function SHMPBStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHMPBStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 2000, 20000, (400 / $g_TickTime), 0.5, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHMPBStandard", "Standard Shield", "Strength: 2000 HP, Recharge delay: 20 sec, Power drain: 50%", $VehicleList::MPB, $VHardpointSize::Internal, $VHardpointType::Omni);
