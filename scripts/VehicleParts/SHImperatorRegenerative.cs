function SHImperatorRegenerative::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHImperatorRegenerative::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 480, 5000, (96 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHImperatorRegenerative", "Regenerative Shield", "Strength: 480 HP, Recharge delay: 5 sec, Power drain: 60%", $VehicleList::Imperator, $VHardpointSize::Internal, $VHardpointType::Omni);
