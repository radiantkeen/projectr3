datablock TurretImageData(TPlasmaBarrelL)
{
   shapeFile                        = "turret_belly_barrell.dts";
   mountPoint                       = 0;
   fireTimeout                      = 500;
   
   projectile                       = PlasmaBolt;
   projectileType                   = LinearFlareProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VTPlasmaAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   stateName[0]                     = "Ready";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "Reload";
   stateTimeoutValue[1]             = 0.45;
   stateFire[1]                     = true;
   stateSound[1]                    = PlasmaFireSound;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";

   stateName[2]                     = "Reload";
   stateTimeoutValue[2]             = 0.05;
   stateTransitionOnTimeout[2]      = "Ready";
};

datablock TurretImageData(TPlasmaBarrelR) : TPlasmaBarrelL
{
   shapeFile                = "turret_belly_barrelr.dts";
   mountPoint               = 1;
   fireTimeout                      = 500;

   projectile                       = PlasmaBolt;
   projectileType                   = LinearFlareProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VTPlasmaAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;
};

function TPlasma::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VTPlasmaAmmo";
    %this.ammoAmount = 150;
}

function TPlasma::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);

            %turret.mountImage(TPlasmaBarrelL, 2);
            %turret.mountImage(TPlasmaBarrelR, 3);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "TPlasma", "Turret: Dual Plasma", "Turret mounted dual plasma array", $VehicleList::General, $VHardpointSize::Turret, $VHardpointType::Ballistic);
