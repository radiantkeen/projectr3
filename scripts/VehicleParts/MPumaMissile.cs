datablock ShapeBaseImageData(MPumaSKY4) : EngineAPEImage
{
   mountPoint = 10;

   shapeFile = "vehicle_grav_scout.dts";

   offset = "4.25 -2 -1.75";
   rotation = "0 1 0 -125";
};

datablock ShapeBaseImageData(MPumaSKY5) : MPumaSKY4
{
   offset = "-4.25 -2 -1.75";
   rotation = "0 1 0 125";
};

datablock ShapeBaseImageData(MPumaSKY6) : MPumaSKY4
{
   offset = "4.25 -1.75 3";
   rotation = "0 1 0 -55";
};

datablock ShapeBaseImageData(MPumaSKY7) : MPumaSKY4
{
   offset = "-4.25 -1.75 3";
   rotation = "0 1 0 55";
};

// Pilot
datablock ShapeBaseImageData(MPumaFIR2) : MPumaSKY4
{
   mountPoint = 0;
   
   offset = "0.5 0 -1.5";
   rotation = "0 1 0 -115";
};

datablock ShapeBaseImageData(MPumaFIR3) : MPumaSKY4
{
   mountPoint = 0;

   offset = "-0.5 0 -1.5";
   rotation = "0 1 0 115";
};

// Gunner - remove?
datablock ShapeBaseImageData(MPumaFIR4) : MPumaSKY4
{
   mountPoint = 0;

   offset = "-4.75 -11 0";
   rotation = "0 1 0 65";
};

datablock ShapeBaseImageData(MPumaFIR5) : MPumaSKY4
{
   mountPoint = 0;

   offset = "4.75 -11 0";
   rotation = "0 1 0 -65";
};

datablock ShapeBaseImageData(MPumaFIR6) : MPumaSKY4
{
   mountPoint = 0;

   offset = "-4.75 -11 -2";
   rotation = "0 1 0 115";
};

datablock ShapeBaseImageData(MPumaFIR7) : MPumaSKY4
{
   mountPoint = 0;

   offset = "4.75 -11 -2";
   rotation = "0 1 0 -115";
};

datablock ShapeBaseImageData(MPumaGUA2) : MPumaSKY4
{
   mountPoint = 0;

   offset = "1.7 -4 2";
   rotation = "1 0 0 -90";
};

datablock ShapeBaseImageData(MPumaGUA3) : MPumaSKY4
{
   mountPoint = 0;

   offset = "-3.1 -4 2";
   rotation = "1 0 0 -90";
};

datablock ShapeBaseImageData(MPumaOUT2) : MPumaSKY4
{
   mountPoint = 0;

   offset = "2.2 1 -1.5";
   rotation = "0 1 0 -120";
};

datablock ShapeBaseImageData(MPumaOUT3) : MPumaSKY4
{
   mountPoint = 0;

   offset = "-3.6 1 -1.5";
   rotation = "0 1 0 120";
};

datablock BombProjectileData(PumaMissileDeploy)
{
   projectileShapeName  = "vehicle_grav_scout.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 3.0;
   damageRadius         = 10;
   radiusDamageType     = $DamageType::Missile;
   kickBackStrength     = 4500;  // z0dd - ZOD, 4/25/02. Was 2500

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 450;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion            = "VehicleBombExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 15.0;
   drag                 = 0.3;
   gravityMod		    = 1.0;

   minRotSpeed          = "0.0 45.0 0.0";
   maxRotSpeed          = "0.0 45.0 0.0";
   
   scale                = "1.0 1.0 1.0";

   sound                = "";

   missileNameBase      = "PumaMissile";
};

datablock LinearProjectileData(PumaMissileDumbfire)
{
   scale = "1.0 1.0 1.0";
   projectileShapeName = "vehicle_grav_scout.dts";
   emitterDelay        = -1;
   directDamage        = 0.5;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 2.5;
   damageRadius        = 10;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 4500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 450;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "VehicleBombExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 250;
   wetVelocity       = 100;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 6000;
   lifetimeMS        = 6000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

datablock SeekerProjectileData(PumaMissile) : ShoulderMissile
{
   scale = "5.0 5.0 5.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "vehicle_grav_scout.dts";
   directDamage        = 0.5;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 2.5;
   damageRadius        = 10;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 4500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 450;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "VehicleBombExplosion";
//   underwaterExplosion = UnderwaterHandGrenadeExplosion;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   lifetimeMS          = 7000;
   muzzleVelocity      = 100.0;
   maxVelocity         = 250.0;
   turningSpeed        = 115.0;
   acceleration        = 100.0;

   proximityRadius     = 4;

   flareDistance = 20;
   flareAngle    = 10;

   sound = ScoutThrustSound;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = false;
   explodeOnWaterImpact = false;
   
   // Used for vehicle-mounted missile system
   isVehicleMissile = true;
};

function MPumaMissile::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.firesMissiles = true;
    
    // Walker prefs
    %this.dryFireSound = "";
    %this.fireSound = "";
    %this.projectileType = "";
    %this.projectile = "";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
    %this.fireTimeout = 6000;
    %this.heatPerShot = 32;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Gamma;
    %this.isSeeker = false;
}

function MPumaMissile::validateFire(%this, %walker, %slot, %time)
{
    return true;
}

function MPumaMissile::spawnProjectile(%this, %walker, %slot)
{
    %walker.launchMissile();
    %walker.heat += 32;

    return 0;
}

function MPumaMissile::installPart(%this, %data, %vehicle, %player, %pid)
{
    if(!%vehicle.isWalker)
    {
        if(%pid < 1)
            %vehicle.mountImage(SeekingParamImage, 0);
        else
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]).mountImage(SeekingTurretParam, 0);
    }
}

function MPumaMissile::reArm(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Puma", %this);
        %vehicle.addMissile($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "", PumaMissileDeploy, PumaMissile);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.addMissile(4, MPumaSKY4, PumaMissileDeploy, PumaMissile);
            %vehicle.addMissile(5, MPumaSKY5, PumaMissileDeploy, PumaMissile);
            %vehicle.addMissile(6, MPumaSKY6, PumaMissileDeploy, PumaMissile);
            %vehicle.addMissile(7, MPumaSKY7, PumaMissileDeploy, PumaMissile);

        case $VehicleID::Outlaw:
            %vehicle.addMissile(2, MPumaOUT2, PumaMissileDeploy, PumaMissile);
            %vehicle.addMissile(3, MPumaOUT3, PumaMissileDeploy, PumaMissile);

        case $VehicleID::Firestorm:
            if(%hardpoint == 0)
            {
                %vehicle.addMissile(2, MPumaFIR2, PumaMissileDeploy, PumaMissile);
                %vehicle.addMissile(3, MPumaFIR3, PumaMissileDeploy, PumaMissile);
            }
            else // todo: make this a drop-bomb puma launcher
            {
//                %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
//                %vehicle.addMissile(4, MPumaFIR4, PumaMissileDeploy, PumaMissile);
//                %vehicle.addMissile(5, MPumaFIR5, PumaMissileDeploy, PumaMissile);
//                %vehicle.addMissile(6, MPumaFIR5, PumaMissileDeploy, PumaMissile);
//                %vehicle.addMissile(7, MPumaFIR5, PumaMissileDeploy, PumaMissile);
            }
            
        case $VehicleID::Guardian:
            %vehicle.addMissile(2, MPumaGUA2, PumaMissileDeploy, PumaMissile);
            %vehicle.addMissile(3, MPumaGUA3, PumaMissileDeploy, PumaMissile);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MPumaMissile", "SLAM Missiles", "When you want to hit like a mack truck", $VehicleList::All, $VHardpointSize::Medium, $VHardpointType::Rack);
