datablock LinearFlareProjectileData(VMitziStreakBlast)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "3.0 3.0 3.0";
   faceViewer          = true;
   directDamageType    = $DamageType::MitziBlast;
   directDamage        = 0.4;
   hasDamageRadius     = true;
   indirectDamage      = 0.36;
   damageRadius        = 7.0;
   kickBackStrength    = 500;
   radiusDamageType    = $DamageType::MitziBlast;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MitziBlast;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[0]   = 40;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[1]   = 40;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   explosion           = "MitziShotgunExplosion";
   splash              = PlasmaSplash;
//   baseEmitter         = MitziLightEmitter;

   dryVelocity       = 200.0;
   wetVelocity       = 300.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "1 0.8 0.9";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 1 1";
};

//datablock AudioProfile(MMitziFireSound)
//{
//   filename    = "fx/vehicles/bomber_turret_fire.wav";
//   description = AudioDefaultLooping3d;
//   preload = true;
//};

datablock ShapeBaseImageData(MMitziFIR1)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_mortar.dts";
   mountPoint = "10";
   offset = "-0.1 4.25 -0.8";
   rotation = "1 0 0 0";

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
   pilotHeadTracking = true;
   
   minEnergy = 10;
   fireEnergy = 20;
   
   projectile = VMitziStreakBlast;
   projectileType = LinearFlareProjectile;
   emap = true;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Deploy";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = MitziFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 1.25;
   stateTransitionOnTimeout[4]   = "Spinup";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(MMitziGUA1) : MMitziFIR1
{
   mountPoint = 1;
   
   usesEnergy = true;
   useMountEnergy = false;
   useCapacitor = false;
   pilotHeadTracking = true;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   offset = "-0.25 4 1.4";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MMitziSKY1) : MMitziFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   stateTimeoutValue[4]          = 1.25;
   
   offset = "1 1.5 0.65";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MMitziSKY2) : MMitziFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   stateTimeoutValue[4]          = 1.25;
   
   offset = "-1 1.5 0.65";
   rotation = "0 1 0 0";

   stateSound[4]                    = "";
};

datablock ShapeBaseImageData(MMitziOUT1) : MMitziFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = true;

   minEnergy = 10;
   fireEnergy = 10;

   stateTimeoutValue[4]          = 1.25;

   offset = "0.6 6.75 -1.5";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MMitziOUT2) : MMitziFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = true;

   minEnergy = 10;
   fireEnergy = 10;

   stateTimeoutValue[4]          = 1.25;

   offset = "-0.775 6.75 -1.5";
   rotation = "0 1 0 0";

   stateSound[4]                    = "";
};

function MMitzi::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "";
    
    // Walker prefs
    %this.dryFireSound = "MortarDryFireSound";
    %this.fireSound = "MitziFireSound";
    %this.projectileType = "LinearFlareProjectile";
    %this.projectile = "VMitziStreakBlast";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
    %this.fireTimeout = 1250;
    %this.heatPerShot = 5;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha;
    %this.isSeeker = false;
}

function MMitzi::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Energy", %this);
        return;
    }

    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.mountImage(MMitziSKY1, 2);
            %vehicle.mountImage(MMitziSKY2, 3);

        case $VehicleID::Outlaw:
            %vehicle.mountImage(MMitziOUT1, 2);
            %vehicle.mountImage(MMitziOUT2, 3);
            
        case $VehicleID::Guardian:
            %vehicle.mountImage(MMitziGUA1, 0);

        case $VehicleID::Firestorm:
            %vehicle.mountImage(MMitziFIR1, 0);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MMitzi", "Mitzi Streak Mount", "Explosive blasts of Mitzi energy, swat a fly!", $VehicleList::General, $VHardpointSize::Medium, $VHardpointType::Energy);
