function SHONightshadeCloak::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHONightshadeCloak::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.nightshadeShield = true;
    %data.installShield(%vehicle, 0, 0, 0, 0.4, "VehicleShieldEmitter");
}
// Allows the Shadow Jetbike to activate a cloaking device
VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHONightshadeCloak", "Nightshade Cloaking Shield", "Allows the Shadow Jetbike to activate a cloaking device", $VehicleList::Shadow, $VHardpointSize::Internal, $VHardpointType::Omni);
