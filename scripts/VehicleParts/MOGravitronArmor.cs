function MOGravitronArmor::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
}

function MOGravitronArmor::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.bonusWeight += %data.mass * -0.33;
    %vehicle.maxHitPoints = mCeil(%vehicle.maxHitPoints * 0.66);
    %vehicle.hitPoints = mCeil(%vehicle.hitPoints * 0.66);
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOGravitronArmor", "Gravitron Internal Structuring", "Reduces maximum HP and weight by 33%", $VehicleList::AllExceptWalkers, $VHardpointSize::Internal, $VHardpointType::Omni);
