datablock GrenadeProjectileData(LFlakShell)
{
   scale = "2.0 2.0 2.0";
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   hasDamageRadius     = true;
   indirectDamage      = 0.06;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Flak;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 80;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 24;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.0;
   bubbleEmitTime      = 1.0;

   sound               = GrenadeProjectileSound;
   explosion           = "FlakShellExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";
   velInheritFactor    = 0.85; // z0dd - ZOD, 3/30/02. Was 0.5
   splash              = GrenadeSplash;

   baseEmitter         = "";
   bubbleEmitter       = "";

   grenadeElasticity = 0.01; // z0dd - ZOD, 9/13/02. Was 0.35
   grenadeFriction   = 0.99;
   numBounces        = 0;
   armingDelayMS     = 0; // z0dd - ZOD, 9/13/02. Was 1000
   muzzleVelocity    = 150.00; // z0dd - ZOD, 3/30/02. GL projectile is faster. Was 47.00
   //drag = 0.1; // z0dd - ZOD, 3/30/02. No drag.
   gravityMod        = 0.2; // z0dd - ZOD, 5/18/02. Make GL projectile heavier, less floaty
};

function LFlakShell::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    if(%proj.tickCount > 96)
        return %this.detonate(%proj);

    InitContainerRadiusSearch(%proj.position, 12, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ProjectileObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.client.team == %proj.instigator.team || %int.team == %proj.instigator.team)
            continue;

        if(%int.getType() & $TypeMasks::ProjectileObjectType)
        {
            if(%int.getDatablock().getName() $= "FlareGrenadeProj")
                return %this.detonate(%proj);
        }
        else
        {
            if(%int.getHeat() > 0.75)
                return %this.detonate(%proj);
        }
    }
}

function LFlakShell::detonate(%this, %proj)
{
    transformProjectile(%proj, "LinearFlareProjectile", "LFlakShellBurst", %proj.position, %proj.initialDirection);
}

datablock LinearFlareProjectileData(LFlakShellBurst)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.24;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Flak;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 25;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.0;

   sound               = GrenadeProjectileSound;
   explosion           = "FlakShellExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock TurretImageData(TFlakBarrelL)
{
   shapeFile                        = "turret_belly_barrell.dts";
   mountPoint                       = 0;
   fireTimeout                      = 500;
   
   projectile                       = LFlakShell;
   projectileType                   = GrenadeProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VTFlakAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   stateName[0]                     = "Ready";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "Reload";
   stateTimeoutValue[1]             = 0.45;
   stateFire[1]                     = true;
   stateSound[1]                    = MBLFireSound;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";

   stateName[2]                     = "Reload";
   stateTimeoutValue[2]             = 0.05;
   stateTransitionOnTimeout[2]      = "Ready";
};

datablock TurretImageData(TFlakBarrelR) : TFlakBarrelL
{
   shapeFile                = "turret_belly_barrelr.dts";
   mountPoint               = 1;
   fireTimeout                      = 500;

   projectile                       = LFlakShell;
   projectileType                   = GrenadeProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VTFlakAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;
};

function TFlak::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VTFlakAmmo";
    %this.ammoAmount = 200;
}

function TFlak::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);

            %turret.mountImage(TFlakBarrelL, 2);
            %turret.mountImage(TFlakBarrelR, 3);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "TFlak", "Turret: Dual Flak Array", "Explodes near heat sources such as vehicles", $VehicleList::General, $VHardpointSize::Turret, $VHardpointType::Ballistic);
