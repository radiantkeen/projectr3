datablock BombProjectileData(BMatchheadDeploy)
{
   projectileShapeName  = "weapon_missile_casement.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 1.5;
   damageRadius         = 25;
   radiusDamageType     = $DamageType::EMP;
   kickBackStrength     = 100;  // z0dd - ZOD, 4/25/02. Was 2500

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::EMP;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 25;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 50;
   mdDamageRadius[1]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "HeavyPlasmaExplosion";
   velInheritFactor     = 1.0;

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 0.5;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticObjectType;
   
   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 15.0;
   drag                 = 0.3;
   gravityMod		    = 1.0;

   minRotSpeed          = "0.0 45.0 0.0";
   maxRotSpeed          = "0.0 45.0 0.0";

   scale                = "1.0 1.0 1.0";

   sound                = "";

   missileNameBase      = "BMatchhead";
};

datablock SeekerProjectileData(BMatchheadMissile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 1.0;
   damageRadius        = 25;
   radiusDamageType    = $DamageType::EMP;
   kickBackStrength    = 100;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::EMP;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 25;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 50;
   mdDamageRadius[1]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "HeavyPlasmaExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 0.5;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticObjectType;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = EMPMissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 10000;
   muzzleVelocity      = 50.0;
   maxVelocity         = 150.0;
   turningSpeed        = 75.0;
   acceleration        = 200.0;

   proximityRadius = 1;

   terrainAvoidanceSpeed         = 90;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 10;

   flareDistance = 50;
   flareAngle    = 15;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 32;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = true;
};

datablock LinearProjectileData(BMatchheadDumbfire) : VLRMRocket
{
   indirectDamage      = 0.5;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::EMP;
   kickBackStrength    = 100;

    mdEnable            = true;
   mdDeathMessageSet   = $DamageType::EMP;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 25;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 50;
   mdDamageRadius[1]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "HeavyPlasmaExplosion";

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 0.5;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticObjectType;
   
   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;

   dryVelocity       = 300;
   wetVelocity       = 150;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 6000;
   lifetimeMS        = 6000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";
};

datablock TurretImageData(BMatchheadBayBayL)
{
   shapeFile                        = "turret_muzzlepoint.dts";
   offset                           = "2 -4 -0.5";
   mountPoint                       = 10;
   fireTimeout                      = 1000;
   
   projectile                       = BMatchheadDeploy;
   projectileType                   = BombProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VBMatchheadBayAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   stateName[0]                     = "Ready";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "Reload";
   stateTimeoutValue[1]             = 1.9;
   stateFire[1]                     = true;
   stateSound[1]                    = BomberTurretDryFireSound;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";

   stateName[2]                     = "Reload";
   stateTimeoutValue[2]             = 0.1;
   stateTransitionOnTimeout[2]      = "Ready";
};

datablock TurretImageData(BMatchheadBayBayR) : BMatchheadBayBayL
{
   shapeFile                = "turret_muzzlepoint.dts";
   offset                           = "-2 -4 -0.5";
   mountPoint                       = 10;
   fireTimeout                      = 1000;
   
   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VBMatchheadBayAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
};

function BMatchheadBayBayL::onFire(%data,%obj,%slot)
{
    %p = Parent::onFire(%data,%obj,%slot);
    
    if(%p)
        %data.onBayLaunch(%obj, %slot, %p);
}

function BMatchheadBayBayR::onFire(%data,%obj,%slot)
{
    %p = Parent::onFire(%data,%obj,%slot);

    if(%p)
        %data.onBayLaunch(%obj, %slot, %p);
}

function BMatchheadBayBay::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VBMatchheadBayAmmo";
    %this.ammoAmount = 15;
}

function BMatchheadBayBay::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            
            %turret.mountImage(SeekingParamImage, 0);
            %turret.mountImage(BMatchheadBayBayL, 4);
            %turret.mountImage(BMatchheadBayBayR, 5);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);

            %turret.mountImage(SeekingParamImage, 0);
            %turret.mountImage(BMatchheadBayBayL, 4);
            %turret.mountImage(BMatchheadBayBayR, 5);
    }
    
    %vehicle.isBombing = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "BMatchheadBayBay", "Matchhead Launch Bay", "Incendiary missiles designed for burning away armor", $VehicleList::Firestorm | $VehicleList::Imperator, $VHardpointSize::Bay, $VHardpointType::Bombs);
