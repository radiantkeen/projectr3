function SHGuardianReinforced::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHGuardianReinforced::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 945, 15000, (190 / $g_TickTime), 0.8, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHGuardianReinforced", "Reinforced Shield", "Strength: 945 HP, Recharge delay: 15 sec, Power drain: 80%", $VehicleList::Guardian, $VHardpointSize::Internal, $VHardpointType::Omni);
