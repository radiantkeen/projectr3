datablock ShapeBaseImageData(MBombPodSKY4) : EngineAPEImage
{
   mountPoint = 10;

   shapeFile = "bomb.dts";

   offset = "4.25 -2 -1.75";
   rotation = "0 1 0 -125";
};

datablock ShapeBaseImageData(MBombPodSKY5) : MBombPodSKY4
{
   offset = "-4.25 -2 -1.75";
   rotation = "0 1 0 125";
};

datablock ShapeBaseImageData(MBombPodSKY6) : MBombPodSKY4
{
   offset = "4.25 -1.75 3";
   rotation = "0 1 0 -55";
};

datablock ShapeBaseImageData(MBombPodSKY7) : MBombPodSKY4
{
   offset = "-4.25 -1.75 3";
   rotation = "0 1 0 55";
};

datablock BombProjectileData(BombPodDeploy)
{
   projectileShapeName  = "bomb.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 2.0;
   damageRadius         = 30;
   radiusDamageType     = $DamageType::BomberBombs;
   kickBackStrength     = 5250;  // z0dd - ZOD, 4/25/02. Was 2500

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::BomberBombs;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 500;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion            = "VehicleBombExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 0.1;
   drag                 = 0.3;
   gravityMod		= 20.0 / mabs($Classic::gravSetting); // z0dd - ZOD, 8/28/02. Compensate for our grav change. Math: base grav / our grav

   minRotSpeed          = "60.0 0.0 0.0";
   maxRotSpeed          = "80.0 0.0 0.0";
   scale                = "1.0 1.0 1.0";

   sound                = BomberBombProjectileSound;
   
   bombOnly             = true;
};

function MBombPodMissile::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.firesMissiles = true;
}

function MBombPodMissile::installPart(%this, %data, %vehicle, %player, %pid)
{
    %vehicle.mountImage(SeekingParamImage, 0);
}

function MBombPodMissile::reArm(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.addMissile(4, MBombPodSKY4, BombPodDeploy, BombPodDeploy);
            %vehicle.addMissile(5, MBombPodSKY5, BombPodDeploy, BombPodDeploy);
            %vehicle.addMissile(6, MBombPodSKY6, BombPodDeploy, BombPodDeploy);
            %vehicle.addMissile(7, MBombPodSKY7, BombPodDeploy, BombPodDeploy);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MBombPodMissile", "Bomb Pods", "Dumbfire gravity-powered crater creators", $VehicleList::Skycutter, $VHardpointSize::Medium, $VHardpointType::Rack);
