function SHImperatorReinforced::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHImperatorReinforced::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 1120, 14000, (224 / $g_TickTime), 0.8, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHImperatorReinforced", "Reinforced Shield", "Strength: 1120 HP, Recharge delay: 14 sec, Power drain: 80%", $VehicleList::Imperator, $VHardpointSize::Internal, $VHardpointType::Omni);
