function SHOutlawReinforced::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHOutlawReinforced::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 540, 12000, (108 / $g_TickTime), 0.8, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHOutlawReinforced", "Reinforced Shield", "Strength: 540 HP, Recharge delay: 12 sec, Power drain: 80%", $VehicleList::Outlaw, $VHardpointSize::Internal, $VHardpointType::Omni);
