datablock TurretImageData(LAntiAirGUA1) : AABarrelLarge
{
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 0";

   useCapacitor = true;
   useMountEnergy = true;
   sharedResourcePool = true;
};

datablock TurretImageData(LAntiAirIMP) : LAntiAirGUA1
{
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 0";
   
   useCapacitor = false;
   useMountEnergy = false;
};

function LAntiAir::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.refImage = AABarrelLarge;

    // Walker prefs
    %this.dryFireSound = "ChaingunDryFireSound";
    %this.fireSound = "AAFireSound";
    %this.projectileType = "TracerProjectile";
    %this.projectile = "AABullet";
    %this.projectileSpread = 2;
    %this.fireEnergy = 0;
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
    %this.fireTimeout = 200;
    %this.heatPerShot = 1;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha;
    %this.isSeeker = false;
}

function LAntiAir::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Energy", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]).mountImage(LAntiAirGUA1, 4);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LAntiAirIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LAntiAirIMP, 0);

        case $VehicleID::MPB:
            %vehicle.barrel = "AABarrelLarge";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LAntiAir", "Turbo Blaster Mount", "Large blaster turret", $VehicleList::General, $VHardpointSize::Large, $VHardpointType::Energy);

