datablock SeekerProjectileData(WTurretMissile) : TurretMissile
{
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 350;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   lifetimeMS          = 10000;
   muzzleVelocity      = 180.0; // z0dd - ZOD, 3/27/02. Was 80. Velocity of projectile
   turningSpeed        = 90.0;

   proximityRadius     = 4;

   flechetteDelayMs = 32;
};

datablock TurretImageData(LMissileGUA1) : MissileBarrelLarge
{
//   shapeFile = "turret_tank_barrelmortar.dts";
   projectile = WTurretMissile;
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 0";

   usesEnergy = false;
   useCapacitor = false;
   useMountEnergy = false;
   sharedResourcePool = true;
   updatePilotAmmo = true;
   ammo = "VLMissileAmmo";
};

datablock TurretImageData(LMissileFIR2T) : LMissileGUA1
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 1;
   offset = "0.1 -1.5 -0.265";
   rotation = "0 1 0 180";

   useCapacitor = false;
   useMountEnergy = false;
};

datablock TurretImageData(LMissileIMP) : LMissileGUA1
{
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 0";

   useCapacitor = false;
   useMountEnergy = false;
};

function LMissileGUA1::onFire(%data,%obj,%slot)
{
   %p = Parent::onFire(%data,%obj,%slot);

   //--------------------------------------------------------
   // z0dd - ZOD, 9/3/02. Anti rapid fire mortar/missile fix.
   if(!%p)
   {
      return;
   }
   //--------------------------------------------------------

   if (%obj.getControllingClient())
   {
      // a player is controlling the turret
      %target = %obj.getLockedTarget();
   }
   else
   {
      // The ai is controlling the turret
      %target = %obj.getTargetObject();
   }

   if(%target)
      %p.setObjectTarget(%target);
   else if(%obj.isLocked())
      %p.setPositionTarget(%obj.getLockedPosition());
   else
      %p.setNoTarget(); // set as unguided. Only happens when itchy trigger can't wait for lock tone.
}

function LMissileIMP::onFire(%data,%obj,%slot)
{
   %p = Parent::onFire(%data,%obj,%slot);

   //--------------------------------------------------------
   // z0dd - ZOD, 9/3/02. Anti rapid fire mortar/missile fix.
   if(!%p)
   {
      return;
   }
   //--------------------------------------------------------

   if (%obj.getControllingClient())
   {
      // a player is controlling the turret
      %target = %obj.getLockedTarget();
   }
   else
   {
      // The ai is controlling the turret
      %target = %obj.getTargetObject();
   }

   if(%target)
      %p.setObjectTarget(%target);
   else if(%obj.isLocked())
      %p.setPositionTarget(%obj.getLockedPosition());
   else
      %p.setNoTarget(); // set as unguided. Only happens when itchy trigger can't wait for lock tone.
}

function LMissileFIR2T::onFire(%data,%obj,%slot)
{
   %p = Parent::onFire(%data,%obj,%slot);

   //--------------------------------------------------------
   // z0dd - ZOD, 9/3/02. Anti rapid fire mortar/missile fix.
   if(!%p)
   {
      return;
   }
   //--------------------------------------------------------

   if (%obj.getControllingClient())
   {
      // a player is controlling the turret
      %target = %obj.getLockedTarget();
   }
   else
   {
      // The ai is controlling the turret
      %target = %obj.getTargetObject();
   }

   if(%target)
      %p.setObjectTarget(%target);
   else if(%obj.isLocked())
      %p.setPositionTarget(%obj.getLockedPosition());
   else
      %p.setNoTarget(); // set as unguided. Only happens when itchy trigger can't wait for lock tone.
}

function LMissile::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "VLMissileAmmo";
    %this.refImage = MissileBarrelLarge;
    
    // Walker prefs
    %this.dryFireSound = "MissileDryFireSound";
    %this.fireSound = "MILFireSound";
    %this.projectileType = "SeekerProjectile";
    %this.projectile = "WTurretMissile";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 80;
    %this.fireTimeout = 5000;
    %this.heatPerShot = 0;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Gamma;
    %this.isSeeker = true;
}

function LMissile::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "LightMissile", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(SeekingTurretParam, 0);
            %turret.mountImage(LMissileGUA1, 4);

        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(SeekingTurretParam, 0);
            %turret.mountImage(LMissileFIR2T, 4);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LMissileIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LMissileIMP, 0);

        case $VehicleID::MPB:
            %vehicle.barrel = "MissileBarrelLarge";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LMissile", "ST-1 Missile Pod", "Turret-sized missile launcher mount", $VehicleList::General, $VHardpointSize::Large, $VHardpointType::Missile);
