function MOLAMS::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    %this.cooldownTime = 30000;
}

function MOLAMS::triggerPush(%this, %data, %vehicle, %player)
{
    if(%vehicle.getEnergyLevel() < 75)
    {
        messageClient(%player.client, 'MsgLAMSFail', '\c2LAMS failed, requires 75kj of energy to ignite.');
        return;
    }
    
    %vehicle.useEnergy(75);
    %pos = %vehicle.getWorldBoxCenter();
    %missileCount = 0;
    
    InitContainerRadiusSearch(%pos, 100, $TypeMasks::ProjectileObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%proj.getClassName() $= "SeekerProjectile")
            %missileCount++;
    }
    
    for(%i = -3; %i < %missileCount; %i++)
    {
         %f = new FlareProjectile()
         {
            dataBlock        = FlareGrenadeProj;
            initialDirection = vectorScale(VectorRand(), 3);
            initialPosition  = %pos;
            sourceObject     = %vehicle;
            sourceSlot       = 0;
         };
         
         FlareSet.add(%f);
         MissionCleanup.add(%f);
         %f.schedule(3000, "delete");
    }
    
    messageClient(%player.client, 'MsgLAMSLaunch', '\c3LAMS flares launched! Cooling down (30 sec)');
}

function MOLAMS::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOLAMS", "Laser Anti-Missile System", "WIP", 0, $VHardpointSize::Internal, $VHardpointType::Omni); // When active, destroys incoming missiles with lasers - $VehicleList::Walkers (not sure if I will actually implement this)
