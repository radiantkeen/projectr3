datablock ShockwaveData(HeavyDisruptorShockwave)
{
   width = 0.5;
   numSegments = 16;
   numVertSegments = 5;
   velocity = 1;
   acceleration = 11;
   lifetimeMS = 600;
   height = 0.2;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1;

   colors[0] = "1.0 0.0 0.0 1";
   colors[1] = "1.0 0.25 0.25 0.5";
   colors[2] = "1.0 1.0 1.0 0.0";
};

datablock ExplosionData(HeavyDisruptorExplosion)
{
   soundProfile   = HeavyDisruptorHitSound;
   shockwave      = HeavyDisruptorShockwave;
   faceViewer     = true;

   sizes[0] = "0.1 0.1 0.1";
   sizes[1] = "0.1 0.1 0.1";
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleData(HeavyDisruptorTrail)
{
   dragCoeffiecient     = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;

   textureName          = "flarebase";

   useInvAlpha =  false;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   colors[0]     = "0.9 0.1 0.1 0.5";
   colors[1]     = "0.6 0.05 0.05 0.2";
   colors[2]     = "0.4 0.0 0.0 0.1";
   colors[3]     = "0.4 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 0.5;
   sizes[2]      = 0.3;
   sizes[3]      = 0.1;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(HeavyDisruptorTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 40;
   overrideAdvance  = true;

   particles = "HeavyDisruptorTrail";
};

datablock LinearFlareProjectileData(HeavyDisruptorBolt)
{
   directDamage        = 0.0;
   directDamageType    = $DamageType::MPDisruptor;

   scale              = "3.75 3.75 3.75";

   explosion          = "HeavyDisruptorExplosion";
   baseEmitter        = HeavyDisruptorTrailEmitter;

   hasDamageRadius     = true;
   indirectDamage      = 0.8;
   damageRadius        = 3.0;
   radiusDamageType    = $DamageType::HeavyDisruptor;

   // Multi-damage system for elemental damage attacks
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MPDisruptor;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 120;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   dryVelocity       = 250.0;
   wetVelocity       = 250.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1750;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2000;

   numFlares         = 20;
   size              = 0.6;
   flareColor        = "1 0.25 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1.0 0.25 0.25";
};

//datablock AudioProfile(MDisruptorFireSound)
//{
//   filename    = "fx/vehicles/bomber_turret_fire.wav";
//   description = AudioDefaultLooping3d;
//   preload = true;
//};

datablock ShapeBaseImageData(MDisruptorFIR1)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   mountPoint = "10";
   offset = "-0.1 4.25 -0.8";
   rotation = "1 0 0 0";

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
   pilotHeadTracking = true;
   
   minEnergy = 10;
   fireEnergy = 40;
   
   projectile = HeavyDisruptorBolt;
   projectileType = LinearFlareProjectile;
   emap = true;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Deploy";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = AAFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 1.5;
   stateTransitionOnTimeout[4]   = "Spinup";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(MDisruptorGUA1) : MDisruptorFIR1
{
   mountPoint = 1;
   
   usesEnergy = true;
   useMountEnergy = false;
   useCapacitor = false;
   pilotHeadTracking = true;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   offset = "-0.25 4 1.4";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MDisruptorSKY1) : MDisruptorFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   stateTimeoutValue[4]          = 1.5;
   
   offset = "1 1.5 0.65";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MDisruptorSKY2) : MDisruptorFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   stateTimeoutValue[4]          = 1.5;
   
   offset = "-1 1.5 0.65";
   rotation = "0 1 0 0";

   stateSound[4]                    = "";
};

datablock ShapeBaseImageData(MDisruptorOUT1) : MDisruptorFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = true;

   minEnergy = 10;
   fireEnergy = 10;

   stateTimeoutValue[4]          = 1.5;

   offset = "0.6 6.75 -1.5";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MDisruptorOUT2) : MDisruptorFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = true;

   minEnergy = 10;
   fireEnergy = 10;

   stateTimeoutValue[4]          = 1.5;

   offset = "-0.775 6.75 -1.5";
   rotation = "0 1 0 0";

   stateSound[4]                    = "";
};

function MDisruptor::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "";
    
    // Walker prefs
    %this.dryFireSound = "ChaingunDryFireSound";
    %this.fireSound = "AAFireSound";
    %this.projectileType = "LinearFlareProjectile";
    %this.projectile = "HeavyDisruptorBolt";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
    %this.fireTimeout = 1000;
    %this.heatPerShot = 10;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha;
    %this.isSeeker = false;
}

function MDisruptor::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Energy", %this);
        return;
    }

    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.mountImage(MDisruptorSKY1, 2);
            %vehicle.mountImage(MDisruptorSKY2, 3);

        case $VehicleID::Outlaw:
            %vehicle.mountImage(MDisruptorOUT1, 2);
            %vehicle.mountImage(MDisruptorOUT2, 3);
            
        case $VehicleID::Guardian:
            %vehicle.mountImage(MDisruptorGUA1, 0);

        case $VehicleID::Firestorm:
            %vehicle.mountImage(MDisruptorFIR1, 0);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MDisruptor", "Heavy Disruptor Mount", "Significantly more powerful version of the handheld Disruptor", $VehicleList::General, $VHardpointSize::Medium, $VHardpointType::Energy);
