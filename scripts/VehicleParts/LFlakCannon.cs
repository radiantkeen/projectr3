datablock TurretImageData(LFlakCannonGUA1) : MortarBarrelLarge
{
   projectileType = GrenadeProjectile;
   projectile = LFlakShell;
   
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 180";

   usesEnergy = false;
   useCapacitor = false;
   useMountEnergy = false;
   sharedResourcePool = true;
   updatePilotAmmo = true;
   ammo = "VLFlakCannonAmmo";

   stateName[3]                = "Fire";
   stateTimeoutValue[3]        = 0.3;
   stateSequence[3]            = "Fire";
   stateSound[3]               = MBLFireSound;

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.033;
   stateSequence[4]              = "Reload";
};

datablock TurretImageData(LFlakCannonFIR2T) : LFlakCannonGUA1
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 1;
   offset = "0.1 -1.5 -0.265";
   rotation = "0 1 0 180";

   useCapacitor = false;
   useMountEnergy = false;
};

datablock TurretImageData(LFlakCannonIMP) : LFlakCannonGUA1
{
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 0";

   useCapacitor = false;
   useMountEnergy = false;
};

function LFlakCannon::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "VLFlakCannonAmmo";
    %this.refImage = MissileBarrelLarge;
    
    // Walker prefs
    %this.dryFireSound = "MissileDryFireSound";
    %this.fireSound = "MBLFireSound";
    %this.projectileType = "GrenadeProjectile";
    %this.projectile = "LFlakShell";
    %this.projectileSpread = 12;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 300;
    %this.fireTimeout = 333;
    %this.heatPerShot = 1;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Beta;
}

function LFlakCannon::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Cannon", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(SeekingTurretParam, 0);
            %turret.mountImage(LFlakCannonGUA1, 4);

        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(SeekingTurretParam, 0);
            %turret.mountImage(LFlakCannonFIR2T, 4);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LFlakCannonIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LFlakCannonIMP, 0);

        case $VehicleID::MPB:
            %vehicle.barrel = "LFlakCannonIMP";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LFlakCannon", "Flak Cannon", "Repeating thermal proximity explosive launcher", $VehicleList::General, $VHardpointSize::Large, $VHardpointType::Ballistic);
