function SHSkyLight::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHSkyLight::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 135, 8000, (27 / $g_TickTime), 0.4, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHSkyLight", "Light Shield", "Strength: 135 HP, Recharge delay: 8 sec, Power drain: 40%", $VehicleList::Skycutter, $VHardpointSize::Internal, $VHardpointType::Omni);
