datablock BombProjectileData(BLRMDeploy)
{
   projectileShapeName  = "weapon_missile_casement.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 1.5;
   damageRadius         = 10;
   radiusDamageType     = $DamageType::Missile;
   kickBackStrength     = 2250;  // z0dd - ZOD, 4/25/02. Was 2500

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 225;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion            = "LargeMissileExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 15.0;
   drag                 = 0.3;
   gravityMod		    = 1.0;

   minRotSpeed          = "0.0 45.0 0.0";
   maxRotSpeed          = "0.0 45.0 0.0";

   scale                = "1.0 1.0 1.0";

   sound                = "";

   missileNameBase      = "BLRM";
};

datablock LinearProjectileData(BLRMDumbfire) : VLRMRocket
{
   indirectDamage      = 0.5;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 750;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MissileExplosion";
   flags               = $Projectile::PlayerFragment;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;

   dryVelocity       = 300;
   wetVelocity       = 150;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 6000;
   lifetimeMS        = 6000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";
};

datablock TurretImageData(BLRMBayBayL)
{
   shapeFile                        = "turret_muzzlepoint.dts";
   offset                           = "2 -4 -0.5";
   mountPoint                       = 10;
   fireTimeout                      = 1000;
   
   projectile                       = BLRMDeploy;
   projectileType                   = BombProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VBLRMBayAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   stateName[0]                     = "Ready";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "Reload";
   stateTimeoutValue[1]             = 1.9;
   stateFire[1]                     = true;
   stateSound[1]                    = BomberTurretDryFireSound;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";

   stateName[2]                     = "Reload";
   stateTimeoutValue[2]             = 0.1;
   stateTransitionOnTimeout[2]      = "Ready";
};

datablock TurretImageData(BLRMBayBayR) : BLRMBayBayL
{
   shapeFile                = "turret_muzzlepoint.dts";
   offset                           = "-2 -4 -0.5";
   mountPoint                       = 10;
   fireTimeout                      = 1000;
   
   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VBLRMBayAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
};

function BLRMBayBayL::onFire(%data,%obj,%slot)
{
    %p = Parent::onFire(%data,%obj,%slot);
    
    if(%p)
        %data.onBayLaunch(%obj, %slot, %p);
}

function BLRMBayBayR::onFire(%data,%obj,%slot)
{
    %p = Parent::onFire(%data,%obj,%slot);

    if(%p)
        %data.onBayLaunch(%obj, %slot, %p);
}

function BLRMBayBay::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VBLRMBayAmmo";
    %this.ammoAmount = 30;
}

function BLRMBayBay::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            
            %turret.mountImage(SeekingParamImage, 0);
            %turret.mountImage(BLRMBayBayL, 4);
            %turret.mountImage(BLRMBayBayR, 5);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);

            %turret.mountImage(SeekingParamImage, 0);
            %turret.mountImage(BLRMBayBayL, 4);
            %turret.mountImage(BLRMBayBayR, 5);
    }
    
    %vehicle.isBombing = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "BLRMBayBay", "LRM Launch Bay", "Long range missiles designed for interception", $VehicleList::Firestorm | $VehicleList::Imperator, $VHardpointSize::Bay, $VHardpointType::Bombs); // Imperator comes later
