function MONightshade::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Toggle;
    %this.cooldownTime = 1000;
}

$NSEnergyUse = 10 / $g_TickTime;

function MONightshade::triggerToggle(%this, %data, %vehicle, %player, %state)
{
    if(%vehicle.nightshadeShield != true)
    {
        messageClient(%player.client, 'MsgNightshadeFail', '\c2Nightshade cloaking shield emitter missing!');
        %vehicle.moduleTriggerState = false;
        return;
    }
    
    if(%state)
        %this.startCloak(%vehicle, %player);
    else
        %this.stopCloak(%vehicle, %player);
}

function MONightshade::startCloak(%this, %vehicle, %player)
{
    messageClient(%player.client, 'MsgNightshadeCloakOn', '\c3Nightshade cloaking emitter activated.');
    %vehicle.setCloaked(true);
    %vehicle.getMountNodeObject(0).setCloaked(true);
    %this.tickCloak(%vehicle, %player);
}

function MONightshade::stopCloak(%this, %vehicle, %player)
{
    messageClient(%player.client, 'MsgNightshadeCloakOff', '\c3Nightshade cloaking emitter deactivated.');
    %vehicle.setCloaked(false);
    %vehicle.getMountNodeObject(0).setCloaked(false);
    %vehicle.moduleTriggerState = false;
}

function MONightshade::tickCloak(%this, %vehicle, %player)
{
    if(%vehicle.moduleTriggerState != true || %vehicle.getEnergyLevel() < 5)
    {
        %vehicle.moduleTriggerState = false;
        
        if(%vehicle.moduleTriggerState != true)
        {
            %vehicle.setCloaked(false);
            %vehicle.getMountNodeObject(0).setCloaked(false);
        }
    }
    else
    {
        %vehicle.useEnergy($NSEnergyUse);
        %this.schedule($g_TickTime, "tickCloak", %vehicle, %player);
    }
}

function MONightshade::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.nightshadeModule = true;
}

// $VehicleList::All - Creates a cloaking effect with the shield emitters
VehiclePart.registerVehiclePart($VehiclePartType::Module, "MONightshade", "Nightshade Cloaking Device", "Creates a cloaking effect with the shield emitters", $VehicleList::Shadow, $VHardpointSize::Internal, $VHardpointType::Omni);
