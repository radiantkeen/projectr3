function MODamageAmp::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
}

function MODamageAmp::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.rechargeTick -= 10 / 32;
    %vehicle.bonusWeight += 50;
    %vehicle.damageBuffFactor += 0.25;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MODamageAmp", "Damage Amp", "Reroutes 10KW to a +25% damage amp, adds +50 KG to weight", $VehicleList::AllExceptWalkers, $VHardpointSize::Internal, $VHardpointType::Omni);
