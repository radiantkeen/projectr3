function SHSkyRegenerative::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHSkyRegenerative::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 135, 4000, (27 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHSkyRegenerative", "Regenerative Shield", "Strength: 135 HP, Recharge delay: 4 sec, Power drain: 60%", $VehicleList::Skycutter, $VHardpointSize::Internal, $VHardpointType::Omni);
