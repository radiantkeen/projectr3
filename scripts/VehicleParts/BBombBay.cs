datablock TurretImageData(BBombBayL)
{
   shapeFile                        = "turret_muzzlepoint.dts";
   offset                           = "2 -4 -0.5";
   mountPoint                       = 10;
   fireTimeout                      = 1000;
   
   projectile                       = BomberBomb;
   projectileType                   = BombProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VTBombAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   stateName[0]                     = "Ready";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "Reload";
   stateTimeoutValue[1]             = 0.9;
   stateFire[1]                     = true;
   stateSound[1]                    = BomberBombFireSound;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";

   stateName[2]                     = "Reload";
   stateTimeoutValue[2]             = 0.1;
   stateTransitionOnTimeout[2]      = "Ready";
};

datablock TurretImageData(BBombBayR) : BBombBayL
{
   shapeFile                = "turret_muzzlepoint.dts";
   offset                           = "-2 -4 -0.5";
   mountPoint                       = 10;
   fireTimeout                      = 1000;
   
   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VTBombAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
};

function BBombBay::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VTBombAmmo";
    %this.ammoAmount = 50;
}

function BBombBay::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            
            %turret.mountImage(BBombBayL, 4);
            %turret.mountImage(BBombBayR, 5);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            
            %turret.mountImage(BBombBayL, 4);
            %turret.mountImage(BBombBayR, 5);
    }
    
    %vehicle.isBombing = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "BBombBay", "'Stomper' 500kt Bombs", "These are gonna hurt when they land.", $VehicleList::Firestorm | $VehicleList::Imperator, $VHardpointSize::Bay, $VHardpointType::Bombs); // Imperator comes later
