datablock TracerProjectileData(SBlasterAABolt)
{
   doDynamicClientHits = true;

   projectileShapeName = "energy_bolt.dts";
   directDamage        = 0.2;
   kickBackStrength    = 0;
   directDamageType    = $DamageType::BlasterRifle;
   explosion           = "ExtendedAAExplosion";
   splash              = ChaingunSplash;
   sound               = ShrikeBlasterProjectile;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::AATurret;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 25;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 675.0;
   wetVelocity       = 337.5;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 500;
   lifetimeMS        = 500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 370;

   tracerLength    = 22;
   tracerAlpha     = true;
   tracerMinPixels = 3;
   tracerColor     = "0.1 0.1 1.0 0.75";
	tracerTex[0]  	 = "special/tracer00";//"special/";
	tracerTex[1]  	 = "small_circle";//"special/tracercross";
   tracerWidth     = 0.2;
   crossSize       = 1.9;
   crossViewAng    = 0.7;
   renderCross     = true;
   emap = true;
};

datablock ShapeBaseImageData(SBlasterSKY1)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";

   projectileType = TracerProjectile;
   projectile = SBlasterAABolt;
   mountPoint = 10;

   offset = "3.25 -1.4 -1.0";
   rotation = "0 1 0 -125";

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
   
   minEnergy = 5;
   fireEnergy = 5;
   fireTimeout = 175;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.4;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "deploy";
   stateSound[3]               = BomberTurretFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.3;
   stateAllowImageChange[4]      = false;
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

datablock ShapeBaseImageData(SBlasterSHA1) : SBlasterSKY1
{
//   shapeFile = "weapon_grenade_launcher.dts";

   offset = "0 0.85 0.15";
   rotation = "0 1 0 0";
   
   stateName[3]                = "Fire";
   stateTimeoutValue[3]        = 0.15;
   stateSequence[3]            = "deploy";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.05;
   stateAllowImageChange[4]      = false;
};

datablock ShapeBaseImageData(SBlasterGUA1) : SBlasterSKY1
{
   mountPoint = 1;
   offset = "0 0 0";
   rotation = "0 1 0 0";
   
   useCapacitor = true;
   useMountEnergy = true;
   
   stateName[3]                = "Fire";
   stateTimeoutValue[3]        = 0.15;
   stateSequence[3]            = "deploy";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.05;
   stateAllowImageChange[4]      = false;
};

function SBlasterMount::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
}

function SBlasterMount::installPart(%this, %data, %vehicle, %player, %pid)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Shadow:
            %vehicle.mountImage(SBlasterSHA1, 0);

        case $VehicleID::Guardian:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(SBlasterGUA1, 2);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "SBlasterMount", "Blaster Array", "High speed anti-air Blaster mount", $VehicleList::General, $VHardpointSize::Small, $VHardpointType::Energy);
