function MOPDS::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    %this.cooldownTime = 30000;
}

function MOPDS::triggerPush(%this, %data, %vehicle, %player)
{
    if(%vehicle.getEnergyLevel() < 75)
    {
        messageClient(%player.client, 'MsgTMDSFail', '\c2TMDS failed, requires 75kj of energy to ignite.');
        return;
    }
    
    %vehicle.useEnergy(75);
    %pos = %vehicle.getWorldBoxCenter();
    %missileCount = 0;
    
    InitContainerRadiusSearch(%pos, 100, $TypeMasks::ProjectileObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%proj.getClassName() $= "SeekerProjectile")
            %missileCount++;
    }
    
    for(%i = -3; %i < %missileCount; %i++)
    {
         %f = new FlareProjectile()
         {
            dataBlock        = FlareGrenadeProj;
            initialDirection = vectorScale(VectorRand(), 3);
            initialPosition  = %pos;
            sourceObject     = %vehicle;
            sourceSlot       = 0;
         };
         
         FlareSet.add(%f);
         MissionCleanup.add(%f);
         %f.schedule(3000, "delete");
    }
    
    messageClient(%player.client, 'MsgTMDSLaunch', '\c3TMDS flares launched! Cooling down (30 sec)');
}

function MOPDS::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOPDS", "Thermal Missile Defense System", "Ejects flares to counter locked missiles within 100m, 30s cooldown", $VehicleList::AllExceptWalkers, $VHardpointSize::Internal, $VHardpointType::Omni);
