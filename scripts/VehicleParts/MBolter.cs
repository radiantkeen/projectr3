//------------------------------------------------------------------------------
datablock TracerProjectileData(BolterBolt)
{
   doDynamicClientHits = true;

   directDamage        = 0.2;
   directDamageType    = $DamageType::Bolter;
   explosion           = "BolterExplosion";
   splash              = ChaingunSplash;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Bolter;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 10;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   kickBackStrength  = 0.0;
   sound 				= ChaingunProjectile;

   dryVelocity       = 350.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   tracerLength    = 12.0;
   tracerAlpha     = false;
   tracerMinPixels = 8;
   tracerColor     = 224.0/255.0 @ " " @ 227.0/255.0 @ " " @ 159.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.20;
   crossSize       = 0.45;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;
};

datablock AudioProfile(WalkerBolterFireSound)
{
   filename    = "fx/vehicles/tank_chaingun.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ShapeBaseImageData(MBolterFIR1)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_chaingun.dts";
   mountPoint = "10";
   offset = "-0.1 4.25 -0.8";
   rotation = "1 0 0 0";

   ammo = "VMBolterAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   pilotHeadTracking = true;
   projectile = BolterBolt;
   projectileType = TracerProjectile;
   emap = true;

   casing              = ShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 6.0;

   projectileSpread = 3.0; // z0dd - ZOD, 8/6/02. Was: 8.0 / 1000.0

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = AssaultChaingunFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.1;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(MBolterGUA1) : MBolterFIR1
{
//   shapeFile = "turret_tank_barrelchain.dts";
   mountPoint = 1;
   offset = "-0.25 4 1.4";
   rotation = "0 1 0 0";
   pilotHeadTracking = true;
   stateTimeoutValue[4]          = 0.1;
};

datablock ShapeBaseImageData(MBolterSKY1) : MBolterFIR1
{
   offset = "1 1.5 0.6";
   rotation = "0 1 0 0";
   pilotHeadTracking = false;
   
   stateTimeoutValue[4]          = 0.1;
};

datablock ShapeBaseImageData(MBolterSKY2) : MBolterFIR1
{
   offset = "-1.0 1.5 0.8";
   rotation = "0 1 0 -180";
   pilotHeadTracking = false;
   
   stateSound[4]                    = "";
   stateTimeoutValue[4]          = 0.1;
};

datablock ShapeBaseImageData(MBolterOUT1) : MBolterFIR1
{
   offset = "0.6 6.75 -1.5";
   rotation = "0 1 0 0";
   pilotHeadTracking = true;

   stateTimeoutValue[4]          = 0.1;
};

datablock ShapeBaseImageData(MBolterOUT2) : MBolterFIR1
{
   offset = "-0.775 6.75 -1.3";
   rotation = "0 1 0 -180";
   pilotHeadTracking = true;

   stateSound[4]                    = "";
   stateTimeoutValue[4]          = 0.1;
};

function MBolter::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VMBolterAmmo";
    
    // Walker prefs
    %this.dryFireSound = "ChaingunDryFireSound";
    %this.fireSound = "WalkerBolterFireSound";
    %this.projectileType = "TracerProjectile";
    %this.projectile = "BolterBolt";
    %this.projectileSpread = 3;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 400;
    %this.fireTimeout = 150;
    %this.heatPerShot = 0;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha;
    %this.isSeeker = false;
}

function MBolter::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "AC", %this);
        return;
    }

    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.mountImage(MBolterSKY1, 2);
            %vehicle.mountImage(MBolterSKY2, 3);

        case $VehicleID::Guardian:
            %vehicle.mountImage(MBolterGUA1, 0);

        case $VehicleID::Outlaw:
            %vehicle.mountImage(MBolterOUT1, 2);
            %vehicle.mountImage(MBolterOUT2, 3);

        case $VehicleID::Firestorm:
            %vehicle.mountImage(MBolterFIR1, 0);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MBolter", "Bolter", "Automatic large bore rotary cannon", $VehicleList::General, $VHardpointSize::Medium, $VHardpointType::Ballistic);
