function SHImperatorLight::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHImperatorLight::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 480, 10000, (96 / $g_TickTime), 0.4, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHImperatorLight", "Light Shield", "Strength: 480 HP, Recharge delay: 10 sec, Power drain: 40%", $VehicleList::Imperator, $VHardpointSize::Internal, $VHardpointType::Omni);
