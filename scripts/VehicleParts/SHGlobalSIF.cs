function SHGlobalSIF::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHGlobalSIF::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 0, 0, 0, 0.2, "VehicleIntegrityField");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHGlobalSIF", "Structural Integrity Field", "Reduces all damage taken by 30%, Power drain: 20%", $VehicleList::AllExceptWalkers, $VHardpointSize::Internal, $VHardpointType::Omni);
