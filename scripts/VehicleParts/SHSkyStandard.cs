function SHSkyStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHSkyStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 225, 10000, (45 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHSkyStandard", "Standard Shield", "Strength: 225 HP, Recharge delay: 10 sec, Power drain: 60%", $VehicleList::Skycutter, $VHardpointSize::Internal, $VHardpointType::Omni);
