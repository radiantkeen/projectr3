datablock LinearProjectileData(VStarHammerRocket) : StarHammerRocket
{
   damageRadius        = 15.0;
   radiusDamageType    = $DamageType::StarHammer;
   kickBackStrength    = 4000;
   bubbleEmitTime      = 1.0;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::StarHammer;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 240;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 75;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CountMAs | $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.5;
};

function VStarHammerRocket::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    if(%proj.tickCount % 3 == 0)
        projectileTrail("LinearFlareProjectile", "StarHammerAreaCharge", %proj.initialDirection, %proj.position, 0, %proj.instigator, 2);
}

datablock TurretImageData(LStarHammerSTD)
{
   shapeFile = "turret_tank_barrelmortar.dts";
   mountPoint = 0;

   projectile = VStarHammerRocket;
   projectileType = LinearProjectile;
   updatePilotAmmo = true;
   sharedResourcePool = true;
   ammo = "VLStarHammerAmmo";

   usesEnergy = false;
   useMountEnergy = false;
   fireEnergy = 75.0;
   minEnergy = 75.0;
   useCapacitor = true;

   // Turret parameters
   activationMS                        = 1000;
   deactivateDelayMS                   = 1500;
   thinkTimeMS                         = 200;
   degPerSecTheta                      = 500;
   degPerSecPhi                        = 500;
   attackRadius                        = 75;

   // State transitions
   stateName[0]                        = "Activate";
   stateTransitionOnNotLoaded[0]       = "Dead";
   stateTransitionOnLoaded[0]          = "ActivateReady";

   stateName[1]                        = "ActivateReady";
   stateSequence[1]                    = "Activate";
   stateSound[1]                       = AssaultTurretActivateSound;
   stateTimeoutValue[1]                = 1.0;
   stateTransitionOnTimeout[1]         = "Ready";
   stateTransitionOnNotLoaded[1]       = "Deactivate";

   stateName[2]                        = "Ready";
   stateTransitionOnNotLoaded[2]       = "Deactivate";
   stateTransitionOnNoAmmo[2]          = "NoAmmo";
   stateTransitionOnTriggerDown[2]     = "Fire";

   stateName[3]                        = "Fire";
   stateSequence[3]                    = "Fire";
   stateTransitionOnTimeout[3]         = "Reload";
   stateTimeoutValue[3]                = 3.0;
   stateFire[3]                        = true;
   stateRecoil[3]                      = LightRecoil;
   stateAllowImageChange[3]            = false;
   stateSound[3]                       = StarHammerFireSound;
   stateScript[3]                      = "onFire";

   stateName[4]                        = "Reload";
   stateSequence[4]                    = "Reload";
   stateTimeoutValue[4]                = 1.0;
   stateAllowImageChange[4]            = false;
   stateSound[4]                       = StarHammerReloadSound;
   stateTransitionOnTimeout[4]         = "Ready";
   //stateTransitionOnNoAmmo[4]          = "NoAmmo";
   stateWaitForTimeout[4]              = true;

   stateName[5]                        = "Deactivate";
   stateDirection[5]                   = false;
   stateSequence[5]                    = "Activate";
   stateTimeoutValue[5]                = 1.0;
   stateTransitionOnLoaded[5]          = "ActivateReady";
   stateTransitionOnTimeout[5]         = "Dead";

   stateName[6]                        = "Dead";
   stateTransitionOnLoaded[6]          = "ActivateReady";
   stateTransitionOnTriggerDown[6]     = "DryFire";

   stateName[7]                        = "DryFire";
   stateSound[7]                       = AssaultMortarDryFireSound;
   stateTimeoutValue[7]                = 1.0;
   stateTransitionOnTimeout[7]         = "NoAmmo";

   stateName[8]                        = "NoAmmo";
   stateSequence[8]                    = "NoAmmo";
   stateTransitionOnAmmo[8]            = "Reload";
   stateTransitionOnTriggerDown[8]     = "DryFire";
};

datablock TurretImageData(LStarHammerIMP) : LStarHammerSTD
{
   useMountEnergy = false;
   useCapacitor = false;
};

datablock TurretImageData(LStarHammerFIR2T) : LStarHammerSTD
{
   shapeFile = "turret_muzzlepoint.dts";
   
   useMountEnergy = false;
   useCapacitor = false;
};

function LStarHammerSTD::onFire(%data, %obj, %slot)
{
     %p = Parent::onFire(%data, %obj, %slot);

     if(%p)
        createRemoteProjectile("LinearFlareProjectile", "PowerDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);
    
//    %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo);
}

function LStarHammerIMP::onFire(%data, %obj, %slot)
{
     %p = Parent::onFire(%data, %obj, %slot);

     if(%p)
        createRemoteProjectile("LinearFlareProjectile", "PowerDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);

//    %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo);
}

function LStarHammer::spawnProjectile(%this, %walker, %slot)
{
     %p = Parent::spawnProjectile(%this, %walker, %slot);

     if(%p)
        createRemoteProjectile("LinearFlareProjectile", "PowerDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);

    return %p;
}

function LStarHammer::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VLStarHammerAmmo";
    %this.refImage = LStarHammerSTD;
    
    // Walker prefs
    %this.dryFireSound = "MortarDryFireSound";
    %this.fireSound = "StarHammerFireSound";
    %this.projectileType = "LinearProjectile";
    %this.projectile = "StarHammerRocket";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 30;
    %this.fireTimeout = 4000;
    %this.heatPerShot = 0;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Beta;
    %this.isSeeker = false;
}

function LStarHammer::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "MainCannon", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(LStarHammerSTD, 4);

        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
//            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(LStarHammerFIR2T, 4);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LStarHammerIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LStarHammerIMP, 0);
            
        case $VehicleID::MPB:
            %vehicle.barrel = "LStarHammerIMP";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LStarHammer", "Star Hammer", "Main gun switched to Star Hammer", $VehicleList::All, $VHardpointSize::Large, $VHardpointType::Ballistic);
