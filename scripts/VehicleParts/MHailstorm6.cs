datablock ShapeBaseImageData(MHailstorm6SKY1)
{
   className = WeaponImage;
   shapeFile = "stackable2m.dts";

   usesEnergy = false;
   ammo = "VMHailstorm6Ammo";
   updatePilotAmmo = true;
   minEnergy = 0;
   fireEnergy = 0;
   fireTimeout = 333;
   sharedResourcePool = true;

   projectile = HailstormRocket;
   projectileType = LinearProjectile;
   useForwardVector = true;
   
   offset = "2 -0.5 0.6";
   rotation = "1 0 0 90";
   
   mountPoint = 10;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Deploy";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateSound[4]            = MBLFireSound;
   stateTimeoutValue[4]          = 0.333;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(MHailstorm6SKY2) : MHailstorm6SKY1
{
   offset = "-2 -0.5 0.6";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(MHailstorm6OUT1) : MHailstorm6SKY1
{
   offset = "0.6 6.75 -1.5";
   rotation = "1 0 0 90";
   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(MHailstorm6OUT2) : MHailstorm6SKY1
{
   offset = "-0.775 6.75 -1.5";
   rotation = "1 0 0 90";
   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(MHailstorm6FIR1) : MHailstorm6SKY1
{
   pilotHeadTracking = true;
   
   offset = "0 4.5 -0.8";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(MHailstorm6GUA1) : MHailstorm6SKY1
{
   pilotHeadTracking = true;
   mountPoint = 1;
   offset = "-0.1 4.25 1.45";
   rotation = "1 0 0 90";
};

function MHailstorm6::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VMHailstorm6Ammo";
    %this.refImage = LHailstorm9IMP;
    
    // Walker prefs
    %this.dryFireSound = "MissileDryFireSound";
    %this.fireSound = "MILFireSound";
    %this.projectileType = "LinearProjectile";
    %this.projectile = "HailstormRocket";
    %this.projectileSpread = 3;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 150;
    %this.fireTimeout = 333;
    %this.heatPerShot = 0;
    %this.useForwardVector = true;
    %this.defaultFireGroup = $VehicleFiregroup::Gamma;
    %this.isSeeker = false;
}

function MHailstorm6::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "ClusterMissile", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.mountImage(MHailstorm6SKY1, 2);
            %vehicle.mountImage(MHailstorm6SKY2, 3);
            
        case $VehicleID::Outlaw:
            %vehicle.mountImage(MHailstorm6OUT1, 2);
            %vehicle.mountImage(MHailstorm6OUT2, 3);

        case $VehicleID::Guardian:
            %vehicle.mountImage(MHailstorm6GUA1, 0);

        case $VehicleID::Firestorm:
            if(%hardpoint == 0)
                %vehicle.mountImage(MHailstorm6FIR1, 0);
            else
                %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]).mountImage(MHailstorm6FIR1T, 2 * %hardpoint);
    }
    
//    %vehicle.setInventory(%this.ammo, %data.max[%this.ammo]);
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MHailstorm6", "Hailstorm Rocket Pod-MD", "Rapid fire dumbfire rocket pod", $VehicleList::General, $VHardpointSize::Medium, $VHardpointType::Missile);
