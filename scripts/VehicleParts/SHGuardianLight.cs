function SHGuardianLight::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHGuardianLight::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 405, 11000, (81 / $g_TickTime), 0.4, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHGuardianLight", "Light Shield", "Strength: 405 HP, Recharge delay: 11 sec, Power drain: 40%", $VehicleList::Guardian, $VHardpointSize::Internal, $VHardpointType::Omni);
