function MOTSH::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Passive;
}

function MOTSH::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.heatDrain -= 2;
    %vehicle.maxHeat = mCeil($VehicleWalkerData[%vehicle.vid, "maxHeat"] * 1.4);
}

//VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOTSH", "Thermal Storage Heater", "Increases heat capacity by 40%, -4 K/s heat drain", $VehicleList::Walkers, $VHardpointSize::Internal, $VHardpointType::Omni);
