datablock EnergyProjectileData(VSpikeShot) : SpikeSingleShot
{
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Spike;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 75;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;
   
   directDamage        = 0.6;
};

datablock ShapeBaseImageData(MSpikeSKY1)
{
   className = WeaponImage;
   shapeFile = "turret_missile_large.dts";

   ammo = "VMSpikeAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   
   projectile = VSpikeShot;
   projectileType = EnergyProjectile;
   mountPoint = 10;

   offset = "1 1.5 0.65";
   rotation = "0 1 0 0";

   usesEnergy = false;
   minEnergy = 0;
   fireEnergy = 0;
   fireTimeout = 375;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateSpinThread[4]       = FullSpeed;
   stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateSound[4]            = OBLFireSound;
   stateSequence[4]         = "Fire";
   // IMPORTANT! The stateTimeoutValue below has been replaced by fireTimeOut
   // above.
   stateTimeoutValue[4]          = 1.0;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateScript[7]              = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(MSpikeSKY2) : MSpikeSKY1
{
   offset = "-1 1.5 0.65";
   rotation = "0 1 0 180";

   stateSound[4]                    = "";
};

datablock ShapeBaseImageData(MSpikeOUT1) : MSpikeSKY1
{
   offset = "0.6 6.75 -1.5";
   rotation = "0 1 0 0";
   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(MSpikeOUT2) : MSpikeSKY1
{
   offset = "-0.775 6.75 -1.5";
   rotation = "0 1 0 0";
   pilotHeadTracking = true;
   
   stateSound[4]                    = "";
};


datablock ShapeBaseImageData(MSpikeGUA1) : MSpikeSKY1
{
   mountPoint = 1;
   offset = "-0.1 4 1.4";
   rotation = "0 1 0 180";
   
   stateTimeoutValue[4]          = 1.0;
   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(MSpikeFIR1) : MSpikeSKY1
{
   offset = "0 4.5 -0.8";
   rotation = "0 1 0 180";
   
   stateTimeoutValue[4]          = 1.0;
   pilotHeadTracking = true;
};

function MSpike::alternateFire(%this, %obj)
{
    if(%obj.blasterFireState)
    {
        %obj.currentFireNum++;

        if(%obj.currentFireNum > 7)
            %obj.currentFireNum = 4;

        for(%t = 4; %t < 8; %t++)
        {
            if(%t == %obj.currentFireNum)
                %obj.setImageTrigger(%t, true);
            else
                %obj.setImageTrigger(%t, false);
        }
        
        %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %this.ammo);
    }
    else
    {
        for(%t = 4; %t < 8; %t++)
            %obj.setImageTrigger(%t, false);
    }
    
    if(isObject(%obj) && %obj.blasterFireState)
        %this.schedule(333, "alternateFire", %obj);
}

function MSpike::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VMSpikeAmmo";
    
    // Walker prefs
    %this.dryFireSound = "PlasmaDryFireSound";
    %this.fireSound = "OBLFireSound";
    %this.projectileType = "EnergyProjectile";
    %this.projectile = "VSpikeShot";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 100;
    %this.fireTimeout = 1000;
    %this.heatPerShot = 0;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha | $VehicleFiregroup::Beta;
    %this.isSeeker = false;
}

function MSpike::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Plasma", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.mountImage(MSpikeSKY1, 2);
            %vehicle.mountImage(MSpikeSKY2, 3);

        case $VehicleID::Outlaw:
            %vehicle.mountImage(MSpikeOUT1, 2);
            %vehicle.mountImage(MSpikeOUT2, 3);
            
        case $VehicleID::Guardian:
            %vehicle.mountImage(MSpikeGUA1, 0);

        case $VehicleID::Firestorm:
            %vehicle.mountImage(MSpikeFIR1, 0);
    }

    %vehicle.currentFireNum = 4;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MSpike", "Spike Mount", "High velocity Spike mount", $VehicleList::General, $VHardpointSize::Medium, $VHardpointType::Ballistic);
