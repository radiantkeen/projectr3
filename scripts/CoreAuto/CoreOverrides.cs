//==============================================================================
// Overriding functions - stuff we need to set assumptions about

function isDemo()
{
    return false;
}

function isDemoServer()
{
    return false;
}

function Lightning::getControllingClient()
{
     return 0;
}

function VerifyCDCheck(%func)
{
    // Nope
}

function TerrainBlock::isMounted()
{
      //prevent console errors
      return false;
}

function InteriorInstance::isMounted()
{
      //prevent console errors
      return false;
}

function AIConnection::getDataBlock()
{
      //prevent console errors
      return 0;
}

function AIConnection::getControllingClient()
{
      //prevent console errors
      return 0;
}

function GameConnection::getDatablock()
{
      //prevent console errors
      return 0;
}

function GameConnection::getControllingClient()
{
      //prevent console errors
      return 0;
}

function InteriorInstance::isHidden()
{
      //prevent console errors
      return false;
}

function GameConnection::isMounted()
{
    //prevent console errors
    return false;
}
