//-----------------------------------------------------------------------------
// Time functions
//--------------------------------------

function setTickTime(%len)
{
    $g_TickTime = %len;
    $g_tickDelta = 1 / %len;
}

//-----------------------------------------------------------------------------
// Main Loop
// Use this and onTime(time, eval) to schedule events to a certain time
// The loop will be accurate to within 1-1000ms of every minute.

function timestamp(%format)
{
     switch(%format)
     {
          case 0:
               %time = "["@formatTimeString("HH:nn:ss")@"]";

          case 1:
               %time = "["@formatTimeString("MM d")@"]";

          case 2:
               %time = "["@formatTimeString("M d HH:nn:ss")@"]";

          case 3:
               %time = formatTimeString("HHnn");

          case 4:
               %time = formatTimeString("ss");

          case 5:
               %time = "["@formatTimeString("HH:nn")@"]";

          case 6:
               %time = formatTimeString("m-d-y");

          default:
               %time = "["@formatTimeString("HH:nn:ss")@"]";
     }

     return %time;
}

function getResyncMainLoopTime()
{
    // Grab seconds left in this minute
    %sync = timestamp(4);

    // If we somehow get negative seconds, return 0 so we don't end up with ""
    if(%sync <= 0)
        return 0;

    return %sync * 1000;
}

function startMainLoop()
{
    // Verify we don't restart the main loop
    if(System.mainLoopActive)
        return;

    System.mainLoopActive = true;

    %timeMS = getResyncMainLoopTime();

    // Schedule to start the loop with the remaining seconds, if any exist
    schedule(60000 - %timeMS, 0, MainLoop);
}

function MainLoop()
{
    %time = timestamp(3);
    %todoListCount = System.doOnTimeCount[%time] ? System.doOnTimeCount[%time] : 0;

    if(%todoListCount)
        for(%idx = 0; %idx < %todoListCount; %idx++)
            eval(EngineBase.doOnTime[%time, %idx]);

    if(isDebugMode())
        echo("MainLoop() -> executed" SPC %todoListCount SPC "functions.");

    // Verify we haven't drifted away from the clock any...
    // This wouldn't be a problem if Torque didn't have that nasty habit of
    // "stopping time" while script is running, including schedules >.o
    %timeMS = getResyncMainLoopTime();

    schedule(60000 - %timeMS, 0, MainLoop);
}

function onTime(%time, %evalString)
{
    // Retrieve our todo list count
    %todoListCount = System.doOnTimeCount[%time] ? System.doOnTimeCount[%time] : 0;

    // If we have anything on it for this time, scan through to make sure we're
    // not doing it more than once
    if(%todoListCount)
        for(%idx = 0; %idx < %todoListCount; %idx++)
            if(System.doOnTime[%time, %idx] $= %evalString)
            {
                error("[System::onTime] Cannot add duplicate eval string" SPC %evalString SPC "at" SPC %time);
                return;
            }

    // Add to the todo list
    System.doOnTime[%time, %todoListCount] = %evalString;
    System.doOnTimeCount[%time]++;

    // If we add an onTime() while we're on the time itself, exec it
    if(%time == timestamp(3))
        eval(%evalString);
}

// Load our code into memory
startMainLoop();
setTickTime(32);

// onTime example
//System.onTime("0000", "System.logManager.restartLoggers();");
