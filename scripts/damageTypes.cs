//--------------------------------------------------------------------------
// Damage Types v2.0
//--------------------------------------------------------------------------
// This has been moved to deathMessages.cs, and the resulting datablocks have been moved to defaultBlocks.cs

// These bitmasks are the basis of elemental damage globally
$DamageGroupMask::Misc             = 1 << 0;
$DamageGroupMask::Energy           = 1 << 1;
$DamageGroupMask::Explosive        = 1 << 2;
$DamageGroupMask::Kinetic          = 1 << 3;
$DamageGroupMask::Plasma           = 1 << 4;
$DamageGroupMask::Mitzi            = 1 << 5;
$DamageGroupMask::Poison           = 1 << 6;
$DamageGroupMask::Psionic          = 1 << 7;
$DamageGroupMask::Sonic            = 1 << 8;
$DamageGroupMask::ExternalMeans    = 1 << 9;
