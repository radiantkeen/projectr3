//-----------------------------------------------------------------------------
// Xi Command System
// Whisper (Private Chat) system - whisper

function Xi::w(%this, %cl, %val)
{
     if(isObject(%cl.lockedClient))
     {
          %cl.lockedClient.lastResponder = %cl;
          messageClient(%cl, 'MsgOutgoingWhisper', '\c5To %1: %2', %cl.lockedClient.name, %val);
          messageClient(%cl.lockedClient, 'MsgIncomingWhisper', '\c5%1 says: %2', %cl.name, %val);
          EngineBase.logManager.logToFile("WhisperLog", timestamp() SPC "[Whisper]" SPC %cl.namebase SPC "->" SPC %cl.lockedClient.namebase@":" SPC %val);
     }
     else if(%cl.lockedClient)
     {
          %cl.lockedClient = 0;
          messageClient(%cl, 'MsgWhisper501', '\c5Whisper: This player has disconnected, auto-unlocking name.');
     }
     else
          messageClient(%cl, 'MsgWhisper500', '\c5Whisper: Who are you talking to?');     
}

Xi.addCommand($XI::Player, "w", "Whisper player a message - ex. /w message");

