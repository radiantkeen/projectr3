//-----------------------------------------------------------------------------
// Xi Command System
// Increase Time Limit

function Xi::inctime(%this, %cl, %val)
{
     %val = firstWord(%val);
     
     // Prevent setting time as a word
     if(%val == 0)
          return;
     
     if($matchStarted)
     {
          $Host::TimeLimit += %val;
          
          //schedule the end of match countdown
          %elapsedTimeMS = getSimTime() - $missionStartTime;
          %curTimeLeftMS = ($Host::TimeLimit * 60 * 1000) - %elapsedTimeMS;
          CancelEndCountdown();
          EndCountdown(%curTimeLeftMS);
          cancel(Game.timeSync);
          Game.checkTimeLimit(true);
          messageClient(%cl, 'MsgXITimeInc', '\c3Xi[Server]: Time limit increased by %1 minutes.', %val);          
     }
     else
          messageClient(%cl, 'MsgXITimeError', '\c3Xi[Server]: Cannot set time inbetween missions.');
}

Xi.addCommand($XI::Admin, "inctime", "Increases current match time (in minutes) - ex. /inctime 10");

