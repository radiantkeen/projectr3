//-----------------------------------------------------------------------------
// Xi Command System
// Jump To Player

function Xi::jumpto(%this, %cl, %val)
{
      if(%val !$= "")
      {
         %client = nameToClient(%val);

         if(%client)
            if(isObject(%client.player))
               %cl.player.setPosition(%client.player.getPosition());
      }
      else
      {
         messageClient(%cl, 'MsgXIWarpNoName', '\c5Warp: No name specified.');
         return 1;
      }
}

Xi.addCommand($XI::Admin, "jumpto", "Allows you to jump to another player's location - ex. /jumpto noob");

