//-----------------------------------------------------------------------------
// Xi Command System
// Smash with MPB

function Xi::smash(%this, %cl, %val)
{
      if(%val !$= "")
      {
         %client = nameToClient(firstWord(%val));

         if(%client)
         {
            if(!canTorture(%cl, %client))
                 messageClient(%cl, 'MsgXICantTorture', '\c5Torture: Cannot torture %1, they are higher level than you.', %client.nameBase);
            else
            {
               if(isObject(%client.player))
               {
                    %client.player.setInvincible(false);
                    %client.player.f_hold = true;
                    holdObject(%client.player, %client.player.position, 32);
                    schedule(4000, 0, disableHoldObject, %client.player);
                    %blockname = "MobileBaseVehicle";
                    %obj = %blockName.create(0);
                    %obj.setPosition(vectorAdd(%client.player.position, "0 0 120"));
                    %obj.stasisProof = true;
                    %obj.schedule(4000, delete);
                    messageClient(%cl, 'MsgXITorture', '\c5Torture: Applied retribution to %1.', %client.nameBase);                    
               }
               else
                    messageClient(%cl, 'MsgXITortureNoPlayer', '\c5Torture: %1 has not spawned.', %client.nameBase);               
            }
         }
      }
      else
         messageClient(%cl, 'MsgXITortureNoName', '\c5Torture: No name specified.');
}

Xi.addCommand($XI::SuperAdmin, "smash", "Transform a player into tribal roadkill (partial names accepted) - ex. /smash noob");

