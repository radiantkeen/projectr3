//-----------------------------------------------------------------------------
// Xi Command System
// Bot Controls

function Xi::startbots(%this, %client, %val)
{
     if($Host::BotsEnabled)
     {
          messageClient(%client, 'MsgXiBotsStartedF', '\c2Server bot support is already on.');
          return;
     }

     $Host::BotsEnabled = 1;
     addMultiRandomBot($Host::BotCount);
     $HostGameBotCount = $Host::BotCount;
     $HostGamePlayerCount = ClientGroup.getCount();
     Game.AIInit();
     $AIDisableChat = $Host::DisableBotTalk;
     
     messageClient(%client, 'MsgXiBotsStartedS', '\c2Server bot support initiated.');
}

Xi.addCommand($XI::Admin, "startbots", "Starts server bot support - ex. /startbots");

function Xi::stopbots(%this, %client, %val)
{
     if(!$Host::BotsEnabled)
     {
          messageClient(%client, 'MsgXiBotsStoppedF', '\c2Server bot support is already off.');
          return;
     }
     
     kickAllBots();

     $HostGameBotCount = 0;
     $HostGamePlayerCount = ClientGroup.getCount();
     $Host::BotsEnabled = 0;
     
     messageClient(%client, 'MsgXiBotsStoppedS', '\c2Server bot support stopped, all bots removed.');
}

Xi.addCommand($XI::Admin, "stopbots", "Stops server bot support - ex. /stopbots");

function Xi::setbotcount(%this, %client, %val)
{
     %bcount = firstWord(%val);

     if(%bCount < 0 || %bCount > 16)
          %bCount = 16;
          
     $Host::BotCount = %bcount;
     messageClient(%client, 'MsgXiBotCountSet', '\c2Server bot count set to %1.', $Host::BotCount);
}

Xi.addCommand($XI::Admin, "setbotcount", "Sets number of bots to spawn using /startbots - ex. /setbotcount 16");

function kickAllBots()
{
//     %iterations = $Host::BotCount;			// -[soph]
//     %loop = 0;	
     
//     while(%loop < %iterations)
//     {						// -[/soph]
          %count = ClientGroup.getCount();

          for( %i = %count - 1 ; %i >= 0 ; %i -- )	// for(%i = 0; %i < %count; %i++) -soph
          {						// ClientGroup prunes when client.drop(), so iterate backwards =soph
     	    %client = ClientGroup.getObject(%i);

               if(!%client.isAIControlled())
                    continue;

               Game.removeFromTeamRankArray( %client );	// +soph
               %client.player.scriptKill(0);
               messageClient(%client, 'onClientKicked', "");
               messageAllExcept(%client, -1, 'MsgClientDrop', "", %client.nameBase, %client);
               freeTarget( %client.getTarget() ) ;
               %client.drop();
          }
          
//          %loop++;					// -soph
//     }						// -soph
}