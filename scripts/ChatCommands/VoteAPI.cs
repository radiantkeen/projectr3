//------------------------------------------------------------------------------
// Custom vote API

function customStartNewVote(%client, %actionMsg, %funcName, %arg1, %arg2, %arg3)
{
   %isAdmin = ( %client.isAdmin || %client.isSuperAdmin );

   // z0dd - ZOD, 4/7/02. Get the Admins name.
   if(%isAdmin)
      $AdminName = %client.name;

   if(!%client.canVote) // && !%isAdmin)
      return;

   if(true)     // z0dd - ZOD, 4/7/02. Allow SuperAdmins to kick Admins
   {
      %teamSpecific = false;
      %gender = (%client.sex $= "Male" ? 'he' : 'she');
      if ( Game.scheduleVote $= "" )
      {
         %clientsVoting = 0;

         //send a message to everyone about the vote...
            for ( %idx = 0; %idx < ClientGroup.getCount(); %idx++ )
            {
               %cl = ClientGroup.getObject( %idx );
               if ( !%cl.isAIControlled() )
               {
	               messageClient( %cl, 'VoteStarted', '\c2%1 initiated a vote to %2.', %client.name, %actionMsg);
                  %clientsVoting++;
               }
            }

         // open the vote hud for all clients that will participate in this vote
            for ( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ )
            {
               %cl = ClientGroup.getObject( %clientIndex );
               if ( !%cl.isAIControlled() )
                  messageClient(%cl, 'openVoteHud', "", %clientsVoting, ($Host::VotePassPercent / 100));
            }

         clearVotes();
         Game.voteType = %typeName;
         Game.scheduleVote = schedule(($Host::VoteTime * 1000), 0, "calcVotesC", %funcName, 0, %actionMsg, %arg1, %arg2, %arg3); // %admin here used for interrupting votes possibly later on
         %client.vote = true;
         messageAll('addYesVote', "");

         if(!%client.team == 0)
            clearBottomPrint(%client);
      }
      else
         messageClient( %client, 'voteAlreadyRunning', '\c2A vote is already in progress.' );
   }

   %client.canVote = false;
   %client.rescheduleVote = schedule(($Host::voteSpread * 1000) + ($Host::voteTime * 1000) , 0, "resetVotePrivs", %client);
}

function calcVotesC(%voteFunc, %admin, %text, %arg1, %arg2, %arg3)
{
   for ( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ )
   {
      %cl = ClientGroup.getObject( %clientIndex );
      messageClient(%cl, 'closeVoteHud', "");

      if ( %cl.vote !$= "" )
      {
         if ( %cl.vote )
         {
            Game.votesFor[%cl.team]++;
            Game.totalVotesFor++;
         }
         else
         {
            Game.votesAgainst[%cl.team]++;
            Game.totalVotesAgainst++;
         }
      }
      else
      {
         Game.votesNone[%cl.team]++;
         Game.totalVotesNone++;
      }
   }

   Game.schedule(0, processCustomVote, %voteFunc, %admin, %text, %arg1, %arg2, %arg3);
   Game.scheduleVote = "";
   Game.kickClient = "";
}

function DefaultGame::processCustomVote(%game, %voteFunc, %admin, %text, %arg1, %arg2, %arg3)
{
   if(%admin)
   {
      messageAll('MsgAdminCustomVote', '\c2The Admin has forced %1.', %text);

      if(%voteFunc !$= "")
         schedule(0, 0, %voteFunc, %arg1);
   }
   else
   {
      %totalVotes = Game.totalVotesFor + Game.totalVotesAgainst;

      if(%totalVotes > 0 && (Game.totalVotesFor / %totalVotes) > ($Host::VotePasspercent / 100))
      {
         messageAll('MsgVotePassed', '\c2Vote to %1 passed!', %text);

         if(%voteFunc !$= "")
            schedule(0, 0, %voteFunc, %arg1);
      }
      else
         messageAll('MsgVoteFailed', '\c2Vote to %1 did not pass: %2 percent.', %text, mFloor(Game.totalVotesFor/%totalVotes * 100));
   }

  clearVotes();
}

//------------------------------------------------------------------------------
// voteFunc stubs

function voteIncTime(%delta)
{
   %boost = %delta;

   if((Xi.currentTimeBoost + %delta) > Xi.maxTimeLimitBoost)
   {
      %boost = Xi.maxTimeLimitBoost - Xi.currentTimeBoost;
      Xi.currentTimeBoost = Xi.maxTimeLimitBoost;
   }

   Xi.inctime(0, %boost);
   messageAll('MsgTimeBoost', '\c2Mission time increased by %1 minutes.', %boost);

   // Only needs to be called once
   if(!Xi.timeBoostFlag)
   {
      Xi.timeBoostFlag = true;
      voteTimeBoostReset();
   }
}

function voteTimeBoostReset()
{
   if($matchStarted)
   {
       // Scheduling end of mission is unreliable with torque, so this will
       // have to do, silly as it is
       schedule(2000, 0, voteTimeBoostReset);
       return;
   }

   $Host::TimeLimit = Xi.defaultTimeLimit;
   Xi.currentTimeBoost = 0;
   Xi.timeBoostFlag = false;
}

function voteBotSupport(%bool)
{
   if(%bool)
   {
      Xi.startbots(0);
      messageAll('MsgBotsOn', '\c2Bots turned on by vote.');
   }
   else
   {
      Xi.stopbots(0);
      messageAll('MsgBotsOff', '\c2Bots turned off by vote.');
   }
}

function voteBaseRape( %state )	// +[soph]
{
   if( %state $= "disable" )
   {
      $voteBaseRapeOn = true ;
      messageAll('MsgBaserapeOn', '\c2Baserape protection disabled by vote.\n Baserape allowed even when the %1-player minimum is not met.' , $Host::MD2::AntiBaseRapeCount ) ;
   }
   else
   {
      $voteBaseRapeOn = false ;
      messageAll('MsgBaserapeOff', '\c2Baserape protection re-enabled by vote.\n Baserape disallowed when there are fewer than %1 players.' , $Host::MD2::AntiBaseRapeCount ) ;
   }
}				// +[/soph]

//------------------------------------------------------------------------------
// XI command plugs

// Do some data collection here first
Xi.defaultTimeLimit = $Host::TimeLimit;
Xi.maxTimeLimitBoost = 30;
Xi.maxTimeLimitInc = 15;
Xi.minTimeLimitInc = 5;
Xi.currentTimeBoost = 0;

function Xi::callvote(%this, %client, %val)
{
     // Prevent no-text voting
     if(%val $= "")
          return;

      customStartNewVote(%client, %val, "", 0, 0, 0);
}

Xi.addCommand($XI::Admin, "callvote", "Start an arbitrary vote - ex. /callvote change map?");

function Xi::votetime(%this, %client, %val)
{
     %val = firstWord(%val);

     if(%val < Xi.minTimeLimitInc)
          %val = Xi.minTimeLimitInc;

     if(%val > Xi.maxTimeLimitInc)
          %val = Xi.maxTimeLimitInc;

     if(Xi.currentTimeBoost >= Xi.maxTimeLimitBoost)
     {
         messageClient(%cl, 'MsgXITimeOut', '\c3Xi[votetime]: Maximum amount of time added to current mission.');
         return;
     }

     customStartNewVote(%client, "increase time by" SPC %val SPC "minutes", "voteIncTime", %val, 0, 0);
}

Xi.addCommand($XI::Player, "votetime", "Allows you to vote for more time in minutes (min "@Xi.minTimeLimitInc@" max "@Xi.maxTimeLimitInc@") - ex. /votetime 10");

function Xi::votebots(%this, %client, %val)
{
//     %val = firstWord(%val) $= "on" ? 1 : 0;					// -soph

     %count = ClientGroup.getCount() ;						// +[soph]
     $Host::BotsEnabled = false ;						// +
     for( %i = 0 ; %i < %count ; %i++ )						// +
     {										// +
          %cl = ClientGroup.getObject( %i ) ;					// +
          if( %cl.isAIControlled() )						// +
          {									// +
               $Host::BotsEnabled = true ;					// +
               break ;								// +
          }									// +
     }										// +[/soph]

     // This seems redundant, but is necessary in case someone just does /votebots with no args
     %word = $Host::BotsEnabled ? "off" : "on" ;				// %word = %val ? "on" : "off"; -soph
     %val = !$Host::BotsEnabled ;						// +soph
     
     customStartNewVote(%client, "turn bot support" SPC %word, "voteBotSupport", %val, 0, 0);
}

Xi.addCommand($XI::Player, "votebots", "Votes bot support on or off." ) ;	// "Allows you to vote bots on|off - ex. /votebots off"); -soph

function Xi::voterape(%this, %client, %val)	// +[soph]
{
     // This seems redundant, but is necessary in case someone just does /votebots with no args
     %word = $voteBaseRapeOn ? "enable" : "disable" ;

     customStartNewVote( %client , %word SPC "baserape protection" , "voteBaseRape" , %word , 0 , 0 ) ;
}

Xi.addCommand($XI::Player, "votebaserape", "Vote to toggle baserape protection. Protection kicks in when fewer than" SPC $Host::MD2::AntiBaseRapeCount SPC "players are ingame." ) ;

function Xi::voteBaseRape(%this, %client, %val)
{
     // This seems redundant, but is necessary in case someone just does /votebots with no args
     %word = $voteBaseRapeOn ? "enable" : "disable" ;
     %phrase = $voteBaseRapeOn ? "when there are fewer than" SPC $Host::MD2::AntiBaseRapeCount SPC "players" : "" ;

     customStartNewVote( %client , %word SPC "baserape protection" , "voteBaseRape" , %word , 0 , 0 ) ;
}

Xi.addCommand($XI::Player, "votebaserape", "Vote to toggle baserape protection. Protection kicks in when fewer than" SPC $Host::MD2::AntiBaseRapeCount SPC "players are ingame." ) ;
						// +[/soph]