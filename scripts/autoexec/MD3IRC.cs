// Tribes 2 IRC Script
// Originally written by Krash123
// Modified by DarkDragonDX (Robert MacGregor)
// Modified by Bakhe (Calvin Bakhe)

// Used for a function down below
$TCIRC::messageSplitThreshold = 424;

package TCIRC {
	function CreateServer(%mission, %missionType) {
		parent::CreateServer(%mission, %missionType);

		new TCPObject( TC_IRC ) {
			retryTime = 1000;
			retryCount = 0;
			debug = false;
			server = "IP:irc.tribalwar.com:6667"; // "IP:irc.darkmyst.net:6667";
			allowBroadcastControl = false; // Allow directed broadcast filtering?
			messageSplitThreshold = $TCIRC::messageSplitThreshold; // Might vary based on IRC server
			listBots = true; // For the !players command
			reportBots = false; // For general messages, mostly for joining and leaving.
			displayTeamChat = false; // Possibly for anti-cheat measures, if the enemy has a listener in IRC
			designateBots = true; // If listBots is enabled, you can use this to designate bots in !players
			captureKills = false; // Messages kills to the IRC
			captureSuicides = false; // Messages suicides to the IRC
			captureAIKills = false; // If kills/MAs done by bots should be captured?
			determineWeapons = false; // If our messages should attempt to determine the weapon type
			captureAISuicides = false; // ... Don't enable this unless you like everyone seeing the 1000*10^9001 suicides from bots. Or if your AI doesn't suck
			channel = "#solconflict"; //  "#md2funhouse"
			captureMAs = false; // Display mid-airs to the IRC. This has its own implementation and a mod may opt for its own.
			MAHeightThreshold = 0; // In meters above the ground the target getting hit has to be for it to count.
			MADamageThreshold = 0; // How much damage your project must apply for the MA to count. (T2 damage units are in percent)
			MADisplayDistance = true; // Should the MA code display what distance the MA was at?
			MADisplayTeamHit = true; // Should the MA code trigger on a friendly MA?
			nick = "MD3Alpha";
			user = "TCIRC 1337 hax :Construct";
		};
	}

	function GameConnection::onConnect( %client, %name, %raceGender, %skin, %voice, %voicePitch ) {
		parent::onConnect( %client, %name, %raceGender, %skin, %voice, %voicePitch );
		if (%client.isAIControlled() && !%this.reportBots)
			return;
		TC_IRC.sendMsg("-->" SPC %client.namebase SPC "has joined the game.");
	}

	function GameConnection::onDrop(%client, %reason) {
		parent::onDrop(%client, %reason);
		if (%client.isAIControlled() && !%this.reportBots)
			return;
		TC_IRC.sendMsg("<--" SPC %client.namebase SPC "has left the game.");
	}

	function chatMessageTeam( %sender, %team, %msgString, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a10 ) {
		if (TC_IRC.displayTeamChat)
			TC_IRC.relayChat( %sender, "(team)" SPC %a2 );
		parent::chatMessageTeam( %sender, %team, %msgString, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a10 );
	}

	function cannedChatMessageTeam( %sender, %team, %msgString, %name, %string, %keys ) {
		if (TC_IRC.displayTeamChat)
			TC_IRC.relayChat( %sender, "(team)" SPC %string );
		parent::cannedChatMessageTeam( %sender, %team, %msgString, %name, %string, %keys );
	}

	function chatMessageAll( %sender, %msgString, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a10 ) {
		TC_IRC.relayChat( %sender, %a2 );
		parent::chatMessageAll( %sender, %msgString, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a10 );
	}

	function cannedChatMessageAll( %sender, %msgString, %name, %string, %keys ) {
		TC_IRC.relayChat( %sender, %string );
		parent::cannedChatMessageAll( %sender, %msgString, %name, %string, %keys );
	}

	function getSubStr( %string, %start, %length )
	{
		while ( %start < 0 ) { %start += strLen( %string ); }
		while ( %length <= 0 ) { %length += strLen( %string ) - %start; }
		parent::getSubStr( %string, %start, %length );
	}

	// Added to capture kills
	function DefaultGame::onClientKilled(%game, %clVictim, %clKiller, %damageType, %implement, %damageLocation)
	{
		parent::onClientKilled(%game, %clVictim, %clKiller, %damageType, %implement, %damageLocation);
		if (!TC_IRC.captureKills || (%clKiller.isAIControlled() && !TC_IRC.captureAIKills))
			return;

		%killerName = %clKiller.namebase;
		%victimName = %clVictim.namebase;
		if (%clKiller.isAIControlled() && TC_IRC.designateBots)
			%killerName = "(B)" SPC %killerName;
		if (%clVictim.isAIControlled() && TC_IRC.designateBots)
			%victimName = "(B)" SPC %victimName;

		if (TC_IRC.determineWeapons)
			%damageText = getTaggedString($DamageTypeText[%damageType]);
		else
			%damageText = "killed";

		if ((%clVictim == %clKiller || %clKiller == 0) && (!TC_IRC.captureSuicides || (%clVictim.isAIControlled() && !TC_IRC.captureAISuicides)))
			return;
		else if (%clVictim == %clKiller || %clKiller == 0)
			return TC_IRC.sendMsg(%victimName SPC "<" @ %damageText @ ">");

		TC_IRC.sendMsg(%killerName SPC "<" @ %damageText @ ">" SPC %victimName);
	}

	function ProjectileData::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
	{
		parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
		if (!TC_IRC.captureMAs)
			return;

		%shooter = %projectile.sourceObject;
		if (!isObject(%shooter) || %shooter.getClassName() !$= "player" || (%shooter.client.isAIControlled() && !TC_IRC.captureAIKills))
			return;

		%projectileDatablock = %projectile.getDatablock();
		%projectileDamage = %projectileDatablock.directDamage;

		%targetObjectHeight = %targetObject.getAltitude();
		if (%targetObject.getClassName() $= "player" && %targetObject.getState() $= "Move" && %targetObjectHeight >= TC_IRC.MAHeightThreshold && %projectileDamage >= TC_IRC.MADamageThreshold)
		{
			if (%shooter.client.team == %targetObject.client.team && !TC_IRC.MADisplayTeamHit)
				return;

			%shooterName = %shooter.client.namebase;
			%victimName = %targetObject.client.namebase;

			if (TC_IRC.determineWeapons)
				%damageTypeName = strCapitalize(getTaggedString($DamageTypeText[%projectileDatablock.directDamageType]));
			else
				%damageTypeName = "";

			if (TC_IRC.MADisplayDistance)
				%maDistance = "at" SPC vectorDistance(%targetObject.getPosition(), %shooter.getPosition()) @ "m";
			else
				%maDistance = "";

			if (TC_IRC.designateBots && %shooter.client.isAIControlled())
				%shooterName = "(B) " @ %shooterName;
			if (TC_IRC.designateBots && %targetObject.client.isAIControlled())
				%victimName = "(B) " @ %victimName;

			TC_IRC.sendMsg(%shooterName SPC "<MA " @ %damageTypeName SPC %maDistance @ ">" SPC %victimName);
		}
	}
};

if( !isActivePackage( TCIRC ) )
	activatePackage( TCIRC );


function TC_IRC::onConnectFailed( %this ) {
	%this.onDisconnect();
}

function TC_IRC::onDNSFailed( %this ) {
	%this.onDisconnect();
}

function TC_IRC::onDisconnect( %this ) {
	//%this.schedule(300000, "connect", %this.server );
	%this.keepAliveBack=1;
	%this.keepAliveDone=1;
	schedule(6000,0,"startIRC");
}

function TC_IRC::onConnected( %this ) {
	%this.retryCount = 0;
	%this.sendCmd( "NICK", %this.nick );
	%this.sendCmd( "USER", %this.user );
}
function TC_IRC::keepAlive( %this ) {
	if (%this.keepAliveBack==0) {
		echo("IRC Failed Version KeepAlive");
		%this.onDisconnect();
		%this.keepAliveDone=1;
	}
	%this.keepAliveBack=0;
	%this.sendCmd("VERSION");
	if (%this.keepAliveDone==0) {
		%this.schedule(20000,"keepAlive",%this);
	}
}
function TC_IRC::onLine( %this, %line ) {
	if ( %this.debug ) echo(%line);
	if ( getsubstr( %line, 0, 1 ) $= ":" ) {
		%line = getsubstr( %line, 1 );
		%line = nextToken( %line, "source", " " );
		%data = nextToken( %line, "command", " " );
	} else
		%data = nextToken( %line, "command", ":" );

	nextToken( %source, "nick", "!" );
	%message = nextToken( %data, "target", ":" );

	switch$( trim( %command ) ) {
		case "PING":
			%this.sendCmd("PONG", ":" @ %data);

		case "PRIVMSG":
			if ( trim( %target ) $= %this.channel )
				%this.onMsg( %nick, %message );

		case "JOIN":
			if ( trim( %target ) $= %this.channel )
				%this.onJoin( %nick );

		case "PART":
			if ( trim( %target ) $= %this.channel )
				%this.onLeave( %nick, %message );

		case "QUIT":
			%this.onLeave( %nick, %message );

		case "433": // nickname in use
			%this.sendCmd( "NICK", %this.nick @ getRandom(9) );
			%this.sendCmd( "USER", %this.user );

		case "004":
			%this.sendCmd( "JOIN", %this.channel );
		case "351":
			%this.keepAliveBack=1;
		case "RPL_VERSION":
			%this.keepAliveBack=1;
	}
}

function TC_IRC::onMsg( %this, %nick, %message ) {
	echo(%messagE);
	// Add us some quick support for chat commands from IRC
	if ( getSubStr( %message, 0, 1 ) $= "!" ) {
		switch$ ( strlwr( getWord(%message,0) ) ) {
			case "!players":
				if ( ClientGroup.getCount() == 0 )
					%this.sendMsg("There are currently no players on the server.");
				else {
					// Hopefully the GVars are kept counting properly for the count to work on ignoring AI's
					if (%this.listBots)
						%message = "Online (" @ ClientGroup.getCount() @ "):";
					else
						%message = "Online (" @ $HostGamePlayerCount-$HostGameBotCount @ "):";

					for ( %i=0; %i < ClientGroup.getCount(); %i++ )
					{
						%client = ClientGroup.getObject(%i);
						if (%client.isAIControlled() && !%this.listBots)
							continue;
						else if (%client.isAIControlled() && %this.designateBots)
						{
							%message = %message SPC "(B) " @ %client.namebase @ ",";
							continue;
						}

						%message = %message SPC %client.namebase @ ",";
					}

					%this.sendMsg( strIRCPrepare(getSubStr(%message, 0, strLen(%message)-1), ",") );
				}
			// This could probably use a bit of improvement but whatever
			case "!help":
				%this.sendMsg("Available commands: !players");
			// Just an example of a command accepting arguments
			case "!info":
				%desiredName = getWords(%message, 1);

				%client = plNameToCid(%desiredName);
				if (trim(%desiredName) $= "" || !isObject(%client))
				{
					%sending = $HostGamePlayerCount SPC " players, " @ $HostGameBotCount @ " are bots. ";
					%sending = %sending @ "Current map: " @ $CurrentMission SPC "(" @ $CurrentMissionType @ ")";
					%this.sendMsg(%sending);
					return;
				}

				else
				{
					%sending = "Full Name: " @ %client.namebase;
					if (!%client.isAIControlled())
						%sending = %sending SPC "(" @ %client.guid @ "), ";
					else
						%sending = %sending SPC "(Bot), ";

					%sending = %sending SPC "Team: " @ %client.team @ ", ";
					%sending = %sending SPC "Kills: " @ %client.kills;
					%this.sendMsg(%sending);
				}
			// Take players out of broadcast
			case "!broadcast":
				if(!%this.allowBroadcastControl)
					return;

				%this.broadCasting[%nick] = !%this.broadCasting[%nick];
				%message = %this.broadCasting[%nick] ? "IRC broadcast Enabled." : "IRC broadcast Disabled.";
				%this.sendCmd("PRIVMSG", %nick SPC ":" @ %message);
		}
	} else {
		if (!%this.broadCasting[%nick] && %this.allowBroadcastControl)
		{
			%message = strlwr(%message);
			for (%i = 0; %i < getWordCount(%message); %i++)
			{
				%client = plNameToCid(getWord(%message, %i));
				if (isObject(%client) && !%client.isAIControlled())
				{
					%this.sendCmd("PRIVMSG", %nick SPC ": The player is ingame and you have your IRC broadcast disabled.");
					return;
				}
			}
			return;
		}

		// If it looks like a duck, lets see if we can make it sound like a duck
		if ( strlen( %message ) < 5 && getSubStr( %message, 0, 1 ) $= "v" ) {
			%keys = strUpr( %message );
			if ( getSubStr( %keys, 0, 3 ) !$= "VGA" ) {
				for ( %i=0; %i < CannedChatItems.getCount(); %i++ ) {
					%chatItem = CannedChatItems.getObject( %i );
					%audioFile = %chatItem.audioFile;
					%text = %chatItem.text;
					%dkeys = %chatItem.defaultKeys;
					if ( %dkeys $= %keys ) {
						%message = "\c0[IRC][" @ %keys @ "]\c4" SPC %nick @ ":" SPC %text @ "~wvoice/Bot1/" @ %audioFile @ ".wav";
						messageAll( 'MsgClient', %message );
						return;
					}
				}
			}
		}

		messageAll( 'MsgClient', "\c0[IRC]\c4" SPC %nick @ ":" SPC %message );
	}
}

function TC_IRC::onJoin( %this, %nick ) {
	%this.broadCasting[%nick] = false;
	messageAll( 'MsgClient', "\c1" @ %nick SPC "has joined" SPC %this.channel );
}

function TC_IRC::onLeave( %this, %nick, %message ) {
	%this.broadCasting[%nick] = true;
	messageAll( 'MsgClient', "\c1" @ %nick SPC "has left" SPC %this.channel SPC "(" @ %message @ ")" );
}

function TC_IRC::sendCmd( %this, %command, %params ) {
	if( %this.debug ) echo( %command SPC %params );

	%this.send( %command SPC %params @ "\r\n" );
}

function TC_IRC::sendMsg ( %this, %message ) {
	if (!strIRCValidateString(%message))
		%message = strIRCPrepare(%message, " ");

	for (%i = 0; %i < getFieldCount(%message); %i++)
	{
		%subSection = getField(%message, %i);

		error(%subSection);
		%this.sendCmd("PRIVMSG", %this.channel SPC ":" @ %subSection);
	}
	return 0;
}

function TC_IRC::relayChat( %this, %client, %message ) {
	if( %this.debug ) echo( %client.namebase @ ":" SPC %message );
	%firstChar = getSubStr( %message, 0, 1 );
	if ( %firstChar $= "/" || %firstChar $= "!" ) return;

	%prefix = %client.isAdmin ? ( %client.isSuperAdmin ? "<@" : "<%" ) : "<";
	%name = %prefix @ %client.nameBase @ ">";

	%wavStart = strstr( %message, "~w" );
	if ( %wavStart != -1 ) {
		%wav = getSubStr( %message, %wavStart + 2 );
		%isWave = ( fileExt( %wav ) $= ".wav" ) ? true : false;
		if ( %isWave || strstr( %wav, "/" ) != -1 )
			%wav = fileBase( %wav );
		%keys = getBindKeys( %wav );
		%name = "["@ %keys @ "]" SPC %name;
		%message = getSubStr( %message, 0, %wavStart );
	}
	%message = stripMLControlChars( %message );

	// Maybe some basic spam prevention
	if ( %client.IRCLine !$= %message )
		%this.sendMsg( %name SPC %message );

	%client.IRCLine = %message;
	if (isEventPending(%client.resetIRC)) cancel(%client.resetIRC);
	%client.resetIRC = %client.schedule( 5000, "resetIRCLine" );
}

function GameConnection::resetIRCLine( %this ) {
	%this.IRCLine = "";
	%this.resetIRC = "";
}

function getBindKeys( %wav ) {
	for ( %i=0; %i < CannedChatItems.getCount(); %i++ ) {
		%chatItem = CannedChatItems.getObject( %i );
		%audioFile = %chatItem.audioFile;
		%keys = %chatItem.defaultKeys;
		if ( %audioFile $= %wav ) {
			if ( getSubStr( %keys, 0, 3 ) !$= "VGA") return %keys;
		}
	}
	return "VPK";
}

// DarkDragonDX: A bunch of lib functions I wrote for this...
function Player::getAltitude(%this)
{
	%controlledObject = %this;
	%mountedObject = %this.getObjectMount();
	if (isObject(%mountedObject))
		%controlledObject = %mountedObject;

	%obj = 0;
	%position = %controlledObject.getPosition();
	%mask = $TypeMasks::VehicleObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType;
	%obj = containerRaycast(%position, vectorAdd(%position, "0 0 -99999"), -1, %controlledObject); // Should hopefully help...

	%x = getWord(%obj, 1);
	%y = getWord(%obj, 2);
	%z = getWord(%obj, 3);
	return vectorDist(%position, %x SPC %y SPC %z);

}

function strCapitalize(%string)
{
	return strUpr(getSubStr(%string, 0, 1)) @ getSubStr(%string, 1, strLen(%string));
}

function strReverse(%string)
{
	%result = "";
	for (%i = strLen(%string); %i > 0; %i--)
		%result = %result @ getSubStr(%string, %i, 1);
	return %result;
}

function subStrPosition(%string, %sub, %occurance)
{
	%StringOccurance = 0;
	%length = strLen(%sub);
	for (%i = 0; %i < strLen(%string); %i++)
	{
		if (getSubStr(%string, %i, %length) $= %sub)
			%StringOccurance++;

		if (%StringOccurance >= %occurance)
			return %i;
	}
	return -1;
}

function subStrCount(%string, %sub)
{
	%result = 0;
	%length = strLen(%sub);
	for (%i = 0; %i < strLen(%string); %i++)
		if (getSubStr(%string, %i, %length) $= %sub)
			%result++;
	return %result;
}

function getSubStrOcc(%string, %sub, %occurance)
{
	%position = subStrPosition(%string, %sub, %occurance);
	if (%position == -1)
		return -1;

	return getSubStr(%string, %position, strLen(%string));
}

function strIRCPrepare(%string, %splitter)
{
	%length = strLen(%string);

	if (%length < $TCIRC::messageSplitThreshold)
		return %string;

	// How many parts should it split into?
	%partCount = mCeil(%length / $TCIRC::messageSplitThreshold);

	%result = "";
	for (%i = 0; %i < %partCount; %i++)
	{
		// Look for the next one above threshold
		%count = subStrcount(%string, %splitter);

		%lastGoodPosition = 0;
		for (%c = 0; %c < %count; %c++)
		{
			%position = subStrPosition(%string, %splitter, %c);

			// Got a new good Position
			if (%position <= $TCIRC::messageSplitThreshold)
				%lastGoodPosition = %position;

			if (%position >= $TCIRC::messageSplitThreshold)
			{
				%result = %result @ getSubStr(%string, 0, %lastGoodPosition) @ "\t";
				%string = getSubStr(%string, %lastGoodPosition + 1, %length);
				break;
			}
		}
	}

	if (%string !$= "")
		%result = %result @ %string @ "\t";

	return %result;
}

function strIRCValidateString(%string)
{
	for (%i = 0; %i < getFieldCount(%string); %i++)
		if (strLen(getField(%string, %i)) > $TCIRC::messageSplitThreshold)
			return false;
	return true;
}

function plNameToCid(%name)
{
	%name = strLwr(%name);
	for (%i = 0; %i < ClientGroup.getCount(); %i++)
	{
		%client = ClientGroup.getObject(%i);
		if (strStr(strlwr(%client.namebase), %name) > -1)
			return %client;
	}
	return 0;
}

function startIRC()
{
    TC_IRC.connect( TC_IRC.server );
    TC_IRC.keepAliveDone=0;
    TC_IRC.keepAliveBack=1;
    TC_IRC.schedule(6000,"keepAlive",TC_IRC);
}

