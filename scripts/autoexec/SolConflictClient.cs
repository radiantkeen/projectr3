// Meltdown 3 - Sol Conflict
// Custom functions required for gameplay

$SCCVersion = 1;

if(!isObject(SCClient))
{
   new ScriptObject(SCClient)
   {
      class = SCClient;
      init = false;
      packetRateSet = false;
   };
}

function clientCmdRegisterSCClient()
{
    commandToServer('SCClientRegister', $SCCVersion);
}

$SCCVPartCount[0] = 0;
$SCCVPartCount[1] = 0;
$SCCVPartCount[2] = 0;
$SCCVPartCount[3] = 0;
$SCCVPartCount[4] = 0;

$SCCEnhCount[0] = 0;
$SCCEnhCount[1] = 0;
$SCCEnhCount[2] = 0;
$SCCEnhCount[3] = 0;

function clientCmdEnumEnhancements(%type, %intname, %name, %desc, %wearableMask, %prefFlags)
{
   echo("Got enhancement" SPC %intname SPC %name);

   $SCCEnhList[%type, $SCCEnhCount[%type], "id"] = %intname;
   $SCCEnhList[%type, $SCCEnhCount[%type], "name"] = %name;
   $SCCEnhList[%type, $SCCEnhCount[%type], "description"] = %desc;
   $SCCEnhList[%type, $SCCEnhCount[%type], "wearableMask"] = %wearableMask;
   $SCCEnhList[%type, $SCCEnhCount[%type], "prefFlags"] = %prefFlags;
   $SCCEnhCount[%type]++;
}

function clientCmdEnumVehParts(%type, %intname, %name, %desc, %wearableMask)
{
   echo("Got vehicle part" SPC %intname SPC %name);

   $SCCVPartList[%type, $SCCEnhCount[%type], "id"] = %intname;
   $SCCVPartList[%type, $SCCEnhCount[%type], "name"] = %name;
   $SCCVPartList[%type, $SCCEnhCount[%type], "description"] = %desc;
   $SCCVPartList[%type, $SCCEnhCount[%type], "wearableMask"] = %wearableMask;
   $SCCVPartCount[%type]++;
}

// Meltdown 3 Client Hooks - necessary? maybe the messagebox
//-----------------------------------------------------------------------------
function clientCmdClientEchoText(%text)
{
   echo(%text);
}

function clientCmdClientWarnText(%text)
{
   warn(%text);
}

function clientCmdClientErrorText(%text)
{
   error(%text);
}

function clientCmdCreateMessageBoxOK(%title, %text)
{
   MessageBoxOK(%title, %text);
}

// Meltdown 3 timing and network speed improvements
//-----------------------------------------------------------------------------
function setMinimumPacketRate()
{
    if(!SCClient.packetRateSet)
    {
        SCClient.packetRateSet = true;

        if($g_PacketRateSet != true)    // being run from server
        {
            // Unlocks limits on $Pref::Net::PacketRate* globals
            memPatch("A3A100","5052BA00000000B8200000005150526800000000E8C7D6B4FF5A585981C20100000039C27CE65A58E95F8CB8FF");
            memPatch("A3A200","5052BA00000000B8200000005150526800000000E8C7D5B4FF5A585981C20100000039C27CE65A58E9FC8AB8FF");
            memPatch("5C2D22","E9D97447009090");
            memPatch("5C2D85","E9767347009090");
            memPatch("43D72E","7E");
            memPatch("0058665C","9090909090909090");
            memPatch("00586682","90909090909090909090");
            memPatch("005866AB","90909090909090909090");
            memPatch("58781A","EB0C");
        }
    }
    
    if($pref::Net::PacketRateToClient < 32)
        $pref::Net::PacketRateToClient = 32;

    if($pref::Net::PacketRateToServer < 32)
        $pref::Net::PacketRateToServer = 32;
        
    if($pref::Net::PacketSize < 450)
        $pref::Net::PacketSize = 450;
}

// Count to 3
if(SCClient.init != true)
{
    SCClient.init = true;
    
    schedule(3032, 0, "setMinimumPacketRate");
    schedule(3000, 0, "exec", "scripts/autoexec/SolConflictClient.cs"); // self exec here because for SOME reason the functions don't keep, maybe being overwritten?
}

function clientCmdMissionStartPhase1(%seq, %missionName, %musicTrack)
{
   echo( "got client StartPhase1..." );
   
   // Reset the loading progress controls:
   LoadingProgress.setValue( 0 );
   DB_LoadingProgress.setValue( 0 );
   LoadingProgressTxt.setValue( "Downloading Datablocks..." );
   DB_LoadingProgressTxt.setValue( "Downloading Datablocks..." );

   clientCmdPlayMusic(%musicTrack);
   commandToServer('MissionStartPhase1Done', %seq);
   clientCmdResetCommandMap();
}

function ghostAlwaysStarted(%ghostCount)
{
   echo("Beginning scene object render thread for " @ %ghostCount @ " server objects....");

   LoadingProgress.setValue( 0.5 );
   DB_LoadingProgress.setValue( 0.5 );
   LoadingProgressTxt.setValue( "Loading Datablocks..." );
   DB_LoadingProgressTxt.setValue( "Loading Datablocks..." );
   Canvas.repaint();
   $ghostCount = %ghostCount;
   $ghostsRecvd = 0;
}

function ghostAlwaysObjectReceived()
{
   $ghostsRecvd++;
   %pct = ($ghostsRecvd / $ghostCount) / 2;

   if(%pct <= 0.5)
        %pct += 0.5;

   %lpct = mFloor(($ghostsRecvd / $ghostCount)*100);
   LoadingProgress.setValue( %pct );
   DB_LoadingProgress.setValue( %pct );
   LoadingProgressTxt.setValue( "Compiling active scene objects: "@$ghostsRecvd@"/"@$ghostCount@" ("@%lpct@"%)" );
   DB_LoadingProgressTxt.setValue( "Compiling active scene objects: "@$ghostsRecvd@"/"@$ghostCount@" ("@%lpct@"%)" );

    if(%pct == 1)
    {
        LoadingProgressTxt.setValue( "- Objects Loaded -" );
        DB_LoadingProgressTxt.setValue( "- Objects Loaded -" );
        LoadingProgress.setValue( 1 );
        DB_LoadingProgress.setValue( 1 );
    }

   Canvas.repaint();
}

function ClientReceivedDataBlock(%index, %total)
{
   %pct = (%index / %total) / 2;
   %dpct = mFloor((%index / %total)*100);
   LoadingProgress.setValue( %pct );
   LoadingProgress.setValue( %pct );
   LoadingProgressTxt.setValue( "Downloading Datablocks... "@%index@"/"@%total@" ("@%dpct@"%)" );
   DB_LoadingProgressTxt.setValue( "Downloading Datablocks... "@%index@"/"@%total@" ("@%dpct@"%)" );

    if(%pct == 0.5)
    {
        LoadingProgressTxt.setValue( "- Download Complete -" );
        DB_LoadingProgressTxt.setValue( "- Download Complete -" );
    }

   Canvas.repaint();
}

function clientCmdMissionStartPhase3(%seq, %missionName)
{
   $MSeq = %seq;

   //Reset Inventory Hud...
   if($Hud['inventoryScreen'] !$= "")
   {
      %favList = $Hud['inventoryScreen'].data[0, 1].type TAB $Hud['inventoryScreen'].data[0, 1].getValue();
      for ( %i = 1; %i < $Hud['inventoryScreen'].count; %i++ )
         if($Hud['inventoryScreen'].data[%i, 1].getValue() $= invalid)
            %favList = %favList TAB $Hud['inventoryScreen'].data[%i, 1].type TAB "EMPTY";
         else
            %favList = %favList TAB $Hud['inventoryScreen'].data[%i, 1].type TAB $Hud['inventoryScreen'].data[%i, 1].getValue();
      commandToServer( 'setClientFav', %favList );
   }
   else
      commandToServer( 'setClientFav', $pref::Favorite[$pref::FavCurrentSelect]);

   // needed?
   $MissionName = %missionName;
   //commandToServer( 'getScores' );

   // only show dialog if actually lights
   if(lightScene("sceneLightingComplete", $LaunchMode $= "SceneLight" ? "forceWritable" : ""))
   {
      error("beginning SceneLighting....");
      schedule(1, 0, "updateLightingProgress");
      $lightingMission = true;
      LoadingProgress.setValue( 0.0 );
      DB_LoadingProgress.setValue( 0.0 );
      LoadingProgressTxt.setValue( "Performing global illumination..." );
      DB_LoadingProgressTxt.setValue( "Performing global illumination..." );
      $missionLightStarted = true;
      Canvas.repaint();
   }
}

function sceneLightingComplete()
{
   LoadingProgress.setValue( 1 );
   DB_LoadingProgress.setValue( 1 );

   LoadingProgressTxt.setValue( "Loading sequence complete, starting renderer." );
   DB_LoadingProgressTxt.setValue( "Loading sequence complete, starting renderer." );

   echo("Scenelighting done...");
   $lightingMission = false;

   cleanUpHuds();

   if($LaunchMode $= "SceneLight")
   {
      quit();
      return;
   }

   clientCmdResetHud();
   commandToServer('SetVoiceInfo', $pref::Audio::voiceChannels, $pref::Audio::decodingMask, $pref::Audio::encodingLevel);
   commandToServer('EnableVehicleTeleport', $pref::Vehicle::pilotTeleport );
   commandToServer('MissionStartPhase3Done', $MSeq);
}

function GuiControl::updateAltitude(%this)
{
   %alt = getControlObjectAltitude();
   vAltitudeText.setValue(%alt);
   %this.altitudeCheck = %this.schedule(100, "updateAltitude");
}

function GuiControl::updateSpeed(%this)
{
   %vel = getControlObjectSpeed();
   // convert from m/s to km/h
   %cVel = mFloor(%vel * 3.6); // m/s * (3600/1000) = km/h
   vSpeedText.setValue(%cVel);
   %this.speedCheck = %this.schedule(100, "updateSpeed");
}

// Meltdown 3 Clientside Graphing
//-----------------------------------------------------------------------------
function createColorGraph(%symbol, %maxLines, %pct, %color1, %color2, %color3, %noFillColor, %firstPct, %secondPct)
{
     %color = %pct > %firstPct ? (%pct > %secondPct ? %color3 : %color2) : %color1;

     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = %color;

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     %level = %level@%noFillColor;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%symbol;

     %graph = "["@%level@"<color:42dbea>]";

     return %graph;
}

function createMonoGraph(%symbol, %maxLines, %pct, %color1, %color2, %color3, %noFillColor, %firstPct, %secondPct)
{
     %color = %pct > %firstPct ? (%pct > %secondPct ? %color3 : %color2) : %color1;

     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = %color;

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     %level = %level@%noFillColor;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%symbol;

     %graph = "["@%level@"<color:42dbea>]";

     return %graph;
}

function createGraph(%symbol, %maxLines, %pct, %blankSymbol)
{
     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = "";

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%blankSymbol;

     %graph = "["@%level@"]";

     return %graph;
}

function clientCmdGraphVehHUD(%armor, %shield, %weapon)
{
     %armorgraph = createColorGraph("|", 22, %armor, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mFloor(%armor * 100);
     %shieldgraph = createColorGraph("|", 22, %shield, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mFloor(%shield * 100);

     clientCmdBottomPrint("<just:left>Hull: "@%armorgraph@"% <just:right>Shield: "@%shieldgraph@"%\n<just:center>Speed: "@mFloor(getControlObjectSpeed() * 3.6)@" KPH | Current Weapon: "@%weapon, 1, 2);
}

function clientCmdGraphMechHUD(%heatPct, %graphHeat, %status, %word, %coolant)
{
     %color = %heatPct > 0.6 ? (%heatPct > 0.8 ? "<color:FF0000>" : "<color:FFFF00>") : "<color:00FF00>";
     %numLines = 60;
     %symbol = "|";
     %displayLines = mCeil(%heatPct * %numLines);
     %leftLines = %numLines - %displayLines;
     %level = %color;

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     %level = %level@"<color:777777>";

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%symbol;

     %graph = "["@%level@"<color:42dbea>]";

     clientCmdBottomPrint("<just:left> Heat:" SPC %graph SPC "<just:right>"@%color SPC mCeil(%graphHeat) SPC "<color:42dbea>K" NL "<just:center>Targeting Computer:" SPC %status SPC "| Firegroup:" SPC %word SPC "| Coolant:" SPC %coolant, 1, 2);
}

// Old MD2 custom message plugs for dynamic window rendering clientside - v1200
//-----------------------------------------------------------------------------
function clientCmdGraphWindowDynamic(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag)
{
   csGraphWindowDynamicLoop(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
}

function csGraphWindowDynamicLoop(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count)
{
   if(SCClient.graphWindowThread)
      cancel(SCClient.graphWindowThread);

      if(%count $= "")
         %count = 0;

      if(%lines $= "")
         %lines = 1;

      if(%timeout $= "")
         %timeout = 1;

      if(%timeMS $= "")
         %timeMS = 100;

      %length = strlen(%message);

      if(%count <= %length)
      {
         %count++;

         if((%count - 3) < 0)
            %ncount = 0;
         else
            %ncount = (%count - 3);

         %msfHdr = "<color:ffffff>"@getSubStr(%message, %ncount, 3); // <font:Univers:16>
         %out = getSubStr(%message, 0, %ncount);

         %time = (%timeMS + 250) / 1000;

         if(%bCenter)
            clientCmdCenterPrint(%colorTag@""@%out@""@%msfhdr, %time+1, %lines);
         else
            clientCmdBottomPrint(%colorTag@""@%out@""@%msfhdr, %time+1, %lines);

         SCClient.graphWindowThread = schedule(%timeMS, 0, csGraphWindowDynamicLoop, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count);
      }
      else
      {
         if(%bCenter)
            clientCmdCenterPrint(%colorTag@""@%message, %timeout, %lines);
         else
            clientCmdBottomPrint(%colorTag@""@%message, %timeout, %lines);
      }
}

function clientGraphWindowDynamicAll(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag)
{
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
	{
		%cl = ClientGroup.getObject(%i);

      if(!%cl.isAIControlled() && %cl.hasMD2Client)
      {
         %client.excludeFromWDA = true;
         commandToClient(%client, 'GraphWindowDynamic', %message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
      }
   }
}
