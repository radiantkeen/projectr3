//--------------------------------------
// Blaster Rifle
//--------------------------------------
datablock AudioProfile(BlasterRifleMode)
{
   filename    = "fx/weapons/plasma_dryfire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock TracerProjectileData(BlasterRifleBolt)
{
   doDynamicClientHits = true;

   projectileShapeName = "energy_bolt.dts";
   directDamage        = 0.15;
   kickBackStrength    = 0;
   directDamageType    = $DamageType::BlasterRifle;
   explosion           = "ExtendedAAExplosion";
   splash              = ChaingunSplash;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::BlasterRifle;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 25;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;
   
   dryVelocity       = 450.0;
   wetVelocity       = 337.5;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 700;
   lifetimeMS        = 750;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 22;
   tracerAlpha     = true;
   tracerMinPixels = 3;
   tracerColor     = "0.1 0.1 1.0 0.75";
	tracerTex[0]  	 = "special/blueSpark";//"special/tracer00";
	tracerTex[1]  	 = "small_circle";//"special/tracercross";
   tracerWidth     = 0.8;
   crossSize       = 1.9;
   crossViewAng    = 0.7;
   renderCross     = true;
   emap = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(BlasterRifleImage) : TriggerProxyImage
{
   shapeFile = "weapon_grenade_launcher.dts";
   offset = "0 0 0";
//   rotation = "1 0 0 180";

   item = BlasterRifle;
   emap = true;
   usesEnergy = true;
   fireEnergy = 0;
   minEnergy = 6;

   enhancementSlots = 2;

   weaponDescName = "Blaster Rifle";
   defaultModeName = "Barrage";
   defaultModeDescription = "Powerful energy weapon that draws from the armor's power core";
   defaultModeSwitchSound = "BlasterRifleMode";
   defaultModeFireSound = "";
   defaultModeFailSound = "PlasmaDryFireSound";
   
   subImage1 = BlasterGunImage;
   subImage2 = BlasterMuzzleImage;
};

datablock ItemData(BlasterRifle)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = BlasterRifleImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a Blaster Rifle";
};

datablock ShapeBaseImageData(BlasterMuzzleImage)
{
   shapeFile = "weapon_energy.dts";
   offset = "0 0.75 0.325";
   rotation = "0 1 0 180";
   emap = true;

   item = BlasterRifle;
   subImage = true;
   usesEnergy = true;
   fireEnergy = 7;
   minEnergy = 7;
   projectileSpread = 4;
   
   projectile = BlasterRifleBolt;
   projectileType = TracerProjectile;
   
   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateSound[0] = "BlasterSwitchSound";
   stateTimeoutValue[0] = 0.01;
   stateSequence[0] = "Activate";

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 0.1;
   stateFire[3] = true;
//   stateSound[3] = "BomberTurretFireSound";
   stateScript[3]  = "onFire";
   stateSequence[3] = "Fire";
//   stateShockwave[3] = true;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.025;
   stateEjectShell[4] = true;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = GrenadeDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";   
};

datablock ShapeBaseImageData(BlasterGunImage) : BlasterMuzzleImage
{
   subImage = true;
   shapeFile = "weapon_sniper.dts";
   offset = "0 0.55 0.075";
   rotation = "0 1 0 0";
   emap = true;
   
   subImage = true;
   
   stateSound[0] = "";
   stateSound[3] = "";
   stateShockwave[3] = false;
   stateEjectShell[4] = false;
};

function BlasterRifleImage::onMount(%this,%obj,%slot)
{
    Parent::onMount(%this,%obj,%slot);

    %obj.play3D(BlasterSwitchSound);
    %obj.setImageAmmo($WeaponSubImage1, true);
    %obj.setImageAmmo($WeaponSubImage2, true);
}

function BlasterGunImage::onFire(%data,%obj,%slot)
{
    %mode = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]];
    %time = getSimTime();

    if(%obj.rateOfFire[%data.item] $= "")
        %obj.rateOfFire[%data.item] = 1.0;

    if(isObject(%mode))
    {
        if(%obj.fireTimeout[%data.item, %mode] > %time)
            return;
    }
    else
    {
        if(%obj.fireTimeout[%data, %data.item] > %time)
            return;
    }

    %obj.setImageTrigger($WeaponSubImage2, true);
    %obj.schedule(32, "setImageTrigger", $WeaponSubImage2, false);

    if(!isObject(%mode))
        %obj.fireTimeout[%data, %data.item] = %time + (125 / (1 + %obj.rateOfFire[%data.item]));
}

function BlasterMuzzleImage::onFire(%data,%obj,%slot)
{
    %p = Parent::onFire(%data,%obj,%slot);
    
    if(isObject(%p))
        %obj.play3d("BomberTurretFireSound");
}
