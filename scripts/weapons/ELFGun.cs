//--------------------------------------------------------------------------
// ELF Gun
//--------------------------------------------------------------------------
datablock AudioProfile(ELFGunSwitchSound)
{
   filename    = "fx/weapons/generic_switch.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ELFGunFireSound)
{
   filename    = "fx/weapons/ELF_fire.wav";
   description = CloseLooping3d;
   preload = true;
};

datablock AudioProfile(ElfFireWetSound)
{
   filename    = "fx/weapons/ELF_underwater.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ELFHitTargetSound)
{
   filename    = "fx/weapons/ELF_hit.wav";
   description = CloseLooping3d;
   preload = true;
};

//--------------------------------------
// Sparks
//--------------------------------------
datablock ParticleData(ELFSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 200;
   lifetimeVarianceMS   = 0;
   textureName          = "special/blueSpark";
   colors[0]     = "0.8 0.8 1.0 1.0";
   colors[1]     = "0.8 0.8 1.0 1.0";
   colors[2]     = "0.8 0.8 1.0 0.0";
   sizes[0]      = 0.35;
   sizes[1]      = 0.15;
   sizes[2]      = 0.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(ELFSparksEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 4;
   velocityVariance = 2;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
//   lifetimeMS       = 100;
   particles = "ELFSparks";
};


//--------------------------------------
// Projectile                    
//--------------------------------------
datablock ELFProjectileData(BasicELF)
{
   beamRange         = 38; // z0dd - ZOD, 5/18/02. WHAT?? INCREASE ELF RANGE?!!? was 37
   numControlPoints  = 8;
   restorativeFactor = 3.75;
   dragFactor        = 4.5;
   endFactor         = 2.25;
   randForceFactor   = 2;
   randForceTime     = 0.125;
	drainEnergy			= 0.653;
	drainHealth			= 0.016;
   directDamageType  = $DamageType::ELF;
	mainBeamWidth     = 0.1;           // width of blue wave beam
	mainBeamSpeed     = 9.0;            // speed that the beam travels forward
	mainBeamRepeat    = 0.25;           // number of times the texture repeats
   lightningWidth    = 0.1;
   lightningDist      = 0.15;           // distance of lightning from main beam
   beamType          = 0;
   
   fireSound    = ElfGunFireSound;
   wetFireSound = ElfFireWetSound;

	textures[0] = "special/ELFBeam";
   textures[1] = "special/ELFLightning";
   textures[2] = "special/BlueImpact";

   emitter = ELFSparksEmitter;
};

//--------------------------------------
// Rifle and item...
//--------------------------------------
datablock ItemData(ELFGun)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_elf.dts";
   image        = ELFGunImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
	pickUpName = "an Arc Welder";

   computeCRC = true;
   emap = true;
};

datablock ShapeBaseImageData(ArcWelderAImage)
{
   shapeFile = "weapon_elf.dts";
   offset = "-0.022 0 -0.03";
   rotation = "0 1 0 0";
   emap = true;
   
   subImage = true;
};

datablock ShapeBaseImageData(ArcWelderBImage) : ArcWelderAImage
{
   offset = "0.0225 0 0.17";
   rotation = "0 1 0 180";
};

datablock ShapeBaseImageData(ELFGunImage)
{
   className = WeaponImage;

   shapeFile = "weapon_shocklance.dts";
   item = ELFGun;
   offset = "0 0.4 -0.05";

   projectile = BasicShocker;
   projectileType = ShocklanceProjectile;
   deleteLastProjectile = true;
   emap = true;

   enhancementSlots = 2;

   weaponDescName = "Arc Welder";
   defaultModeName = "Lightning Arc";
   defaultModeDescription = "300% damage from behind, reach out and touch someone!";
   defaultModeSwitchSound = "ElfFireWetSound";
   defaultModeFireSound = "";
   defaultModeFailSound = "ElfFireWetSound";

   subImage1 = ArcWelderAImage;
   subImage2 = ArcWelderBImage;

	usesEnergy = true;
 	minEnergy = 3;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Activate";
	stateSound[0]                    = ELFGunSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "CheckWet";
//   stateSequence[2]                 = "Activate";
   
   stateName[3]                     = "Fire";
//	stateEnergyDrain[3]              = 5;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onFire";
   stateTransitionOnTriggerUp[3]    = "Deconstruction";
   stateTransitionOnNoAmmo[3]       = "Deconstruction";
   //stateSound[3]                    = ElfFireWetSound;

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Deconstruction";
   stateScript[5]                   = "deconstruct";
   stateTransitionOnTimeout[5]      = "Ready";
   stateTransitionOnNoAmmo[6]       = "NoAmmo";
   
   stateName[6]                     = "DryFire";
   stateSound[6]                    = ElfFireWetSound;
   stateTimeoutValue[6]             = 0.5;
   stateTransitionOnTimeout[6]      = "Ready";
   
   stateName[7]                     = "CheckWet";
   stateTransitionOnWet[7]          = "DryFire";
   stateTransitionOnNotWet[7]       = "Fire";
};

function ELFGunImage::spawnProjectile(%data, %obj, %slot, %mode) // bug: ELF not changing modes, just using this spawnProjectile code, might have to do mode redirect?
{
    if(%mode !$= "")
    {
        if(%data.deleteLastProjectile)
        {
            if(isObject(%obj.lastProjectile))
                %obj.lastProjectile.delete();

            %obj.deleteLastProjectile = %data.deleteLastProjectile;
        }
    
        return %mode.spawnProjectile(%data, %obj, %slot, %vector, %obj.getMuzzlePoint(%slot));
    }

//    %obj.play3D(ShockLanceDryFireSound);
    %time = getSimTime();

    if(%obj.fireTimeout[%data, %mode] !$= "")
    {
        if(%obj.fireTimeout[%data, %mode] > %time)
        {
            %obj.playAudio(0, ShockLanceDryFireSound);
            return;
        }
    }

    %p = 0;
    %proj = "BasicShocker"; // not set at compile time, but available at runtime, so we do a trick...
    %pos = %obj.getMuzzlePoint(0);
    %vector = %obj.getMuzzleVector(0);
    
    %dmgMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;
    %everythingElseMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticObjectType | $TypeMasks::MoveableObjectType | $TypeMasks::DamagableItemObjectType;
    %mask = %dmgMasks | %everythingElseMask;
    %cast = castRay(%pos, %vector, 10, %mask, %obj);

    if(%cast)
    {
        if(%cast.hitObj.getType() & %dmgMasks)
        {
            %p = createLance(%proj, %vector, %pos, %obj, %slot, %obj, %cast.hitObj);
            %cast.hitObj.applyImpulse(%cast.hitPos, vectorScale(%vector, %proj.impulse));
            %obj.playAudio(0, ShockLanceHitSound);

            %damageMultiplier = 1.0;

            if(%cast.hitObj.isPlayer())
            {
                // Now we see if we hit from behind...
                %forwardVec = %cast.hitObj.getForwardVector();
                %objDir2D   = getWord(%forwardVec, 0) @ " " @ getWord(%forwardVec,1) @ " " @ "0.0";
                %objPos     = %cast.hitPos;
                %dif        = VectorSub(%objPos, %pos);
                %dif        = getWord(%dif, 0) @ " " @ getWord(%dif, 1) @ " 0";
                %dif        = VectorNormalize(%dif);
                %dot        = VectorDot(%dif, %objDir2D);

                // 120 Deg angle test...
                // 1.05 == 60 degrees in radians
                if (%dot >= mCos(1.05))
                {
                   // Rear hit
                   %damageMultiplier = 3.0;
                }
            }

            %cast.hitObj.lastHitFlags = %proj.flags;
            %cast.hitObj.damage(%obj, %cast.hitPos, %damageMultiplier * %proj.mdDamageAmount[0] * %p.damageBuffFactor, %proj.mdDeathMessageSet, %p, %proj.mdDamageType[0]);
        }
    }
    else
    {
        %p = createLance(%proj, %vector, %pos, %obj, %slot, %obj, 0);
        %obj.playAudio(0, ShockLanceMissSound);
        %obj.useEnergy(-27);
    }

    %p.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];
    
    %obj.lastProjectile = 0;
    %obj.fireTimeout[%data, %mode] = %time + 2000;
    %obj.schedule(2000, "playAudio", 0, "ShockLanceReloadSound");
    
    if(%data.deleteLastProjectile)
    {
        if(isObject(%obj.lastProjectile))
            %obj.lastProjectile.delete();

        %obj.deleteLastProjectile = %data.deleteLastProjectile;
    }
    
    %obj.setImageTrigger(%slot, false);

    return %p;
}
