//--------------------------------------------------------------------------
// Particle Beam Cannon
// 
// 
//--------------------------------------------------------------------------
datablock AudioProfile(PBCSwitchSound)
{
   filename    = "fx/misc/diagnostic_on.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PBCFireSound)
{
   filename    = "fx/misc/lightning_impact.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PBCExpSound)
{
   filename    = "fx/powered/turret_plasma_explode.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PBCShockwaveSound)
{
   filename    = "fx/misc/cannonshot.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------

datablock ShockwaveData(PBWShockwave)
{
   width = 6;
   numSegments = 32;
   numVertSegments = 7;
   velocity = 40;
   acceleration = 35.0;
   lifetimeMS = 999;
   height = 0.5;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 1.5;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.6 0.6 1.0 1.0";
   colors[1] = "0.6 0.3 1.0 0.5";
   colors[2] = "0.0 0.0 1.0 0.0";
};

datablock ExplosionData(PBWExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = PBCExpSound;
   shockwave      = PBWShockwave;
   faceViewer     = true;
   playSpeed      = 0.3;

   sizes[0] = "2 2 2";
   sizes[1] = "2 2 2";
   times[0]      = 0.0;
   times[1]      = 1.0;

   shakeCamera = true;
   camShakeFreq = "10.0 6.0 9.0";
   camShakeAmp = "15.0 15.0 15.0";
   camShakeDuration = 1.5;
   camShakeRadius = 10.0;
};

//--------------------------------------
// Projectile
//--------------------------------------
datablock SniperProjectileData(ParticleBeam)
{
   directDamage        = 1.0;
   velInheritFactor    = 1.0;
   faceViewer          = true;
//   sound 			   = PBWProjectileSound;
   explosion           = "PBWExplosion";
   splash              = PlasmaSplash;
   directDamageType    = $DamageType::PBC;
   kickBackStrength    = 2250;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::PBC;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 33;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 66;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   maxRifleRange       = 300;
   rifleHeadMultiplier = 1.25;
   beamColor           = "0.7 0.7 1";
   fadeTime            = 1.0;

   hasFalloff = false;
   optimalRange = 150;
   falloffRange = 300;
   falloffDamagePct = 0.25;

   startBeamWidth		  = 0.5;
   endBeamWidth 	     = 0.025;
   pulseBeamWidth 	  = 0.75;
   beamFlareAngle 	  = 45.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 15.0;
   pulseLength         = 0.15;

   lightRadius         = 10.0;
   lightColor          = "0.7 0.7 1";

   textureName[0]   = "special/flare";
   textureName[1]   = "special/nonlingradient";
   textureName[2]   = "special/ELFBeam";
   textureName[3]   = "special/ELFLightning";
   textureName[4]   = "special/skyLightning";
   textureName[5]   = "special/ELFBeam";
   textureName[6]   = "special/ELFLightning";
   textureName[7]   = "special/skyLightning";
   textureName[8]   = "special/ELFBeam";
   textureName[9]   = "special/ELFLightning";
   textureName[10]   = "special/skyLightning";
   textureName[11]   = "special/flare";
};

datablock SniperProjectileData(ParticleBeamV)
{
   directDamage        = 1.0;
   velInheritFactor    = 1.0;
   faceViewer          = true;
//   sound 			   = PBWProjectileSound;
   explosion           = "PBWExplosion";
   splash              = PlasmaSplash;
   directDamageType    = $DamageType::PBC;
   kickBackStrength    = 2250;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::PBC;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 150;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 300;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   maxRifleRange       = 600;
   rifleHeadMultiplier = 1.25;
   beamColor           = "0.7 0.7 1";
   fadeTime            = 1.0;

   hasFalloff = false;
   optimalRange = 450;
   falloffRange = 600;
   falloffDamagePct = 0.25;

   startBeamWidth		  = 0.5;
   endBeamWidth 	     = 0.025;
   pulseBeamWidth 	  = 0.75;
   beamFlareAngle 	  = 45.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 15.0;
   pulseLength         = 0.15;

   lightRadius         = 10.0;
   lightColor          = "0.7 0.7 1";

   textureName[0]   = "special/flare";
   textureName[1]   = "special/nonlingradient";
   textureName[2]   = "special/ELFBeam";
   textureName[3]   = "special/ELFLightning";
   textureName[4]   = "special/skyLightning";
   textureName[5]   = "special/ELFBeam";
   textureName[6]   = "special/ELFLightning";
   textureName[7]   = "special/skyLightning";
   textureName[8]   = "special/ELFBeam";
   textureName[9]   = "special/ELFLightning";
   textureName[10]   = "special/skyLightning";
   textureName[11]   = "special/flare";
};

datablock ShockwaveData(PBWFireShockwave)
{
   width = 6;
   numSegments = 24;
   numVertSegments = 7;
   velocity = 1;
   acceleration = -90.0;
   lifetimeMS = 300;
   height = 0.5;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 1.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.3 1.0 1.0";
   colors[1] = "0.5 0.8 0.5 0.5";
   colors[2] = "0.0 1.0 0.0 0.0";
};

datablock ExplosionData(PBCDisplayExplosion)
{
   soundProfile   = PBCShockwaveSound;
   shockwave = PBWFireShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(PBCDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 1250;

   explosion           = "PBCDisplayExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

//--------------------------------------
// Ammo
//--------------------------------------
datablock ItemData(PBCAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some PBC particle canisters";
};

//--------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(ParticleBeamCannon)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_sniper.dts";
   image        = ParticleBeamCannonImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
	pickUpName = "a Particle Beam Cannon";

   computeCRC = true;
};

//function ParticleBeamCannon::hasAmmo(%this, %weapon)
//{
//    return %this.getInventory(%weapon.image.ammo);
//}

datablock ShapeBaseImageData(PBCDecalAImage)
{
   shapeFile = "ammo_plasma.dts";
   offset = "0.35 1.05 -0.2";
   rotation = "0 1 0 60";
   emap = true;
   
   subImage = true;
};

datablock ShapeBaseImageData(PBCDecalBImage)
{
   shapeFile = "ammo_plasma.dts";
   offset = "-0.25 1.05 -0.2";
   rotation = "0 1 0 -60";
   emap = true;
   
   subImage = true;
};

datablock ShapeBaseImageData(PBCDecalCImage)
{
   shapeFile = "weapon_missile.dts";
   offset = "0.1 1.6 -0.25";
   rotation = "0 1 0 0";
   emap = true;
   
   subImage = true;
};

datablock ShapeBaseImageData(ParticleBeamCannonImage)
{
	className = WeaponImage;
   shapeFile = "turret_tank_barrelchain.dts";
   item = ParticleBeamCannon;
   ammo = PBCAmmo;
   projectile = ParticleBeam;
   projectileType = SniperProjectile;

   offset = "0 0.1 0";
   enhancementSlots = 2;

   weaponDescName = "Particle Beam Cannon";
   defaultModeName = "Tachyon Beam";
   defaultModeDescription = "High damage beam of charged particles, full effect when held on target";
   defaultModeSwitchSound = "DiscDryFireSound";
   defaultModeFireSound = "PBCFireSound";
   defaultModeFailSound = "MissileDryFireSound";

   subImage1 = PBCDecalAImage;
   subImage2 = PBCDecalBImage;
   subImage3 = PBCDecalCImage;

   usesEnergy = true;
   fireEnergy = 150;
   minEnergy = 125;

   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "ActivateReady";
   stateSound[0]                    = PBCSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnLoaded[1]       = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "CheckWet";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.05;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";

   stateName[4]                     = "Reload";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.05;
   stateAllowImageChange[4]         = false;

   stateName[5]                     = "CheckWet";
   stateTransitionOnWet[5]          = "DryFire";
   stateTransitionOnNotWet[5]       = "Fire";
   
   stateName[6]                     = "NoAmmo";
   stateTransitionOnAmmo[6]         = "Reload";
   stateTransitionOnTriggerDown[6]  = "DryFire";
   stateSequence[6]                 = "NoAmmo";
   
   stateName[7]                     = "DryFire";
   stateSound[7]                    = MissileDryFireSound;
   stateTimeoutValue[7]             = 1.0;
   stateTransitionOnTimeout[7]      = "Ready";
};

function ParticleBeamCannonImage::validateFireMode(%data, %obj)
{
    %ammoUse = 1;
    %gyanUse = 0;
    %energyUse = 150;

    if(%obj.getEnergyLevel() < %energyUse)
        return "f";
        
    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energyUse SPC %ammoUse SPC %gyanUse;
}

function ParticleBeamCannonImage::spawnProjectile(%data, %obj, %slot, %mode)
{
     %vector = %obj.getMuzzleVector(%slot);

     if(%mode !$= "")
          %proj = %mode.spawnProjectile(%data, %obj, %slot, %vector, %obj.getMuzzlePoint(%slot));
     else
     {
//        if(%obj.laserFocus) // reserved for PBC Beam Focuser (+falloff +range? maybe)
//            %proj = createProjectile(%data.projectileType, %data.projectile@"Extended", %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
          %proj = createProjectile("LinearFlareProjectile", "PBCDisplayCharge", %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
//         createRemoteProjectile("LinearFlareProjectile", "PBCDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);
          %data.schedule(300, "fragmentBeam", %obj, %slot);
          %data.schedule(400, "fragmentBeam", %obj, %slot);
          %data.schedule(500, "fragmentBeam", %obj, %slot);
     }

    %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];

    return %proj;
}

function ParticleBeamCannonImage::fragmentBeam(%data, %obj, %slot)
{
     %proj = createProjectile(%data.projectileType, %data.projectile, %obj.getMuzzleVector(%slot), %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
     %proj.setEnergyPercentage(0.8);
     %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];
}

function ParticleBeamCannonImage::onFire(%data,%obj,%slot)
{
    %mode = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]];
    %time = getSimTime();

    if(%obj.rateOfFire[%data.item] $= "")
        %obj.rateOfFire[%data.item] = 1.0;
    
    if(isObject(%mode))
    {
        if(%obj.fireTimeout[%data, %mode] > %time)
            return;
    }
    else
    {
        if(%obj.fireTimeout[%data, %data.item] > %time)
            return;
    }
    
    %p = Parent::onFire(%data,%obj,%slot);

    if(!isObject(%mode))
    {
        if(isObject(%p))
            %obj.play3D(%data.defaultModeFireSound);
        
        %obj.fireTimeout[%data, %data.item] = %time + (3000 / (1 + %obj.rateOfFire[%data.item]));
    }
}
