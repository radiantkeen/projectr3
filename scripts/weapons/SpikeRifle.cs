//--------------------------------------
// Spike Rifle
//--------------------------------------
datablock AudioProfile(DUPHeadCollision)
{
   filename    = "fx/vehicles/crash_ground_vehicle.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock ParticleData(SpikeSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.3;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 150;
   textureName          = "special/bigspark";
   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.75;
   sizes[2]      = 1.0;
   times[0]      = 0.25;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(SpikeSparksEmitter)
{
   ejectionPeriodMS = 2;
   periodVarianceMS = 1;
   ejectionVelocity = 10;
   velocityVariance = 4.375;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "SpikeSparks";
};

datablock ExplosionData(SpikeCoreExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 150;

   offset = 0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(SpikeExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = DUPHeadCollision;

   emitter[0] = SpikeSparksEmitter;
   subExplosion[0] = SpikeCoreExplosion;

   shakeCamera = true;
   camShakeFreq = "5.0 5.0 5.0";
   camShakeAmp = "5.0 5.0 5.0";
   camShakeDuration = 0.35;
   camShakeRadius = 4.0;

   faceViewer           = true;
};

datablock DecalData(SpikeHitDecal)
{
   sizeX       = 0.5;
   sizeY       = 0.5;
   textureName = "special/bullethole1";
};

datablock EnergyProjectileData(SpikeSingleShot)
{
   emitterDelay        = -1;
   directDamage        = 0.5;
   directDamageType    = $DamageType::Spike;
   kickBackStrength    = 0.0;
   bubbleEmitTime      = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Spike;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 75;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   sound = BlasterProjectileSound;
   velInheritFactor    = 1.0;

   explosion           = "SpikeExplosion";
   splash              = BlasterSplash;

   grenadeElasticity = 0.998;
   grenadeFriction   = 0.0;
   armingDelayMS     = 0;

   drag = 0.05;

   gravityMod        = 0.0;

   muzzleVelocity    = 425.0;
   dryVelocity       = 425.0;
   wetVelocity       = 225.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 800;
   lifetimeMS        = 900;

   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 900;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = 230.0/255.0 @ " " @ 230.0/255.0 @ " " @ "0.0";

   scale = "0.225 35.0 1.0";
   crossViewAng = 0.99;
   crossSize = 0.7;

   blurLifetime   = 0.2;
   blurWidth      = 0.3;
   blurColor = 230.0/255.0 @ " " @ 230.0/255.0 @ " " @ "0.0 1.0";

   texture[0] = "special/tracer00";
   texture[1] = "special/landSpikeBoltCross";
   
   decalData[0] = SpikeHitDecal;
};

datablock DebrisData(SpikeRifleShells)
{
   shapeName = "grenade_projectile.dts";

   lifetime = 7.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.75;
   friction = 0.1;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

datablock ShockwaveData(SpikeMuzzleFlash)
{
   width = 0.45;
   numSegments = 13;
   numVertSegments = 3;
   velocity = 3.0;
   acceleration = -1.0;
   lifetimeMS = 300;
   height = 0.1;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = false;
   orientToNormal = true;
   renderSquare = true;

   texture[0] = "special/blasterHit";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.8 0.5 1.0";
   colors[1] = "1.0 0.8 0.5 1.0";
   colors[2] = "1.0 0.8 0.5 0.0";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(SpikeAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a spike clip";
   emap = true;
};

datablock ShapeBaseImageData(SpikeRifleImage) : TriggerProxyImage
{
   shapeFile = "weapon_mortar.dts";
   offset = "0.2 0.7 0.125";
   rotation = "1 0 0 180";

   item = SpikeRifle;
   ammo = SpikeAmmo;
   emap = true;

   enhancementSlots = 2;

   weaponDescName = "Compressed Spike Rifle";
   defaultModeName = "Single Shot";
   defaultModeDescription = "High precision Gauss-based battle rifle";
   defaultModeSwitchSound = "CameraGrenadeAttachSound";
   defaultModeFireSound = "OBLFireSound";
   defaultModeFailSound = "GrenadeDryFireSound";
   
   subImage1 = SpikeGunImage;
   subImage2 = SpikeMuzzleImage;
};

datablock ItemData(SpikeRifle)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = SpikeRifleImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a Compressed Spike Rifle";
};

datablock ShapeBaseImageData(SpikeMuzzleImage)
{
   shapeFile = "weapon_plasma.dts";
   offset = "0.3 0.4 0.025";
   rotation = "1 0 0 0";
   emap = true;

   item = SpikeRifle;
   ammo = SpikeAmmo;

   subImage = true;
//   muzzleFlash = SpikeMuzzleFlash;
   
   projectile = SpikeSingleShot;
   projectileType = EnergyProjectile;
   
   casing              = SpikeRifleShells;
   shellExitDir        = "0.0 0.0 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 4.0;
   shellVelocity       = 12.0;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateSound[0] = "GrenadeSwitchSound";
   stateTimeoutValue[0] = 0.01;
   stateSequence[0] = "Activate";

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 0.125;
   stateFire[3] = true;
   stateSound[3] = "OBLFireSound";
   stateScript[3]  = "onFire";
   stateSequence[3] = "Fire";
//   stateShockwave[3] = true;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.025;
   stateEjectShell[4] = true;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = GrenadeDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";   
};

datablock ShapeBaseImageData(SpikeGunImage) : SpikeMuzzleImage
{
   subImage = true;
   shapeFile = "weapon_sniper.dts";
   offset = "0.3 0.7 0.1";
   rotation = "0 1 0 180";
   emap = true;
   
   subImage = true;
   
   stateSound[0] = "";
   stateSound[3] = "";
   stateShockwave[3] = false;
   stateEjectShell[4] = false;
};

function SpikeRifleImage::onMount(%this,%obj,%slot)
{
    Parent::onMount(%this,%obj,%slot);
    %obj.play3D(GrenadeSwitchSound);
    
    if(%obj.getInventory(%this.ammo) > 0)
    {
        %obj.setImageAmmo($WeaponSubImage1, true);
        %obj.setImageAmmo($WeaponSubImage2, true);
    }
}

function SpikeGunImage::onFire(%data,%obj,%slot)
{
    %mode = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]];
    %time = getSimTime();

    if(%obj.rateOfFire[%data.item] $= "")
        %obj.rateOfFire[%data.item] = 1.0;

    if(isObject(%mode))
    {
        if(%obj.fireTimeout[%data.item, %mode] > %time)
            return;
    }
    else
    {
        if(%obj.fireTimeout[%data, %data.item] > %time)
            return;
    }

    %obj.setImageTrigger($WeaponSubImage2, true);
    %obj.schedule(32, "setImageTrigger", $WeaponSubImage2, false);

    if(!isObject(%mode))
    {
        %obj.schedule(800, "play3D", GrenadeDryFireSound);
        %obj.fireTimeout[%data, %data.item] = %time + (1000 / (1 + %obj.rateOfFire[%data.item]));
    }
}

function SpikeMuzzleImage::onFire(%data,%obj,%slot)
{
    %p = Parent::onFire(%data,%obj,%slot);
    
    if(isObject(%p))
        %obj.play3d("OBLFireSound");
}
