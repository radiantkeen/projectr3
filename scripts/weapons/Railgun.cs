datablock ParticleData(HowitzerSmokeParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.02;
   inheritedVelFactor   = 0.1;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 100;

   textureName          = "special/flare";

   useInvAlpha = false;
   spinRandomMin = 0.0;
   spinRandomMax = 0.0;

   colors[0]     = "0.0 0.75 1.0 0.0";
   colors[1]     = "0.2 0.5 0.5 1.0";
   colors[2]     = "0.3 0.3 0.3 0.0";
   sizes[0]      = 1.3;
   sizes[1]      = 2.6;
   sizes[2]      = 3.5;
   times[0]      = 0.0;
   times[1]      = 0.1;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(HowitzerSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 1.5;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "HowitzerSmokeParticle";
};

datablock LinearProjectileData(TungstenRail)
{
   scale = "10.0 20.0 10.0";
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 2.5;
   hasDamageRadius     = false;
   directDamageType    = $DamageType::Railgun;
   kickBackStrength    = 5000;
   bubbleEmitTime      = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Railgun;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 200;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;

   explosion           = "MissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;
   
   baseEmitter         = HowitzerSmokeEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 1000;
   wetVelocity       = 300;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

datablock LinearProjectileData(TungstenRailV)
{
   scale = "10.0 20.0 10.0";
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 2.5;
   hasDamageRadius     = false;
   directDamageType    = $DamageType::Railgun;
   kickBackStrength    = 5000;
   bubbleEmitTime      = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Railgun;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 300;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;

   explosion           = "MissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = HowitzerSmokeEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 1000;
   wetVelocity       = 300;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(RailgunAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a railgun clip";
   emap = true;
};

datablock ItemData(Railgun)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = RailgunImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a Railgun";
};

datablock ShapeBaseImageData(RailgunImage)
{
   className = WeaponImage;
   shapeFile = "turret_tank_barrelchain.dts";
   item = Railgun;
   ammo = RailgunAmmo;
   offset = "0 0 0";
   rotation = "1 0 0 0";
   emap = true;

   usesEnergy = true;
   fireEnergy = 75;
   minEnergy = 75;

   enhancementSlots = 1;

   weaponDescName = "Railgun";
   defaultModeName = "Tungsten Rail";
   defaultModeDescription = "Is that the Mech Howitzer?!";
   defaultModeSwitchSound = "CameraGrenadeAttachSound";
   defaultModeFireSound = "MILFireSound";
   defaultModeFailSound = "GrenadeDryFireSound";

   subImage1 = "RailgunBody";
   subImage2 = "RailgunChargerL";
   subImage3 = "RailgunChargerR";

   projectile = TungstenRail;
   projectileType = LinearProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateSound[0] = "MILSwitchSound";
   stateTimeoutValue[0] = 0.01;
   stateSequence[0] = "Activate";

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 2.0;
   stateFire[3] = true;
   stateEjectShell[3] = true;
   stateSound[3] = "MILFireSound";
   stateScript[3]  = "onFire";
   stateSequence[3] = "Fire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.5;
   stateSound[4] = "MissileReloadSound";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = GrenadeDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

function RailgunImage::onMount(%this,%obj,%slot)
{
    Parent::onMount(%this,%obj,%slot);
    
    %obj.play3D(MILSwitchSound);
}

function xRailgunImage::onFire(%data,%obj,%slot)
{
    %p = Parent::onFire(%data,%obj,%slot);

    if(isObject(%p))
        %obj.play3d("MILFireSound");
}

datablock ShapeBaseImageData(RailgunBody) //: RailgunImage - this caused a crash bug when the weapon was unmounted for reasons I still can't explain
{
   shapeFile = "weapon_missile.dts";
   offset = "-0.25 0.9 0.05";
   rotation = degreesToRotation("0 -90 180");
   emap = false;

   subImage = true;
};

datablock ShapeBaseImageData(RailgunChargerL) : RailgunBody
{
   shapeFile = "ammo_grenade.dts";
   offset = "0.075 1.25 -0.125";
   rotation = "0 1 0 -125";
   emap = false;

   subImage = true;
};

datablock ShapeBaseImageData(RailgunChargerR) : RailgunBody
{
   shapeFile = "ammo_grenade.dts";
   offset = "-0.075 1.25 -0.125";
   rotation = "0 1 0 125";
   emap = false;

   subImage = true;
};
