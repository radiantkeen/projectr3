//--------------------------------------
// Dual Disruptor
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(DualDisruptorSwitchSound)
{
   filename    = "fx/weapons/sniper_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------

datablock ShapeBaseImageData(DualDisruptorAImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   item = DualDisruptor;
   projectile = DisruptorBolt;
   projectileType = LinearFlareProjectile;

   usesEnergy = true;
   fireEnergy = 12;
   minEnergy = 12;

   offset = "0 1 0";
   rotation = "0 1 0 -90";
   emap = true;
   subImage = true;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 0.24;
   stateFire[3] = true;
   stateRecoil[3] = NoRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
//   stateSound[3] = BlasterFireSound;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTriggerUp[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.2;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

datablock ShapeBaseImageData(DualDisruptorBImage) : DualDisruptorAImage
{
   offset = "0 1 0";
   rotation = "0 1 0 90";

   subImage = true;
};

datablock ShapeBaseImageData(DualDisruptorImage)
{
   shapeFile = "turret_aa_large.dts";
   item = DualDisruptor;
   className = WeaponImage;
   usesEnergy = true;
   minEnergy = 0;

   offset = "0 0 0";
   rotation = "0 1 0 90";

   enhancementSlots = 2;

   subImage1 = "DualDisruptorAImage";
   subImage2 = "DualDisruptorBImage";

   weaponDescName = "Dual Disruptor";
   defaultModeName = "Disruptor Blast";
   defaultModeDescription = "Directed energy weapon - fires bursts of energy";
   defaultModeSwitchSound = "BlasterDryFireSound";
   defaultModeFireSound = "BlasterFireSound";
   defaultModeFailSound = "BlasterDryFireSound";
   
   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Deploy";
   stateSound[0] = DualDisruptorSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 0.25;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTriggerUp[4] = "Ready";
   stateAllowImageChange[4] = false;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.25;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

function DualDisruptorImage::onFire(%data,%obj,%slot)
{
    %mode = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]];
    %time = getSimTime();

    if(%obj.rateOfFire[%data.item] $= "")
        %obj.rateOfFire[%data.item] = 1.0;

    if(isObject(%mode))
    {
        if(%obj.fireTimeout[%data.item, %mode] > %time)
            return;
            
        if(%mode $= "MPDStarstorm")
        {
            %obj.setImageTrigger($WeaponSubImage1, true);
            %obj.setImageTrigger($WeaponSubImage2, true);
            %obj.schedule(32, "setImageTrigger", $WeaponSubImage1, false);
            %obj.schedule(32, "setImageTrigger", $WeaponSubImage2, false);
            
            %obj.fireTimeout[%data.item, %mode] = %time + (1500 / (1 + %obj.rateOfFire[%data.item]));
            return;
        }
    }
    else
    {
        if(%obj.fireTimeout[%data, %data.item] > %time)
            return;
    }

    %obj.ddState = !%obj.ddState;
    %trigger = %obj.ddState ? $WeaponSubImage1 : $WeaponSubImage2;

    %obj.setImageTrigger(%trigger, true);
    %obj.schedule(32, "setImageTrigger", %trigger, false);
}

function DualDisruptorAImage::onFire(%data,%obj,%slot)
{
    %p = Parent::onFire(%data,%obj,%slot);

    if(isObject(%p))
        %obj.play3d("BlasterFireSound");
}

function DualDisruptorBImage::onFire(%data,%obj,%slot)
{
    %p = Parent::onFire(%data,%obj,%slot);

    if(isObject(%p))
        %obj.play3d("BlasterFireSound");
}

datablock ItemData(DualDisruptor)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_energy.dts";
   image = DualDisruptorImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a Dual Disruptor";
};
