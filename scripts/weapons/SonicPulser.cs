// Sonic Pulser
// ------------------------------------------------------------------------

//--------------------------------------------------------------------------
// Item Data
//--------------------------------------------------------------------------
datablock ItemData(SonicPulserThrown)
{
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.5;
   explosion = ConcussionGrenadeExplosion;
   indirectDamage      = 0.45;
   damageRadius        = 4.0;
   radiusDamageType    = $DamageType::SonicPulser;
   kickBackStrength    = 3500;

   computeCRC = true;
};

datablock ItemData(SonicPulser)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = SonicPulserThrown;
	pickUpName = "some Sonic Pulsers";
	isGrenade = true;
};

function SonicPulserThrown::onThrow(%this, %gren)
{
   AIGrenadeThrown(%gren);
   %gren.detThread = schedule(1500, %gren, "detonateGrenade", %gren);
}

function SonicPulserThrown::onRPGHit(%this, %gren)
{
   AIGrenadeThrown(%gren);
   detonateGrenade(%gren);
}
