//--------------------------------------
// DualVulcan
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(DualVulcanSwitchSound)
{
   filename    = "fx/powered/turret_sentry_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(DualVulcanFireSound)
{
   filename    = "fx/vehicles/tank_chaingun.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------

datablock ShapeBaseImageData(DualVulcanImage) : TriggerProxyImage
{
   item      = DualVulcan;
   ammo 	 = ChaingunAmmo;

   shapeFile = "turret_elf_large.dts";
   rotation = "0 0 0 180";
   offset = "0 0 0";

   enhancementSlots = 2;

   weaponDescName = "Dual Vulcan";
   defaultModeName = "Lead Bullet";
   defaultModeDescription = "Standard bullet, low damage, moderate spread.";
   defaultModeSwitchSound = "DualVulcanSwitchSound";
   defaultModeFireSound = "";
   defaultModeFailSound = "ChaingunDryFireSound";
   
   subImage1 = "DualVulcanBImage";
//   subImage2 = "DualVulcanGunAImage";
//   subImage3 = "DualVulcanGunBImage";
};

datablock ShapeBaseImageData(DualVulcanBImage) : TriggerProxyImage
{
   item      = DualVulcan;
   ammo 	 = ChaingunAmmo;
   subImage = true;
   
   shapeFile = "turret_elf_large.dts";
   rotation = "0 0 0 180";
   offset = "-0.95 0 0";
};

function DualVulcanImage::onTriggerDown(%data, %obj, %slot)
{
    %obj.setImageTrigger($WeaponSubImage2, true);
    %obj.setImageTrigger($WeaponSubImage3, true);
}

function DualVulcanImage::onTriggerUp(%data, %obj, %slot)
{
    %obj.setImageTrigger($WeaponSubImage2, false);
    %obj.setImageTrigger($WeaponSubImage3, false);
}

datablock ShapeBaseImageData(DualVulcanGunAImage)
{
   className = WeaponImage;
   shapeFile = "weapon_chaingun.dts";
   item      = DualVulcan;
   ammo 	 = ChaingunAmmo;
   projectile = ChaingunBullet;
   projectileType = TracerProjectile;
   emap = true;

   offset = "-0.9 0.65 -0.1";
   rotation = "0 1 0 90";

   casing              = ShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 5.0;

   subImage = true;
   projectileSpread = 9.0;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateSound[0]            = ChaingunSwitchSound;
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = ChaingunSpinupSound;
   //
   stateTimeoutValue[3]          = 0.5;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = ChaingunFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.15;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;
   stateWaitForTimeout[5]          = true;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(DualVulcanGunBImage) : DualVulcanGunAImage
{
   subImage = true;
   projectileSpread = 9.0;
   
   rotation = "0 1 0 0";
   offset = "-0.1 0.65 -0.075";
};

datablock ShapeBaseImageData(DualVulcanGunATurboImage) : DualVulcanGunAImage
{
   subImage = true;

   offset = "-0.9 0.65 -0.1";
   rotation = "0 1 0 90";
   
   shellVelocity       = 9.0;

   stateTimeoutValue[3]          = 0.1;
   stateTimeoutValue[4]          = 0.1;
   stateTimeoutValue[5]            = 0.25;
};

datablock ShapeBaseImageData(DualVulcanGunBTurboImage) : DualVulcanGunAImage
{
   subImage = true;
   
   rotation = "0 1 0 0";
   offset = "-0.1 0.65 -0.075";

   shellVelocity       = 9.0;

   stateTimeoutValue[3]          = 0.1;
   stateTimeoutValue[4]          = 0.1;
   stateTimeoutValue[5]            = 0.25;
};

function DualVulcanImage::onMount(%this,%obj,%slot)
{
    Parent::onMount(%this,%obj,%slot);

//    %mode = %obj.client.weaponMode[%this.item, %obj.client.selectedFireMode[%this.item]];
    %subImage1 = "DualVulcanGunAImage";
    %subImage2 = "DualVulcanGunBImage";

    if(%obj.rateOfFire[%this.item] > 0)
    {
        %subImage1 = "DualVulcanGunATurboImage";
        %subImage2 = "DualVulcanGunBTurboImage";
    }

    %obj.mountImage(%subImage1, $WeaponSubImage2);
    %obj.mountImage(%subImage2, $WeaponSubImage3);
}

function DualVulcanImage::onUnmount(%this,%obj,%slot)
{
    %obj.unmountImage($WeaponSubImage2);
    %obj.unmountImage($WeaponSubImage3);

    Parent::onUnmount(%this, %obj, %slot);
}

function DualVulcanGunATurboImage::validateFireMode(%data, %obj)
{
    %ammoUse = 1;
    %energyUse = %obj.rateOfFire[%obj.getMountedImage(0).item] > 0 ? 1 : 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.getEnergyLevel() < %energyUse)
        return "f";

    %gyanUse = 0;

    return %energyUse SPC %ammoUse SPC %gyanUse;
}

function DualVulcanGunBTurboImage::validateFireMode(%data, %obj)
{
    %ammoUse = 1;
    %energyUse = %obj.rateOfFire[%obj.getMountedImage(0).item] > 0 ? 1 : 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.getEnergyLevel() < %energyUse)
        return "f";

    %gyanUse = 0;

    return %energyUse SPC %ammoUse SPC %gyanUse;
}

datablock ItemData(DualVulcan)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_chaingun.dts";
   image        = DualVulcanImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
   pickUpName   = "a Dual Vulcan";

   computeCRC = true;
   emap = true;
};
