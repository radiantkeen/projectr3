datablock ItemData(BatteryGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = FlareGrenadeThrown;
	pickUpName = "a battery pack";
	isGrenade = true;

   computeCRC = true;
};

function BatteryGrenade::onUse(%this, %obj)
{
    if(Game.handInvOnUse(%this, %obj))
    {
        %time = getSimTime();

        if(%time > %obj.nextUseTime[%this] && %obj.getEnergyPct() < 1)
        {
            %obj.setEnergyLevel(%obj.getEnergyLevel() + 100);
            messageClient(%obj.client, 'MsgBatteryUse', "\c3Boosted capacitor +100kj");
            %obj.decInventory(%this, 1);
//            commandToClient(%obj.client, 'setInventoryHudAmount', 0, 0);
            %obj.play3D(SniperRifleSwitchSound);

            // miscellaneous grenade-throwing cleanup stuff
            %obj.lastThrowTime[%this] = %time;
            %obj.nextUseTime[%this] = %time + 5000;
            %obj.throwStrength = 0;
        }
        else 
            messageClient(%obj.client, 'MsgBatteryWait', "\c3Battery pack cooling (5 sec)");
    }
}
