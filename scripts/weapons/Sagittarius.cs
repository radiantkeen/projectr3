datablock AudioProfile(SagittariusFireSound)
{
   filename    = "fx/vehicles/tank_mortar_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ParticleData(SagittariusSmokeParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.001;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 200;

   textureName          = "small_circle";

   useInvAlpha = false;
   spinRandomMin = 0.0;
   spinRandomMax = 0.0;

   colors[0]     = "0.0 0.75 1.0 0.0";
   colors[1]     = "0.2 0.5 0.5 1.0";
   colors[2]     = "0.3 0.3 0.3 0.0";
   sizes[0]      = 1.3;
   sizes[1]      = 2.6;
   sizes[2]      = 3.5;
   times[0]      = 0.0;
   times[1]      = 0.1;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(SagittariusSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 0.1;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "SagittariusSmokeParticle";
};

datablock LinearProjectileData(SolidRail)
{
//   scale = "10.0 20.0 10.0";
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 1.5;
   hasDamageRadius     = false;
   directDamageType    = $DamageType::Sniper;
   kickBackStrength    = 3250;
   bubbleEmitTime      = 1.0;

   explosion           = "ChaingunExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Sagittarius;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 100;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;

   baseEmitter         = SagittariusSmokeEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 1500;
   wetVelocity       = 750;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

function SolidRail::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    %snd = %projectile.isWet() ? getRandomChaingunWetSound() : getRandomChaingunSound();
    %projectile.play3D(%snd);

    Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(SagittariusAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a sagittarius clip";
   emap = true;
};

datablock ItemData(Sagittarius)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = SagittariusImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a Sagittarius";
};

datablock ShapeBaseImageData(SagittariusImage)
{
   className = WeaponImage;
   shapeFile = "weapon_sniper.dts";
   item = Sagittarius;
   ammo = SagittariusAmmo;
   offset = "0 0 0";
   rotation = "1 0 0 0";
   emap = true;
   armThread = looksn;
   
   usesEnergy = true;
   fireEnergy = 20;
   minEnergy = 20;

   enhancementSlots = 2;

   weaponDescName = "Sagittarius";
   defaultModeName = "Solid Rail";
   defaultModeDescription = "Fires a solid depleted uranium bolt which does significant damage";
   defaultModeSwitchSound = "BlasterDryFireSound";
   defaultModeFireSound = "";
   defaultModeFailSound = "BlasterDryFireSound";

   subImage1 = SagittariusDecalA;
   subImage2 = SagittariusDecalB;
   subImage3 = SagittariusDecalC;

   projectile = SolidRail;
   projectileType = LinearProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateSound[0] = "BlasterSwitchSound";
   stateTimeoutValue[0] = 0.01;
   stateSequence[0] = "Activate";

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 1.5;
   stateFire[3] = true;
   stateEjectShell[3] = true;
   stateSound[3] = "SagittariusFireSound";
   stateScript[3]  = "onFire";
   stateSequence[3] = "Fire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.5;
   stateSound[4] = "BlasterDryFireSound";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = BlasterDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

function xSagittariusImage::onMount(%this,%obj,%slot)
{
    Parent::onMount(%this,%obj,%slot);

    %obj.play3D(MILSwitchSound);
}

function xSagittariusImage::onFire(%data,%obj,%slot)
{
    %p = Parent::onFire(%data,%obj,%slot);

    if(isObject(%p))
        %obj.play3d("SagittariusFireSound");
}

datablock ShapeBaseImageData(SagittariusDecalA)
{
   shapeFile = "turret_belly_barrelr.dts";
   offset = "0 0.4 0.25";
   rotation = "0 0 1 180";
   
   subImage = true;
};

datablock ShapeBaseImageData(SagittariusDecalB)
{
   shapeFile = "turret_belly_barrell.dts";
   offset = "0 0.5 0.25";

   subImage = true;
};

datablock ShapeBaseImageData(SagittariusDecalC)
{
   shapeFile = "turret_belly_barrell.dts";
   offset = "0 1.25 0.1";

   subImage = true;
};
