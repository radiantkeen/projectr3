// ----------------------------------------------
// Koi Mine
// ----------------------------------------------

$TeamDeployableMax[KoiMineDeployed]		= 10;

datablock ItemData(KoiMineDeployed) : MineDeployed
{
   className = Weapon;
   shapeFile = "mine.dts";
   mass = 0.75;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 3;
   maxDamage = 0.2;
   explosion = MineExplosion;
   underwaterExplosion = UnderwaterMineExplosion;
   indirectDamage = 0.1;
   damageRadius = 1.0;
   radiusDamageType = $DamageType::Mine;
   kickBackStrength = 500;
	aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
	spacing = 6.0; // how close together mines can be
	proximity = 1.0; // how close causes a detonation (by player/vehicle)
	armTime = 2200; // 2.2 seconds to arm a mine after it comes to rest
	maxDepCount = 5; // try to deploy this many times before detonating
 
   // MD3 radial status effect code
   radialStatusEffect  = "PoisonEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
};

datablock ItemData(KoiMine)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_mine.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = KoiMineDeployed;
	pickUpName = "some Koi mines";
};

function KoiMineDeployed::onThrow(%this, %mine, %thrower)
{
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.theClient = %thrower.client;
   %mine.isAMine = true;
   $TeamDeployedCount[%mine.sourceObject.team, KoiMineDeployed]++; // z0dd - ZOD, 8/13/02, Moved this from deployMineCheck to here. Fixes mine count bug

   schedule(1500, %mine, "deployMineCheck", %mine, %thrower);
}

function KoiMineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   //check to see what it is that collided with the mine
   %struck = %col.getClassName();
   if(%struck $= "Player" || %struck $= "WheeledVehicle" || %struck $= "FlyingVehicle")
   {
      //error("Mine detonated due to collision with #"@%col@" ("@%struck@"); armed = "@%obj.armed);
      explodeMine(%obj, false);
   }
}

function KoiMineDeployed::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   // -----------------------------
   // z0dd - ZOD, 4/17/02. added minedisk
   //if(!%targetObject.armed)
   //   return;
   // -----------------------------

   if(%targetObject.boom)
      return;

   %targetObject.damaged += %amount;

   if(%targetObject.damaged >= %data.maxDamage)
   {
      %targetObject.setDamageState(Destroyed);
   }
}

function KoiMineDeployed::onDestroyed(%data, %obj, %lastState)
{
   %obj.boom = true;
   %mineTeam = %obj.team;
   $TeamDeployedCount[%mineTeam, KoiMineDeployed]--;
   // %noDamage is a boolean flag -- don't want to set off all other mines in
   // vicinity if there's a "mine overload", so apply no damage/impulse if true
   if(!%obj.noDamage)
       RadiusRadiationExplosion(%obj,
                      %obj.getPosition(),
                      %data.damageRadius,
                      %data.indirectDamage,
                      %data.kickBackStrength,
                      %obj.sourceObject,
                      true); // %data.radiusDamageType

   %obj.schedule(600, "delete");
}
