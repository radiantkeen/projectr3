// ------------------------------------------------------------------------
// EM Pulse grenade
// ------------------------------------------------------------------------

datablock ShockwaveData(EMPShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 45;
   acceleration = 100.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 1.0 0.4 0.50";
   colors[1] = "0.4 1.0 0.4 0.25";
   colors[2] = "0.4 1.0 0.4 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ShockwaveData(EMPShockwave2D)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 45;
   acceleration = 100.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 1.0 0.4 0.50";
   colors[1] = "0.4 1.0 0.4 0.25";
   colors[2] = "0.4 1.0 0.4 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ExplosionData(EMPSubExplosion)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;
   playSpeed = 1.5;

   sizes[0] = "2.5 2.5 2.5";
   sizes[1] = "2.5 2.5 2.5";
   times[0] = 0.0;
   times[1] = 1.0;

   shockwave = EMPShockwave2D;
};

datablock ExplosionData(EMPExplosion)
{
   soundProfile   = GrenadeExplosionSound;

   shockwave = EMPShockwave;

   emitter[0] = GrenadeSparksEmitter;
   subExplosion[0] = EMPSubExplosion;

   shakeCamera = true;
   camShakeFreq = "10.0 6.0 9.0";
   camShakeAmp = "5.0 5.0 5.0";
   camShakeDuration = 0.25;
   camShakeRadius = 10.0;
};

datablock ItemData(EMPGrenadeThrown)
{
	className = Weapon;
	shapeFile = "grenade_flash.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.5;
	explosion = EMPExplosion;
	underwaterExplosion = EMPExplosion;
   indirectDamage      = 0.6;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::EMP;
   kickBackStrength    = 500;
   
   // MD3 radial status effect code
   radialStatusEffect  = "EMPulseEffect";
   statusEffectChance  = 0.75;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::EMP;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 60;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   launcherProjectile = "EMPGrenadeShoulder";
};

datablock GrenadeProjectileData(EMPGrenadeShoulder) : ShoulderGrenadeTemplate
{
   projectileShapeName = "grenade_flash.dts";
   hasDamageRadius     = true;
   damageRadius        = 5.0;
   kickBackStrength    = 500;

   // MD3 radial status effect code
   radialStatusEffect  = "EMPulseEffect";
   statusEffectChance  = 0.75;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::EMP;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 60;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = EMPExplosion;
   underwaterExplosion = EMPExplosion;
   velInheritFactor    = 1.0;

   baseEmitter         = GrenadeSmokeEmitter;
};

//function EMPGrenadeShoulder::onExplode(%data, %proj, %pos, %mod)
//{
//    Parent::onExplode(%data, %proj, %pos, %mod);
//}

datablock ItemData(EMPGrenade)
{
	className = HandInventory;
	catagory = "Handheld";
	shapeFile = "grenade.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = EMPGrenadeThrown;
	pickUpName = "some EM Pulse grenades";
	isGrenade = true;
};

function EMPGrenadeThrown::onThrow(%this, %gren)
{
   AIGrenadeThrown(%gren);
   %gren.detThread = schedule(1500, %gren, "detonateGrenade", %gren);
}
