// grenade (thrown by hand) script
// ------------------------------------------------------------------------
datablock AudioProfile(FlashGrenadeExplosionSound)
{
   filename = "fx/explosions/grenade_flash_explode.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock ExplosionData(FlashGrenadeExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = FlashGrenadeExplosionSound;

   faceViewer     = true;
};

datablock ItemData(FlashGrenadeThrown)
{
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.4;
   explosion = FlashGrenadeExplosion;
   indirectDamage      = 0.5;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Grenade;
   kickBackStrength    = 1000;

   computeCRC = true;

    maxWhiteout = 0.9; // z0dd - ZOD, 9/8/02. Was 1.2
    launcherProjectile = "FlashGrenadeShoulder";
};

datablock GrenadeProjectileData(FlashGrenadeShoulder) : ShoulderGrenadeTemplate
{
   projectileShapeName = "grenade.dts";
   hasDamageRadius     = true;
   damageRadius        = 1.0;
   kickBackStrength    = 500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Grenade;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 1;
   mdDamageRadius[0]   = true;

   flags               = 0;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = FlashGrenadeExplosion;
   underwaterExplosion = FlashGrenadeExplosion;
   velInheritFactor    = 1.0;

   baseEmitter         = GrenadeSmokeEmitter;
};

function FlashGrenadeShoulder::onExplode(%data, %proj, %pos, %mod)
{
    detonateFlashGrenadeS(%proj);
    Parent::onExplode(%data, %proj, %pos, %mod);
}

function detonateFlashGrenadeS(%hg)
{
   %maxWhiteout = 0.9;
   %thrower = %hg.sourceObject.client;
   %hgt = %hg.getTransform();
   %plX = firstword(%hgt);
   %plY = getWord(%hgt, 1);
   %plZ = getWord(%hgt, 2);
   %pos = %plX @ " " @ %plY @ " " @ %plZ;
   //all this stuff below ripped from projectiles.cs

   InitContainerRadiusSearch(%pos, 100.0, $TypeMasks::PlayerObjectType |
                                          $TypeMasks::TurretObjectType);

   while ((%damage = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrDist();

      %eyeXF = %damage.getEyeTransform();
      %epX   = firstword(%eyeXF);
      %epY   = getWord(%eyeXF, 1);
      %epZ   = getWord(%eyeXF, 2);
      %eyePos = %epX @ " " @ %epY @ " " @ %epZ;
      %eyeVec = %damage.getEyeVector();

      // Make sure we can see the thing...
      if (ContainerRayCast(%eyePos, %pos, $TypeMasks::TerrainObjectType |
                                          $TypeMasks::InteriorObjectType |
                                          $TypeMasks::StaticObjectType, %damage) !$= "0")
      {
         continue;
      }

      %distFactor = 1.0;
      if (%dist >= 100)
         %distFactor = 0.0;
      else if (%dist >= 20) {
         %distFactor = 1.0 - ((%dist - 20.0) / 80.0);
      }

      %dif = VectorNormalize(VectorSub(%pos, %eyePos));
      %dot = VectorDot(%eyeVec, %dif);

      %difAcos = mRadToDeg(mAcos(%dot));
      %dotFactor = 1.0;
      if (%difAcos > 60)
         %dotFactor = ((1.0 - ((%difAcos - 60.0) / 120.0)) * 0.2) + 0.3;
      else if (%difAcos > 45)
         %dotFactor = ((1.0 - ((%difAcos - 45.0) / 15.0)) * 0.5) + 0.5;

      %totalFactor = %dotFactor * %distFactor;

	  %prevWhiteOut = %damage.getWhiteOut();

		if(!%prevWhiteOut)
			if(!$teamDamage)
			{
				if(%damage.client != %thrower && %damage.client.team == %thrower.team)
					messageClient(%damage.client, 'teamWhiteOut', '\c1You were hit by %1\'s whiteout grenade.', getTaggedString(%thrower.name));
			}

      %whiteoutVal = %prevWhiteOut + %totalFactor;
      if(%whiteoutVal > %maxWhiteout)
      {
        //error("whitout at max");
        %whiteoutVal = %maxWhiteout;
      }

      %damage.setWhiteOut( %whiteoutVal );
   }
}

datablock ItemData(FlashGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = FlashGrenadeThrown;
	pickUpName = "some flash grenades";
	isGrenade = true;

   computeCRC = true;

};

//--------------------------------------------------------------------------
// Functions:
//--------------------------------------------------------------------------
function FlashGrenadeThrown::onCollision( %data, %obj, %col )
{
   // Do nothing...
}

