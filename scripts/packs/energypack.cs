// ------------------------------------------------------------------
// ENERGY PACK
// can be used by any armor type
// does not have to be activated
// increases the user's energy recharge rate

datablock ShapeBaseImageData(EnergyPackImage)
{
   shapeFile = "pack_upgrade_energy.dts";
   item = EnergyPack;
   mountPoint = 1;
   offset = "0 0 0";
   rechargeRateBoost = 0.3;

   usesEnergy = true;
   minEnergy = -1;

   stateName[0] = "Idle";
	stateSequence[0] = "activation";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

   stateName[2] = "Deactivate";
   stateScript[2] = "onDeactivate";
   stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(EnergyPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_energy.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "EnergyPackImage";
	pickUpName = "a power core";

   computeCRC = true;
};

function EnergyPackImage::onMount(%data, %obj, %node)
{
    %obj.hasEnergyPack = true;
    %obj.baseRechargeRate = %obj.getDatablock().rechargeRate * %obj.rechargeRateFactor;
    %obj.setRechargeRate(%obj.baseRechargeRate + %data.rechargeRateBoost);
}

function EnergyPackImage::onUnmount(%data, %obj, %node)
{
    %obj.setRechargeRate(%obj.baseRechargeRate);
    %obj.hasEnergyPack = false;
}

function EnergyPackImage::onActivate(%data, %obj, %slot)
{
    messageClient(%obj.client, 'MsgEnergyODUse', '\c2Overclocking Power Core, reactor output increased.');
    
    %obj.setRechargeRate((%obj.baseRechargeRate + %data.rechargeRateBoost) * 1.5);
    %obj.epackOD = true;
    ePackOverdrive(%obj);

//    %obj.play3D(DepInvActivateSound); // for clientside
}

function ePackOverdrive(%obj)
{
    if(%obj.hasEnergyPack == true && %obj.epackOD == true)
    {
        %obj.setHeat(1.0);
        schedule(512, %obj, "ePackOverdrive", %obj);
    }
}

function EnergyPackImage::onDeactivate(%data, %obj, %slot)
{
    messageClient(%obj.client, 'MsgEnergyODOff', '\c2Power Core reactor output returned to normal.');
    %obj.setRechargeRate(%obj.baseRechargeRate);
    %obj.epackOD = false;
}

function EnergyPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
