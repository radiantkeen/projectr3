// ------------------------------------------------------------------
// Synomic Regenerator

datablock AudioProfile(SynomiumIdle)
{
	filename = "fx/misc/nexus_idle.wav";
	description = ClosestLooping3d;
    preload = true;
};

datablock AudioProfile(SynomiumOn)
{
	filename = "fx/weapons/chaingun_start.wav";
	description = AudioDefault3D;
    preload = true;
};

datablock AudioProfile(SynomiumOff)
{
	filename = "fx/weapons/chaingun_off.wav";
	description = AudioDefault3D;
    preload = true;
};

datablock ShapeBaseImageData(SynomicRegeneratorImage)
{
   shapeFile = "ammo_plasma.dts";
   item = SynomicRegenerator;
   mountPoint = 1;
   offset = "0 -0.12 -0.5";

   usesEnergy = true;
   minEnergy = 5;

   stateName[0] = "Idle";
	stateSequence[0] = "activation";
	stateSound[0] = SynomiumIdle;
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateSequence[1] = "fire";
   stateSound[1] = SensorJammerActivateSound;
   stateEnergyDrain[1] = 14.0;
   stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";
   
   stateName[2] = "Deactivate";
   stateScript[2] = "onDeactivate";
   stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(SynomicRegenerator)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "SynomicRegeneratorImage";
	pickUpName = "a synomium device";

   computeCRC = true;
};

function SynomicRegeneratorImage::onMount(%data, %obj, %node)
{
    if(isEventPending(%obj.synomiumThread))
        cancel(%obj.synomiumThread);

    %obj.synomiumActive = true;
    %obj.synomiumTickCount = 0;
    %obj.synomiumThread = %data.schedule(100, "onTick", %obj);
    
    %obj.play3d(SynomiumOn);
}

function SynomicRegeneratorImage::onActivate(%data, %obj, %slot)
{
    messageClient(%obj.client, 'MsgSynDamageAmpOn', '\c2Engaging synomic damage infusion.');
    
    %obj.damageBuffFactor += 0.4;
    %obj.synAmpState = true;
}

function SynomicRegeneratorImage::onDeactivate(%data, %obj, %slot)
{
    messageClient(%obj.client, 'MsgSynDamageAmpOff', '\c2Disengaging synomic damage infusion.');

    %obj.damageBuffFactor -= 0.4;
    %obj.synAmpState = false;
    %obj.setImageTrigger(%slot, false);
}

function SynomicRegeneratorImage::onTick(%data, %obj, %node)
{
    if(%obj.isDead || !%obj.synomiumActive)
        return;
        
    if(%obj.synomiumTickCount % 4 == 0)
    {
        zapEffect(%obj, "FXZap");

        if(getRandom() >  0.8)
            zapEffect(%obj, "FXPulse");
    }
    
    %obj.synomiumTickCount++;
    %obj.synomiumThread = %data.schedule(100, "onTick", %obj);
}

function SynomicRegeneratorImage::onUnmount(%data, %obj, %node)
{
    %obj.synomiumActive = false;
    %obj.play3d(SynomiumOff);
    
    if(isEventPending(%obj.synomiumThread))
        cancel(%obj.synomiumThread);
        
    if(%obj.synAmpState == true)
    {
        %obj.synAmpState = false;
        %obj.damageBuffFactor -= 0.4;
    }
}

function SynomicRegenerator::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
