// ------------------------------------------------------------------
// SHIELD PACK
// can be used by any armor type
// while activated, absorbs damage at cost of energy

datablock AudioProfile(ShieldPackActivateSound)
{
	filename = "fx/powered/turret_heavy_idle.wav";
	description = ClosestLooping3d;
   preload = true;
};

datablock ShapeBaseImageData(ShieldPackImage)
{
    shapeFile = "pack_upgrade_shield.dts";
    item = ShieldPack;
    mountPoint = 1;
    offset = "0 0 0";

    usesEnergy = true;
    minEnergy = 3;
   
	stateName[0] = "Idle";
	stateSequence[0] = "fire";
    stateSound[0] = ShieldPackActivateSound;
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
 
	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(ShieldPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_shield.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "ShieldPackImage";
	pickUpName = "a shield pack";

   computeCRC = true;
};

function ShieldPackImage::onMount(%data, %obj, %node)
{
    messageClient(%obj.client, 'MsgShieldPackOn', '\c2Shield emitter activated.');

    %obj.isShielded = true;
    %obj.wearingShieldPack = true;

    %max = mCeil((%obj.getDatablock().maxEnergy / 75) * 100);
    %obj.shieldMax = %max;
    
    if(%obj.shieldCap $= "")
        %obj.shieldCap = %max;
        
    if(%obj.shieldCap > 0)
        commandToClient(%obj.client, 'setShieldIconOn');
    else
        commandToClient(%obj.client, 'setShieldIconOff');
}

function ShieldPackImage::onUnmount(%data, %obj, %node)
{
    messageClient(%obj.client, 'MsgShieldPackOff', '\c2Shield emitter deactivated.');
    
    %obj.wearingShieldPack = false;
    %obj.setImageTrigger(%node, false);
    %obj.isShielded = false;
    %obj.shieldCap = 0;
    %obj.shieldMax = 0;
}

function ShieldPackImage::onActivate(%data, %obj, %slot)
{
    if(%obj.shieldCap < %obj.shieldMax && %obj.shieldRecharging != true)
    {
        messageClient(%obj.client, 'MsgShieldPackRecharge', '\c2Shield emitter recharging, please wait...');

        shieldStartRecharge(%obj);
    }
    else if(%obj.shieldRecharging == true)
    {
        messageClient(%obj.client, 'MsgShieldPackStopRech', '\c2Shield emitter stopped recharging.');

        shieldEndRecharge(%obj);
    }
    
    %obj.setImageTrigger(%slot, false);
}

function shieldStartRecharge(%obj)
{
    if(%obj.shieldCap < %obj.shieldMax)
    {
        if(%obj.shieldPackRechargeThread > 0)
            return;

        commandToClient(%obj.client, 'setShieldIconOn');
        
        %obj.shieldRecharging = true;
        %obj.shieldRechargeRate = %obj.getRechargeRate();

        %obj.setRechargeRate(0);
        shieldTickRecharge(%obj);
    }
}

function shieldTickRecharge(%obj)
{
    if(%obj.wearingShieldPack && %obj.shieldRecharging == true)
    {
        %obj.shieldCap += mFloor(%obj.shieldRechargeRate * 8);
        
        if(%obj.shieldCap >= %obj.shieldMax)
        {
            %obj.shieldCap = %obj.shieldMax;
            shieldEndRecharge(%obj);
        }
        
        %obj.shieldPackRechargeThread = schedule(256, %obj, "shieldTickRecharge", %obj);
    }
    else
    {
        if(%obj.shieldCap == %obj.shieldMax)
            messageClient(%obj.client, 'MsgShieldPackRecFinish', '\c2Shield emitter fully charged.');
            
        shieldEndRecharge(%obj);
    }
}

function shieldEndRecharge(%obj)
{
    %obj.shieldPackRechargeThread = 0;
    %obj.shieldRecharging = false;
    %obj.setRechargeRate(%obj.shieldRechargeRate);

    cancel(%obj.shieldPackRechargeThread);
}

function ShieldPackImage::onDeactivate(%data, %obj, %slot)
{

}

function ShieldPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
