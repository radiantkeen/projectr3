// Walker Datablocks

// Audio
//------------------------------------------------------------------------------
datablock AudioProfile(WalkerPowerOn)
{
   filename    = "fx/powered/base_power_on.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(WalkerPowerOff)
{
   filename    = "fx/powered/base_power_off.wav";
   description = AudioDefault3d;
   preload = true;
};

// TSShape Definitions
//------------------------------------------------------------------------------
datablock TSShapeConstructor(TR2HeavyMaleDts)
{
   baseShape = "TR2heavy_male.dts";
   sequence0 = "TR2heavy_male_root.dsq root";
   sequence1 = "TR2heavy_male_forward.dsq run";
   sequence2 = "TR2heavy_male_back.dsq back";
   sequence3 = "TR2heavy_male_side.dsq side";
   sequence4 = "heavy_male_lookde.dsq look";
   sequence5 = "heavy_male_head.dsq head";
   sequence6 = "TR2heavy_male_fall.dsq fall";
   sequence7 = "TR2heavy_male_jet.dsq jet";
   sequence8 = "TR2heavy_male_land.dsq land";
   sequence9 = "TR2heavy_male_jump.dsq jump";
   sequence10 = "heavy_male_recoilde.dsq light_recoil";
   sequence11 = "heavy_male_idlepda.dsq pda";
   sequence12 = "heavy_male_headside.dsq headside";
   sequence13 = "heavy_male_lookms.dsq lookms";
   sequence14 = "TR2heavy_male_diehead.dsq death1";
   sequence15 = "TR2heavy_male_diechest.dsq death2";
   sequence16 = "TR2heavy_male_dieback.dsq death3";
   sequence17 = "TR2heavy_male_diesidelf.dsq death4";
   sequence18 = "TR2heavy_male_diesidert.dsq death5";
   sequence19 = "TR2heavy_male_dieforward.dsq death6";      // heavy_male_dieleglf
   sequence20 = "TR2heavy_male_diechest.dsq death7";        // heavy_male_dielegrt
   sequence21 = "TR2heavy_male_dieslump.dsq death8";
   sequence22 = "TR2heavy_male_dieforward.dsq death9";      // heavy_male_dieknees
   sequence23 = "TR2heavy_male_dieforward.dsq death10";
   sequence24 = "TR2heavy_male_diespin.dsq death11";
   sequence25 = "TR2heavy_male_celsalute.dsq cel1";
   sequence26 = "TR2heavy_male_celwave.dsq cel2";
   sequence27 = "TR2heavy_male_tauntbest.dsq cel3";
   sequence28 = "TR2heavy_male_tauntimp.dsq cel4";
   sequence29 = "TR2heavy_male_celdance.dsq cel5";
   sequence30 = "TR2heavy_male_celflex.dsq cel6";
   sequence31 = "TR2heavy_male_celtaunt.dsq cel7";
   sequence32 = "TR2heavy_male_celjump.dsq cel8";
   sequence33 = "TR2heavy_male_ski.dsq ski";
   sequence34 = "TR2heavy_male_standjump.dsq standjump";
   sequence35 = "heavy_male_looknw.dsq looknw";
};

datablock TSShapeConstructor(TR2MediumMaleDts)
{
   baseShape = "TR2medium_male.dts";
   sequence0 = "TR2medium_male_root.dsq root";
   sequence1 = "TR2medium_male_forward.dsq run";
   sequence2 = "TR2medium_male_back.dsq back";
   sequence3 = "TR2medium_male_side.dsq side";
   sequence4 = "medium_male_lookde.dsq look";
   sequence5 = "medium_male_head.dsq head";
   sequence6 = "TR2medium_male_fall.dsq fall";
   sequence7 = "TR2medium_male_jet.dsq jet";
   sequence8 = "TR2medium_male_land.dsq land";
   sequence9 = "TR2medium_male_jump.dsq jump";
   sequence10 = "medium_male_recoilde.dsq light_recoil";
   sequence11 = "medium_male_headside.dsq headside";
   sequence12 = "medium_male_looksn.dsq looksn";
   sequence13 = "medium_male_lookms.dsq lookms";
   sequence14 = "TR2medium_male_sitting.dsq sitting";
   sequence15 = "TR2medium_male_diehead.dsq death1";
   sequence16 = "TR2medium_male_diechest.dsq death2";
   sequence17 = "TR2medium_male_dieback.dsq death3";
   sequence18 = "TR2medium_male_diesidelf.dsq death4";
   sequence19 = "TR2medium_male_diesidert.dsq death5";
   sequence20 = "TR2medium_male_dieleglf.dsq death6";
   sequence21 = "TR2medium_male_diechest.dsq death7";          // medium_male_dielegrt
   sequence22 = "TR2medium_male_dieback.dsq death8";
   sequence23 = "TR2medium_male_dieknees.dsq death9";
   sequence24 = "TR2medium_male_dieforward.dsq death10";
   sequence25 = "TR2medium_male_diespin.dsq death11";
   sequence26 = "medium_male_idlepda.dsq pda";
   sequence27 = "TR2medium_male_celsalute.dsq cel1";
   sequence28 = "TR2medium_male_celwave.dsq cel2";
   sequence29 = "TR2medium_male_tauntbest.dsq cel3";
   sequence30 = "TR2medium_male_tauntimp.dsq cel4";
   sequence31 = "TR2medium_male_celdance.dsq cel5";
   sequence32 = "TR2medium_male_celflex.dsq cel6";
   sequence33 = "TR2medium_male_celtaunt.dsq cel7";
   sequence34 = "TR2medium_male_celrocky.dsq cel8";
   sequence35 = "TR2medium_male_ski.dsq ski";
   sequence36 = "TR2medium_male_standjump.dsq standjump";
   sequence37 = "medium_male_looknw.dsq looknw";
};

datablock ShapeBaseImageData(WalkerMissileParam)
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 10;
   offset = "0 0 0";
   rotation = "0 1 0 0";

   isSeeker     = true;
   seekRadius   = 300;
   maxSeekAngle = 30;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 10;
};

datablock ShapeBaseImageData(WalkerDefaultParam) : WalkerMissileParam
{
   isSeeker = false;
};

datablock SimDataBlock(WalkerDamageProfile) : ArmorDamageProfile
{
   damageScale[$DamageType::Impact] 				= 0.75;
   damageScale[$DamageType::Ground] 				= 1.5;
};

datablock SensorData(WalkerBaseSensor)
{
   detects = true;
   detectsUsingLOS = true;
   detectsPassiveJammed = false;
   detectRadius = 400;
   detectionPings = false;
   detectsFOVOnly = true;
   detectFOVPercent = 1.2;
   useObjectFOV = true;
};

datablock DebrisData(WalkerDebris) : MagIonDebris
{
   emitters[0] = MagIonDebrisFireEmitter;
   emitters[1] = MagIonDebrisSmokeEmitter;

   elasticity = 0.15;
   friction = 0.75;
};

datablock ParticleData(WalkerJetParticle)
{
   dragCoefficient      = 1.5;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 200;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.9 0.7 0.3 0.6";
   colors[1]     = "0.3 0.3 0.5 0";
   sizes[0]      = 2;
   sizes[1]      = 6;
};

datablock ParticleEmitterData(WalkerJetEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 25;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 15;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "WalkerJetParticle";
};

datablock DebrisData(WAutoShellDebris)
{
   shapeName = "weapon_missile_casement.dts";

   lifetime = 5.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.5;
   friction = 0.2;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

datablock PlayerData(FuryWalkerBase) : WalkerDamageProfile
{
   emap = true;

   armorType = $ArmorType::Walker;
   armorMask = $ArmorMask::Fury;
   armorClass = $ArmorClassMask::Walker;
   armorFlags = $ArmorFlags::ExplodeOnDeath | $ArmorFlags::NoDeathFiring | $ArmorFlags::NoFlagTouching;
   enhancementMultiplier = 1.0;
   enhancementSlots = 4;

   // mech armor specials and enhancements? maybe
   armorSpecialCount = 0;
   armorSpecial[0] = "TargetingLaserFakeImage";
   armorSpecial[1] = "ArmorSpecialImage";
   
   className = Armor;
   shapeFile = "TR2medium_male.dts";
   cameraMaxDist = 12;
   computeCRC = true;

   debrisShapeName = "debris_player.dts";
   debris = WalkerDebris;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle.png";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 1249;
   drag = 0.5;
   maxdrag = 0.6;
   density = 10;
   maxDamage = 5.0;
   maxEnergy = 110;
   repairRate = 0.0011;
   energyPerDamagePoint = 25.0; // shield energy required to block one point of damage

   rechargeRate = 0.384;
   hasJumpJets = false;
   jetForce = (21.4 * 182) * 11.1;
   underwaterJetForce = (21.4 * 182) * 13;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = 2.0;
   underwaterJetEnergyDrain =  1.85;
   minJetEnergy = 751;
   maxJetHorizontalPercentage = 0.8;

   runForce = (35 * 210) * 4;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 27;
   maxBackwardSpeed = 18;
   maxSideSpeed = 9;

   maxUnderwaterForwardSpeed = 18;
   maxUnderwaterBackwardSpeed = 9;
   maxUnderwaterSideSpeed = 4.5;

   recoverDelay = 6;
   recoverRunForceScale = 1.2;

   jumpForce = (9.25 * 215) * 10;
   jumpEnergyDrain = 0;
   minJumpEnergy = 65535;
   jumpDelay = 0;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 0.01; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 10; // takes 3.0 seconds of constant jet to get full heat sig.

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 55;
   jumpSurfaceAngle = 55;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;

   horizMaxSpeed = 1000;
   horizResistSpeed = 20;
   horizResistFactor = 0.3;
   maxJetForwardSpeed = 8;

   upMaxSpeed = 60;
   upResistSpeed = 35;
   upResistFactor = 0.15;

   minImpactSpeed = 60;
   speedDamageScale = 0.006;

   jetSound = BomberFlyerThrustSound;
   wetJetSound = AssaultVehicleThrustSound;
   jetEmitter = WalkerJetEmitter;

   boundingBox = "2.9 2.3 5.2";
   pickupRadius = 1.5; //0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = MediumMaleFootprint;
   decalOffset = 0.4;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootHeavySoftSound;
   RFootSoftSound       = RFootHeavySoftSound;
   LFootHardSound       = LFootHeavyHardSound;
   RFootHardSound       = RFootHeavyHardSound;
   LFootMetalSound      = LFootHeavyMetalSound;
   RFootMetalSound      = RFootHeavyMetalSound;
   LFootSnowSound       = LFootHeavySnowSound;
   RFootSnowSound       = RFootHeavySnowSound;
   LFootShallowSound    = LFootHeavyShallowSplashSound;
   RFootShallowSound    = RFootHeavyShallowSplashSound;
   LFootWadingSound     = LFootHeavyWadingSound;
   RFootWadingSound     = RFootHeavyWadingSound;
   LFootUnderwaterSound = LFootHeavyUnderwaterSound;
   RFootUnderwaterSound = RFootHeavyUnderwaterSound;
   LFootBubblesSound    = LFootHeavyBubblesSound;
   RFootBubblesSound    = RFootHeavyBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactHeavySoftSound;
   impactHardSound      = ImpactHeavyHardSound;
   impactMetalSound     = ImpactHeavyMetalSound;
   impactSnowSound      = ImpactHeavySnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactHeavyWaterEasySound;
   impactWaterMedium    = ImpactHeavyWaterMediumSound;
   impactWaterHard      = ImpactHeavyWaterHardSound;

   groundImpactMinSpeed    = 20.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterHeavySound;

   maxWeapons = 6;           // Max number of different weapons the mech can have
   maxGrenades = 0;
   maxMines = 0;

   // Inventory restrictions
   max[RepairKit]          = 1000;
   max[Beacon]             = 1000;
   max[Mine]               = 1000;
   max[Grenade]            = 1000;

//   max[TargetingLaser]     = 1;

   observeParameters = "1.0 12.0 12.0";
   shieldEffectScale = "1.0 1.0 1.0";
};

datablock PlayerData(FuryWalkerJet) : FuryWalkerBase
{
   hasJumpJets = true;
   
   jetForce = (12.5 * FuryWalkerBase.mass) * 3;
   underwaterJetForce = (16.4 * FuryWalkerBase.mass) * 4;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = 2.0;
   underwaterJetEnergyDrain = 1.85;
   minJetEnergy = 2;
   maxJetHorizontalPercentage = 0.4;

   horizMaxSpeed = 100;
   horizResistSpeed = 24;
   horizResistFactor = 0.275;
   maxJetForwardSpeed = 12;

   upMaxSpeed = 60;
   upResistSpeed = 35;
   upResistFactor = 0.125;
};

datablock PlayerData(FuryWalkerSpeed) : FuryWalkerBase
{
   runForce = (35 * 210) * 4;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   
   maxForwardSpeed = 40.5;
   maxBackwardSpeed = 27;
   maxSideSpeed = 9;

   maxUnderwaterForwardSpeed = 27;
   maxUnderwaterBackwardSpeed = 13.5;
   maxUnderwaterSideSpeed = 4;
};

datablock PlayerData(StormguardWalkerBase) : WalkerDamageProfile
{
   emap = true;

   armorType = $ArmorType::Walker;
   armorMask = $ArmorMask::Stormguard;
   armorClass = $ArmorClassMask::Walker;
   armorFlags = $ArmorFlags::ExplodeOnDeath | $ArmorFlags::NoDeathFiring | $ArmorFlags::NoFlagTouching;
   enhancementMultiplier = 1.0;
   enhancementSlots = 5;

   // mech armor specials and enhancements? maybe
   armorSpecialCount = 0;
   armorSpecial[0] = "TargetingLaserFakeImage";
   armorSpecial[1] = "ArmorSpecialImage";

   className = Armor;
   shapeFile = "TR2heavy_male.dts";
   cameraMaxDist = 12;
   computeCRC = true;

   debrisShapeName = "debris_player.dts";
   debris = WalkerDebris;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle.png";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 1911;
   drag = 0.5;
   maxdrag = 0.6;
   density = 10;
   maxDamage = 15.0;
   maxEnergy = 300;
   repairRate = 0.0011;
   energyPerDamagePoint = 25.0;

   rechargeRate = 0.625;
   hasJumpJets = false;
   jetForce = (21.4 * 182) * 11.1;
   underwaterJetForce = (21.4 * 182) * 13;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = (6 / 32) * 1.359;
   underwaterJetEnergyDrain =  (6 / 32) * 1.25;
   minJetEnergy = 65536;
   maxJetHorizontalPercentage = 0.8;

   runForce = (35 * 210) * 5;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 15;
   maxBackwardSpeed = 10;
   maxSideSpeed = 5;

   maxUnderwaterForwardSpeed = 10;
   maxUnderwaterBackwardSpeed = 5;
   maxUnderwaterSideSpeed = 2.5;

   recoverDelay = 6;
   recoverRunForceScale = 1.2;

   jumpForce = (9.25 * 215) * 10;
   jumpEnergyDrain = 0;
   minJumpEnergy = 65535;
   jumpDelay = 0;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 0.01; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 10; // takes 3.0 seconds of constant jet to get full heat sig.

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 55;
   jumpSurfaceAngle = 55;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;

   horizMaxSpeed = 1000;
   horizResistSpeed = 20;
   horizResistFactor = 0.3;
   maxJetForwardSpeed = 8;

   upMaxSpeed = 60;
   upResistSpeed = 35;
   upResistFactor = 0.15;

   minImpactSpeed = 60;
   speedDamageScale = 0.006;

   jetSound = HAPCFlyerThrustSound;
   wetJetSound = AssaultVehicleThrustSound;
   jetEmitter = WalkerJetParticle;

   boundingBox = "6.2 6.2 9.0";
   pickupRadius = 1.5; //0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = HeavyMaleFootprint;
   decalOffset = 0.4;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootHeavySoftSound;
   RFootSoftSound       = RFootHeavySoftSound;
   LFootHardSound       = LFootHeavyHardSound;
   RFootHardSound       = RFootHeavyHardSound;
   LFootMetalSound      = LFootHeavyMetalSound;
   RFootMetalSound      = RFootHeavyMetalSound;
   LFootSnowSound       = LFootHeavySnowSound;
   RFootSnowSound       = RFootHeavySnowSound;
   LFootShallowSound    = LFootHeavyShallowSplashSound;
   RFootShallowSound    = RFootHeavyShallowSplashSound;
   LFootWadingSound     = LFootHeavyWadingSound;
   RFootWadingSound     = RFootHeavyWadingSound;
   LFootUnderwaterSound = LFootHeavyUnderwaterSound;
   RFootUnderwaterSound = RFootHeavyUnderwaterSound;
   LFootBubblesSound    = LFootHeavyBubblesSound;
   RFootBubblesSound    = RFootHeavyBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactHeavySoftSound;
   impactHardSound      = ImpactHeavyHardSound;
   impactMetalSound     = ImpactHeavyMetalSound;
   impactSnowSound      = ImpactHeavySnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactHeavyWaterEasySound;
   impactWaterMedium    = ImpactHeavyWaterMediumSound;
   impactWaterHard      = ImpactHeavyWaterHardSound;

   groundImpactMinSpeed    = 20.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterHeavySound;

   maxWeapons = 6;           // Max number of different weapons the mech can have
   maxGrenades = 0;
   maxMines = 0;

   // Inventory restrictions
   max[RepairKit]          = 1000;
   max[Beacon]             = 1000;
   max[Mine]               = 1000;
   max[Grenade]            = 1000;

   observeParameters = "1.0 12.0 12.0";
   shieldEffectScale = "1.0 1.0 1.0";
};

datablock PlayerData(StormguardWalkerSpeed) : StormguardWalkerBase
{
   runForce = (35 * 210) * 5;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   
   maxForwardSpeed = 22.5;
   maxBackwardSpeed = 15;
   maxSideSpeed = 5;

   maxUnderwaterForwardSpeed = 15;
   maxUnderwaterBackwardSpeed = 7.5;
   maxUnderwaterSideSpeed = 2.5;
};
