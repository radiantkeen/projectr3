// Datablock cache for walker weapon slots

// Stormguard
//------------------------------------------------------------------------------
datablock ShapeBaseImageData(StormguardEnergy2)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
   mountPoint = 0;
   subImage = true;

   usesEnergy = true;
   minEnergy = -1;
   fireEnergy = -1;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateTimeoutValue[0]        = 0.01;
   stateTransitionOnTimeout[0] = "Ready";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Fire";
   //--------------------------------------
   stateName[2]             = "Fire";
   stateFire[2]             = true;
   stateSequence[2]         = "Fire";
   stateTimeoutValue[2]          = 0.1;
   stateTransitionOnTriggerUp[2] = "Ready";
   stateTransitionOnTimeout[2] = "Fire";
};

datablock ShapeBaseImageData(StormguardEnergy3) : StormguardEnergy2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardEnergy6) : StormguardEnergy2
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardLaser2) : StormguardEnergy2
{
   shapeFile = "TR2weapon_shocklance.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardLaser3) : StormguardLaser2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardLaser6) : StormguardLaser2
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardCannon2) : StormguardEnergy2
{
   shapeFile = "turret_mortar_large.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardCannon3) : StormguardCannon2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardCannon6) : StormguardCannon2
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardPlasma2) : StormguardEnergy2
{
   shapeFile = "TR2weapon_mortar.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardPlasma3) : StormguardPlasma2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardPlasma6) : StormguardPlasma2
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardAC2) : StormguardEnergy2
{
   shapeFile = "TR2weapon_chaingun.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";

   casing              = WAutoShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 6.0;
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateTimeoutValue[0]        = 0.01;
   stateTransitionOnTimeout[0] = "Ready";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSequence[1]         = "Spin";
   stateSpinThread[1]         = Stop;
   stateTransitionOnTriggerDown[1] = "Fire";
   //--------------------------------------
   stateName[2]             = "Fire";
   stateFire[2]             = true;
   stateSpinThread[2]       = FullSpeed;
   stateEjectShell[2]       = true;
   stateSequenceRandomFlash[2] = true;
   stateSequence[2]         = "Fire_Vis";
   stateTimeoutValue[2]          = 0.1;
   stateTransitionOnTriggerUp[2] = "Ready";
   stateTransitionOnTimeout[2] = "Fire";
};

datablock ShapeBaseImageData(StormguardAC3) : StormguardAC2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardAC6) : StormguardAC2
{
   shapeFile = "turret_tank_barrelchain.dts";
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 90";
};

datablock ShapeBaseImageData(StormguardMainCannon2) : StormguardEnergy2
{
   shapeFile = "turret_tank_barrelmortar.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardMainCannon3) : StormguardMainCannon2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0" ;
};

datablock ShapeBaseImageData(StormguardMainCannon6) : StormguardMainCannon2
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0" ;
};

datablock ShapeBaseImageData(StormguardMissile4) : StormguardEnergy2
{
   shapeFile = "stackable1s.dts";
   offset = $WalkerHardpointPos["Stormguard", 4];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardMissile5) : StormguardMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 5];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardMissile6) : StormguardMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardLightMissile4) : StormguardEnergy2
{
   shapeFile = "turret_missile_large.dts";
   offset = $WalkerHardpointPos["Stormguard", 4];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardLightMissile5) : StormguardLightMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 5];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardLightMissile6) : StormguardLightMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardClusterMissile4) : StormguardEnergy2
{
   shapeFile = "stackable2m.dts";
   offset = $WalkerHardpointPos["Stormguard", 4];
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(StormguardClusterMissile5) : StormguardClusterMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 5];
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(StormguardClusterMissile6) : StormguardClusterMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "1 0 0 90";
};

// Fury
//------------------------------------------------------------------------------
datablock ShapeBaseImageData(FuryEnergy2)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   offset = $WalkerHardpointPos["Fury", 2];
   rotation = "0 1 0 0";
   mountPoint = 0;
   subImage = true;
   
   usesEnergy = true;
   minEnergy = -1;
   fireEnergy = -1;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateTimeoutValue[0]        = 0.01;
   stateTransitionOnTimeout[0] = "Ready";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Fire";
   //--------------------------------------
   stateName[2]             = "Fire";
   stateFire[2]             = true;
   stateSequence[2]         = "Fire";
   stateTimeoutValue[2]          = 0.1;
   stateTransitionOnTriggerUp[2] = "Ready";
   stateTransitionOnTimeout[2] = "Fire";
};

datablock ShapeBaseImageData(FuryEnergy3) : FuryEnergy2
{
   offset = $WalkerHardpointPos["Fury", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(FuryCannon2) : FuryEnergy2
{
   shapeFile = "turret_mortar_large.dts";
   offset = $WalkerHardpointPos["Fury", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(FuryCannon3) : FuryCannon2
{
   offset = $WalkerHardpointPos["Fury", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(FuryPlasma2) : FuryEnergy2
{
   shapeFile = "TR2weapon_mortar.dts";
   offset = $WalkerHardpointPos["Fury", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(FuryPlasma3) : FuryPlasma2
{
   offset = $WalkerHardpointPos["Fury", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(FuryLaser2) : FuryEnergy2
{
   shapeFile = "TR2weapon_shocklance.dts";
   offset = $WalkerHardpointPos["Fury", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(FuryLaser3) : FuryLaser2
{
   offset = $WalkerHardpointPos["Fury", 3];
   rotation = "0 1 0 0";
};


datablock ShapeBaseImageData(FuryAC2) : FuryEnergy2
{
   shapeFile = "TR2weapon_chaingun.dts";
   offset = $WalkerHardpointPos["Fury", 2];
   rotation = "0 1 0 0";

   casing              = WAutoShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 6.0;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateTimeoutValue[0]        = 0.01;
   stateTransitionOnTimeout[0] = "Ready";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSequence[1]         = "Spin";
   stateSpinThread[1]         = Stop;
   stateTransitionOnTriggerDown[1] = "Fire";
   //--------------------------------------
   stateName[2]             = "Fire";
   stateFire[2]             = true;
   stateSpinThread[2]       = FullSpeed;
   stateEjectShell[2]       = true;
   stateSequenceRandomFlash[2] = true;
   stateSequence[2]         = "Fire_Vis";
   stateTimeoutValue[2]          = 0.1;
   stateTransitionOnTriggerUp[2] = "Ready";
   stateTransitionOnTimeout[2] = "Fire";
};

datablock ShapeBaseImageData(FuryAC3) : FuryAC2
{
   offset = $WalkerHardpointPos["Fury", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(FuryMainCannon2) : FuryEnergy2
{
   shapeFile = "turret_tank_barrelmortar.dts";
   offset = $WalkerHardpointPos["Fury", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(FuryMainCannon3) : FuryMainCannon2
{
   offset = $WalkerHardpointPos["Fury", 3];
   rotation = "0 1 0 0" ;
};

datablock ShapeBaseImageData(FuryMissile4) : FuryEnergy2
{
   shapeFile = "stackable1s.dts";
   offset = $WalkerHardpointPos["Fury", 4];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(FuryMissile5) : FuryMissile4
{
   offset = $WalkerHardpointPos["Fury", 5];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(FuryLightMissile4) : FuryEnergy2
{
   shapeFile = "turret_missile_large.dts";
   offset = $WalkerHardpointPos["Fury", 4];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(FuryLightMissile5) : FuryLightMissile4
{
   offset = $WalkerHardpointPos["Fury", 5];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(FuryClusterMissile4) : FuryEnergy2
{
   shapeFile = "stackable2m.dts";
   offset = $WalkerHardpointPos["Fury", 4];
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(FuryClusterMissile5) : FuryClusterMissile4
{
   offset = $WalkerHardpointPos["Fury", 5];
   rotation = "1 0 0 90";
};
