// Default Damage Profiles
datablock SimDataBlock(UniversalDamageProfile)
{
   shieldDamageScale[$DamageType::Blaster] 			= 1.0;
   shieldDamageScale[$DamageType::Bullet] 			= 1.0;
   shieldDamageScale[$DamageType::ELF] 				= 1.0;
   shieldDamageScale[$DamageType::ShockLance] 		= 1.0;
   shieldDamageScale[$DamageType::Laser] 			= 1.0;
   shieldDamageScale[$DamageType::ShrikeBlaster] 	= 1.0;
   shieldDamageScale[$DamageType::BellyTurret] 		= 1.0;
   shieldDamageScale[$DamageType::AATurret] 		= 1.0;
   shieldDamageScale[$DamageType::IndoorDepTurret] 	= 1.0;
   shieldDamageScale[$DamageType::OutdoorDepTurret] = 1.0;
   shieldDamageScale[$DamageType::SentryTurret] 	= 1.0;
   shieldDamageScale[$DamageType::Disc] 			= 1.0;
   shieldDamageScale[$DamageType::Grenade] 			= 1.0;
   shieldDamageScale[$DamageType::Mine] 			= 1.0;
   shieldDamageScale[$DamageType::Missile] 			= 1.0;
   shieldDamageScale[$DamageType::Mortar] 			= 1.0;
   shieldDamageScale[$DamageType::Plasma] 			= 1.0;
   shieldDamageScale[$DamageType::BomberBombs] 		= 1.0;
   shieldDamageScale[$DamageType::TankChaingun] 	= 1.0;
   shieldDamageScale[$DamageType::TankMortar] 		= 1.0;
   shieldDamageScale[$DamageType::MissileTurret] 	= 1.0;
   shieldDamageScale[$DamageType::MortarTurret] 	= 1.0;
   shieldDamageScale[$DamageType::PlasmaTurret] 	= 1.0;
   shieldDamageScale[$DamageType::SatchelCharge] 	= 1.0;
   shieldDamageScale[$DamageType::Default] 			= 1.0;
   shieldDamageScale[$DamageType::Impact] 			= 1.0;
   shieldDamageScale[$DamageType::Ground] 			= 1.0;
   shieldDamageScale[$DamageType::Explosion] 		= 1.0;
   shieldDamageScale[$DamageType::Lightning] 		= 10.0;

   damageScale[$DamageType::Blaster] 				= 1.0;
   damageScale[$DamageType::Bullet] 				= 1.0;
   damageScale[$DamageType::ELF] 					= 1.0;
   damageScale[$DamageType::ShockLance] 			= 1.0;
   damageScale[$DamageType::Laser] 					= 1.0;
   damageScale[$DamageType::ShrikeBlaster] 			= 1.0;
   damageScale[$DamageType::BellyTurret] 			= 1.0;
   damageScale[$DamageType::AATurret] 				= 1.0;
   damageScale[$DamageType::IndoorDepTurret] 		= 1.0;
   damageScale[$DamageType::OutdoorDepTurret] 		= 1.0;
   damageScale[$DamageType::SentryTurret] 			= 1.0;
   damageScale[$DamageType::Disc] 					= 1.0;
   damageScale[$DamageType::Grenade] 				= 1.0;
   damageScale[$DamageType::Mine] 					= 1.0;
   damageScale[$DamageType::Missile] 				= 1.0;
   damageScale[$DamageType::Mortar] 				= 1.0;
   damageScale[$DamageType::Plasma] 				= 1.0;
   damageScale[$DamageType::BomberBombs] 			= 1.0;
   damageScale[$DamageType::TankChaingun] 			= 1.0;
   damageScale[$DamageType::TankMortar] 			= 1.0;
   damageScale[$DamageType::MissileTurret] 			= 1.0;
   damageScale[$DamageType::MortarTurret] 			= 1.0;
   damageScale[$DamageType::PlasmaTurret] 			= 1.0;
   damageScale[$DamageType::SatchelCharge] 			= 1.0;
   damageScale[$DamageType::Default] 				= 1.0;
   damageScale[$DamageType::Impact] 				= 1.0;
   damageScale[$DamageType::Ground] 				= 1.0;
   damageScale[$DamageType::Explosion] 				= 1.0;
   damageScale[$DamageType::Lightning] 				= 10.0;

   damageScale[$DamageType::BlueBlaster] 			= 1.0;
   shieldDamageScale[$DamageType::BlueBlaster] 		= 1.0;
   damageScale[$DamageType::GreenBlaster] 			= 1.0;
   shieldDamageScale[$DamageType::GreenBlaster]		= 1.0;
   damageScale[$DamageType::MitziBlast] 			= 1.0;
   shieldDamageScale[$DamageType::MitziBlast]		= 1.0;
   damageScale[$DamageType::PBW] 			        = 1.0;
   shieldDamageScale[$DamageType::PBW]		        = 1.0;
   damageScale[$DamageType::Sniper] 			    = 1.0;
   shieldDamageScale[$DamageType::Sniper]		    = 1.0;
   damageScale[$DamageType::Annihalator] 			= 1.0;
   shieldDamageScale[$DamageType::Annihalator]		= 1.0;
   damageScale[$DamageType::EMP] 			        = 1.0;
   shieldDamageScale[$DamageType::EMP]		        = 1.0;
   damageScale[$DamageType::Burn] 			        = 1.0;
   shieldDamageScale[$DamageType::Burn]		        = 1.0;
   damageScale[$DamageType::Poison] 			    = 1.0;
   shieldDamageScale[$DamageType::Poison]		    = 1.0;
   damageScale[$DamageType::Flak] 			        = 1.0;
   shieldDamageScale[$DamageType::Flak]		        = 1.0;
   damageScale[$DamageType::AutoCannon] 			= 1.0;
   shieldDamageScale[$DamageType::AutoCannon]		= 1.0;
   damageScale[$DamageType::Flamethrower] 			= 1.0;
   shieldDamageScale[$DamageType::Flamethrower]		= 1.0;
   damageScale[$DamageType::StarHammer] 			= 1.0;
   shieldDamageScale[$DamageType::StarHammer]		= 1.0;
   damageScale[$DamageType::MeteorCannon] 			= 1.0;
   shieldDamageScale[$DamageType::MeteorCannon]		= 1.0;
   damageScale[$DamageType::MagIonReactor] 			= 1.0;
   shieldDamageScale[$DamageType::MagIonReactor]	= 1.0;
   damageScale[$DamageType::Turbocharger] 			= 1.0;
   shieldDamageScale[$DamageType::Turbocharger]		= 1.0;
   damageScale[$DamageType::BulletHE] 			    = 1.0;
   shieldDamageScale[$DamageType::BulletHE]	    	= 1.0;
   damageScale[$DamageType::BulletEM] 	     		= 1.0;
   shieldDamageScale[$DamageType::BulletEM]	    	= 1.0;
   damageScale[$DamageType::OutdoorDepTurretF] 	   	= 1.0;
   shieldDamageScale[$DamageType::OutdoorDepTurretF]= 1.0;
   damageScale[$DamageType::Railgun] 	          	= 1.0;
   shieldDamageScale[$DamageType::Railgun]          = 1.0;
   damageScale[$DamageType::SubspaceMagnet] 	   	= 1.0;
   shieldDamageScale[$DamageType::SubspaceMagnet]   = 1.0;
   damageScale[$DamageType::BulletIN] 	     		= 1.0;
   shieldDamageScale[$DamageType::BulletIN]	    	= 1.0;
   damageScale[$DamageType::StingerRail]     		= 1.0;
   shieldDamageScale[$DamageType::StingerRail]    	= 1.5;
   damageScale[$DamageType::MitziRail]         		= 1.0;
   shieldDamageScale[$DamageType::MitziRail]    	= 1.0;
   damageScale[$DamageType::Bolter]         		= 1.0;
   shieldDamageScale[$DamageType::Bolter]    	    = 1.0;
   damageScale[$DamageType::PlasmaCannon]         	= 1.0;
   shieldDamageScale[$DamageType::PlasmaCannon]    	= 1.0;
   damageScale[$DamageType::MPDisruptor]           	= 1.0;
   shieldDamageScale[$DamageType::MPDisruptor]      = 1.0;
   damageScale[$DamageType::BlasterRifle]         	= 1.0;
   shieldDamageScale[$DamageType::BlasterRifle]    	= 1.0;
   damageScale[$DamageType::PBCDevistator]         	= 1.0;
   shieldDamageScale[$DamageType::PBCDevistator]   	= 1.0;
   damageScale[$DamageType::PBCQuantum]         	= 1.0;
   shieldDamageScale[$DamageType::PBCQuantum]    	= 1.0;
   damageScale[$DamageType::VectorGlitch]         	= 1.0;
   shieldDamageScale[$DamageType::VectorGlitch]    	= 1.0;
   damageScale[$DamageType::SonicPulser]         	= 1.0;
   shieldDamageScale[$DamageType::SonicPulser]    	= 0.0;
   damageScale[$DamageType::RotaryCannon]         	= 1.0;
   shieldDamageScale[$DamageType::RotaryCannon]    	= 1.0;
};

datablock SimDataBlock(ArmorDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::AATurret] 				= 0.5;
   damageScale[$DamageType::Impact] 				= 0.75;
   damageScale[$DamageType::Ground] 				= 0.75;
};

datablock SimDataBlock(VehicleDamageProfile) : UniversalDamageProfile
{
   shieldDamageScale[$DamageType::AATurret] 		= 1.0;

   damageScale[$DamageType::AATurret] 				= 1.0;
   damageScale[$DamageType::Impact] 				= 1.25;
   damageScale[$DamageType::Ground] 				= 1.25;
};

datablock SimDataBlock(VTurretDamageProfile) : UniversalDamageProfile
{
   // Turret ammo here
   max[ChaingunAmmo] = 500;
   max[DiscAmmo] = 75;
   max[SpikeAmmo] = 150;
   max[MissileLauncherAmmo] = 80;
   max[BolterAmmo] = 300;
   max[PlasmaAmmo] = 125;
   max[MortarAmmo] = 60;
   max[GrenadeLauncherAmmo] = 200;
   max[AssaultMortarAmmo] = 180; // hailstorm ammo
   max[BombAmmo] = 60; // bomb ammo
};

//----------------------------------------------------------------------------
// VEHICLE DAMAGE PROFILES
//----------------------------------------------------------------------------

//**** SHRIKE SCOUT FIGHTER ****
datablock SimDataBlock(ShrikeDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning] 				= 10.0;
};

//**** THUNDERSWORD BOMBER ****
datablock SimDataBlock(BomberDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning] 				= 10.0;
};

//**** HAVOC TRANSPORT ****
datablock SimDataBlock(HavocDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning] 				= 10.0;
};

//**** WILDCAT GRAV CYCLE ****
datablock SimDataBlock(WildcatDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning]	= 5.0;
};

//**** BEOWULF TANK ****
datablock SimDataBlock(TankDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning]	= 10.0;
};

//**** JERICHO MPB ****
datablock SimDataBlock(MPBDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning]	= 10.0;
};

//----------------------------------------------------------------------------
// TURRET DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(TurretDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	= 5.0;
};

//----------------------------------------------------------------------------
// STATIC SHAPE DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(StaticShapeDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	= 5.0;
};

//----------------------------------------------------------------------------
// PLAYER DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(LightPlayerDamageProfile) : ArmorDamageProfile
{
   damageScale[$DamageType::Lightning]	=		10.0;
};

datablock SimDataBlock(HeavyPlayerDamageProfile) : ArmorDamageProfile
{
   damageScale[$DamageType::Lightning]	=		1.4;
};
