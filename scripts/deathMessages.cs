// Death Message Setup v2.0

//------------------------------------------------------------------------------
$DamageTypeCount = 0;

// Damage Types are now used as death message sets instead of proper damage types, so they are defined along with their messages
$DamageType::Default = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

/////////////////////////////////////////////////////////////////////////////////////////////////
// %1 = Victim's name                                                                          //
// %2 = Victim's gender (value will be either "him" or "her")                                  //
// %3 = Victim's possessive gender	(value will be either "his" or "her")                      //
// %4 = Killer's name                                                                          //
// %5 = Killer's gender (value will be either "him" or "her")                                  //
// %6 = Killer's possessive gender (value will be either "his" or "her")                       //
// %7 = implement that killed the victim (value is the object number of the bullet, disc, etc) //
/////////////////////////////////////////////////////////////////////////////////////////////////

$DeathMessageCampingCount = 1;
$DeathMessageCamping[0] = '\c0%1 was killed for camping near the Nexus.';
																						   
 //Out of Bounds deaths
$DeathMessageOOBCount = 1;
$DeathMessageOOB[0] = '\c0%1 was killed for loitering outside the mission area.';

$DeathMessageLavaCount = 6;
$DeathMessageLava[0] = '\c0%1\'s last thought before falling into the lava : \'Oops\'.';
$DeathMessageLava[1] = '\c0%1 makes the supreme sacrifice to the lava gods.';
$DeathMessageLava[2] = '\c0%1 looks surprised by the lava - but only briefly.';
$DeathMessageLava[3] = '\c0%1 wimps out by jumping into the lava and trying to make it look like an accident.'; 
$DeathMessageLava[4] = '\c0%1 wanted to experience death by lava first hand.';
$DeathMessageLava[5] = '\c0%1 dives face first into the molten kool-aid... oh wait.';

$DeathMessageLightningCount = 3;
$DeathMessageLightning[0] = '\c0%1 was killed by lightning!';
$DeathMessageLightning[1] = '\c0%1 caught a lightning bolt!';
$DeathMessageLightning[2] = '\c0%1 stuck %3 finger in Mother Nature\'s light socket.';

//these used when a player presses ctrl-k
$DeathMessageSuicideCount = 8;
$DeathMessageSuicide[0] = '\c0%1 blows %3 own head off!';  
$DeathMessageSuicide[1] = '\c0%1 ends it all. Cue violin music.';
$DeathMessageSuicide[2] = '\c0%1 kills %2self.';
$DeathMessageSuicide[3] = '\c0%1 goes for the quick and dirty respawn.';
$DeathMessageSuicide[4] = '\c0%1 self-destructs in a fit of ennui.';
$DeathMessageSuicide[5] = '\c0%1 shows off %3 mad dying skills!';
$DeathMessageSuicide[6] = '\c0%1 wanted to make sure %3 gun was loaded.';
$DeathMessageSuicide[7] = '\c0%1 was last heard shouting "death before dishonor!!".';

$DeathMessageLSSuicideCount = 4;
$DeathMessageLSSuicide[0] = '\c0%1 couldn\'t get to a station in time!';
$DeathMessageLSSuicide[1] = '\c0%1 dies from internal organ failure.';
$DeathMessageLSSuicide[2] = '\c0%1\'s life support system lets %3 down.';
$DeathMessageLSSuicide[3] = '\c0%1 shuffles off %3 mortal coil.';

$DeathMessageVehPadCount = 1;
$DeathMessageVehPad[0] = '\c0%1 got caught in a vehicle\'s spawn field.'; 

$DeathMessageFFPowerupCount = 1;
$DeathMessageFFPowerup[0] = '\c0%1 got caught up in a forcefield during power up.'; 

$DeathMessageRogueMineCount = 1;
$DeathMessageRogueMine[$DamageType::Mine, 0] = '\c0%1 is all mine.';

//These used when a player is run over by a vehicle
$DeathMessageVehicleCount = 5;
$DeathMessageVehicle[0] = '\c0%4 says to %1: "Hey! You scratched my paint job!".';
$DeathMessageVehicle[1] = '\c0%1 acquires that run-down feeling from %4.';
$DeathMessageVehicle[2] = '\c0%4 shows %1 %6 new ride.';
$DeathMessageVehicle[3] = '\c0%1 makes a painfully close examination of %4\'s front bumper.';
$DeathMessageVehicle[4] = '\c0%1\'s messy death leaves a mark on %4\'s vehicle finish.';

$DeathMessageVehicleFriendlyCount = 3;
$DeathMessageVehicleFriendly[0] = '\c0%1 gets in the way of a friendly vehicle.';
$DeathMessageVehicleFriendly[1] = '\c0Sadly, a friendly vehicle turns %1 into roadkill.';
$DeathMessageVehicleFriendly[2] = '\c0%1 becomes an unsightly ornament on a team vehicle\'s hood.';

$DeathMessageVehicleUnmannedCount = 3;
$DeathMessageVehicleUnmanned[0] = '\c0%1 gets in the way of a runaway vehicle.';
$DeathMessageVehicleUnmanned[1] = '\c0An unmanned vehicle kills the pathetic %1.';
$DeathMessageVehicleUnmanned[2] = '\c0%1 is struck down by an empty vehicle.';

//These used when a player is killed by a nearby equipment explosion
$DeathMessageExplosionCount = 3;
$DeathMessageExplosion[0] = '\c0%1 was killed by exploding equipment!';
$DeathMessageExplosion[1] = '\c0%1 stood a little too close to the action!';
$DeathMessageExplosion[2] = '\c0%1 learns how to be collateral damage.';

$DeathMessageTurretSelfKillCount = 3;
$DeathMessageTurretSelfKill[0] = '\c0%1 somehow kills %2self with a turret.';
$DeathMessageTurretSelfKill[1] = '\c0%1 apparently didn\'t know the turret was loaded.';
$DeathMessageTurretSelfKill[2] = '\c0%1 helps his team by killing himself with a turret.';

// Used when no other death messages are defined
$DefaultDeathMessageCount = 3;
$DefaultDeathMessage[0] = '\c0%4 kills %1.';
$DefaultDeathMessage[1] = '\c0%4 murders %1.';
$DefaultDeathMessage[2] = '\c0%4 smacks %1 down.';

//these used when a player kills himself (other than by using ctrl - k)
$DeathMessageSelfKillCount = 5;

//used when a player is killed by a teammate
$DeathMessageTeamKillCount = 1;

//used when a player is killed by a teammate controlling a turret
$DeathMessageCTurretTeamKillCount = 1;

//used when a player is killed by an uncontrolled, friendly turret
$DeathMessageCTurretAccdtlKillCount = 1;

//these messages for owned or controlled turrets and vehicles
$DeathMessageCTurretKillCount = 3;

//These used when an automated turret kills an  enemy player
$DeathMessageTurretKillCount = 3;

//these used when a player is killed by an enemy
$DeathMessageCount = 5;

//------------------------------------------------------------------------------
// Death message blocks
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Disruptor
$DamageType::Disruptor = $DamageTypeCount;
$DamageType::Blaster = $DamageTypeCount; // Legacy compatibility
$DamageTypeText[$DamageTypeCount] = 'Disruptor';

$DeathMessageSelfKill[$DamageType::Disruptor, 0] = '\c0%1 kills %2self.';
$DeathMessageSelfKill[$DamageType::Disruptor, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::Disruptor, 2] = '\c0%1\'s weapon kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::Disruptor, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::Disruptor, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::Disruptor, 0] = '\c0%4 kills %1 with a disruptor.';
$DeathMessage[$DamageType::Disruptor, 1] = '\c0%4 pings %1 to death.';
$DeathMessage[$DamageType::Disruptor, 2] = '\c0%1 gets a pointer in disruptor use from %4.';
$DeathMessage[$DamageType::Disruptor, 3] = '\c0%4 fatally embarrasses %1 with %6 handheld disruptor.';
$DeathMessage[$DamageType::Disruptor, 4] = '\c0%4 unleashes a terminal disruptor barrage into %1.';

$DeathMessageTeamKill[$DamageType::Disruptor, 0] = '\c0%4 TEAMKILLED %1 with a disruptor!';
$DeathMessageCTurretTeamKill[$DamageType::Disruptor, 0] = '\c0%4 TEAMKILLED %1 with a disruptor turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Disruptor, 0] = '\c0%1 got in the way of a friendly disruptor turret!';

$DeathMessageCTurretKill[$DamageType::Disruptor, 0] = '\c0%4 blows away %1 with a disruptor turret.';
$DeathMessageCTurretKill[$DamageType::Disruptor, 1] = '\c0%4 fries %1 with a disruptor turret.';
$DeathMessageCTurretKill[$DamageType::Disruptor, 2] = '\c0%4 lights up %1 with a disruptor turret.';

$DeathMessageTurretKill[$DamageType::Disruptor, 0] = '\c0%1 gets lit up by a disruptor turret.';
$DeathMessageTurretKill[$DamageType::Disruptor, 1] = '\c0%1 gets rained on by a nearby disruptor turret.';
$DeathMessageTurretKill[$DamageType::Disruptor, 2] = '\c0%1 finds %2self on the wrong end of a disruptor turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Plasma
$DamageType::Plasma = $DamageTypeCount;
$DamageType::PlasmaTurret = $DamageTypeCount; // Legacy compatibility
$DamageTypeText[$DamageTypeCount] = 'Plasma';

$DeathMessageSelfKill[$DamageType::Plasma, 0] = '\c0%1 kills %2self with plasma.';
$DeathMessageSelfKill[$DamageType::Plasma, 1] = '\c0%1 turns %2self into plasma-charred briquettes.';
$DeathMessageSelfKill[$DamageType::Plasma, 2] = '\c0%1 swallows a white-hot mouthful of %3 own plasma.';
$DeathMessageSelfKill[$DamageType::Plasma, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::Plasma, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessage[$DamageType::Plasma, 0] = '\c0%4 roasts %1 with the plasma rifle.';
$DeathMessage[$DamageType::Plasma, 1] = '\c0%4 asks %1: "Need a light?"';
$DeathMessage[$DamageType::Plasma, 2] = '\c0%4 entices %1 to try a faceful of plasma.';
$DeathMessage[$DamageType::Plasma, 3] = '\c0%4 introduces %1 to the plasma immolation dance.';
$DeathMessage[$DamageType::Plasma, 4] = '\c0%4 places the Hot Kiss of Death on %1.';

$DeathMessageTeamKill[$DamageType::Plasma, 0] = '\c0%4 TEAMKILLED %1 with Plasma!';
$DeathMessageCTurretTeamKill[$DamageType::Plasma, 0] = '\c0%4 TEAMKILLED %1 with a Plasma turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Plasma, 0] = '\c0%1 got in the way of a friendly Plasma turret!';

$DeathMessageCTurretKill[$DamageType::Plasma, 0] = '\c0%4 converts %1 into a living bonfire.';
$DeathMessageCTurretKill[$DamageType::Plasma, 1] = '\c0%4 gave %1 an ashen look.';
$DeathMessageCTurretKill[$DamageType::Plasma, 2] = '\c0%4 burns and fries %1 with the power of Plasma.';

$DeathMessageTurretKill[$DamageType::Plasma, 0] = '\c0%1 is killed by a plasma turret.';
$DeathMessageTurretKill[$DamageType::Plasma, 1] = '\c0%1\'s body now marks the location of a plasma turret.';
$DeathMessageTurretKill[$DamageType::Plasma, 2] = '\c0%1 is fried by a plasma turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Vulcan
$DamageType::Vulcan = $DamageTypeCount;
$DamageType::Bullet = $DamageTypeCount; // legacy
$DamageTypeText[$DamageTypeCount] = 'Vulcan';

$DeathMessageSelfKill[$DamageType::Vulcan, 0] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Vulcan, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::Vulcan, 2] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::Vulcan, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::Vulcan, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::Vulcan, 0] = '\c0%4 rips %1 up with the vulcan.';
$DeathMessage[$DamageType::Vulcan, 1] = '\c0%4 happily chews %1 into pieces with %6 vulcan.';
$DeathMessage[$DamageType::Vulcan, 2] = '\c0%4 administers a dose of Vitamin Lead to %1.';
$DeathMessage[$DamageType::Vulcan, 3] = '\c0%1 suffers a serious hosing from %4\'s vulcan.';
$DeathMessage[$DamageType::Vulcan, 4] = '\c0%4 bestows the blessings of %6 vulcan on %1.';

$DeathMessageTeamKill[$DamageType::Vulcan, 0] = '\c0%4 TEAMKILLED %1 with a vulcan!';
$DeathMessageCTurretTeamKill[$DamageType::Vulcan, 0] = '\c0%4 TEAMKILLED %1 with a vulcan turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Vulcan, 0] = '\c0%1 got in the way of a friendly vulcan turret!';

$DeathMessageCTurretKill[$DamageType::Vulcan, 0] = '\c0%1 enjoys the rich, metallic taste of %4\'s vulcan bullet.';
$DeathMessageCTurretKill[$DamageType::Vulcan, 1] = '\c0%4\'s vulcan turret plays sweet music all over %1.';
$DeathMessageCTurretKill[$DamageType::Vulcan, 2] = '\c0%1 receives a stellar exit wound from %4\'s vulcan bullet.';

$DeathMessageTurretKill[$DamageType::Vulcan, 0] = '\c0%1 gets chunked apart by a vulcan turret.';
$DeathMessageTurretKill[$DamageType::Vulcan, 1] = '\c0%1 gets rained on by a nearby vulcan turret.';
$DeathMessageTurretKill[$DamageType::Vulcan, 2] = '\c0%1 finds %2self on the wrong end of a vulcan turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Disc
$DamageType::Disc = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Disc';

$DeathMessageSelfKill[$DamageType::Disc, 0] = '\c0%1 kills %2self with a disc.';
$DeathMessageSelfKill[$DamageType::Disc, 1] = '\c0%1 catches %3 own disc.';
$DeathMessageSelfKill[$DamageType::Disc, 2] = '\c0%1 heroically falls on %3 own disc.';
$DeathMessageSelfKill[$DamageType::Disc, 3] = '\c0%1 helpfully jumps into %3 own disc\'s explosion.';
$DeathMessageSelfKill[$DamageType::Disc, 4] = '\c0%1 plays Russian roulette with %3 disc launcher.';

$DeathMessage[$DamageType::Disc, 0] = '\c0%4 demolishes %1 with the disc launcher.';
$DeathMessage[$DamageType::Disc, 1] = '\c0%4 serves %1 a blue plate special.';
$DeathMessage[$DamageType::Disc, 2] = '\c0%4 shares a little blue friend with %1.';
$DeathMessage[$DamageType::Disc, 3] = '\c0%4 plays skeet shoot with %1.';
$DeathMessage[$DamageType::Disc, 4] = '\c0%1 becomes one of %4\'s greatest hits.';

$DeathMessageTeamKill[$DamageType::Disc, 0] = '\c0%4 TEAMKILLED %1 with a Disc!';
$DeathMessageCTurretTeamKill[$DamageType::Disc, 0] = '\c0%4 TEAMKILLED %1 with a Disc turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Disc, 0] = '\c0%1 got in the way of a friendly Disc turret!';

$DeathMessageCTurretKill[$DamageType::Disc, 0] = '\c0%4 plays skeet shoot with %1.';
$DeathMessageCTurretKill[$DamageType::Disc, 1] = '\c0%4 serves %1 a blue plate special.';
$DeathMessageCTurretKill[$DamageType::Disc, 2] = '\c0%4 demolishes %1 with the disc launcher.';

$DeathMessageTurretKill[$DamageType::Disc, 0] = '\c0%1 is killed by a disc turret.';
$DeathMessageTurretKill[$DamageType::Disc, 1] = '\c0%1\'s body now marks the location of a disc turret.';
$DeathMessageTurretKill[$DamageType::Disc, 2] = '\c0%1 is blown away by a disc turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Grenade
$DamageType::Grenade = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Grenade';

$DeathMessageSelfKill[$DamageType::Grenade, 0] = '\c0%1 kills %2self with a HE grenade.';
$DeathMessageSelfKill[$DamageType::Grenade, 1] = '\c0%1 catches %3 own \'nade.';
$DeathMessageSelfKill[$DamageType::Grenade, 2] = '\c0%1\'s own grenade turns on %2.';
$DeathMessageSelfKill[$DamageType::Grenade, 3] = '\c0%1 helpfully jumps into %3 own grenade\'s explosion.';
$DeathMessageSelfKill[$DamageType::Grenade, 4] = '\c0%1 pulled the pin a shade early.';

$DeathMessage[$DamageType::Grenade, 0] = '\c0%4 eliminates %1 with a HE grenade.';
$DeathMessage[$DamageType::Grenade, 1] = '\c0%1 swallows %4\'s HE grenade and promptly explodes!';
$DeathMessage[$DamageType::Grenade, 2] = '\c0%1 gets annihilated by %4\'s HE grenade.';
$DeathMessage[$DamageType::Grenade, 3] = '\c0%1 receives a kaboom lesson from %4.';
$DeathMessage[$DamageType::Grenade, 4] = '\c0%4 turns %1 into grenade salad.';

$DeathMessageTeamKill[$DamageType::Grenade, 0] = '\c0%4 TEAMKILLED %1 with a Grenade!';
$DeathMessageCTurretTeamKill[$DamageType::Grenade, 0] = '\c0%4 TEAMKILLED %1 with a grenade launcher turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Grenade, 0] = '\c0%1 got in the way of a friendly grenade launcher turret!';

$DeathMessageCTurretKill[$DamageType::Grenade, 0] = '\c0%4 shoves a HE grenade down %1\'s throat.';
$DeathMessageCTurretKill[$DamageType::Grenade, 1] = '\c0%4 throws %1 a HE grenade and shouts "CATCH"!';
$DeathMessageCTurretKill[$DamageType::Grenade, 2] = '\c0%4 demolishes %1 a grenade launcher turret.';

$DeathMessageTurretKill[$DamageType::Grenade, 0] = '\c0%1 is killed by a flak turret.';
$DeathMessageTurretKill[$DamageType::Grenade, 1] = '\c0%1\'s body now marks the location of a flak turret.';
$DeathMessageTurretKill[$DamageType::Grenade, 2] = '\c0%1 is blown away by a flak turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Laser
$DamageType::Laser = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Laser';

$DeathMessageSelfKill[$DamageType::Laser, 0] = '\c0%1 amazingly bends spacetime to kill %2self with a Laser.';
$DeathMessageSelfKill[$DamageType::Laser, 1] = '\c0%1 amazingly bends spacetime to kill %2self with a Laser.';
$DeathMessageSelfKill[$DamageType::Laser, 2] = '\c0%1 amazingly bends spacetime to kill %2self with a Laser.';
$DeathMessageSelfKill[$DamageType::Laser, 3] = '\c0%1 amazingly bends spacetime to kill %2self with a Laser.';
$DeathMessageSelfKill[$DamageType::Laser, 4] = '\c0%1 amazingly bends spacetime to kill %2self with a Laser.';

$DeathMessage[$DamageType::Laser, 0] = '\c0%4 sets %1 ablaze with a well placed laser pulse.';
$DeathMessage[$DamageType::Laser, 1] = '\c0%4 picks off %1 with %6 laser.';
$DeathMessage[$DamageType::Laser, 2] = '\c0%4 holds %1 down under a magnifying glass under the sun.';
$DeathMessage[$DamageType::Laser, 3] = '\c0%1 finally realized what that red dot was.';
$DeathMessage[$DamageType::Laser, 4] = '\c0%4 plays laser light tag with %1.';

$DeathMessageTeamKill[$DamageType::Laser, 0] = '\c0%4 TEAMKILLED %1 with a Laser!';
$DeathMessageCTurretTeamKill[$DamageType::Laser, 0] = '\c0%4 TEAMKILLED %1 with a Laser turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Laser, 0] = '\c0%1 got in the way of a friendly Laser turret!';

$DeathMessageCTurretKill[$DamageType::Laser, 0] = '\c0%1 is instantly vaporized by %4\'s Laser.';
$DeathMessageCTurretKill[$DamageType::Laser, 1] = '\c0%4\'s deadly aim with the Laser singes %1.';
$DeathMessageCTurretKill[$DamageType::Laser, 2] = '\c0%4\'s Laser torches off %1\'s hair.';

$DeathMessageTurretKill[$DamageType::Laser, 0] = '\c0%1 is killed by a laser turret.';
$DeathMessageTurretKill[$DamageType::Laser, 1] = '\c0%1\'s body now marks the location of a laser turret.';
$DeathMessageTurretKill[$DamageType::Laser, 2] = '\c0%1 is fried by a laser turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// ELF
$DamageType::ELF = $DamageTypeCount;
$DamageType::ELFTurret = $DamageTypeCount; // Legacy
$DamageTypeText[$DamageTypeCount] = 'Arc Welder';

$DeathMessageSelfKill[$DamageType::ELF, 0] = '\c0%1 ELFs %2self with the ELF.';
$DeathMessageSelfKill[$DamageType::ELF, 1] = '\c0%1 ELFs %2self with the ELF.';
$DeathMessageSelfKill[$DamageType::ELF, 2] = '\c0%1 ELFs %2self with the ELF.';
$DeathMessageSelfKill[$DamageType::ELF, 3] = '\c0%1 ELFs %2self with the ELF.';
$DeathMessageSelfKill[$DamageType::ELF, 4] = '\c0%1 ELFs %2self with the ELF.';

$DeathMessage[$DamageType::Elf, 0] = '\c0%4 fries %1 with the arc welder.';
$DeathMessage[$DamageType::Elf, 1] = '\c0%4 bug zaps %1 with %6 arc welder.';
$DeathMessage[$DamageType::Elf, 2] = '\c0%1 learns the shocking truth about %4\'s arc welder skills.';
$DeathMessage[$DamageType::Elf, 3] = '\c0%4 electrocutes %1 without a sponge.';
$DeathMessage[$DamageType::Elf, 4] = '\c0%4\'s arc welder leaves %1 a crispy critter.';

$DeathMessageTeamKill[$DamageType::ELF, 0] = '\c0%4 TEAMKILLED %1 with an Arc Welder!';
$DeathMessageCTurretTeamKill[$DamageType::ELF, 0] = '\c0%4 TEAMKILLED %1 with an Arc Welder turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::ELF, 0] = '\c0%1 got in the way of a friendly Arc Welder turret!';

$DeathMessageCTurretKill[$DamageType::ELF, 0] = '\c0%1 gets zapped by Arc Welder gunner %4.';
$DeathMessageCTurretKill[$DamageType::ELF, 1] = '\c0%1 gets barbecued by Arc Welder gunner %4.';
$DeathMessageCTurretKill[$DamageType::ELF, 2] = '\c0%1 gets shocked by Arc Welder gunner %4.';

$DeathMessageTurretKill[$DamageType::ELF, 0] = '\c0%1 is killed by an Arc Welder turret.';
$DeathMessageTurretKill[$DamageType::ELF, 1] = '\c0%1 is zapped by an Arc Welder turret.';
$DeathMessageTurretKill[$DamageType::ELF, 2] = '\c0%1 is short-circuited by an Arc Welder turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Mortar
$DamageType::Mortar = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Mortar';

$DeathMessageSelfKill[$DamageType::Mortar, 0] = '\c0%1 kills %2self with a mortar!';
$DeathMessageSelfKill[$DamageType::Mortar, 1] = '\c0%1 hugs %3 own big boomie.';
$DeathMessageSelfKill[$DamageType::Mortar, 2] = '\c0%1 mortars %2self all over the map.';
$DeathMessageSelfKill[$DamageType::Mortar, 3] = '\c0%1 experiences %3 mortar\'s payload up close.';
$DeathMessageSelfKill[$DamageType::Mortar, 4] = '\c0%1 suffered the wrath of %3 own mortar.';

$DeathMessage[$DamageType::Mortar, 0] = '\c0%4 obliterates %1 with the mortar.';
$DeathMessage[$DamageType::Mortar, 1] = '\c0%4 drops a mortar round right in %1\'s lap.';
$DeathMessage[$DamageType::Mortar, 2] = '\c0%4 delivers a mortar payload straight to %1.';
$DeathMessage[$DamageType::Mortar, 3] = '\c0%4 offers a little "heavy love" to %1.';
$DeathMessage[$DamageType::Mortar, 4] = '\c0%1 stumbles into %4\'s mortar reticle.';

$DeathMessageTeamKill[$DamageType::Mortar, 0] = '\c0%4 TEAMKILLED %1 with a Mortar!';
$DeathMessageCTurretTeamKill[$DamageType::Mortar, 0] = '\c0%4 TEAMKILLED %1 with a Mortar turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Mortar, 0] = '\c0%1 got in the way of a friendly Mortar turret!';

$DeathMessageCTurretKill[$DamageType::Mortar, 0] = '\c0Whoops! %1 + %4\'s mortar = Dead %1.';
$DeathMessageCTurretKill[$DamageType::Mortar, 1] = '\c0%1 learns the happy explosion dance from %4\'s mortar.';
$DeathMessageCTurretKill[$DamageType::Mortar, 2] = '\c0%4\'s mortar has a blast with %1.';

$DeathMessageTurretKill[$DamageType::Mortar, 0] = '\c0%1 is pureed by a mortar turret.';
$DeathMessageTurretKill[$DamageType::Mortar, 1] = '\c0%1 enjoys a mortar turret\'s attention.';
$DeathMessageTurretKill[$DamageType::Mortar, 2] = '\c0%1 is blown to kibble by a mortar turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Missile
$DamageType::Missile = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Missile';

$DeathMessageSelfKill[$DamageType::Missile, 0] = '\c0%1 kills %2self with a missile!';
$DeathMessageSelfKill[$DamageType::Missile, 1] = '\c0%1 runs a missile up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::Missile, 2] = '\c0%1 tests the missile\'s shaped charge on %2self.';
$DeathMessageSelfKill[$DamageType::Missile, 3] = '\c0%1 achieved missile lock on %2self.';
$DeathMessageSelfKill[$DamageType::Missile, 4] = '\c0%1 gracefully smoked %2self with a missile!';

$DeathMessage[$DamageType::Missile, 0] = '\c0%4 intercepts %1 with a missile.';
$DeathMessage[$DamageType::Missile, 1] = '\c0%4 watches %6 missile touch %1 and go boom.';
$DeathMessage[$DamageType::Missile, 2] = '\c0%4 got sweet tone on %1.';
$DeathMessage[$DamageType::Missile, 3] = '\c0By now, %1 has realized %4\'s missile killed %2.';
$DeathMessage[$DamageType::Missile, 4] = '\c0%4\'s missile rains little pieces of %1 all over the ground.';

$DeathMessageTeamKill[$DamageType::Missile, 0] = '\c0%4 TEAMKILLED %1 with a Missile!';
$DeathMessageCTurretTeamKill[$DamageType::Missile, 0] = '\c0%4 TEAMKILLED %1 with a Missile turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Missile, 0] = '\c0%1 got in the way of a friendly Missile turret!';

$DeathMessageCTurretKill[$DamageType::Missile, 0] = '\c0%4 shows %1 a new world of pain with a missile.';
$DeathMessageCTurretKill[$DamageType::Missile, 1] = '\c0%4 pops %1 with a missile.';
$DeathMessageCTurretKill[$DamageType::Missile, 2] = '\c0%4\'s missile lights up %1\'s, uh, ex-life.';

$DeathMessageTurretKill[$DamageType::Missile, 0] = '\c0%1 is killed by a missile turret.';
$DeathMessageTurretKill[$DamageType::Missile, 1] = '\c0%1 is shot down by a missile turret.';
$DeathMessageTurretKill[$DamageType::Missile, 2] = '\c0%1 is blown away by a missile turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// ShockLance
$DamageType::ShockLance = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'ShockLance';

$DeathMessageSelfKill[$DamageType::ShockLance, 0] = '\c0%1 kills %2self with a ShockLance!';
$DeathMessageSelfKill[$DamageType::ShockLance, 1] = '\c0%1 runs a ShockLance up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::ShockLance, 2] = '\c0%1 tests the ShockLance\'s shaped charge on %2self.';
$DeathMessageSelfKill[$DamageType::ShockLance, 3] = '\c0%1 achieved ShockLance lock on %2self.';
$DeathMessageSelfKill[$DamageType::ShockLance, 4] = '\c0%1 gracefully smoked %2self with a ShockLance!';

$DeathMessage[$DamageType::Shocklance, 0] = '\c0%4 reaps a harvest of %1 with the lightning arc.';
$DeathMessage[$DamageType::Shocklance, 1] = '\c0%4 feeds %1 the business end of %6 lightning arc.';
$DeathMessage[$DamageType::Shocklance, 2] = '\c0%4 stops %1 dead with the lightning arc.';
$DeathMessage[$DamageType::Shocklance, 3] = '\c0%4 eliminates %1 in close combat.';
$DeathMessage[$DamageType::Shocklance, 4] = '\c0%4 ruins %1\'s day with one zap of a lightning arc.';

$DeathMessageTeamKill[$DamageType::ShockLance, 0] = '\c0%4 TEAMKILLED %1 with a lightning arc!';
$DeathMessageCTurretTeamKill[$DamageType::ShockLance, 0] = '\c0%4 TEAMKILLED %1 with a lightning arc turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::ShockLance, 0] = '\c0%1 got in the way of a friendly lightning arc turret!';

$DeathMessageCTurretKill[$DamageType::ShockLance, 0] = '\c0%4 shows %1 a new world of pain with a lightning arc.';
$DeathMessageCTurretKill[$DamageType::ShockLance, 1] = '\c0%4 pops %1 with a lightning arc.';
$DeathMessageCTurretKill[$DamageType::ShockLance, 2] = '\c0%4\'s lightning arc lights up %1\'s, uh, ex-life.';

$DeathMessageTurretKill[$DamageType::ShockLance, 0] = '\c0%1 is killed by a lightning arc turret.';
$DeathMessageTurretKill[$DamageType::ShockLance, 1] = '\c0%1 is shot down by a lightning arc turret.';
$DeathMessageTurretKill[$DamageType::ShockLance, 2] = '\c0%1 is blown away by a lightning arc turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// BomberBombs
$DamageType::BomberBombs = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'BomberBombs';

$DeathMessageSelfKill[$DamageType::BomberBombs, 0] = '\c0%1 kills %2self with a freefall bomb!';
$DeathMessageSelfKill[$DamageType::BomberBombs, 1] = '\c0%1 kills %2self with a freefall bomb!';
$DeathMessageSelfKill[$DamageType::BomberBombs, 2] = '\c0%1 kills %2self with a freefall bomb!';
$DeathMessageSelfKill[$DamageType::BomberBombs, 3] = '\c0%1 kills %2self with a freefall bomb!';
$DeathMessageSelfKill[$DamageType::BomberBombs, 4] = '\c0%1 kills %2self with a freefall bomb!';

$DeathMessage[$DamageType::BomberBombs, 0] = '\c0%4 kills %1 with a freefall bomb.';
$DeathMessage[$DamageType::BomberBombs, 1] = '\c0%4 kills %1 with a freefall bomb.';
$DeathMessage[$DamageType::BomberBombs, 2] = '\c0%4 kills %1 with a freefall bomb.';
$DeathMessage[$DamageType::BomberBombs, 3] = '\c0%4 kills %1 with a freefall bomb.';
$DeathMessage[$DamageType::BomberBombs, 4] = '\c0%4 kills %1 with a freefall bomb.';

$DeathMessageTeamKill[$DamageType::BomberBombs, 0] = '\c0%4 TEAMKILLED %1 with a freefall bomb!';
$DeathMessageCTurretTeamKill[$DamageType::BomberBombs, 0] = '\c0%4 TEAMKILLED %1 with a freefall bomb turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::BomberBombs, 0] = '\c0%1 got in the way of a friendly freefall bomb turret!';

$DeathMessageCTurretKill[$DamageType::BomberBombs, 0] = '\c0%1 catches %4\'s bomb in both teeth.';
$DeathMessageCTurretKill[$DamageType::BomberBombs, 1] = '\c0%4 leaves %1 a smoking bomb crater.';
$DeathMessageCTurretKill[$DamageType::BomberBombs, 2] = '\c0%4 bombs %1 back to the 20th century.';

$DeathMessageTurretKill[$DamageType::BomberBombs, 0] = '\c0%1 catches %4\'s bomb in both teeth.';
$DeathMessageTurretKill[$DamageType::BomberBombs, 1] = '\c0%4 leaves %1 a smoking bomb crater.';
$DeathMessageTurretKill[$DamageType::BomberBombs, 2] = '\c0%4 bombs %1 back to the 20th century.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// SatchelCharge
$DamageType::SatchelCharge = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'SatchelCharge';

$DeathMessageSelfKill[$DamageType::SatchelCharge, 0] = '\c0%1 goes out with a bang!';  //applies to most explosion types
$DeathMessageSelfKill[$DamageType::SatchelCharge, 1] = '\c0%1 blows %2self into tiny bits and pieces.';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 2] = '\c0%1 explodes in that fatal kind of way.';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 3] = '\c0%1 experiences explosive decompression!';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 4] = '\c0%1 splashes all over the map.';

$DeathMessage[$DamageType::SatchelCharge, 0] = '\c0%4 buys %1 a ticket to the moon.';  //satchel charge only
$DeathMessage[$DamageType::SatchelCharge, 1] = '\c0%4 blows %1 into tiny bits.';
$DeathMessage[$DamageType::SatchelCharge, 2] = '\c0%4 makes %1 a hugely explosive offer.';
$DeathMessage[$DamageType::SatchelCharge, 3] = '\c0%4 turns %1 into a cloud of satchel-vaporized armor.';
$DeathMessage[$DamageType::SatchelCharge, 4] = '\c0%4\'s satchel charge leaves %1 nothin\' but smokin\' boots.';

$DeathMessageTeamKill[$DamageType::SatchelCharge, 0] = '\c0%4 TEAMKILLED %1 with a satchel charge!';
$DeathMessageCTurretTeamKill[$DamageType::SatchelCharge, 0] = '\c0%4 TEAMKILLED %1 with a satchel charge turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::SatchelCharge, 0] = '\c0%1 got in the way of a friendly satchel charge turret!';

$DeathMessageCTurretKill[$DamageType::SatchelCharge, 0] = '\c0%1 catches %4\'s satchel charge in both teeth.';
$DeathMessageCTurretKill[$DamageType::SatchelCharge, 1] = '\c0%4 leaves %1 a smoking satchel charge crater.';
$DeathMessageCTurretKill[$DamageType::SatchelCharge, 2] = '\c0%4 satchel charge\'s %1 back to the 20th century.';

$DeathMessageTurretKill[$DamageType::SatchelCharge, 0] = '\c0%1 catches %4\'s satchel charge in both teeth.';
$DeathMessageTurretKill[$DamageType::SatchelCharge, 1] = '\c0%4 leaves %1 a smoking satchel charge crater.';
$DeathMessageTurretKill[$DamageType::SatchelCharge, 2] = '\c0%4 satchel charge\'s %1 back to the 20th century.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// MitziBlast
$DamageType::MitziBlast = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'MitziBlast';

$DeathMessageSelfKill[$DamageType::MitziBlast, 0] = '\c0%1 lets %3 mitzi blast eat %2 alive.';
$DeathMessageSelfKill[$DamageType::MitziBlast, 1] = '\c0%1 was bit by %3 own mitzi blast.';
$DeathMessageSelfKill[$DamageType::MitziBlast, 2] = '\c0%1 vaporizes %2self with a mitzi blast.';
$DeathMessageSelfKill[$DamageType::MitziBlast, 3] = '\c0%1\'s own mitzi blast ran %2 down.';
$DeathMessageSelfKill[$DamageType::MitziBlast, 4] = '\c0%1 could not dodge the mitzi blast in time.';

$DeathMessage[$DamageType::MitziBlast, 0] = '\c0%1 is the victim of %4\'s mitzi blast.';
$DeathMessage[$DamageType::MitziBlast, 1] = '\c0%4 jams some mitzi in %1\'s face.';
$DeathMessage[$DamageType::MitziBlast, 2] = '\c0%4 stops %1 dead with a solid mitzi blast to the chest.';
$DeathMessage[$DamageType::MitziBlast, 3] = '\c0%4 says to %1: "Mitzi. Kills bugs dead!"';
$DeathMessage[$DamageType::MitziBlast, 4] = '\c0%4\'s mitzi gives %1 a bad case of static cling.';

$DeathMessageTeamKill[$DamageType::MitziBlast, 0] = '\c0%4 TEAMKILLED %1 with a mitzi blast!';
$DeathMessageCTurretTeamKill[$DamageType::MitziBlast, 0] = '\c0%4 TEAMKILLED %1 with a mitzi blast turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::MitziBlast, 0] = '\c0%1 got in the way of a friendly mitzi blast turret!';

$DeathMessageCTurretKill[$DamageType::MitziBlast, 0] = '\c0%4 stops %1 dead with a solid mitzi blast to the chest.';
$DeathMessageCTurretKill[$DamageType::MitziBlast, 1] = '\c0%1 is the victim of %4\'s mitzi blast.';
$DeathMessageCTurretKill[$DamageType::MitziBlast, 2] = '\c0%4\'s mitzi gives %1 a bad case of static cling.';

$DeathMessageTurretKill[$DamageType::MitziBlast, 0] = '\c0%1 ends up in the crosshairs of a mitzi blast turret.';
$DeathMessageTurretKill[$DamageType::MitziBlast, 1] = '\c0%1 got goosed by a mitzi blast turret.';
$DeathMessageTurretKill[$DamageType::MitziBlast, 2] = '\c0%1 gets smacked down by a mitzi blast turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// PBC
$DamageType::PBC = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'PBC';

$DeathMessageSelfKill[$DamageType::PBC, 0] = '\c0%1 somehow kills %2self with a PBC.';
$DeathMessageSelfKill[$DamageType::PBC, 1] = '\c0%1 somehow kills %2self with a PBC.';
$DeathMessageSelfKill[$DamageType::PBC, 2] = '\c0%1 somehow kills %2self with a PBC.';
$DeathMessageSelfKill[$DamageType::PBC, 3] = '\c0%1 somehow kills %2self with a PBC.';
$DeathMessageSelfKill[$DamageType::PBC, 4] = '\c0%1 somehow kills %2self with a PBC.';

$DeathMessage[$DamageType::PBC, 0] = '\c0%4 gives %1 the smackdown with %6 particle beam cannon.';
$DeathMessage[$DamageType::PBC, 1] = '\c0%4\'s particle beam cannon punches that fatal hole through %1.';
$DeathMessage[$DamageType::PBC, 2] = '\c0%4\'s particle beam cannon beams %1 well... apart really.';
$DeathMessage[$DamageType::PBC, 3] = '\c0%4 blasts %1 to kibbles \'n bits.';
$DeathMessage[$DamageType::PBC, 4] = '\c0%4 makes %1\'s day with a zap of %6 particle beam cannon.';

$DeathMessageTeamKill[$DamageType::PBC, 0] = '\c0%4 TEAMKILLED %1 with a particle beam cannon!';
$DeathMessageCTurretTeamKill[$DamageType::PBC, 0] = '\c0%4 TEAMKILLED %1 with a particle beam turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::PBC, 0] = '\c0%1 got in the way of a friendly particle beam turret!';

$DeathMessageCTurretKill[$DamageType::PBC, 0] = '\c0%4\'s particle beam cannon punches that fatal hole through %1.';
$DeathMessageCTurretKill[$DamageType::PBC, 1] = '\c0%4 blasts %1 to kibbles \'n bits.';
$DeathMessageCTurretKill[$DamageType::PBC, 2] = '\c0%4 gives %1 the smackdown with %6 particle beam cannon.';

$DeathMessageTurretKill[$DamageType::PBC, 0] = '\c0%4\'s PBC punches that fatal hole through %1.';
$DeathMessageTurretKill[$DamageType::PBC, 1] = '\c0%4 blasts %1 to kibbles \'n bits.';
$DeathMessageTurretKill[$DamageType::PBC, 2] = '\c0%4 gives %1 the smackdown with %6 PBC.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Annihalator
$DamageType::Annihalator = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Annihalator';

$DeathMessageSelfKill[$DamageType::Annihalator, 0] = '\c0%1 nukes %2self with %3 Annihalator.';
$DeathMessageSelfKill[$DamageType::Annihalator, 1] = '\c0%1 is broken down into %3 component atoms by a nearby Annihalator.';
$DeathMessageSelfKill[$DamageType::Annihalator, 2] = '\c0%1 used the force on %2self.';
$DeathMessageSelfKill[$DamageType::Annihalator, 3] = '\c0%1 nukes %2self with %3 Annihalator.';
$DeathMessageSelfKill[$DamageType::Annihalator, 4] = '\c0%1 is broken down into %3 component atoms by %3 own Annihalator!';

$DeathMessage[$DamageType::Annihalator, 0] = '\c0%4 nukes %1 with %6 Annihalator.';
$DeathMessage[$DamageType::Annihalator, 1] = '\c0%1 is broken down into %3 component atoms by %4\'s nearby Annihalator.';
$DeathMessage[$DamageType::Annihalator, 2] = '\c0%1 felt the sun\'s burning rays from %4\'s Annihalator barrage.';
$DeathMessage[$DamageType::Annihalator, 3] = '\c0%1 gets sucked into %4\'s localized Annihalator singularity.';
$DeathMessage[$DamageType::Annihalator, 4] = '\c0%4\'s Annihalator finds %1 a tasty sacrifice.';

$DeathMessageTeamKill[$DamageType::Annihalator, 0] = '\c0%4 TEAMKILLED %1 with an Annihalator!';
$DeathMessageCTurretTeamKill[$DamageType::Annihalator, 0] = '\c0%4 TEAMKILLED %1 with an Annihalator turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Annihalator, 0] = '\c0%1 got in the way of a friendly Annihalator turret!';

$DeathMessageCTurretKill[$DamageType::Annihalator, 0] = '\c0%4 somehow spawns an Annihalator from a turret and kills %1.';
$DeathMessageCTurretKill[$DamageType::Annihalator, 1] = '\c0%4 somehow spawns an Annihalator from a turret and kills %1.';
$DeathMessageCTurretKill[$DamageType::Annihalator, 2] = '\c0%4 somehow spawns an Annihalator from a turret and kills %1.';

$DeathMessageTurretKill[$DamageType::Annihalator, 0] = '\c0%4 somehow spawns an Annihalator from a turret and kills %1.';
$DeathMessageTurretKill[$DamageType::Annihalator, 1] = '\c0%4 somehow spawns an Annihalator from a turret and kills %1.';
$DeathMessageTurretKill[$DamageType::Annihalator, 2] = '\c0%4 somehow spawns an Annihalator from a turret and kills %1.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// EMP
$DamageType::EMP = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'EMP';

$DeathMessageSelfKill[$DamageType::EMP, 0] = '\c0%1 accidentally drops an EMP and attempts to stuff it back in %2 armor.';
$DeathMessageSelfKill[$DamageType::EMP, 1] = '\c0%1 feels the wrath of %3 own electromagnetic pulse.';
$DeathMessageSelfKill[$DamageType::EMP, 2] = '\c0%1 kills %2self with an EMP!';
$DeathMessageSelfKill[$DamageType::EMP, 3] = '\c0%1\'s EMP causes %3 to do the electric slide.';
$DeathMessageSelfKill[$DamageType::EMP, 4] = '\c0%1 attempts to harness the awesome power of electromagnetism... and fails.';

$DeathMessage[$DamageType::EMP, 0] = '\c0%4 fries %1 with an EM pulse.';
$DeathMessage[$DamageType::EMP, 1] = '\c0%4 was last heard screaming "UNLIMITED POWAH!" in %1\s direction.';
$DeathMessage[$DamageType::EMP, 2] = '\c0%1 learns the shocking truth about %4\'s EM Pulse.';
$DeathMessage[$DamageType::EMP, 3] = '\c0%4 blows out %1\'s armor with an EM Pulse.';
$DeathMessage[$DamageType::EMP, 4] = '\c0%4\'s EM Pulse leaves %1 a crispy critter.';

$DeathMessageTeamKill[$DamageType::EMP, 0] = '\c0%4 TEAMKILLED %1 with an EM pulse!';
$DeathMessageCTurretTeamKill[$DamageType::EMP, 0] = '\c0%4 TEAMKILLED %1 with an EM pulse turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::EMP, 0] = '\c0%1 got in the way of a friendly EM pulse turret!';

$DeathMessageCTurretKill[$DamageType::EMP, 0] = '\c0%4 blows out %1\'s armor with an EM Pulse.';
$DeathMessageCTurretKill[$DamageType::EMP, 1] = '\c0%1 learns the shocking truth about %4\'s EM Pulse.';
$DeathMessageCTurretKill[$DamageType::EMP, 2] = '\c0%4\'s EM Pulse leaves %1 a crispy critter.';

$DeathMessageTurretKill[$DamageType::EMP, 0] = '\c0%4 blows out %1\'s armor with an EM Pulse.';
$DeathMessageTurretKill[$DamageType::EMP, 1] = '\c0%1 learns the shocking truth about %4\'s EM Pulse.';
$DeathMessageTurretKill[$DamageType::EMP, 2] = '\c0%4\'s EM Pulse leaves %1 a crispy critter.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Burn
$DamageType::Burn = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Burn';

$DeathMessageSelfKill[$DamageType::Burn, 0] = '\c0%1 shows %2self the awesome power of fire.';
$DeathMessageSelfKill[$DamageType::Burn, 1] = '\c0%1 is now painfully aware of how a plasma fire feels.';
$DeathMessageSelfKill[$DamageType::Burn, 2] = '\c0%1 swallows a white-hot mouthful of %3 own plasma.';
$DeathMessageSelfKill[$DamageType::Burn, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::Burn, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessage[$DamageType::Burn, 0] = '\c0%4 converts %1 into a living bonfire.';
$DeathMessage[$DamageType::Burn, 1] = '\c0%1 forgot to put out %4\'s flames.';
$DeathMessage[$DamageType::Burn, 2] = '\c0%4 sets %1 on fire.';
$DeathMessage[$DamageType::Burn, 3] = '\c0%4 roasts some s\'mores over %1\'s burning corpse.';
$DeathMessage[$DamageType::Burn, 4] = '\c0%4 experiences a plasma fire first hand thanks to %1.';

$DeathMessageTeamKill[$DamageType::Burn, 0] = '\c0%4 TEAMKILLED %1 with a plasma fire!';
$DeathMessageCTurretTeamKill[$DamageType::Burn, 0] = '\c0%4 TEAMKILLED %1 with a plasma fire!';
$DeathMessageCTurretAccdtlKill[$DamageType::Burn, 0] = '\c0%1 got in the way of a friendly plasma fire!';

$DeathMessageCTurretKill[$DamageType::Burn, 0] = '\c0%4 experiences a plasma fire first hand thanks to %1.';
$DeathMessageCTurretKill[$DamageType::Burn, 1] = '\c0%4 sets %1 on fire.';
$DeathMessageCTurretKill[$DamageType::Burn, 2] = '\c0%4 roasts some s\'mores over %1\'s burning corpse.';

$DeathMessageTurretKill[$DamageType::Burn, 0] = '\c0%1 is lit on fire by a turret.';
$DeathMessageTurretKill[$DamageType::Burn, 1] = '\c0%1 is lit on fire by a turret.';
$DeathMessageTurretKill[$DamageType::Burn, 2] = '\c0%1 is lit on fire by a turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Poison
$DamageType::Poison = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Poison';

$DeathMessageSelfKill[$DamageType::Poison, 0] = '\c0%1 dies with green flesh open.';
$DeathMessageSelfKill[$DamageType::Poison, 1] = '\c0%1 dies with green flesh open.';
$DeathMessageSelfKill[$DamageType::Poison, 2] = '\c0%1 heard "USE THE REPAIR KIT!" from far off.';
$DeathMessageSelfKill[$DamageType::Poison, 3] = '\c0%1 dies with green flesh open.';
$DeathMessageSelfKill[$DamageType::Poison, 4] = '\c0%1 let out the metroid!';

$DeathMessage[$DamageType::Poison, 0] = '\c0%1 dies with green flesh open from %4\'s poison.';
$DeathMessage[$DamageType::Poison, 1] = '\c0%4 gives %1 a deadly dose of poison.';
$DeathMessage[$DamageType::Poison, 2] = '\c0%4 gives %1 "the clap".';
$DeathMessage[$DamageType::Poison, 3] = '\c0%1 takes %4\'s two pills, and doesn\'t call in the morning.';
$DeathMessage[$DamageType::Poison, 4] = '\c0%1 comes down with a nasty case of death from %4\'s poison.';

$DeathMessageTeamKill[$DamageType::Poison, 0] = '\c0%4 TEAMKILLED %1 with poison!';
$DeathMessageCTurretTeamKill[$DamageType::Poison, 0] = '\c0%4 TEAMKILLED %1 with a poison turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Poison, 0] = '\c0%1 got in the way of a friendly poison turret!';

$DeathMessageCTurretKill[$DamageType::Poison, 0] = '\c0%4 gives %1 a deadly dose of poison.';
$DeathMessageCTurretKill[$DamageType::Poison, 1] = '\c0%1 takes %4\'s two pills, and doesn\'t call in the morning.';
$DeathMessageCTurretKill[$DamageType::Poison, 2] = '\c0%1 dies with green flesh open from %4\'s poison.';

$DeathMessageTurretKill[$DamageType::Poison, 0] = '\c0%1 is poisoned by a turret.';
$DeathMessageTurretKill[$DamageType::Poison, 1] = '\c0%1 is poisoned by a turret.';
$DeathMessageTurretKill[$DamageType::Poison, 2] = '\c0%1 is poisoned by a turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Sagittarius
$DamageType::Sagittarius = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Sagittarius';

$DeathMessageSelfKill[$DamageType::Sagittarius, 0] = '\c0%1 turns %2 Sagittarius around and shoots %2self.';
$DeathMessageSelfKill[$DamageType::Sagittarius, 1] = '\c0%1 turns %2 Sagittarius around and shoots %2self.';
$DeathMessageSelfKill[$DamageType::Sagittarius, 2] = '\c0%1 turns %2 Sagittarius around and shoots %2self.';
$DeathMessageSelfKill[$DamageType::Sagittarius, 3] = '\c0%1 turns %2 Sagittarius around and shoots %2self.';
$DeathMessageSelfKill[$DamageType::Sagittarius, 4] = '\c0%1 turns %2 Sagittarius around and shoots %2self.';

$DeathMessage[$DamageType::Sagittarius, 0] = '\c0%4\'s crack shot puts a hole in %1.';
$DeathMessage[$DamageType::Sagittarius, 1] = '\c0%4 gives %1 a shot right between the eyes.';
$DeathMessage[$DamageType::Sagittarius, 2] = '\c0%4 stops %1 dead with the Sagittarius.';
$DeathMessage[$DamageType::Sagittarius, 3] = '\c0%4 takes a pot shot at %1.';
$DeathMessage[$DamageType::Sagittarius, 4] = '\c0%4 picks off %1 from afar.';

$DeathMessageTeamKill[$DamageType::Sagittarius, 0] = '\c0%4 TEAMKILLED %1 with Sagittarius!';
$DeathMessageCTurretTeamKill[$DamageType::Sagittarius, 0] = '\c0%4 TEAMKILLED %1 with a Sagittarius turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Sagittarius, 0] = '\c0%1 got in the way of a friendly Sagittarius turret!';

$DeathMessageCTurretKill[$DamageType::Sagittarius, 0] = '\c0%4 gives %1 a deadly dose of Sagittarius.'; // lol
$DeathMessageCTurretKill[$DamageType::Sagittarius, 1] = '\c0%1 takes %4\'s two pills, and doesn\'t call in the morning.';
$DeathMessageCTurretKill[$DamageType::Sagittarius, 2] = '\c0%1 dies with green flesh open from %4\'s Sagittarius.';

$DeathMessageTurretKill[$DamageType::Sagittarius, 0] = '\c0%1 is Sagittariused by a turret.';
$DeathMessageTurretKill[$DamageType::Sagittarius, 1] = '\c0%1 is Sagittariused by a turret.';
$DeathMessageTurretKill[$DamageType::Sagittarius, 2] = '\c0%1 is Sagittariused by a turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// AutoCannon
$DamageType::AutoCannon = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Grenade Launcher';

$DeathMessageSelfKill[$DamageType::AutoCannon, 0] = '\c0%1 dives heroically on %3 own grenade.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 1] = '\c0%1 dives heroically on %3 own grenade.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 2] = '\c0%1 learns that grenades go "pop"!';
$DeathMessageSelfKill[$DamageType::AutoCannon, 3] = '\c0%1 dives heroically on %3 own grenader.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 4] = '\c0%1 gets up close and personal with %3 own grenade.';

$DeathMessage[$DamageType::AutoCannon, 0] = '\c0%4 pops %1 good with a launched grenade.';
$DeathMessage[$DamageType::AutoCannon, 1] = '\c0%4 happily blows %1 into pieces with %6 grenade launcher.';
$DeathMessage[$DamageType::AutoCannon, 2] = '\c0%4 commands %1 to dance the grenade launcher.';
$DeathMessage[$DamageType::AutoCannon, 3] = '\c0%4 blows out %1\'s... everything with the grenade launcher.';
$DeathMessage[$DamageType::AutoCannon, 4] = '\c0%4 blows chunks away from %1 with %6 grenade launcher.';

$DeathMessageTeamKill[$DamageType::AutoCannon, 0] = '\c0%4 TEAMKILLED %1 with an grenade launcher!';
$DeathMessageCTurretTeamKill[$DamageType::AutoCannon, 0] = '\c0%4 TEAMKILLED %1 with a grenade turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::AutoCannon, 0] = '\c0%1 got in the way of a friendly grenade turret!';

$DeathMessageCTurretKill[$DamageType::AutoCannon, 0] = '\c0%4 pops %1 good with an grenade launcher.';
$DeathMessageCTurretKill[$DamageType::AutoCannon, 1] = '\c0%4 happily blows %1 into pieces with %6 grenade launcher.';
$DeathMessageCTurretKill[$DamageType::AutoCannon, 2] = '\c0%4 blows chunks away from %1 with %6 grenade launcher.';

$DeathMessageTurretKill[$DamageType::AutoCannon, 0] = '\c0%1 plays chicken with a grenade turret and loses.';
$DeathMessageTurretKill[$DamageType::AutoCannon, 1] = '\c0%1 comes apart at the sight of an grenade turret.';
$DeathMessageTurretKill[$DamageType::AutoCannon, 2] = '\c0%1 gets popped by a nearby grenade turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Flamethrower
$DamageType::Flamethrower = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Flamethrower';

$DeathMessageSelfKill[$DamageType::Flamethrower, 0] = '\c0%1 roasts %2self.';
$DeathMessageSelfKill[$DamageType::Flamethrower, 1] = '\c0%1 turns %2self into cajun meat chunks.';
$DeathMessageSelfKill[$DamageType::Flamethrower, 2] = '\c0%1 tries to act like a fire eater, but failed.';
$DeathMessageSelfKill[$DamageType::Flamethrower, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::Flamethrower, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessage[$DamageType::Flamethrower, 0] = '\c0%4 roasts %1 with the flamethrower.';
$DeathMessage[$DamageType::Flamethrower, 1] = '\c0%4 introduces %1 to the wonders of %6 flamethrower.';
$DeathMessage[$DamageType::Flamethrower, 2] = '\c0%4 entices %1 to check %6 pilot light.';
$DeathMessage[$DamageType::Flamethrower, 3] = '\c0%4 introduces %1 to 5000 degrees of fun.';
$DeathMessage[$DamageType::Flamethrower, 4] = '\c0%1 steps into %4\'s stream of flames.';

$DeathMessageTeamKill[$DamageType::Flamethrower, 0] = '\c0%4 TEAMKILLED %1 with a flamethrower!';
$DeathMessageCTurretTeamKill[$DamageType::Flamethrower, 0] = '\c0%4 TEAMKILLED %1 with a flamethrower turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Flamethrower, 0] = '\c0%1 got in the way of a friendly flamethrower turret!';

$DeathMessageCTurretKill[$DamageType::Flamethrower, 0] = '\c0%4 introduces %1 to the wonders of %6 flamethrower.';
$DeathMessageCTurretKill[$DamageType::Flamethrower, 1] = '\c0%4 entices %1 to check %6 pilot light.';
$DeathMessageCTurretKill[$DamageType::Flamethrower, 2] = '\c0%1 steps into %4\'s stream of flames.';

$DeathMessageTurretKill[$DamageType::Flamethrower, 0] = '\c0%1 attempts to bend fire... and fails.';
$DeathMessageTurretKill[$DamageType::Flamethrower, 1] = '\c0%1 becomes the human torch thanks to a nearby flamethrower turret.';
$DeathMessageTurretKill[$DamageType::Flamethrower, 2] = '\c0%1 is baptized in fire from a nearby flamethrower turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// StarHammer
$DamageType::StarHammer = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'StarHammer';

$DeathMessageSelfKill[$DamageType::StarHammer, 0] = '\c0%1 kills %2self with a Star Hammer!';
$DeathMessageSelfKill[$DamageType::StarHammer, 1] = '\c0%1 destroys %2self with a starhammer.';
$DeathMessageSelfKill[$DamageType::StarHammer, 2] = '\c0%1 starhammers %2self to pieces.';
$DeathMessageSelfKill[$DamageType::StarHammer, 3] = '\c0%1 experiences %3 Star Hammer\'s payload up close.';
$DeathMessageSelfKill[$DamageType::StarHammer, 4] = '\c0%1 gracefully smoked %2self with a Star Hammer!.';

$DeathMessage[$DamageType::StarHammer, 0] = '\c0%4 obliterates %1 with the star hammer.';
$DeathMessage[$DamageType::StarHammer, 1] = '\c0%4 chalks up another star hammer kill, courtesy of %1.';
$DeathMessage[$DamageType::StarHammer, 2] = '\c0%4 makes %1 _very_ dead with a well placed star hammer rocket.';
$DeathMessage[$DamageType::StarHammer, 3] = '\c0%4\'s star hammer blast leaves %1 nothin\' but smokin\' boots.';
$DeathMessage[$DamageType::StarHammer, 4] = '\c0%1 gets hammered by %4 and falls apart.';

$DeathMessageTeamKill[$DamageType::StarHammer, 0] = '\c0%4 TEAMKILLED %1 with a star hammer!';
$DeathMessageCTurretTeamKill[$DamageType::StarHammer, 0] = '\c0%4 TEAMKILLED %1 with a star hammer turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::StarHammer, 0] = '\c0%1 got in the way of a friendly star hammer turret!';

$DeathMessageCTurretKill[$DamageType::StarHammer, 0] = '\c0%4 makes %1 _very_ dead with a well placed star hammer rocket.';
$DeathMessageCTurretKill[$DamageType::StarHammer, 1] = '\c0%4 chalks up another star hammer kill, courtesy of %1.';
$DeathMessageCTurretKill[$DamageType::StarHammer, 2] = '\c0%1 gets hammered by %4 and falls apart.';

$DeathMessageTurretKill[$DamageType::StarHammer, 0] = '\c0%1 attempts to bend fire... and fails.';
$DeathMessageTurretKill[$DamageType::StarHammer, 1] = '\c0%1 becomes the human torch thanks to a nearby StarHammer turret.';
$DeathMessageTurretKill[$DamageType::StarHammer, 2] = '\c0%1 is baptized in fire from a nearby StarHammer turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// MeteorCannon
$DamageType::MeteorCannon = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'MeteorCannon';

$DeathMessageSelfKill[$DamageType::MeteorCannon, 0] = '\c0%1 plays catch with %3 meteor cannon.';
$DeathMessageSelfKill[$DamageType::MeteorCannon, 1] = '\c0%1 plays catch with %3 meteor cannon.';
$DeathMessageSelfKill[$DamageType::MeteorCannon, 2] = '\c0%1 plays catch with %3 meteor cannon.';
$DeathMessageSelfKill[$DamageType::MeteorCannon, 3] = '\c0%1 plays catch with %3 meteor cannon.';
$DeathMessageSelfKill[$DamageType::MeteorCannon, 4] = '\c0%1 plays catch with %3 meteor cannon.';

$DeathMessage[$DamageType::MeteorCannon, 0] = '\c0%1 eats a big helping of %4\'s meteor blast.';
$DeathMessage[$DamageType::MeteorCannon, 1] = '\c0%4 plants a meteor blast in %1\'s chest.';
$DeathMessage[$DamageType::MeteorCannon, 2] = '\c0%1 fails to evade %4\'s deft meteor cannon barrage.';
$DeathMessage[$DamageType::MeteorCannon, 3] = '\c0%1 helpfully jumps into %4\'s meteor blast.';
$DeathMessage[$DamageType::MeteorCannon, 4] = '\c0%1 gets knocked into next week from %4\'s meteor blast.';

$DeathMessageTeamKill[$DamageType::MeteorCannon, 0] = '\c0%4 TEAMKILLED %1 with a meteor blast!';
$DeathMessageCTurretTeamKill[$DamageType::MeteorCannon, 0] = '\c0%4 TEAMKILLED %1 with a meteor cannon turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::MeteorCannon, 0] = '\c0%1 got in the way of a friendly meteor cannon turret!';

$DeathMessageCTurretKill[$DamageType::MeteorCannon, 0] = '\c0%4 plants a meteor blast in %1\'s chest.';
$DeathMessageCTurretKill[$DamageType::MeteorCannon, 1] = '\c0%1 fails to evade %4\'s deft meteor cannon barrage.';
$DeathMessageCTurretKill[$DamageType::MeteorCannon, 2] = '\c0%1 gets knocked into next week from %4\'s meteor blast.';

$DeathMessageTurretKill[$DamageType::MeteorCannon, 0] = '\c0%1 attempts to bend fire... and fails.';
$DeathMessageTurretKill[$DamageType::MeteorCannon, 1] = '\c0%1 becomes the human torch thanks to a nearby MeteorCannon turret.';
$DeathMessageTurretKill[$DamageType::MeteorCannon, 2] = '\c0%1 is baptized in fire from a nearby MeteorCannon turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// MagIonReactor
$DamageType::MagIonReactor = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'MagIonReactor';

$DeathMessageSelfKill[$DamageType::MagIonReactor, 0] = '\c0%1 is the victim of a sudden dreadnought reactor outburst.';
$DeathMessageSelfKill[$DamageType::MagIonReactor, 1] = '\c0%1 is the victim of a sudden dreadnought reactor outburst.';
$DeathMessageSelfKill[$DamageType::MagIonReactor, 2] = '\c0%1 is the victim of a sudden dreadnought reactor outburst.';
$DeathMessageSelfKill[$DamageType::MagIonReactor, 3] = '\c0%1 is the victim of a sudden dreadnought reactor outburst.';
$DeathMessageSelfKill[$DamageType::MagIonReactor, 4] = '\c0%1 is the victim of a sudden dreadnought reactor outburst.';

$DeathMessage[$DamageType::MagIonReactor, 0] = '\c0%1 is annihilated by %4\'s explosive overload.';
$DeathMessage[$DamageType::MagIonReactor, 1] = '\c0%1 is blown away by %4\'s core breach.';
$DeathMessage[$DamageType::MagIonReactor, 2] = '\c0%1 was last heard shouting to %4: "I\'m Dreadnought and I love explosive hugs"!';
$DeathMessage[$DamageType::MagIonReactor, 3] = '\c0%1 gets a fatal booster shot from %4\'s reactor explosion.';
$DeathMessage[$DamageType::MagIonReactor, 4] = '\c0Ouch! %1 + %4\'s overloaded dreadnought reactor = Dead %1.';

$DeathMessageTeamKill[$DamageType::MagIonReactor, 0] = '\c0%4 TEAMKILLED %1 with a well placed dreadnought overload!';
$DeathMessageCTurretTeamKill[$DamageType::MagIonReactor, 0] = '\c0%4 TEAMKILLED %1 with a dreadnought overload turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::MagIonReactor, 0] = '\c0%1 got in the way of a friendly dreadnought overload turret!';

$DeathMessageCTurretKill[$DamageType::MagIonReactor, 0] = '\c0%4 plants a meteor blast in %1\'s chest.';
$DeathMessageCTurretKill[$DamageType::MagIonReactor, 1] = '\c0%1 fails to evade %4\'s deft meteor cannon barrage.';
$DeathMessageCTurretKill[$DamageType::MagIonReactor, 2] = '\c0%1 gets knocked into next week from %4\'s meteor blast.';

$DeathMessageTurretKill[$DamageType::MagIonReactor, 0] = '\c0%1 somehow figures out how to weaponize an exploding dreadnought core.';
$DeathMessageTurretKill[$DamageType::MagIonReactor, 1] = '\c0%1 somehow figures out how to weaponize an exploding dreadnought core.';
$DeathMessageTurretKill[$DamageType::MagIonReactor, 2] = '\c0%1 somehow figures out how to weaponize an exploding dreadnought core.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// WalkerReactor
$DamageType::WalkerReactor = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'WalkerReactor';

$DeathMessageSelfKill[$DamageType::WalkerReactor, 0] = '\c0%1 is the victim of a sudden walker reactor outburst.';
$DeathMessageSelfKill[$DamageType::WalkerReactor, 1] = '\c0%1 is the victim of a sudden walker reactor outburst.';
$DeathMessageSelfKill[$DamageType::WalkerReactor, 2] = '\c0%1 is the victim of a sudden walker reactor outburst.';
$DeathMessageSelfKill[$DamageType::WalkerReactor, 3] = '\c0%1 is the victim of a sudden walker reactor outburst.';
$DeathMessageSelfKill[$DamageType::WalkerReactor, 4] = '\c0%1 is the victim of a sudden walker reactor outburst.';

$DeathMessage[$DamageType::WalkerReactor, 0] = '\c0%1 is annihilated by %4\'s explosive overload.';
$DeathMessage[$DamageType::WalkerReactor, 1] = '\c0%1 is blown away by %4\'s core breach.';
$DeathMessage[$DamageType::WalkerReactor, 2] = '\c0%1 contacts %4\'s exploding walker reactor and goes boom!';
$DeathMessage[$DamageType::WalkerReactor, 3] = '\c0%1 gets a fatal booster shot from %4\'s reactor explosion.';
$DeathMessage[$DamageType::WalkerReactor, 4] = '\c0Ouch! %1 + %4\'s overloaded walker reactor = Dead %1.';

$DeathMessageTeamKill[$DamageType::WalkerReactor, 0] = '\c0%4 TEAMKILLED %1 with a well placed walker overload!';
$DeathMessageCTurretTeamKill[$DamageType::WalkerReactor, 0] = '\c0%4 TEAMKILLED %1 with a walker overload turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::WalkerReactor, 0] = '\c0%1 got in the way of a friendly walker overload turret!';

$DeathMessageCTurretKill[$DamageType::WalkerReactor, 0] = '\c0%4 plants a meteor blast in %1\'s chest.';
$DeathMessageCTurretKill[$DamageType::WalkerReactor, 1] = '\c0%1 fails to evade %4\'s deft meteor cannon barrage.';
$DeathMessageCTurretKill[$DamageType::WalkerReactor, 2] = '\c0%1 gets knocked into next week from %4\'s meteor blast.';

$DeathMessageTurretKill[$DamageType::WalkerReactor, 0] = '\c0%1 somehow figures out how to weaponize an exploding walker core.';
$DeathMessageTurretKill[$DamageType::WalkerReactor, 1] = '\c0%1 somehow figures out how to weaponize an exploding walker core.';
$DeathMessageTurretKill[$DamageType::WalkerReactor, 2] = '\c0%1 somehow figures out how to weaponize an exploding walker core.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Turbocharger
$DamageType::Turbocharger = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Turbocharger';

$DeathMessageSelfKill[$DamageType::Turbocharger, 0] = '\c0%1 is the victim of a sudden turbocharger reactor outburst.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 1] = '\c0%1 is the victim of a sudden turbocharger reactor outburst.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 2] = '\c0%1 is the victim of a sudden turbocharger reactor outburst.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 3] = '\c0%1 is the victim of a sudden turbocharger reactor outburst.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 4] = '\c0%1 is the victim of a sudden turbocharger reactor outburst.';

$DeathMessage[$DamageType::Turbocharger, 0] = '\c0%1 is annihilated by %4\'s explosive overload.';
$DeathMessage[$DamageType::Turbocharger, 1] = '\c0%1 is blown away by %4\'s core breach.';
$DeathMessage[$DamageType::Turbocharger, 2] = '\c0%1 contacts %4\'s exploding turbocharger and goes boom!';
$DeathMessage[$DamageType::Turbocharger, 3] = '\c0%1 gets a fatal booster shot from %4\'s turbocharger explosion.';
$DeathMessage[$DamageType::Turbocharger, 4] = '\c0Ouch! %1 + %4\'s overloaded turbocharger = Dead %1.';

$DeathMessageTeamKill[$DamageType::Turbocharger, 0] = '\c0%4 TEAMKILLED %1 with a turbocharger!';
$DeathMessageCTurretTeamKill[$DamageType::Turbocharger, 0] = '\c0%4 TEAMKILLED %1 with a turbocharger turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Turbocharger, 0] = '\c0%1 got in the way of a turbocharger overload turret!';

$DeathMessageCTurretKill[$DamageType::Turbocharger, 0] = '\c0%4 plants a meteor blast in %1\'s chest.';
$DeathMessageCTurretKill[$DamageType::Turbocharger, 1] = '\c0%1 fails to evade %4\'s deft meteor cannon barrage.';
$DeathMessageCTurretKill[$DamageType::Turbocharger, 2] = '\c0%1 gets knocked into next week from %4\'s meteor blast.';

$DeathMessageTurretKill[$DamageType::Turbocharger, 0] = '\c0%1 somehow figures out how to weaponize an exploding turbocharger.';
$DeathMessageTurretKill[$DamageType::Turbocharger, 1] = '\c0%1 somehow figures out how to weaponize an exploding turbocharger.';
$DeathMessageTurretKill[$DamageType::Turbocharger, 2] = '\c0%1 somehow figures out how to weaponize an exploding turbocharger.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Railgun
$DamageType::Railgun = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Railgun';

$DeathMessageSelfKill[$DamageType::Railgun, 0] = '\c0%1 somehow kills %2self with a railgun.';
$DeathMessageSelfKill[$DamageType::Railgun, 1] = '\c0%1 somehow kills %2self with a railgun.';
$DeathMessageSelfKill[$DamageType::Railgun, 2] = '\c0%1 somehow kills %2self with a railgun.';
$DeathMessageSelfKill[$DamageType::Railgun, 3] = '\c0%1 somehow kills %2self with a railgun.';
$DeathMessageSelfKill[$DamageType::Railgun, 4] = '\c0%1 somehow kills %2self with a railgun.';

$DeathMessage[$DamageType::Railgun, 0] = '\c0%4 wrecks %1 hard with a supersonic rail.';
$DeathMessage[$DamageType::Railgun, 1] = '\c0%1 rides %4\s rails.';
$DeathMessage[$DamageType::Railgun, 2] = '\c0%1 gets extra friendly with %4\'s rails.';
$DeathMessage[$DamageType::Railgun, 3] = '\c0%4\'s railgun gives %1 a hot tungsten injection.';
$DeathMessage[$DamageType::Railgun, 4] = '\c0%1 has a very brief and rough fling with %4\'s railgun.';

$DeathMessageTeamKill[$DamageType::Railgun, 0] = '\c0%4 TEAMKILLED %1 with a railgun!';
$DeathMessageCTurretTeamKill[$DamageType::Railgun, 0] = '\c0%4 TEAMKILLED %1 with a railgun turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Railgun, 0] = '\c0%1 got in the way of a railgun overload turret!';

$DeathMessageCTurretKill[$DamageType::Railgun, 0] = '\c0%4 wrecks %1 hard with a supersonic rail.';
$DeathMessageCTurretKill[$DamageType::Railgun, 1] = '\c0%4\'s railgun gives %1 a hot tungsten injection.';
$DeathMessageCTurretKill[$DamageType::Railgun, 2] = '\c0%1 has a very brief and rough fling with %4\'s railgun.';

$DeathMessageTurretKill[$DamageType::Railgun, 0] = '\c0%1 somehow figures out how to turretize a Railgun.';
$DeathMessageTurretKill[$DamageType::Railgun, 1] = '\c0%1 somehow figures out how to turretize a Railgun.';
$DeathMessageTurretKill[$DamageType::Railgun, 2] = '\c0%1 somehow figures out how to turretize a Railgun.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Bolter
$DamageType::Bolter = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Bolter';

$DeathMessageSelfKill[$DamageType::Bolter, 0] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Bolter, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::Bolter, 2] = '\c0%1 manages to kill %2self with a reflected bolt.';
$DeathMessageSelfKill[$DamageType::Bolter, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::Bolter, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::Bolter, 0] = '\c0%4 rips %1 up with the bolter.';
$DeathMessage[$DamageType::Bolter, 1] = '\c0%4 happily chews %1 into pieces with %6 bolter.';
$DeathMessage[$DamageType::Bolter, 2] = '\c0%4 administers a dose of depleted uranium to %1.';
$DeathMessage[$DamageType::Bolter, 3] = '\c0%1 suffers a serious hosing from %4\'s bolter.';
$DeathMessage[$DamageType::Bolter, 4] = '\c0%4 bestows the blessings of %6 bolter on %1.';

$DeathMessageTeamKill[$DamageType::Bolter, 0] = '\c0%4 TEAMKILLED %1 with a bolter!';
$DeathMessageCTurretTeamKill[$DamageType::Bolter, 0] = '\c0%4 TEAMKILLED %1 with a bolter turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Bolter, 0] = '\c0%1 got in the way of a friendly bolter turret!';

$DeathMessageCTurretKill[$DamageType::Bolter, 0] = '\c0%1 enjoys the rich, metallic taste of %4\'s bolter.';
$DeathMessageCTurretKill[$DamageType::Bolter, 1] = '\c0%4\'s bolter turret plays sweet music all over %1.';
$DeathMessageCTurretKill[$DamageType::Bolter, 2] = '\c0%1 receives a stellar exit wound from %4\'s bolter.';

$DeathMessageTurretKill[$DamageType::Bolter, 0] = '\c0%1 gets chunked apart by a bolter turret.';
$DeathMessageTurretKill[$DamageType::Bolter, 1] = '\c0%1 gets rained on by a nearby bolter turret.';
$DeathMessageTurretKill[$DamageType::Bolter, 2] = '\c0%1 finds %2self on the wrong end of a bolter turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// BlasterRifle
$DamageType::BlasterRifle = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'BlasterRifle';

$DeathMessageSelfKill[$DamageType::BlasterRifle, 0] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::BlasterRifle, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::BlasterRifle, 2] = '\c0%1 manages to kill %2self with a reflected blaster.';
$DeathMessageSelfKill[$DamageType::BlasterRifle, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::BlasterRifle, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::BlasterRifle, 0] = '\c0%4\'s blaster barrage catches %1 with %3 pants down.';
$DeathMessage[$DamageType::BlasterRifle, 1] = '\c0%4\'s blasters burns away %1 with ease.';
$DeathMessage[$DamageType::BlasterRifle, 2] = '\c0%1 gets drilled big-time by %4\'s blasters.';
$DeathMessage[$DamageType::BlasterRifle, 3] = '\c0%1 suffers a serious hosing from %4\'s blasters.';
$DeathMessage[$DamageType::BlasterRifle, 4] = '\c0%1 gets railed apart from %4\'s expert blaster usage.';

$DeathMessageTeamKill[$DamageType::BlasterRifle, 0] = '\c0%4 TEAMKILLED %1 with a blaster!';
$DeathMessageCTurretTeamKill[$DamageType::BlasterRifle, 0] = '\c0%4 TEAMKILLED %1 with a blaster turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::BlasterRifle, 0] = '\c0%1 got in the way of a friendly blaster turret!';

$DeathMessageCTurretKill[$DamageType::BlasterRifle, 0] = '\c0%4\'s blaster barrage catches %1 with %3 pants down.';
$DeathMessageCTurretKill[$DamageType::BlasterRifle, 1] = '\c0%1 gets railed apart from %4\'s expert blaster usage.';
$DeathMessageCTurretKill[$DamageType::BlasterRifle, 2] = '\c0%1 gets drilled big-time by %4\'s blasters.';

$DeathMessageTurretKill[$DamageType::BlasterRifle, 0] = '\c0%1 gets blasted by a turret.';
$DeathMessageTurretKill[$DamageType::BlasterRifle, 1] = '\c0%1 gets stormtroopered by a nearby blaster turret.';
$DeathMessageTurretKill[$DamageType::BlasterRifle, 2] = '\c0%1 couldn\'t dodge the barrage of blaster bolts from a nearby turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// ShieldPulse
$DamageType::ShieldPulse = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'ShieldPulse';

$DeathMessageSelfKill[$DamageType::ShieldPulse, 0] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::ShieldPulse, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::ShieldPulse, 2] = '\c0%1 manages to kill %2self with a reflected blaster.';
$DeathMessageSelfKill[$DamageType::ShieldPulse, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::ShieldPulse, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::ShieldPulse, 0] = '\c0%4 expands and crushes %1.';
$DeathMessage[$DamageType::ShieldPulse, 1] = '\c0%4\'s shield expansion crushes %1.';
$DeathMessage[$DamageType::ShieldPulse, 2] = '\c0%1 gets all 2-dimensional from %4\'s shield expansion.';
$DeathMessage[$DamageType::ShieldPulse, 3] = '\c0%1 turns into a tribal pizza thanks to %4\'s shield expansion.';
$DeathMessage[$DamageType::ShieldPulse, 4] = '\c0%1 couldn\'t take the pressure.';

$DeathMessageTeamKill[$DamageType::ShieldPulse, 0] = '\c0%4 TEAMKILLED %1 with a shield pulse!';
$DeathMessageCTurretTeamKill[$DamageType::ShieldPulse, 0] = '\c0%4 TEAMKILLED %1 with a shield pulse turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::ShieldPulse, 0] = '\c0%1 got in the way of a friendly shield pulse turret!';

$DeathMessageCTurretKill[$DamageType::ShieldPulse, 0] = '\c0%1 turns into a tribal pizza thanks to %4\'s shield expansion.';
$DeathMessageCTurretKill[$DamageType::ShieldPulse, 1] = '\c0%4\'s ShieldPulse turret plays sweet music all over %1.';
$DeathMessageCTurretKill[$DamageType::ShieldPulse, 2] = '\c0%1 receives a stellar exit wound from %4\'s ShieldPulse.';

$DeathMessageTurretKill[$DamageType::ShieldPulse, 0] = '\c0%1 gets blasted by a turret.';
$DeathMessageTurretKill[$DamageType::ShieldPulse, 1] = '\c0%4\'s shield expansion crushes %1.';
$DeathMessageTurretKill[$DamageType::ShieldPulse, 2] = '\c0%1 turns into a tribal pizza thanks to %4\'s shield expansion.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// SonicPulser
$DamageType::SonicPulser = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'SonicPulser';

$DeathMessageSelfKill[$DamageType::SonicPulser, 0] = '\c0%1 points the business end of a sonic pulser at %2self.';
$DeathMessageSelfKill[$DamageType::SonicPulser, 1] = '\c0%1\'s bones turn to jelly from %3 own sonic pulser.';
$DeathMessageSelfKill[$DamageType::SonicPulser, 2] = '\c0%1 points the business end of a sonic pulser at %2self.';
$DeathMessageSelfKill[$DamageType::SonicPulser, 3] = '\c0%1\'s bones turn to jelly from %3 own sonic pulser.';
$DeathMessageSelfKill[$DamageType::SonicPulser, 4] = '\c0%1 points the business end of a sonic pulser at %2self.';

$DeathMessage[$DamageType::SonicPulser, 0] = '\c0%4 vibrates %1 apart with the sonic pulser.';
$DeathMessage[$DamageType::SonicPulser, 1] = '\c0%1 gets tossed like a human salad by %4.';
$DeathMessage[$DamageType::SonicPulser, 2] = '\c0%1 gets pulsed into oblivion.';
$DeathMessage[$DamageType::SonicPulser, 3] = '\c0%1 is deafened then deaded by %4\'s sonic pulser.';
$DeathMessage[$DamageType::SonicPulser, 4] = '\c0%1 is forced from their armor by a %4\s sonic pulser.';

$DeathMessageTeamKill[$DamageType::SonicPulser, 0] = '\c0%4 TEAMKILLED %1 with a sonic pulser!';
$DeathMessageCTurretTeamKill[$DamageType::SonicPulser, 0] = '\c0%4 TEAMKILLED %1 with a sonic pulser turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::SonicPulser, 0] = '\c0%1 got in the way of a friendly sonic pulser turret!';

$DeathMessageCTurretKill[$DamageType::SonicPulser, 0] = '\c0%1 turns into a tribal pizza thanks to %4\'s sonic pulser.';
$DeathMessageCTurretKill[$DamageType::SonicPulser, 1] = '\c0%4\'s sonic pulser turret plays sweet music all over %1.';
$DeathMessageCTurretKill[$DamageType::SonicPulser, 2] = '\c0%1 receives a stellar exit wound from %4\'s sonic pulser.';

$DeathMessageTurretKill[$DamageType::SonicPulser, 0] = '\c0%1 gets blasted by a sonic pulser.';
$DeathMessageTurretKill[$DamageType::SonicPulser, 1] = '\c0%4\'s sonic pulser expansion crushes %1.';
$DeathMessageTurretKill[$DamageType::SonicPulser, 2] = '\c0%1 turns into a tribal pizza thanks to %4\'s sonic pulser.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// VectorGlitch
$DamageType::VectorGlitch = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'VectorGlitch';

$DeathMessageSelfKill[$DamageType::VectorGlitch, 0] = '\c0%1\'s life signs were forcefully terminated by rematerializing in a solid object.';
$DeathMessageSelfKill[$DamageType::VectorGlitch, 1] = '\c0%1 becomes one with the scenery.';
$DeathMessageSelfKill[$DamageType::VectorGlitch, 2] = '\c0%1 finds out %3 can\'t VectorPort into solid objects.';
$DeathMessageSelfKill[$DamageType::VectorGlitch, 3] = '\c0%1 phases back into existence inbetween a rock and a building wall.';
$DeathMessageSelfKill[$DamageType::VectorGlitch, 4] = '\c0%1 stuck %3 body where it didn\'t belong.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// RotaryCannon
$DamageType::RotaryCannon = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'RotaryCannon';

$DeathMessageSelfKill[$DamageType::RotaryCannon, 0] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::RotaryCannon, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::RotaryCannon, 2] = '\c0%1 manages to kill %2self with a reflected bolt.';
$DeathMessageSelfKill[$DamageType::RotaryCannon, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::RotaryCannon, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::RotaryCannon, 0] = '\c0%4 rips %1 up with the rotary cannon.';
$DeathMessage[$DamageType::RotaryCannon, 1] = '\c0%4 happily chews %1 into pieces with %6 rotary cannon.';
$DeathMessage[$DamageType::RotaryCannon, 2] = '\c0%4 administers a dose of explosive shell to %1.';
$DeathMessage[$DamageType::RotaryCannon, 3] = '\c0%1 suffers a serious hosing from %4\'s rotary cannon.';
$DeathMessage[$DamageType::RotaryCannon, 4] = '\c0%4 bestows the blessings of %6 rotary cannon on %1.';

$DeathMessageTeamKill[$DamageType::RotaryCannon, 0] = '\c0%4 TEAMKILLED %1 with a rotary cannon!';
$DeathMessageCTurretTeamKill[$DamageType::RotaryCannon, 0] = '\c0%4 TEAMKILLED %1 with a rotary cannon turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::RotaryCannon, 0] = '\c0%1 got in the way of a friendly rotary cannon turret!';

$DeathMessageCTurretKill[$DamageType::RotaryCannon, 0] = '\c0%1 enjoys the rich, explosive taste of %4\'s rotary cannon.';
$DeathMessageCTurretKill[$DamageType::RotaryCannon, 1] = '\c0%4\'s rotary cannon turret plays sweet music all over %1.';
$DeathMessageCTurretKill[$DamageType::RotaryCannon, 2] = '\c0%1 receives a stellar exit wound from %4\'s rotary cannon.';

$DeathMessageTurretKill[$DamageType::RotaryCannon, 0] = '\c0%1 gets chunked apart by a rotary cannon turret.';
$DeathMessageTurretKill[$DamageType::RotaryCannon, 1] = '\c0%1 gets rained on by a nearby rotary cannon turret.';
$DeathMessageTurretKill[$DamageType::RotaryCannon, 2] = '\c0%1 finds %2self on the wrong end of a rotary cannon turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// MPDisruptor
$DamageType::MPDisruptor = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Heavy Disruptor';

$DeathMessageSelfKill[$DamageType::MPDisruptor, 0] = '\c0%1 plays catch with %3 heavy disruptor.';
$DeathMessageSelfKill[$DamageType::MPDisruptor, 1] = '\c0%1 tries to cuddle with %3 heavy disruptor... and failed!';
$DeathMessageSelfKill[$DamageType::MPDisruptor, 2] = '\c0%1 plays catch with %3 heavy disruptor.';
$DeathMessageSelfKill[$DamageType::MPDisruptor, 3] = '\c0%1 tries to cuddle with %3 heavy disruptor... and failed!';
$DeathMessageSelfKill[$DamageType::MPDisruptor, 4] = '\c0%1 plays catch with %3 heavy disruptor.';

$DeathMessage[$DamageType::MPDisruptor, 0] = '\c0%4 annihilates %1 with the heavy disruptor.';
$DeathMessage[$DamageType::MPDisruptor, 1] = '\c0%4 teaches %1 that %6 heavy disruptor should be feared.';
$DeathMessage[$DamageType::MPDisruptor, 2] = '\c0%1 underestimates the power of %4\'s heavy disruptor.';
$DeathMessage[$DamageType::MPDisruptor, 3] = '\c0%1 is liberated from %3 armor thanks to %4\'s heavy disruptor.';
$DeathMessage[$DamageType::MPDisruptor, 4] = '\c0%4 takes careful aim, and nails %1 with %6 heavy disruptor.';

$DeathMessageTeamKill[$DamageType::MPDisruptor, 0] = '\c0%4 TEAMKILLED %1 with a heavy disruptor!';
$DeathMessageCTurretTeamKill[$DamageType::MPDisruptor, 0] = '\c0%4 TEAMKILLED %1 with a heavy disruptor turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::MPDisruptor, 0] = '\c0%1 got in the way of a friendly heavy disruptor turret!';

$DeathMessageCTurretKill[$DamageType::MPDisruptor, 0] = '\c0%1 is liberated from %3 armor thanks to %4\'s heavy disruptor.';
$DeathMessageCTurretKill[$DamageType::MPDisruptor, 1] = '\c0%4 teaches %1 that %6 heavy disruptor should be feared.';
$DeathMessageCTurretKill[$DamageType::MPDisruptor, 2] = '\c0%4 annihilates %1 with the heavy disruptor.';

$DeathMessageTurretKill[$DamageType::MPDisruptor, 0] = '\c0%1 gets lit up by a heavy disruptor turret.';
$DeathMessageTurretKill[$DamageType::MPDisruptor, 1] = '\c0%1 gets rained on by a nearby heavy disruptor turret.';
$DeathMessageTurretKill[$DamageType::MPDisruptor, 2] = '\c0%1 finds %2self on the wrong end of a heavy disruptor turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Turret
$DamageType::Turret = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Turret';

$DeathMessageSelfKill[$DamageType::Turret, 0] = '\c0%1 kills %2self with a turret.';
$DeathMessageSelfKill[$DamageType::Turret, 1] = '\c0%1 kills %2self with a turret.';
$DeathMessageSelfKill[$DamageType::Turret, 2] = '\c0%1 kills %2self with a turret.';
$DeathMessageSelfKill[$DamageType::Turret, 3] = '\c0%1 kills %2self with a turret.';
$DeathMessageSelfKill[$DamageType::Turret, 4] = '\c0%1 kills %2self with a turret.';

$DeathMessage[$DamageType::Turret, 0] = '\c0%4 kills %1 with a turret.';
$DeathMessage[$DamageType::Turret, 1] = '\c0%4 kills %1 with a turret.';
$DeathMessage[$DamageType::Turret, 2] = '\c0%4 kills %1 with a turret.';
$DeathMessage[$DamageType::Turret, 3] = '\c0%4 kills %1 with a turret.';
$DeathMessage[$DamageType::Turret, 4] = '\c0%4 kills %1 with a turret.';

$DeathMessageTeamKill[$DamageType::Turret, 0] = '\c0%4 TEAMKILLED %1 with a turret!';
$DeathMessageCTurretTeamKill[$DamageType::Turret, 0] = '\c0%4 TEAMKILLED %1 with a turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Turret, 0] = '\c0%1 got in the way of a friendly turret!';

$DeathMessageCTurretKill[$DamageType::Turret, 0] = '\c0%4 kills %1 with a turret.';
$DeathMessageCTurretKill[$DamageType::Turret, 1] = '\c0%4 kills %1 with a turret.';
$DeathMessageCTurretKill[$DamageType::Turret, 2] = '\c0%4 kills %1 with a turret.';

$DeathMessageTurretKill[$DamageType::Turret, 0] = '\c0%1 gets killed by a turret.';
$DeathMessageTurretKill[$DamageType::Turret, 1] = '\c0%1 gets killed by a turret.';
$DeathMessageTurretKill[$DamageType::Turret, 2] = '\c0%1 gets killed by a turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Spike
$DamageType::Spike = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Spike';

$DeathMessageSelfKill[$DamageType::Spike, 0] = '\c0%1 kills %2self with a spike.';
$DeathMessageSelfKill[$DamageType::Spike, 1] = '\c0%1 kills %2self with a spike.';
$DeathMessageSelfKill[$DamageType::Spike, 2] = '\c0%1 kills %2self with a spike.';
$DeathMessageSelfKill[$DamageType::Spike, 3] = '\c0%1 kills %2self with a spike.';
$DeathMessageSelfKill[$DamageType::Spike, 4] = '\c0%1 kills %2self with a spike.';

$DeathMessage[$DamageType::Spike, 0] = '\c0%4\'s spike neatly drills %1.';
$DeathMessage[$DamageType::Spike, 1] = '\c0%1 dies under %4\'s spike love.';
$DeathMessage[$DamageType::Spike, 2] = '\c0%1 is chewed up by %4\'s spike.';
$DeathMessage[$DamageType::Spike, 3] = '\c0%1 feels the burn from %4\'s spike.';
$DeathMessage[$DamageType::Spike, 4] = '\c0%1 is nailed by %4\'s spike.';

$DeathMessageTeamKill[$DamageType::Spike, 0] = '\c0%4 TEAMKILLED %1 with a spike!';
$DeathMessageCSpikeTeamKill[$DamageType::Spike, 0] = '\c0%4 TEAMKILLED %1 with a spike turret!';
$DeathMessageCSpikeAccdtlKill[$DamageType::Spike, 0] = '\c0%1 got in the way of a friendly spike turret!';

$DeathMessageCSpikeKill[$DamageType::Spike, 0] = '\c0%4\'s spike neatly drills %1.';
$DeathMessageCSpikeKill[$DamageType::Spike, 1] = '\c0%1 is chewed up by %4\'s spike.';
$DeathMessageCSpikeKill[$DamageType::Spike, 2] = '\c0%1 is nailed by %4\'s spike.';

$DeathMessageSpikeKill[$DamageType::Spike, 0] = '\c0%1 is chewed up by a spike turret.';
$DeathMessageSpikeKill[$DamageType::Spike, 1] = '\c0%1 is nailed by a spike turret.';
$DeathMessageSpikeKill[$DamageType::Spike, 2] = '\c0%1 gets in the crosshairs of a spike turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// GeneralExplosive
$DamageType::GeneralExplosive = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'General Explosives';

$DeathMessageSelfKill[$DamageType::GeneralExplosive, 0] = '\c0%1 goes out with a bang.';
$DeathMessageSelfKill[$DamageType::GeneralExplosive, 1] = '\c0%1 finds out that this gun explodes.';
$DeathMessageSelfKill[$DamageType::GeneralExplosive, 2] = '\c0%1 blows %s brains out.';
$DeathMessageSelfKill[$DamageType::GeneralExplosive, 3] = '\c0%1 comes apart from their own explosives.';
$DeathMessageSelfKill[$DamageType::GeneralExplosive, 4] = '\c0%1 finds out what explosives do.';

$DeathMessage[$DamageType::GeneralExplosive, 0] = '\c0%4 teaches %1 the happy explosion dance.';
$DeathMessage[$DamageType::GeneralExplosive, 1] = '\c0%4 leaves %1 as a smoking crater.';
$DeathMessage[$DamageType::GeneralExplosive, 2] = '\c0%4 shows %1 a new world of explosive pain.';
$DeathMessage[$DamageType::GeneralExplosive, 3] = '\c0%4\'s explosives makes armored chowder out of %1.';
$DeathMessage[$DamageType::GeneralExplosive, 4] = '\c0%4 buys %1 a ticket to the moon.';

$DeathMessageTeamKill[$DamageType::Turret, 0] = '\c0%4 TEAMKILLED %1 with an explosive effect!';
$DeathMessageCTurretTeamKill[$DamageType::Turret, 0] = '\c0%4 TEAMKILLED %1 with an explosive effect!';
$DeathMessageCTurretAccdtlKill[$DamageType::Turret, 0] = '\c0%1 got in the way of a friendly explosive effect!';

$DeathMessageCTurretKill[$DamageType::GeneralExplosive, 0] = '\c0%4\'s explosives makes armored chowder out of %1.';
$DeathMessageCTurretKill[$DamageType::GeneralExplosive, 1] = '\c0%4 leaves %1 as a smoking crater.';
$DeathMessageCTurretKill[$DamageType::GeneralExplosive, 2] = '\c0%4 shows %1 a new world of explosive pain.';

$DeathMessageTurretKill[$DamageType::GeneralExplosive, 0] = '\c0%1 gets exploded by a turret.';
$DeathMessageTurretKill[$DamageType::GeneralExplosive, 1] = '\c0%1 gets exploded by a turret.';
$DeathMessageTurretKill[$DamageType::GeneralExplosive, 2] = '\c0%1 gets exploded by a turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// DisruptorPhaser
$DamageType::DisruptorPhaser = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Disruptor Phaser';

$DeathMessageSelfKill[$DamageType::DisruptorPhaser, 0] = '\c0%1 kills %2self.';
$DeathMessageSelfKill[$DamageType::DisruptorPhaser, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::DisruptorPhaser, 2] = '\c0%1\'s phaser kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::DisruptorPhaser, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::DisruptorPhaser, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::DisruptorPhaser, 0] = '\c0%4 kills %1 with a phaser blast.';
$DeathMessage[$DamageType::DisruptorPhaser, 1] = '\c0%4 pings %1 to death.';
$DeathMessage[$DamageType::DisruptorPhaser, 2] = '\c0%1 gets a pointer in phaser use from %4.';
$DeathMessage[$DamageType::DisruptorPhaser, 3] = '\c0%4 fatally embarrasses %1 with %6 handheld phaser.';
$DeathMessage[$DamageType::DisruptorPhaser, 4] = '\c0%4 unleashes a terminal phaser barrage into %1.';

$DeathMessageTeamKill[$DamageType::DisruptorPhaser, 0] = '\c0%4 TEAMKILLED %1 with a disruptor phaser!';
$DeathMessageCTurretTeamKill[$DamageType::DisruptorPhaser, 0] = '\c0%4 TEAMKILLED %1 with a disruptor phaser turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::DisruptorPhaser, 0] = '\c0%1 got in the way of a friendly disruptor phaser turret!';

$DeathMessageCTurretKill[$DamageType::DisruptorPhaser, 0] = '\c0%4 blows away %1 with a disruptor phaser turret.';
$DeathMessageCTurretKill[$DamageType::DisruptorPhaser, 1] = '\c0%4 fries %1 with a disruptor phaser turret.';
$DeathMessageCTurretKill[$DamageType::DisruptorPhaser, 2] = '\c0%4 lights up %1 with a disruptor phaser turret.';

$DeathMessageTurretKill[$DamageType::DisruptorPhaser, 0] = '\c0%1 gets lit up by a disruptor phaser turret.';
$DeathMessageTurretKill[$DamageType::DisruptorPhaser, 1] = '\c0%1 gets rained on by a nearby disruptor phaser turret.';
$DeathMessageTurretKill[$DamageType::DisruptorPhaser, 2] = '\c0%1 finds %2self on the wrong end of a disruptor phaser turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Cluster
$DamageType::Cluster = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Cluster';

$DeathMessageSelfKill[$DamageType::Cluster, 0] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Cluster, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::Cluster, 2] = '\c0%1 manages to kill %2self with reflected buckshot.';
$DeathMessageSelfKill[$DamageType::Cluster, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::Cluster, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::Cluster, 0] = '\c0%4 shreds %1 with a well placed cluster shot.';
$DeathMessage[$DamageType::Cluster, 1] = '\c0%1 lets freedom ring with a scatter shot from %4.';
$DeathMessage[$DamageType::Cluster, 2] = '\c0%4 flaks %1 with a wall of buckshot.';
$DeathMessage[$DamageType::Cluster, 3] = '\c0%1 becomes %4\'s latest scatter shot pincushion.';
$DeathMessage[$DamageType::Cluster, 4] = '\c0%4 decimates %1 with the cluster shot.';

$DeathMessageTeamKill[$DamageType::Cluster, 0] = '\c0%4 TEAMKILLED %1 with a scatter shot!';
$DeathMessageCTurretTeamKill[$DamageType::Cluster, 0] = '\c0%4 TEAMKILLED %1 with a scatter shot turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Cluster, 0] = '\c0%1 got in the way of a friendly scatter shot turret!';

$DeathMessageCTurretKill[$DamageType::Cluster, 0] = '\c0%4 shreds %1 with a well placed cluster shot.';
$DeathMessageCTurretKill[$DamageType::Cluster, 1] = '\c0%1 lets freedom ring with a scatter shot from %4.';
$DeathMessageCTurretKill[$DamageType::Cluster, 2] = '\c0%4 decimates %1 with the cluster shot.';

$DeathMessageTurretKill[$DamageType::Cluster, 0] = '\c0%1 gets chunked apart by a scatter shot turret.';
$DeathMessageTurretKill[$DamageType::Cluster, 1] = '\c0%1 gets rained on by a nearby scatter shot turret.';
$DeathMessageTurretKill[$DamageType::Cluster, 2] = '\c0%1 finds %2self on the wrong end of a scatter shot turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Flak
$DamageType::Flak = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Flak';

$DeathMessageSelfKill[$DamageType::Flak, 0] = '\c0%1 goes out with a bang!';
$DeathMessageSelfKill[$DamageType::Flak, 1] = '\c0%1 blows %2self into tiny bits and pieces.';
$DeathMessageSelfKill[$DamageType::Flak, 2] = '\c0%1 explodes in that fatal kind of way.';
$DeathMessageSelfKill[$DamageType::Flak, 3] = '\c0%1 caught %2self in fatal flak.';
$DeathMessageSelfKill[$DamageType::Flak, 4] = '\c0%1 flaks %2self all over the map.';

$DeathMessage[$DamageType::Flak, 0] = '\c0%4 intercepts %1 with flak.';
$DeathMessage[$DamageType::Flak, 1] = '\c0%4 watches %6 flak charge blow %1 to pieces.';
$DeathMessage[$DamageType::Flak, 2] = '\c0%1 rides %4\'s flak charge.';
$DeathMessage[$DamageType::Flak, 3] = '\c0%4 delivers a flak payload straight to %1.';
$DeathMessage[$DamageType::Flak, 4] = '\c0%4\'s flak rains little pieces of %1 all over the ground.';

$DeathMessageTeamKill[$DamageType::Flak, 0] = '\c0%4 TEAMKILLED %1 with flak!';
$DeathMessageCTurretTeamKill[$DamageType::Flak, 0] = '\c0%4 TEAMKILLED %1 with flak!';
$DeathMessageCTurretAccdtlKill[$DamageType::Flak, 0] = '\c0%1 got in the way of a friendly flak blasts!';

$DeathMessageCTurretKill[$DamageType::Flak, 0] = '\c0%4 watches %6 flak charge blow %1 to pieces.';
$DeathMessageCTurretKill[$DamageType::Flak, 1] = '\c0%4\'s flak rains little pieces of %1 all over the ground.';
$DeathMessageCTurretKill[$DamageType::Flak, 2] = '\c0%4 delivers a flak payload straight to %1.';

$DeathMessageTurretKill[$DamageType::Flak, 0] = '\c0%1 is killed by a flak turret.';
$DeathMessageTurretKill[$DamageType::Flak, 1] = '\c0%1\'s body now marks the location of a flak turret.';
$DeathMessageTurretKill[$DamageType::Flak, 2] = '\c0%1 is blown away by a flak turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Maser
$DamageType::Maser = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Maser';

$DeathMessageSelfKill[$DamageType::Maser, 0] = '\c0%1 amazingly bends spacetime to kill %2self with a Maser.';
$DeathMessageSelfKill[$DamageType::Maser, 1] = '\c0%1 amazingly bends spacetime to kill %2self with a Maser.';
$DeathMessageSelfKill[$DamageType::Maser, 2] = '\c0%1 amazingly bends spacetime to kill %2self with a Maser.';
$DeathMessageSelfKill[$DamageType::Maser, 3] = '\c0%1 amazingly bends spacetime to kill %2self with a Maser.';
$DeathMessageSelfKill[$DamageType::Maser, 4] = '\c0%1 amazingly bends spacetime to kill %2self with a Maser.';

$DeathMessage[$DamageType::Maser, 0] = '\c0%4 sets %1 ablaze with a well placed maser pulse.';
$DeathMessage[$DamageType::Maser, 1] = '\c0%4 reaches out and torches %1 with %6 maser.';
$DeathMessage[$DamageType::Maser, 2] = '\c0%4 holds %1 down under a magnifying glass under the sun.';
$DeathMessage[$DamageType::Maser, 3] = '\c0%1 finally realized what that orange dot was.';
$DeathMessage[$DamageType::Maser, 4] = '\c0%4 torches off %1\s hair with a maser.';

$DeathMessageTeamKill[$DamageType::Maser, 0] = '\c0%4 TEAMKILLED %1 with a Maser!';
$DeathMessageCTurretTeamKill[$DamageType::Maser, 0] = '\c0%4 TEAMKILLED %1 with a Maser turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Maser, 0] = '\c0%1 got in the way of a friendly Maser turret!';

$DeathMessageCTurretKill[$DamageType::Maser, 0] = '\c0%1 is instantly vaporized by %4\'s Maser.';
$DeathMessageCTurretKill[$DamageType::Maser, 1] = '\c0%4\'s deadly aim with the Maser singes %1.';
$DeathMessageCTurretKill[$DamageType::Maser, 2] = '\c0%4\'s Maser torches off %1\'s hair.';

$DeathMessageTurretKill[$DamageType::Maser, 0] = '\c0%1 is killed by a maser turret.';
$DeathMessageTurretKill[$DamageType::Maser, 1] = '\c0%1\'s body now marks the location of a maser turret.';
$DeathMessageTurretKill[$DamageType::Maser, 2] = '\c0%1 is fried by a maser turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Explosive Bug
$DamageType::ExplosiveBug = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Explosive Bug';

$DeathMessageSelfKill[$DamageType::ExplosiveBug, 0] = '\c0%1 kills %2self with an explosive bug!';
$DeathMessageSelfKill[$DamageType::ExplosiveBug, 1] = '\c0%1 kills %2self with an explosive bug!';
$DeathMessageSelfKill[$DamageType::ExplosiveBug, 2] = '\c0%1 kills %2self with an explosive bug!';
$DeathMessageSelfKill[$DamageType::ExplosiveBug, 3] = '\c0%1 kills %2self with an explosive bug!';
$DeathMessageSelfKill[$DamageType::ExplosiveBug, 4] = '\c0%1 kills %2self with an explosive bug!';

$DeathMessage[$DamageType::ExplosiveBug, 0] = '\c0%4 obliterates %1 with an explosive bug.';
$DeathMessage[$DamageType::ExplosiveBug, 1] = '\c0%4 latches an explosive bug on %1.';
$DeathMessage[$DamageType::ExplosiveBug, 2] = '\c0%4 delivers an explosive bug straight to %1.';
$DeathMessage[$DamageType::ExplosiveBug, 3] = '\c0%4 offers a little explosive bug to %1.';
$DeathMessage[$DamageType::ExplosiveBug, 4] = '\c0%1 didn\'t notice %4\'s explosive bug in time.';

$DeathMessageTeamKill[$DamageType::ExplosiveBug, 0] = '\c0%4 TEAMKILLED %1 with an Explosive Bug!';
$DeathMessageCTurretTeamKill[$DamageType::ExplosiveBug, 0] = '\c0%4 TEAMKILLED %1 with a Explosive Bug turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::ExplosiveBug, 0] = '\c0%1 got in the way of a friendly Explosive Bug turret!';

$DeathMessageCTurretKill[$DamageType::ExplosiveBug, 0] = '\c0Whoops! %1 + %4\'s mortar = Dead %1.';
$DeathMessageCTurretKill[$DamageType::ExplosiveBug, 1] = '\c0%1 learns the happy explosion dance from %4\'s mortar.';
$DeathMessageCTurretKill[$DamageType::ExplosiveBug, 2] = '\c0%4\'s mortar has a blast with %1.';

$DeathMessageTurretKill[$DamageType::ExplosiveBug, 0] = '\c0%1 is pureed by a mortar turret.';
$DeathMessageTurretKill[$DamageType::ExplosiveBug, 1] = '\c0%1 enjoys a mortar turret\'s attention.';
$DeathMessageTurretKill[$DamageType::ExplosiveBug, 2] = '\c0%1 is blown to kibble by a mortar turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Misc strings

$DamageType::Mine = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';

$DeathMessage[$DamageType::Mine, 0] = '\c0%4 reminds %1 that a mine is a terrible thing to waste.';
$DeathMessage[$DamageType::Mine, 1] = '\c0%1 doesn\'t see %4\'s mine in time.';
$DeathMessage[$DamageType::Mine, 2] = '\c0%4 gives %1 a piece of %6 mine.';
$DeathMessage[$DamageType::Mine, 3] = '\c0%1 puts their foot on %4\'s mine.';
$DeathMessage[$DamageType::Mine, 4] = '\c0%1 stepped on %4\'s toe-popper.';

$DeathMessageSelfKill[$DamageType::Mine, 0] = '\c0%1 kills %2self with a mine!';
$DeathMessageSelfKill[$DamageType::Mine, 1] = '\c0%1\'s mine violently reminds %2 of its existence.';
$DeathMessageSelfKill[$DamageType::Mine, 2] = '\c0%1 plants a decisive foot on %3 own mine!';
$DeathMessageSelfKill[$DamageType::Mine, 3] = '\c0%1 fatally trips on %3 own mine!';
$DeathMessageSelfKill[$DamageType::Mine, 4] = '\c0%1 makes a note not to run over %3 own mines.';

$DeathMessageTeamKill[$DamageType::Mine, 0] = '\c0%4 TEAMKILLED %1 with a mine!';

$DamageTypeCount++;

$DamageType::Explosion = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

$DamageType::Impact = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

$DamageType::Ground = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

$DamageType::OutOfBounds = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

$DamageType::Lightning = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

$DamageType::VehicleSpawn = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

$DamageType::ForceFieldPowerup = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

$DamageType::Crash = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

// DMM -- added so MPBs that blow up under water get a message
$DamageType::Water = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

//Tinman - used in Hunters for cheap bastards  ;)
$DamageType::NexusCamping = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

// MES -- added so CTRL-K can get a distinctive message
$DamageType::Suicide = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

$DamageType::LifeSupportSuicide = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

// dependant on being pre-defined

$DeathMessageVehicleCrashCount = 5;
$DeathMessageVehicleCrash[$DamageType::Crash, 0] = '\c0%1 fails to eject in time.';
$DeathMessageVehicleCrash[$DamageType::Crash, 1] = '\c0%1 becomes one with their vehicle dashboard.';
$DeathMessageVehicleCrash[$DamageType::Crash, 2] = '\c0%1 drives under the influence of death.';
$DeathMessageVehicleCrash[$DamageType::Crash, 3] = '\c0%1 makes a perfect three hundred point landing.';
$DeathMessageVehicleCrash[$DamageType::Crash, 4] = '\c0%1 heroically pilots his vehicle into something really, really hard.';

$DeathMessageHeadshotCount = 3;
$DeathMessageHeadshot[$DamageType::Laser, 0] = '\c0%4 drills right through %1\'s braincase with %6 laser.';
$DeathMessageHeadshot[$DamageType::Laser, 1] = '\c0%4 pops %1\'s head like a cheap balloon.';
$DeathMessageHeadshot[$DamageType::Laser, 2] = '\c0%1 loses %3 head over %4\'s laser skill.';

// z0dd - ZOD, 8/25/02. Added Lance rear shot messages
$DeathMessageRearshotCount = 3;
$DeathMessageRearshot[$DamageType::ShockLance, 0] = '\c0%4 delivers a backdoor Lance to %1.';
$DeathMessageRearshot[$DamageType::ShockLance, 1] = '\c0%4 sends high voltage up %1\'s bum.';
$DeathMessageRearshot[$DamageType::ShockLance, 2] = '\c0%1 receives %4\'s rear-entry Lance attack.';

// Executes default datablocks for damage
exec("scripts/defaultBlocks.cs");
