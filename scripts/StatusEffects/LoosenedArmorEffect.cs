// Defender Designator

function LoosenedArmorEffect::setup(%this)
{
    %this.duration = 15000;
    %this.stackable = false;
    %this.stackableDuration = 0;
    %this.ticking = false;
    %this.effectInvulnPeriod = 7000;
}

function LooseArmorEffect::validateEffect(%this, %obj, %instigator)
{
    if(%obj.isShielded || %obj.isMounted())
        return false;

    return true;
}

function LooseArmorEffect::startEffect(%this, %obj, %instigator)
{
    Parent::startEffect(%this, %obj, %instigator);

    messageClient(%obj.client , 'MsgStartLooseArmor' , "\c2Warning! Armor misaligned from impact, incoming damage increased by 50%!");
    %obj.damageReduceFactor += 0.5;
}

function LooseArmorEffect::endEffect(%this, %obj)
{
    if(Parent::endEffect(%this, %obj))
    {
        %obj.damageReduceFactor -= 0.5;
        messageClient(%obj.client , 'MsgEndLooseArmor' , '\c2Armor platings realigned and integrity restored.');
    }
}

StatusEffect.registerEffect("LoosenedArmorEffect");
