// ExplosiveBug

datablock LinearFlareProjectileData(ExplosiveBugTriggered)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 1.5;
   damageRadius        = 7.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 1500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::ExplosiveBug;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 200;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MissileExplosion";
//   underwaterExplosion = "UnderwaterMortarExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function ExplosiveBugEffect::setup(%this)
{
    %this.duration = 60000;
    %this.stackable = true;
    %this.stackableDuration = 10000;
    %this.ticking = true;
    %this.tickTimeMultiplier = 1.0;
    %this.effectInvulnPeriod = 20000;
}

function ExplosiveBugEffect::validateEffect(%this, %obj, %instigator)
{
    if(%obj.isShielded || %obj.isMounted())
        return false;

    return true;
}

function ExplosiveBugEffect::startEffect(%this, %obj, %instigator)
{
    Parent::startEffect(%this, %obj, %instigator);

    %obj.bugDetonated = false;
    
    if(%obj.isPlayer())
        messageClient(%obj.client, 'MsgStartExplosiveBug' , '\c2Explosive bug latching mechanism detected, beginning disarming sequence...');
}

function ExplosiveBugEffect::tickEffect(%this, %obj, %instigator)
{
    if(Parent::tickEffect(%this, %obj, %instigator))
    {
        if(%obj.getHeat() > 0.85 && %obj.bugDetonated != true)
        {
            %p = createRemoteProjectile("LinearFlareProjectile", "ExplosiveBugTriggered", %obj.getVelocity(), %obj.getWorldBoxCenter(), 0, %instigator);
            %p.damageBuffFactor += %obj.effectStacks[%this.getName()] * 0.5;

            if(%obj.isPlayer())
                messageClient(%obj.client, 'MsgExpExplosiveBug' , '\c2Explosive bug detonation detected!');
            
            %obj.bugDetonated = true;
            %this.forceEndEffect(%obj);
        }
    }
}

function ExplosiveBugEffect::endEffect(%this, %obj)
{
    if(Parent::endEffect(%this, %obj) && !%obj.bugDetonated && %obj.isPlayer())
        messageClient(%obj.client, 'MsgEndExplosiveBug', '\c2Explosive bug disarmed!');
}

StatusEffect.registerEffect("ExplosiveBugEffect");
