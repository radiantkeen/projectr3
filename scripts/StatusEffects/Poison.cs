// Poison - ignores armor
// poison cloud? - green smoke
function PoisonEffect::setup(%this)
{
    %this.duration = 30000;
    %this.stackable = true;
    %this.stackableDuration = 10000;
    %this.ticking = true;
    %this.tickTimeMultiplier = 1.0;
    %this.effectInvulnPeriod = 15000;

    %this.poisonSpikeDamage = 1;
}

function PoisonEffect::validateEffect(%this, %obj, %instigator)
{
    if(%obj.isShielded || %obj.isMounted() || %obj.lifeSupport || %obj.isWalker)
        return false;
    else if(%obj.isPlayer())
        return true;
        
    return false;
}

function PoisonEffect::startEffect(%this, %obj, %instigator)
{
    Parent::startEffect(%this, %obj, %instigator);

//    %obj.play3D("PosonCoughSound");

    messageClient(%obj.client , 'MsgStartPoisonEffect' , '\c2Warning! Neurotoxins detected, applying anti-toxins...');
}

function PoisonEffect::onStackEffect(%this, %obj, %instigator)
{
    Parent::onStackEffect(%this, %obj, %instigator);

    messageClient(%obj.client , 'MsgStackPoisonEffect' , '\c2Warning! Additional neurotoxins applied!');
}

function PoisonEffect::tickEffect(%this, %obj, %instigator)
{
    if(%obj.isDead)
        return;
        
    if(Parent::tickEffect(%this, %obj, %instigator))
    {
        %self = %this.getName();
        
        if(%obj.tickCount[%self] % 32 == 0 && getRandom() > 0.8)
        {
            %obj.doInternalDamage(%instigator, %this.poisonSpikeDamage, $DamageType::Poison, $DamageGroupMask::ExternalMeans, 0); // element, srcproj
            playPain(%obj);
        }

//        if(getRandom() > 0.9)
//        {
            // cough and bellow green smoke
//            createLifeEmitter(%obj.position, "PoisonBreathEmitter", 400);
//        }
    }
}

function PoisonEffect::endEffect(%this, %obj)
{
    if(Parent::endEffect(%this, %obj))
        messageClient(%obj.client , 'MsgEndPoisonEffect' , '\c2Neurotoxin load has been reduced to non-lethal levels.');
}

StatusEffect.registerEffect("PoisonEffect");
