function EnduriumPlating::applyEffect(%this, %obj, %armorName)
{
    %amount = 20 * %obj.getClassSize();

    %obj.maxHitPoints += %amount;
    %obj.hitPoints += %amount;
    %obj.bonusWeight += 6;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "EnduriumPlating", "Endurium Plating", "Increases Max HP/Weight by 20/10kg x Class size", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
