function KineticResistPlating::applyEffect(%this, %obj, %armorName)
{
    %obj.armorDamageFactor[$DamageGroupMask::Kinetic] -= 0.2;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "KineticResistPlating", "Ballistic Plating", "20% Kinetic damage reduction", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
