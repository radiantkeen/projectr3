function ArmorHPPlus10::applyEffect(%this, %obj, %armorName)
{
    %amount = 10 * %obj.getClassSize();

    %obj.maxHitPoints += %amount;
    %obj.hitPoints += %amount;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "ArmorHPPlus10", "Armor Plating", "Increases Max HP by 10 x Class size", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
