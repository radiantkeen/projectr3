function GravitronThruster::applyEffect(%this, %obj, %armorName)
{
    %obj.bonusWeight -= 6;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "GravitronThruster", "Gravitron Thruster", "Reduces weight by 10kg", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
