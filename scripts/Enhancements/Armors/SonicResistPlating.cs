function SonicResistPlating::applyEffect(%this, %obj, %armorName)
{
    %obj.armorDamageFactor[$DamageGroupMask::Sonic] -= 0.2;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "SonicResistPlating", "Aural Compressor", "20% Sonic damage reduction", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
