function ArmorDRPlus::applyEffect(%this, %obj, %armorName)
{
    %obj.damageReduction += 1 * %obj.getClassSize();
}

Enhancement.registerEnhancement($EnhancementType::Armor, "ArmorDRPlus", "Deflection Plating", "Reduces HP damage taken by 1 x Class size", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
