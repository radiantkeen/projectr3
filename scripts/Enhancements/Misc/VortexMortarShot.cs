datablock GrenadeProjectileData(VortexMortarShot) : MortarShot
{
   armingDelayMS     = 15000;
   muzzleVelocity    = 95.5;
   drag              = 0.1;
};

function VortexMortarShot::onExplode(%data, %proj, %pos, %mod)
{
    %proj.vortexAura.stop();

    Parent::onExplode(%data, %proj, %pos, %mod);
}

function VortexMortarShell::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 0;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function VortexMortarShell::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = createProjectile("GrenadeProjectile", "VortexMortarShot", %vector, %pos, %obj, %slot, %obj);
    %aura = Aura.create("VortexMortar");
    %aura.attachToObject(%p);
    %aura.schedule(1000, "start");
    %p.vortexAura = %aura;
    
    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "VortexMortarShell", "Vortex Mortar", "Test mode - sucks everything in and explodes", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mortar);
