datablock ExplosionData(MitziOrbSubExplosion1)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 15.0;

   playSpeed = 1.5;

   sizes[0] = "3 3 3";
   sizes[1] = "3 3 3";
   times[0] = 0.0;
   times[1] = 1.0;

};

datablock ExplosionData(MitziOrbSubExplosion2)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;

   delayMS = 50;

   offset = 15.0;

   playSpeed = 1.0;

   sizes[0] = "6.0 6.0 6.0";
   sizes[1] = "6.0 6.0 6.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(MitziOrbSubExplosion3)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = 0.0;
   playSpeed = 0.7;

   sizes[0] = "6.0 6.0 6.0";
   sizes[1] = "9 9 9";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ParticleData(MitziOrbitalExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 250;
   textureName          = "particleTest";
   colors[0]     = "0.2 1.0 0.1 1.0";
   colors[1]     = "0.075 0.8 0.01 0.0";
   sizes[0]      = 3;
   sizes[1]      = 7;
};

datablock ParticleEmitterData(MitziOrbitalExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 90;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MitziOrbitalExplosionParticle";
};

datablock ExplosionData(MitziOrbitalExplosion)
{
   explosionShape = "mortar_explosion.dts";
   soundProfile   = UnderwaterMortarExplosionSound;

   sizes[0] = "6 6 6";
   sizes[1] = "8 8 8";
   sizes[2] = "12 12 12";

   subExplosion[0] = MitziOrbSubExplosion1;
   subExplosion[1] = MitziOrbSubExplosion2;
   subExplosion[2] = MitziOrbSubExplosion3;

   emitter[0] = MortarExplosionSmokeEmitter;
   
   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "25.0 25.0 25.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;

   particleEmitter = MitziOrbitalExplosionEmitter;
   particleDensity = 600;
   particleRadius = 9.5;
   faceViewer = true;
};

datablock EnergyProjectileData(MitziOrbitalBlast)
{
   emitterDelay        = -1;
   directDamage        = 0.0;
   directDamageType    = $DamageType::MitziBlast;
   kickBackStrength    = 0.0;
   bubbleEmitTime      = 1.0;

   hasDamageRadius     = true;
   indirectDamage      = 25;
   kickBackStrength    = 3000;
   damageRadius        = 16.0;
   radiusDamageType    = $DamageType::MitziBlast;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MitziBlast;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 2500;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   sound = BlasterProjectileSound;
   velInheritFactor    = 1.0;

   explosion           = "MitziOrbitalExplosion";
   splash              = BlasterSplash;

   grenadeElasticity = 0.998;
   grenadeFriction   = 0.0;
   armingDelayMS     = 250;

   muzzleVelocity    = 500;

   drag = 0.05;

   gravityMod        = 0.0;

   dryVelocity       = 500;
   wetVelocity       = 500;

   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 30000;

   hasLight    = true;
   lightRadius = 20.0;
   lightColor  = "0.1 1.0 0.2";

   scale = "12 12 12";
   crossViewAng = 0.8;
   crossSize = 12.0;

   ignoreReflection = true;

   lifetimeMS     = 30000;
   blurLifetime   = 1.5;
   blurWidth      = 10.0;
   blurColor = "0.1 1.0 0.2 1.0";

   texture[0] = "special/expflare";//"special/blasterBolt";
   texture[1] = "special/expflare";//"special/blasterBoltCross";
};
