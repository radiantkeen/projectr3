datablock LinearFlareProjectileData(GreenDisruptorBolt)
{
   scale               = "2 2 2";
   directDamage        = 0.4;
   directDamageType    = $DamageType::GreenBlaster;
   hasDamageRadius     = true;
   indirectDamage      = 0.6;
   damageRadius        = 4.0;
   kickBackStrength    = 1000;
   radiusDamageType    = $DamageType::Blaster;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disruptor;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 30;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Explosive;
   mdDamageAmount[1]   = 30;
   mdDamageRadius[1]   = true;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   explosion           = "PCRExplosion";
   underwaterExplosion = "PCRExplosion";
   
   dryVelocity       = 200.0;
   wetVelocity       = 200.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 100;

   numFlares         = 15;
   size              = 0.25;
   flareColor        = "0.25 1.0 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 2.5;
   lightColor  = "0.25 1.0 0.25";
};

function DisruptorGreen::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 24;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function DisruptorGreen::onInit(%this)
{
    %this.modeSwitchSound = "ElfFireWetSound";
    %this.fireSound = "PCRFireSound";
}

function DisruptorGreen::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearFlareProjectile", "GreenDisruptorBolt", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "DisruptorGreen", "Phaser Disruptor", "Explosive blast of disruptor energy, uses 24 power", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Blaster | $WeaponsList::DualDisruptor);
