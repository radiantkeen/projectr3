function ArcArc::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 3;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function ArcArc::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = createProjectile("ELFProjectile", "BasicELF", %vector, %pos, %obj, %slot, %obj);
    %obj.lastProjectile = %p;

    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ArcArc", "Arc Beam", "Constant raw electrical dump straight from the onboard reactor", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::ELFGun);
