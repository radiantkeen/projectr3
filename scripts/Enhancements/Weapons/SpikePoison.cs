datablock EnergyProjectileData(SpikePoisonShot) : SpikeSingleShot
{
   directDamage        = 0.0;
   kickBackStrength    = 400.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Spike;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;
   
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 1.5;
   radiusDamageType    = $DamageType::OutdoorDepTurret;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.322 0.737 0.333 1";
   scale = "1.5 10.0 1.0";
   crossViewAng = 0.99;
   crossSize = 1.0;
   blurLifetime   = 0.2;
   blurWidth      = 0.5;
   blurColor = "0.322 0.737 0.333 1.0";
   texture[0] = "special/underwaterSpark";
   texture[1] = "special/blasterHit";

   radialStatusEffect  = "PoisonEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
};

function SpikePoison::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 0;
    %gyan = 1;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }
    
    return %energy SPC %ammoUse SPC %gyan;
}

function SpikePoison::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %obj.fireTimeout[%data.item, %this] = getSimTime() + (1500 / (1 + %obj.rateOfFire[%data.item]));
    %obj.schedule(1250, "play3D", GrenadeDryFireSound);
    
    %obj.play3D("OBLFireSound");
    %p = createProjectile("EnergyProjectile", "SpikePoisonShot", %vector, %pos, %obj, %slot, %obj);

    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "SpikePoison", "Spike Poison", "Formulated for doing internal damage, uses 2 ammo, 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Spike);
