//----------------------------------------------------
//  Explosion
//----------------------------------------------------
datablock LinearFlareProjectileData(WarpDisruptorBolt)
{
   directDamage        = 0.1;
   directDamageType    = $DamageType::Disruptor;
   doDynamicClientHits = true;

   explosion           = "SentryTurretExplosion";
   kickBackStrength  = 0.0;
   activateDelayMS = -1;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disruptor;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 20;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   dryVelocity       = 1500.0;
   wetVelocity       = 750.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   numFlares         = 10;
   size              = 0.35;
   flareColor        = "1 0.35 0.35";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1 0.35 0.35";
};


//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(HandDisruptorImage)
{
   className = WeaponImage;
   shapeFile = "turret_muzzlepoint.dts";
   item = Blaster;
   projectile = DisruptorBolt;
   projectileType = LinearFlareProjectile;

   usesEnergy = true;
   fireEnergy = 8;
   minEnergy = 8;

   enhancementSlots = 0;

   weaponDescName = "Hand Disruptor";
   defaultModeName = "Disruptor Blast";
   defaultModeDescription = "Disruptor built into the palms of the Plugsuit for self defense";
   defaultModeSwitchSound = "BlasterDryFireSound";
   defaultModeFireSound = "BlasterFireSound";
   defaultModeFailSound = "BlasterDryFireSound";

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = HandDisruptorActivateSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 1.0;
   stateFire[3] = true;
   stateRecoil[3] = NoRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
//   stateSound[3] = BlasterFireSound;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTriggerUp[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.2;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

datablock ItemData(HandDisruptor)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "turret_muzzlepoint.dts";
   image = HandDisruptorImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a Hand Disruptor";
};

function DisruptorWarp::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 15;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";
        
    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function DisruptorWarp::onInit(%this)
{
    %this.modeSwitchSound = "ElfFireWetSound";
    %this.fireSound = "SentryTurretFireSound";
}

function DisruptorWarp::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearFlareProjectile", "WarpDisruptorBolt", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "DisruptorWarp", "Warp Disruptor", "A low power disruptor that travels very fast", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Blaster | $WeaponsList::DualDisruptor);
