function EVulcanGauss::applyEffect(%this, %obj, %item)
{
    %obj.spreadFactor[%item] -= 0.6;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "EVulcanGauss", "Gauss Barrel", "Reduces spread by 60%", $Enhancement::AllArmorCompat, $WeaponsList::Chaingun | $WeaponsList::DualVulcan);
