datablock LinearFlareProjectileData(MPDPulsarBolt)
{
   directDamage        = 0.0;
   directDamageType    = $DamageType::MPDisruptor;

   scale              = "2.9 2.9 2.9";

   explosion          = "MegaPulseExplosion";
   baseEmitter        = MegaPulseTrailEmitter;

   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 3.0;
   radiusDamageType    = $DamageType::MPDisruptor;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MPDisruptor;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 75;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   dryVelocity       = 250.0;
   wetVelocity       = 250.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1750;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2000;

   numFlares         = 20;
   size              = 0.6;
   flareColor        = "1 0.5 0.5";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1.0 0.5 0.5";
};

function MPDPulsar::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 40;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function MPDPulsar::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    schedule(250, %obj, "PBurstFire", %obj, %slot);
    return PBurstFire(%obj, %slot);
}

function PBurstFire(%obj, %slot, %pos)
{
    %vector = vectorSpread(%obj.getMuzzleVector(0), 5);
    %pos = %obj.getMuzzlePoint(%slot);
    %obj.play3D("SentryTurretFireSound");
    return createProjectile("LinearFlareProjectile", "MPDPulsarBolt", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MPDPulsar", "Pulsar", "Burst fire, trades accuracy for damage", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::MPDisruptor);
