datablock LinearFlareProjectileData(PlasmaIonicBolt) : PlasmaCannonBolt
{
   damageRadius        = 14.0;
   kickBackStrength    = 0;
   explosion           = PlasmaBarrelBoltExplosion;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Plasma;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 160;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Energy;
   mdDamageAmount[1]   = 40;
   mdDamageRadius[1]   = false;
   
   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.5;

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 0.333;
   statusEffectMask    = $TypeMasks::PlayerObjectType;

   dryVelocity       = 200.0;
   fizzleTimeMS      = 2400;
   lifetimeMS        = 2500;
};

datablock ShockLanceProjectileData(IonicBallArc)
{
   directDamage        = 0.0;
   radiusDamageType    = $DamageType::SubspaceMagnet;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Plasma;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 10;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   zapDuration = 1.0;
   impulse = 500;
   boltLength = 20.0;
   extension = 29.0;            // script variable indicating distance you can shock people from
   lightningFreq = 25.0;
   lightningDensity = 1.5;  //3 controls the lighning lines, less = no sparks off the player
   lightningAmp = 0.45;
   lightningWidth = 0.08;  //makes it thicker lightning bolts

   shockwave = ShocklanceHit;

   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/BlueImpact";
   texture[1] = "special/BlueImpact";
   texture[2] = "special/BlueImpact";
   texture[3] = "special/ELFBeam";

   emitter[0] = ShockParticleEmitter;
};

function PlasmaIonicBolt::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);
    %data = %this; // this is just being lazy now
    
    InitContainerRadiusSearch(%proj.position, 15, $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.team == %proj.instigator.team)
            continue;

        %tgtPos = %int.getWorldBoxCenter();
        %hit = ContainerRayCast(%proj.position, %tgtPos, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType | $TypeMasks::ForceFieldObjectType, 0);

        if(%hit)
            continue;
        else
        {
            %vector = VectorNormalize(VectorSub(%tgtPos, %proj.position));
            %int.lastHitFlags = IonicBallArc.flags;
            
            createRemoteLance("IonicBallArc", %vector, %proj.position, 0, %proj.instigator, %int);
            %int.damage(%proj.instigator, %tgtPos, 20 * %proj.damageBuffFactor, %data.mdDeathMessageSet, %proj, %proj.mdDamageType[0]);
            %int.play3D(ShockLanceHitSound);
        }
    }
}


function PCIonicPlasma::validateUse(%this, %obj, %data)
{
    %ammoUse = 30;
    %energy = 0;
    %gyan = 2;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function PCIonicPlasma::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = createProjectile("LinearFlareProjectile", "PCDisplayCharge", %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
    %obj.play3D("PBCShockwaveSound");
    %this.schedule(250, "PCDelayFire", %obj, %slot);

    return %p;
}

function PCIonicPlasma::PCDelayFire(%this, %obj, %slot)
{
    %data = %this.data;
    %proj = createProjectile("LinearFlareProjectile", "PlasmaIonicBolt", %obj.getMuzzleVector(%slot), %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
    %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "PCIonicPlasma", "Ionic Plasma Ball", "Charged plasma zaps passing targets - 30 ammo, 2 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::PlasmaCannon);
