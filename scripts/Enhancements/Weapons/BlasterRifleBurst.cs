datablock TracerProjectileData(BlasterBurstBolt) : BlasterRifleBolt
{
   directDamage        = 0.45;
   kickBackStrength    = 350.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::BlasterRifle;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 65;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   tracerLength    = 22;
   tracerAlpha     = true;
   tracerMinPixels = 3;
   tracerColor     = "1.0 0.647 0.0 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "small_circle";
   tracerWidth     = 0.2;
   crossSize       = 1.9;
   crossViewAng    = 0.7;
   renderCross     = true;
};

function BlasterBurstFire::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 21;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function BlasterBurstFire::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %obj.fireTimeout[%data.item, %this] = getSimTime() + (1000 / (1 + %obj.rateOfFire[%data.item]));
    %obj.schedule(32, "setImageTrigger", 0, false);
    
    // Since this is only 2 shots, and it has to be done specially... will go a different route
    schedule(175, %obj, "BBurstFire", %obj, %slot);
    return BBurstFire(%obj, %slot);
}

function BBurstFire(%obj, %slot, %pos)
{
    %vector = vectorSpread(%obj.getMuzzleVector(%slot), 2 * (%obj.spreadFactorBase + %obj.spreadFactor[%data.item]));
    %pos = %obj.getMuzzlePoint(%slot);
    %obj.play3D("BomberTurretFireSound");
    return createProjectile("TracerProjectile", "BlasterBurstBolt", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "BlasterBurstFire", "Dual Shot", "Fires off a 2 round burst, slightly more accuracy.", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::BlasterRifle);
