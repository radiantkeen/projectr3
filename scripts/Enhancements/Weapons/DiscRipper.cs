datablock LinearProjectileData(RipperDiscProjectile)
{
   projectileShapeName = "disc.dts";
   emitterDelay        = -1;
   directDamage        = 0.25;
   directDamageType    = $DamageType::Disc;
   hasDamageRadius     = true;
   indirectDamage      = 0.50;
   damageRadius        = 7.5;
   radiusDamageType    = $DamageType::Disc;
   kickBackStrength    = 2000;  // z0dd - ZOD, 4/24/02. Was 1750

   // Multi-damage system for elemental damage attacks
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disc;
   mdPiercing          = true;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 250;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   sound 				= discProjectileSound;
   explosion           = "DiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   dryVelocity       = 95; // z0dd - ZOD, 3/30/02. Slight disc speed up. was 90
   wetVelocity       = 55; // z0dd - ZOD, 3/30/02. Slight disc speed up. was 50
   velInheritFactor  = 0.75; // z0dd - ZOD, 3/30/02. was 0.5
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 20.0; // z0dd - ZOD, 4/24/02. Was 0.0. 20 degrees skips off water
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = 200;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

function DiscRipper::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 0;
    %gyan = 1;
       
    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function DiscRipper::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearProjectile", "RipperDiscProjectile", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "DiscRipper", "Ripper Disc", "Causes devistating internal damage to armors - 2 ammo 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Ripper | $WeaponsList::Disc);
