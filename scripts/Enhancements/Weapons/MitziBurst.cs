datablock LinearFlareProjectileData(MitziShotgunProjectile)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "3.0 3.0 3.0";
   faceViewer          = true;
   directDamageType    = $DamageType::MitziBlast;
   directDamage        = 0.15;
   hasDamageRadius     = true;
   indirectDamage      = 0.225;
   damageRadius        = 3.0;
   kickBackStrength    = 500;
   radiusDamageType    = $DamageType::MitziBlast;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MitziBlast;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[0]   = 23;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[1]   = 15;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;
   
   explosion           = "MitziShotgunExplosion";
   splash              = PlasmaSplash;

   dryVelocity       = 200.0;
   wetVelocity       = 300.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 750;
   lifetimeMS        = 1250;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "1 0.8 0.9";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 1 1";
};

function MitziBurstShot::validateUse(%this, %obj, %data)
{
    %ammoUse = 50;
    %energy = 0;
    %gyan = 2;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function MitziBurstShot::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = 0;
    %item = %obj.getMountedImage($WeaponSlot).item;

    for(%i = 0; %i < 7; %i++)
    {
        %p = createProjectile("LinearFlareProjectile", "MitziShotgunProjectile", vectorSpread(%vector, 17), %pos, %obj, %slot, %obj);
        %p.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%item];
    }

    %obj.applyKick(-1650);
    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MitziBurstShot", "Mitzi Burst", "Shotgun blast of Mitzi Bolts - 50 cap, 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mitzi);
