datablock ParticleData(OtherSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 200;
   lifetimeVarianceMS   = 0;
   textureName          = "special/blueSpark";
   colors[0]     = "1.0 0.8 0.8 1.0";
   colors[1]     = "1.0 0.8 0.8 1.0";
   colors[2]     = "1.0 0.8 0.8 0.0";
   sizes[0]      = 0.35;
   sizes[1]      = 0.15;
   sizes[2]      = 0.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(OtherSparksEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 4;
   velocityVariance = 2;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   particles = "OtherSparks";
};

datablock ELFProjectileData(GrappleBeam)
{
   beamRange         = 45;
   numControlPoints  = 8;
   restorativeFactor = 3.75;
   dragFactor        = 4.5;
   endFactor         = 2.25;
   randForceFactor   = 2;
   randForceTime     = 0.125;
	drainEnergy			= 0.0;
	drainHealth			= 0.0;
   directDamageType  = $DamageType::ELF;
	mainBeamWidth     = 0.1;           // width of blue wave beam
	mainBeamSpeed     = 9.0;            // speed that the beam travels forward
	mainBeamRepeat    = 0.25;           // number of times the texture repeats
   lightningWidth    = 0.2;
   lightningDist      = 0.225;           // distance of lightning from main beam
   beamType          = 1;
   
   fireSound    = ElfGunFireSound;
   wetFireSound = ElfFireWetSound;

	textures[0] = "special/redbump2";
   textures[1] = "special/ELFBeam";
   textures[2] = "special/redflare";

   emitter = OtherSparksEmitter;
};

function ELFProjectile::tickGrapple(%this, %data, %target, %targeter)
{
//    %weapon = %targeter.getMountedImage(0).item;

    // Prevent pulling own vehicle
    %objMount = %targeter.getObjectMount();

    if(%target == %objMount && %objMount.getType() & $TypeMasks::VehicleObjectType)
        return;

    %tType = %target.getType();
    
    if(%tType & ($TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ItemObjectType))
    {
        %sMass = %targeter.getDatablock().mass;
        %tMass = %target.getDatablock().mass;
        %sPos = %targeter.getWorldBoxCenter();
        %tPos = %target.getWorldBoxCenter();
        %massDiff = %sMass - %tMass;

        if(%massDiff < -500 || %massDiff > 500)
            %massDiff = %massDiff < 0 ? -500 : 500;

        %vec = VectorFromPoints(%sPos, %tPos);
        %newVec = VectorScale(%vec, %massDiff);
        %newVec = VectorAdd(%newVec, "0 0 0.1");
        
        if(%massDiff < 0)
            %targeter.applyImpulse(%sPos, %newVec);
        else
            %target.applyImpulse(%tPos, %newVec);
    }
}
         
function ArcGrappleBeam::validateUse(%this, %obj, %data)
{
    return "0 0 0";
}

function ArcGrappleBeam::onInit(%this)
{
    %this.addDrain = 2;
}

function ArcGrappleBeam::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = createProjectile("ELFProjectile", "GrappleBeam", %vector, %pos, %obj, %slot, %obj);
    %obj.lastProjectile = %p;

    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ArcGrappleBeam", "Grapple Beam", "Pulls the lighter object to the heavier object.", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::ELFGun);
