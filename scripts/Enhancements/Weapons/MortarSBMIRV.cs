datablock GrenadeProjectileData(StarburstMIRVShot)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 500;

   explosion           = "MissileExplosion";
//   underwaterExplosion = "MissileExplosion";
   velInheritFactor    = 1.0;
   splash              = MortarSplash;
//   depthTolerance      = 10.0; // depth at which it uses underwater explosion

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   baseEmitter         = StreakTrailEmitter;
   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.01;
   grenadeFriction   = 0.99;
   armingDelayMS     = 250;

   gravityMod        = 1.35;
   muzzleVelocity    = 100.0;
   drag              = 0.1;

   sound			 = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.2 0.2 0.2";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";
};

datablock GrenadeProjectileData(StarburstMIRVMortarShot)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.3;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 30;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion           = "MissileExplosion";
//   underwaterExplosion = "MissileExplosion";
   velInheritFactor    = 0.5;
   splash              = MortarSplash;
//   depthTolerance      = 10.0; // depth at which it uses underwater explosion

   baseEmitter         = MortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.01;
   grenadeFriction   = 0.99;
   armingDelayMS     = 250;

   gravityMod        = 1.1;
   muzzleVelocity    = 95.5;
   drag              = 0.1;

   sound			 = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.05 0.2 0.05";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";
};

datablock LinearFlareProjectileData(StarburstMIRVMortarTriggered)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MissileExplosion";
//   underwaterExplosion = "UnderwaterMortarExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function MortarSBMIRV::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }
    
    return %energy SPC %ammoUse SPC %gyan;
}

function MortarSBMIRV::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = createProjectile("GrenadeProjectile", "StarburstMIRVMortarShot", %vector, %pos, %obj, %slot, %obj);
    %p.lastPosition = %pos;
    
    return %p;
}

function MortarSBMIRV::onTriggerFire(%this, %obj, %data)
{
    // Mode glitch bug
    if(%obj.lastProjectile.getDatablock().getName() !$= "StarburstMIRVMortarShot")
        return;
        
    // GrenadeProjectile::getVelocity() by DarkDragonDX
    %fwdVec = vectorNormalize(%obj.lastProjectile.getVelocity()); 

    for(%i = 0; %i < 12; %i++)
        createRemoteProjectile("GrenadeProjectile", "StarburstMIRVShot", vectorSpread(%fwdVec, 28), %obj.lastProjectile.position, %slot, %obj);

    transformProjectile(%obj.lastProjectile, "LinearFlareProjectile", "StarburstMIRVMortarTriggered", %obj.lastProjectile.position, %obj.lastProjectile.initialDirection);
}

function MortarSBMIRV::onInit(%this)
{
    %this.hasTrigger = true;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MortarSBMIRV", "Starburst MIRV Shell", "Trigger charge, sprays grenades forwards in a cone. 2 ammo.", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mortar);
