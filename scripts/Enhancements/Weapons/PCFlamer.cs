datablock ParticleData(SmallFlameParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 1654;
    lifetimeVarianceMS = 443;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "special/tracer00.PNG";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.892000 0.304000 0.340000 1.000000";
    colors[1] = "0.850394 0.304000 0.192000 0.850394";
    colors[2] = "0.000000 0.000000 0.000000 0.709677";
    sizes[0] = 1.97581;
    sizes[1] = 2.15163;
    sizes[1] = 2.35484;
};

datablock ParticleEmitterData(SmallFlameEmitter)
{
    ejectionPeriodMS = 18;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 14.5161;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles = 0;
    orientOnVelocity = 1;
    particles = "SmallFlameParticle";
};

datablock LinearProjectileData(PlasmaFlameStream)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.1;
   damageRadius        = 8;
   radiusDamageType    = $DamageType::Plasma;
   kickBackStrength    = 5;
   baseEmitter         = SmallFlameEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Flamethrower;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 10;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   sound 				= GrenadeProjectileSound;
   explosion           = "PlasmaBoltExplosion";
   splash              = PlayerSplash;

   dryVelocity       = 80;
   wetVelocity       = 1;
   velInheritFactor  = 0.5;
   fizzleTimeMS      = 520;
   lifetimeMS        = 520;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 32;

    activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6;
   lightColor  = "1 0.8 0.01";
};

function PlasmaFlameStream::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);

    StatusEffect.applyEffect("BurnEffect", %targetObject, %projectile.instigator);
}

datablock ShapeBaseImageData(PlasmaFlamerImage) : PlasmaCannonImage
{
   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Fire";
   stateTransitionOnTriggerUp[3] = "Reload";
   stateTimeoutValue[3] = 0.05;
   stateFire[3] = true;
   stateRecoil[3] = "";
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateSound[3] = ScoutFlyerThrustSound;

   stateName[4] = "Reload";
   stateTimeoutValue[4] = 1.0;
};

function PlasmaFlamerImage::validateFireMode(%data, %obj)
{
    %ammoUse = 1;
    %gyanUse = 0;
    %energyUse = 1;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energyUse SPC %ammoUse SPC %gyanUse;
}

function PCFlamer::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %gyanUse = 0;
    %energyUse = 1;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function PCFlamer::onInit(%this)
{
    %this.subImage = "PlasmaFlamerImage";
    %this.fireSound = "n";
}

function PCFlamer::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %obj.setHeat(%obj.getHeat() + 0.01);

    return createProjectile("LinearProjectile", "PlasmaFlameStream", vectorSpread(%vector, 3), %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "PCFlamer", "Flamethrower", "Reach out and torch someone!", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::PlasmaCannon);
