datablock GrenadeProjectileData(ImpactMortarShot)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 2500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 200;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   velInheritFactor    = 0.5;
   splash              = MortarSplash;
   depthTolerance      = 10.0; // depth at which it uses underwater explosion

   baseEmitter         = MortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.01;
   grenadeFriction   = 0.99;
   armingDelayMS     = 0;
   
   gravityMod        = 1.1;
   muzzleVelocity    = 95.5;
   drag              = 0.1;

   sound			 = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.05 0.2 0.05";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";
};

function MortarImpactShell::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function MortarImpactShell::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("GrenadeProjectile", "ImpactMortarShot", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MortarImpactShell", "Impact Explosive Shell", "50% damage, explodes on impact", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mortar);
