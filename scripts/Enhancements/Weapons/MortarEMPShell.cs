datablock ShockwaveData(EMPShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 45;
   acceleration = 100.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 1.4 0.50";
   colors[1] = "0.4 0.4 1.4 0.25";
   colors[2] = "0.4 0.4 1.4 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ShockwaveData(EMPShockwave2D)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 45;
   acceleration = 100.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 1.4 0.50";
   colors[1] = "0.4 0.4 1.4 0.25";
   colors[2] = "0.4 0.4 1.4 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ParticleData(EMPulseMortarExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.30;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 500;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.7 0.7 0.9 0.0";
   colors[1]     = "0.4 0.4 0.8 0.5";
   colors[2]     = "0.4 0.4 0.8 0.5";
   colors[3]     = "0.4 0.4 0.8 0.0";
   sizes[0]      = 5.0;
   sizes[1]      = 6.0;
   sizes[2]      = 10.0;
   sizes[3]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(EMPulseMortarExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 8.0;


   ejectionVelocity = 1.25;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 90.0;

   lifetimeMS       = 500;

   particles = "EMPulseMortarExplosionSmoke";
};

datablock ParticleData(EMPulseDebrisSmokeParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 2177;
    lifetimeVarianceMS = 443;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 0.354839;
    times[2] = 1;
    colors[0] = "0.149606 0.296000 1.000000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    colors[2] = "1.000000 1.000000 1.000000 0.000000";
    sizes[0] = 1.0;
    sizes[1] = 0.67484;
    sizes[2] = 0.3;
};

datablock ParticleEmitterData(EMPulseDebrisSmokeEmitter)
{
    ejectionPeriodMS = 9;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 13.7903;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "EMPulseDebrisSmokeParticle";
};

datablock ParticleData( EMPulseCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0]     = "0.7 0.7 1.0 1.0";
   colors[1]     = "0.7 0.7 1.0 0.5";
   colors[2]     = "0.7 0.7 1.0 0.0";
   sizes[0]      = 4.0;
   sizes[1]      = 8.0;
   sizes[2]      = 9.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( EMPulseCrescentEmitter )
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 0;
   ejectionVelocity = 40;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "EMPulseCrescentParticle";
};

datablock DebrisData(EMPulseMortarDebris)
{
   emitters[0] = EMPulseDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 2.5;
   lifetimeVariance = 0.8;

   numBounces = 2;
};

datablock ExplosionData(EMPulseSubExplosion1)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 5.0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(EMPulseSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;

   delayMS = 50;

   offset = 5.0;

   playSpeed = 1.0;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(EMPulseSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = 0.0;
   playSpeed = 0.7;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "2.0 2.0 2.0";
   times[0] = 0.0;
   times[1] = 1.0;

   shockwave = EMPShockwave2D;
};

datablock ExplosionData(EMPulseMortarExplosion)
{
   soundProfile   = UnderwaterMortarExplosionSound;

   shockwave = EMPShockwave;

   subExplosion[0] = EMPulseSubExplosion1;
   subExplosion[1] = EMPulseSubExplosion2;
   subExplosion[2] = EMPulseSubExplosion3;

   emitter[0] = EMPulseMortarExplosionSmokeEmitter;
   emitter[1] = EMPulseCrescentEmitter;

   debris = EMPulseMortarDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 50;
   debrisNum = 12;
   debrisVelocity = 50.0;
   debrisVelocityVariance = 15.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock ParticleData(EMPMortarSmokeParticle)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.3;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  1200;
   lifetimeVarianceMS   =  200;
   useInvAlpha          =  true;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.7 7.0 1.0 0.5";
   colors[1]     = "0.3 0.3 0.7 0.8";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 2.0;
   sizes[2]      = 4.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(EMPMortarSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 3;

   ejectionVelocity = 2.25;
   velocityVariance = 0.55;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "EMPMortarSmokeParticle";
};

datablock GrenadeProjectileData(EMPulseMortarShot)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 2.0;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::EMPulse;
   kickBackStrength    = 1500;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::EMP;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 75;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Energy;
   mdDamageAmount[1]   = 225;
   mdDamageRadius[1]   = true;
   
   flags               = $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   radialStatusEffect  = "EMPulseEffect";
   statusEffectChance  = 0.75;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;

   explosion           = "EMPulseMortarExplosion";
   velInheritFactor    = 1.0;
   splash              = MortarSplash;

   baseEmitter         = EMPMortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.15;
   grenadeFriction   = 0.4;
   armingDelayMS     = 4000;
   muzzleVelocity    = 95.5;
   drag              = 0.1;

   sound			 = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.05 0.05 0.2";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";
};

function MortarEMPShell::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 100;
    %gyan = 1;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function MortarEMPShell::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("GrenadeProjectile", "EMPulseMortarShot", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MortarEMPShell", "EM Pulse Shell", "Disables nearby electronics - 1 GP, 100 power", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mortar);
