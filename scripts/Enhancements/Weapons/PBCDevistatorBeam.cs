datablock SniperProjectileData(DevistatorBeam)
{
   directDamage        = 1.25;
   velInheritFactor    = 1.0;
   faceViewer          = true;
//   sound 			   = PBWProjectileSound;
   explosion           = "PBWExplosion";
   splash              = PlasmaSplash;
   directDamageType    = $DamageType::PBCDevistator;
   kickBackStrength    = 2450;
   
   maxRifleRange       = 300;
   rifleHeadMultiplier = 1.25;
   beamColor           = "1.0 1.0 1.0";
   fadeTime            = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::PBC;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 83;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   hasFalloff = false;
   optimalRange = 200;
   falloffRange = 300;
   falloffDamagePct = 0.25;

   startBeamWidth		  = 0.6;
   endBeamWidth 	     = 0.05;
   pulseBeamWidth 	  = 1.6;
   beamFlareAngle 	  = 45.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 12.0;
   pulseLength         = 0.2;

   lightRadius         = 10.0;
   lightColor          = "1.0 1.0 1.0";

   textureName[0]   = "special/flare";
   textureName[1]   = "special/nonlingradient";
   textureName[2]   = "special/skylightning";
   textureName[3]   = "special/pulse";
   textureName[4]   = "special/jetexhaust02";
   textureName[5]   = "special/skylightning";
   textureName[6]   = "special/nonlingradient";
   textureName[7]   = "special/skylightning";
   textureName[8]   = "special/pulse";
   textureName[9]   = "special/jetexhaust02";
   textureName[10]   = "special/skylightning";
   textureName[11]   = "special/flare";
};

function PBCDevistator::onInit(%this)
{
    %this.data = ParticleBeamCannonImage;
}

function PBCDevistator::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 0;
    %gyan = 1;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";
        
    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function PBCDevistator::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = createProjectile("LinearFlareProjectile", "PBCDisplayCharge", %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
    %this.schedule(300, "fragmentBeam", %obj, %slot);
    %this.schedule(425, "fragmentBeam", %obj, %slot);
    %this.schedule(550, "fragmentBeam", %obj, %slot);
    %obj.fireTimeout[%data, %this] = getSimTime() + (4000 / (1 + %obj.rateOfFire[%data.item]));

    return %p;
}

function PBCDevistator::fragmentBeam(%this, %obj, %slot)
{
    %data = %this.data;
    %proj = createProjectile(%data.projectileType, "DevistatorBeam", %obj.getMuzzleVector(%slot), %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
    %proj.setEnergyPercentage(0.8);
    %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "PBCDevistator", "Gyan-Infused Beam", "Substitutes energy usage for 1 GP, 200m optimal range", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::ParticleBeamCannon);
