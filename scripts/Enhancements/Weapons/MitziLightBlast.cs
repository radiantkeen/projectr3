datablock ParticleData(MitziLightParticle)
{
   dragCoefficient      = 1.25;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 500;
   textureName          = "particleTest";
   colors[0]     = "1 1 1 1";
   colors[1]     = "0.1 0.1 0.1 0";
   sizes[0]      = 1.0;
   sizes[1]      = 0.75;
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleEmitterData(MitziLightEmitter)
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 0;
   phiReferenceVel  = 720;
   phiVariance      = 0;
   overrideAdvances = false;
   particles = "MitziLightParticle";
};

datablock LinearFlareProjectileData(MitziLightProjectile)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "3.0 3.0 3.0";
   faceViewer          = true;
   directDamageType    = $DamageType::MitziBlast;
   directDamage        = 0.3;
   hasDamageRadius     = true;
   indirectDamage      = 0.275;
   damageRadius        = 7.0;
   kickBackStrength    = 500;
   radiusDamageType    = $DamageType::MitziBlast;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MitziBlast;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[0]   = 30;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[1]   = 30;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   explosion           = "MitziShotgunExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = MitziLightEmitter;

   dryVelocity       = 200.0;
   wetVelocity       = 300.0;
   velInheritFactor  = 0.3;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "1 0.8 0.9";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 1 1";
};

function MitziLightBlast::validateUse(%this, %obj, %data)
{
    %ammoUse = 5;
    %energy = 0;
    %gyan = 0;
    
    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function MitziLightBlast::onInit(%this)
{
//    %this.modeSwitchSound = "DiscDryFireSound";
//    %this.modeFireSound = "DiscFireSound";
}

function MitziLightBlast::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearFlareProjectile", "MitziLightProjectile", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MitziLightBlast", "Mitzi Bolt", "Low damage, 5 cap, fast travel speed - shoot a bird!", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mitzi);
