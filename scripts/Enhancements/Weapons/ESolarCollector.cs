function ESolarCollector::applyEffect(%this, %obj, %item)
{
    %obj.energyEfficiency[%item] -= 0.25;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ESolarCollector", "Solar Collector", "Reduces power usage per shot by 25%", $Enhancement::AllArmorCompat, $Enhancement::EnergyWeapons);
