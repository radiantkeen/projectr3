function EDamageAmp::applyEffect(%this, %obj, %item)
{
    %obj.weaponBonusDamage[%item] += 0.15;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "EDamageAmp", "Damage Amp", "Increases damage output by 15%", $Enhancement::AllArmorCompat, $Enhancement::AllWeapons);
