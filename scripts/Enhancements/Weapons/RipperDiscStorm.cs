function RipperDiscStorm::validateUse(%this, %obj, %data)
{
    %ammoUse = 3;
    %energy = 0;
    %gyan = 0;
       
    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function RipperDiscStorm::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    schedule(200, %obj, "RDSBurstFire", %obj, %slot);
    schedule(400, %obj, "RDSBurstFire", %obj, %slot);
    return RDSBurstFire(%obj, %slot);
}

function RDSBurstFire(%obj, %slot)
{
    %vector = vectorSpread(%obj.getMuzzleVector(%slot), 4 * (%obj.spreadFactorBase + %obj.spreadFactor[%data.item]));
    %pos = %obj.getMuzzlePoint(%slot);
    %obj.play3D("RipperFireSound");
    return createProjectile("LinearProjectile", "DiscProjectile", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "RipperDiscStorm", "Disc Storm", "Rapid fires 3 standard discs in a spread - 3 ammo", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Ripper);
