datablock EnergyProjectileData(SpikeAutoShot) : SpikeSingleShot
{
   directDamage        = 0.15;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Spike;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 30;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;
};

function SpikeAutoFire::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function SpikeAutoFire::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %obj.fireTimeout[%data.item, %this] = getSimTime() + (150 / (1 + %obj.rateOfFire[%data.item]));
    %vector = vectorSpread(%vector, 3 * (%obj.spreadFactorBase + %obj.spreadFactor[%data.item]));
    return createProjectile("EnergyProjectile", "SpikeAutoShot", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "SpikeAutoFire", "Spike Auto Fire", "Replaces the shell loader with a chain loader", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Spike);
