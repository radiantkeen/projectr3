datablock LinearProjectileData(GravitronDisc)
{
   projectileShapeName = "disc.dts";
   scale               = "2.5 2.5 2.5";
   emitterDelay        = -1;
   directDamage        = 0.5;
   directDamageType    = $DamageType::Disc;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 25.0;
   radiusDamageType    = $DamageType::Disc;
   kickBackStrength    = 3500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disc;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 200;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 100;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.25;

   sound 				= DiscLoopSound;
   explosion           = "LrgDiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   dryVelocity       = 0.01;
   wetVelocity       = 0.01;
   velInheritFactor  = 0.01;
   fizzleTimeMS      = 15000;
   lifetimeMS        = 15000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 15.0;
   lightColor  = "0.175 0.175 0.5";
};

function GravitronDisc::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    InitContainerRadiusSearch(%proj.position, 20, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.client.team == %proj.instigator.team || %int.team == %proj.instigator.team)
            continue;

        transformProjectile(%proj, "LinearFlareProjectile", "GravDiscBurst", %proj.position, %proj.initialDirection);
    }
}

datablock LinearFlareProjectileData(GravDiscBurst)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 25.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 3500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disc;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 200;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 100;
   mdDamageRadius[1]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;
   
   sound               = GrenadeProjectileSound;
   explosion           = "LrgDiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function RipperGravitronDisc::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 0;
    %gyan = 0;
       
    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function RipperGravitronDisc::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearProjectile", "GravitronDisc", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "RipperGravitronDisc", "Gravitron Disc", "Proximity-enabled hovering disc mine - 2 ammo", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Ripper);
