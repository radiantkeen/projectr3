function EPlasmaDegausser::applyEffect(%this, %obj, %item)
{
    %obj.rateOfFire[%item] += 0.3;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "EPlasmaDegausser", "Degaussing Matrix", "Increases rate of fire by 30%", $Enhancement::AllArmorCompat, $WeaponsList::Plasma);
