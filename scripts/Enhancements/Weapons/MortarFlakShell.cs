datablock GrenadeProjectileData(FlakMortarShot)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 150;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   velInheritFactor    = 0.5;
   splash              = MortarSplash;
   depthTolerance      = 10.0; // depth at which it uses underwater explosion

   baseEmitter         = MortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.01;
   grenadeFriction   = 0.99;
   armingDelayMS     = 250;
   
   gravityMod        = 1.1;
   muzzleVelocity    = 95.5;
   drag              = 0.1;

   sound			 = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.05 0.2 0.05";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";
};

datablock LinearFlareProjectileData(FlakMortarTriggered)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 1.5;
   damageRadius        = 30.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 2500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 300;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function MortarFlakShell::validateUse(%this, %obj, %data)
{
    %ammoUse = 3;
    %energy = 0;
    %gyan = 1;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }
    
    return %energy SPC %ammoUse SPC %gyan;
}

function MortarFlakShell::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("GrenadeProjectile", "FlakMortarShot", %vector, %pos, %obj, %slot, %obj);
}

function MortarFlakShell::onTriggerFire(%this, %obj, %data)
{
    // Mode glitch bug
    if(%obj.lastProjectile.getDatablock().getName() !$= "FlakMortarShot")
        return;
        
    transformProjectile(%obj.lastProjectile, "LinearFlareProjectile", "FlakMortarTriggered", %obj.lastProjectile.position, %obj.lastProjectile.initialDirection);
}

function MortarFlakShell::onInit(%this)
{
    %this.hasTrigger = true;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MortarFlakShell", "Trigger Flak Shell", "Aerial explosive - 3 ammo, 1 GP. Press fire again to detonate.", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mortar);
