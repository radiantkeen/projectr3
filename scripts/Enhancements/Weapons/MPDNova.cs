datablock EnergyProjectileData(NovaBolt)
{
   projectileShapeName = "";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 1.33;
   damageRadius        = 6.0;
   radiusDamageType    = $DamageType::MPDisruptor;
   kickBackStrength    = 1000;
   bubbleEmitTime      = 1.0;

   sound = BlasterProjectileSound;
   velInheritFactor    = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MPDisruptor;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 66;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Sonic;
   mdDamageAmount[1]   = 150;
   mdDamageRadius[1]   = false;
   
   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound | $Projectile::IgnoreShields | $Projectile::IgnoreReflections | $Projectile::IgnoreReflectorField;
   ticking             = false;
   headshotMultiplier  = 1.25;
   
   explosion           = "PhaserExplosion";
   splash              = BlasterSplash;

   grenadeFriction   = 0.75;
   grenadeElasticity = 0.35;
   armingDelayMS     = 1000;
   gravityMod        = 1.0;
   muzzleVelocity    = 200.0;
   drag = 0.05;
   dryVelocity       = 200.0;
   wetVelocity       = 200.0;

   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.8 0.0 0.6";

   scale = "3 3 3";
   crossViewAng = 0.05;
   crossSize = 5.5;

   lifetimeMS     = 1500;
   blurLifetime   = 0.75;
   blurWidth      = 1.5;
   blurColor = "0.8 0.0 0.6 1.0";

   texture[0] = "flarebase";
   texture[1] = "flarebase";
};

function NovaBurst::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 100;
    %gyan = 1;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function NovaBurst::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("EnergyProjectile", "NovaBolt", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "NovaBurst", "Nova Burst", "Direct hits ignore shields and repulsors, uses 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::MPDisruptor);
