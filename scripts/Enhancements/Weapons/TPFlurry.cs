datablock LinearFlareProjectileData(LightPlasmaBolt)
{
   projectileShapeName = "plasmabolt.dts";
   scale               = "0.5 0.5 0.5";
   faceViewer          = true;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.3;
   damageRadius        = 3.0;
   kickBackStrength    = 0.0;
   radiusDamageType    = $DamageType::Plasma;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Plasma;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 40;
   mdDamageRadius[0]   = true;
   
   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;
   
   explosion           = "PlasmaExplosion";
   splash              = PlasmaSplash;
   
   dryVelocity       = 200.0;
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 400;
   lifetimeMS        = 500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = -1;

   size[0]           = 0.2;
   size[1]           = 0.5;
   size[2]           = 0.1;


   numFlares         = 35;
   flareColor        = "1 0.75 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "1 0.75 0.25";
};

datablock ShapeBaseImageData(PlasmaGunFlurryImage) : PlasmaGunImage
{
   stateName[3] = "Fire";
   stateTimeoutValue[3] = 0.15;
   stateEmitterTime[3] = 0.15;

   stateName[4] = "Reload";
   stateTimeoutValue[4] = 0.2;
};

function TPFlurry::onInit(%this)
{
    %this.subImage = "PlasmaGunFlurryImage";
}

function TPFlurry::validateUse(%this, %obj, %data)
{
    %ammoUse = 5;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function TPFlurry::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearFlareProjectile", "LightPlasmaBolt", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "TPFlurry", "Plasma Flurry", "Lower damage and range but fires faster, uses 5 ammo", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Plasma);
