datablock AudioProfile(ExplosiveBulletHit)
{
   filename    = "fx/weapons/cg_soft2.wav";
   description = AudioClose3d;
   preload = true;
};

datablock ExplosionData(HEChaingunExplosion)
{
   soundProfile = ExplosiveBulletHit;

   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   playSpeed = 2.0;

   sizes[0] = "0.15 0.15 0.15";
   sizes[1] = "0.15 0.15 0.15";
   times[0] = 0.0;
   times[1] = 1.0;
   
   emitter[0] = ChaingunImpactSmoke;
   emitter[1] = ChaingunSparkEmitter;
};

datablock DecalData(HEChaingunDecal1)
{
   sizeX       = 0.4;
   sizeY       = 0.4;
   textureName = "special/bullethole4";
};
datablock DecalData(HEChaingunDecal2)
{
   sizeX       = 0.4;
   sizeY       = 0.4;
   textureName = "special/bullethole5";
};

datablock TracerProjectileData(ExplosiveBullet)
{
   doDynamicClientHits = true;
   directDamage        = 0.0;
   directDamageType    = $DamageType::BulletHE;
   hasDamageRadius     = true;
   indirectDamage      = 0.08;
   damageRadius        = 3;
   radiusDamageType    = $DamageType::BulletHE;
   kickBackStrength    = 100;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Vulcan;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 10;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 5;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion           = "HEChaingunExplosion";
   splash              = ChaingunSplash;

   kickBackStrength  = 0.0;
   sound 				= ChaingunProjectile;

   dryVelocity       = 500.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 15.0;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.10;
   crossSize       = 0.20;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = HEChaingunDecal1;
   decalData[1] = HEChaingunDecal2;
};

function VulcanExplosiveShot::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function VulcanExplosiveShot::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("TracerProjectile", "ExplosiveBullet", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "VulcanExplosiveShot", "Vulcan Explosive Bullet", "Mini-explosive shells, uses 2 ammo per shot", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Chaingun | $WeaponsList::DualVulcan);
