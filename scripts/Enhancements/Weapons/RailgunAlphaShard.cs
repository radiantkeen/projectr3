datablock LinearProjectileData(AlphaShardRail)
{
   scale = "10.0 20.0 10.0";
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 2.0;
   hasDamageRadius     = false;
   directDamageType    = $DamageType::AlphaShardRail;
   kickBackStrength    = 2250;
   bubbleEmitTime      = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Railgun;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 75;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 175;
   mdDamageRadius[1]   = false;
   
   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;

   explosion           = "MissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = HowitzerSmokeEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 1000;
   wetVelocity       = 300;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

function AlphaShardRail::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);

    RadiusRadiationExplosion(%proj, %proj.position, 8, 2, 0, %proj.instigator, false);
}

function RailgunAlphaShard::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 150;
    %gyan = 1;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function RailgunAlphaShard::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearProjectile", "AlphaShardRail", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "RailgunAlphaShard", "Alpha Shard Rail", "Gamma ray burst on hit, does internal damage, 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Railgun);
