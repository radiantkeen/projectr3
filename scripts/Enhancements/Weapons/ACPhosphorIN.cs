datablock GrenadeProjectileData(ACPhosphorINShell) : AutoCannonHEShell
{
   damageRadius        = 12.0;
   kickBackStrength    = 500;
   radiusDamageType    = $DamageType::Plasma;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Burn;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 90;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 25;
   mdDamageRadius[1]   = false;
   
   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "HeavyPlasmaExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";

   baseEmitter         = GrenadeSmokeEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
};

function ACPhosphorINShell::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);

    StatusEffect.applyEffect("BurnEffect", %targetObject, %projectile.instigator);
}

function ACPhosphorIN::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 0;
    %gyan = 1;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }
        
    return %energy SPC %ammoUse SPC %gyan;
}

function ACPhosphorIN::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("GrenadeProjectile", "ACPhosphorINShell", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ACPhosphorIN", "Incendiary Grenade", "Instantly sets fire to enemy armors - 1 ammo, 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::GrenadeLauncher);
