datablock ParticleData(EMPMissileFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 1.0;

   lifetimeMS           = 300;
   lifetimeVarianceMS   = 000;

   textureName          = "particleTest";

   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "0.2 1.0 0.2 1.0";
   colors[1]     = "0.0 1.0 0.0 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0;
   sizes[1]      = 1;
   sizes[2]      = 1.5;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(EMPMissileFireEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;

   ejectionVelocity = 15.0;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "EMPMissileFireParticle";
};

datablock SeekerProjectileData(EMPMissile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 1.0;
   damageRadius        = 25;
   radiusDamageType    = $DamageType::EMP;
   kickBackStrength    = 100;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::EMP;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Energy;
   mdDamageAmount[1]   = 100;
   mdDamageRadius[1]   = true;
   
   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "EMPulseMortarExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   radialStatusEffect  = "EMPulseEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = EMPMissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 10000;
   muzzleVelocity      = 50.0;
   maxVelocity         = 150.0;
   turningSpeed        = 75.0;
   acceleration        = 200.0;

   proximityRadius = 1;

   terrainAvoidanceSpeed         = 90;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 10;

   flareDistance = 50;
   flareAngle    = 15;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 32;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = true;
};

function RLEMPMissile::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 50;
    %gyan = 2;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough GP ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function RLEMPMissile::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("SeekerProjectile", "EMPMissile", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "RLEMPMissile", "EM Pulse Missile", "Locks onto hot targets and saps power, 2 GP, 50 Power", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::MissileLauncher | $WeaponsList::LaserMissileLauncher | $WeaponsList::RocketLauncher);
