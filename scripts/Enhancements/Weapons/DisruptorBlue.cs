datablock ParticleData(BBlasterExplosionParticle1)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent4";
   colors[0] = "0.2 0.4 1.0 1.0";
   colors[1] = "0.2 0.2 1.0 1.0";
   colors[2] = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(BBlasterExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 2;
   velocityVariance = 1.5;
   ejectionOffset   = 0.0;
   thetaMin         = 70;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "BBlasterExplosionParticle1";
};

datablock ParticleData(BBlasterExplosionParticle2)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/blasterHit";
   colors[0] = "0.2 0.2 1.0 1.0";
   colors[1] = "0.2 0.2 1.0 0.5";
   colors[2] = "0.2 0.0 0.0 0.0";
   sizes[0]      = 0.3;
   sizes[1]      = 0.90;
   sizes[2]      = 1.50;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(BBlasterExplosionEmitter2)
{
   ejectionPeriodMS = 30;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = false;
   lifetimeMS       = 200;
   particles = "BBlasterExplosionParticle2";
};

datablock ExplosionData(BBlasterExplosion)
{
   soundProfile   = blasterExpSound;
   emitter[0]     = BBlasterExplosionEmitter;
   emitter[1]     = BBlasterExplosionEmitter2;
};

datablock LinearFlareProjectileData(BlueDisruptorBolt)
{
   directDamage        = 0.4;
   directDamageType    = $DamageType::BlueBlaster;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disruptor;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 40;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound | $Projectile::ShieldsOnly;
   ticking             = false;
   headshotMultiplier  = 1.5;

   explosion           = "BBlasterExplosion";

   dryVelocity       = 200.0;
   wetVelocity       = 200.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 100;

   numFlares         = 10;
   size              = 0.35;
   flareColor        = "0.25 0.25 1.0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = ShrikeBlasterProjectile;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "0.25 0.25 1.0";
};

function DisruptorBlue::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 14;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function DisruptorBlue::onInit(%this)
{
    %this.modeSwitchSound = "ElfFireWetSound";
    %this.fireSound = "SentryTurretFireSound";
}

function DisruptorBlue::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearFlareProjectile", "BlueDisruptorBolt", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "DisruptorBlue", "Nullifier Disruptor", "Frequency tuned to do double damage to shields, but none to armor", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Blaster | $WeaponsList::DualDisruptor);
