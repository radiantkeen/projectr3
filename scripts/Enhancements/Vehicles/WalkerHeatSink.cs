function WalkerHeatSink::applyEffect(%this, %obj, %armorName)
{
    %obj.heatFactor += 0.1;
}

Enhancement.registerEnhancement($EnhancementType::Vehicle, "WalkerHeatSink", "Heat Pump", "Increases reactor cooling efficiency by 10%", $Enhancement::Stackable, $VehicleList::Walkers);
