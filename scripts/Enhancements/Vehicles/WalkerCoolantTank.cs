function WalkerCoolantTank::applyEffect(%this, %obj, %armorName)
{
    %obj.coolant += 3;
}

//Enhancement.registerEnhancement($EnhancementType::Vehicle, "WalkerCoolantTank", "Extra Coolant Tank", "Increases coolant charges by 3", $Enhancement::Stackable, $VehicleList::Walkers);
