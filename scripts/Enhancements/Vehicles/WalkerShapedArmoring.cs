function WalkerShapedArmoring::applyEffect(%this, %obj, %armorName)
{
    %obj.damageReduction += 3;
}

Enhancement.registerEnhancement($EnhancementType::Vehicle, "WalkerShapedArmoring", "Shaped Armoring", "Reduces damage taken by 3 HP", $Enhancement::Stackable, $VehicleList::Walkers);
