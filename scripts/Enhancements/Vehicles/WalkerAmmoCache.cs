function WalkerAmmoCache::applyEffect(%this, %obj, %armorName)
{
    %obj.maxAmmoCapacityFactor += 0.25;
}

Enhancement.registerEnhancement($EnhancementType::Vehicle, "WalkerAmmoCache", "Ammo Cache", "Increases total ammo capacity by 25%", $Enhancement::Stackable, $VehicleList::Walkers);
