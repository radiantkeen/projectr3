function WalkerArmorPlate::applyEffect(%this, %obj, %armorName)
{
    %obj.maxHitPoints += 100;
    %obj.hitPoints += 100;
}

Enhancement.registerEnhancement($EnhancementType::Vehicle, "WalkerArmorPlate", "Endurium Internal Armoring", "Increases Max HP by 100", $Enhancement::Stackable, $VehicleList::Walkers);
