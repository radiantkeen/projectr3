//--------------------------------------------------------------------------
// Assault RPG
//--------------------------------------

datablock ShapeBaseImageData(AssaultRPGImage)
{
   className = WeaponImage;
   shapeFile = "weapon_grenade_launcher.dts";
   offset = "-0.45 -0.5 0.275";
   rotation = "0 1 0 32";
   emap = true;
   mountPoint = 1;
   isShoulderWeapon = true;
   
   projectile = AssaultRPGRocket;
   projectileType = LinearProjectile;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;
   ammo = "";

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.5;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
   stateScript[3]                   = "onFire";
//   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.5;
   stateAllowImageChange[4]         = false;
   stateSound[4]                    = GrenadeReloadSound;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function AssaultRPGImage::onFire(%data, %obj, %slot)
{
    %time = getSimTime();

    if(%obj.fireTimeout[%data, %data.item] > %time)
    {
        %obj.play3D(GrenadeDryFireSound);
        return;
    }

    if(%obj.arpgGrenade.launcherProjectile $= "")
    {
        %obj.play3D(GrenadeDryFireSound);
        return;
    }

    %p = Parent::onFire(%data, %obj, %slot);

    if(%p)
    {
        %obj.applyKick(-640);
        %obj.play3D(MILFireSound);
        %obj.fireTimeout[%data, %data.item] = %time + (1000 / (1 + %obj.rateOfFire[%data.item]));
    }
    else
    {
        %obj.fireTimeout[%data, %data.item] = %time + (1000 / (1 + %obj.rateOfFire[%data.item]));
        %obj.play3D(GrenadeDryFireSound);
    }

    %obj.setImageTrigger(%slot, false);
}

function AssaultRPGImage::spawnProjectile(%data, %obj, %slot, %mode)
{
    %vector = %obj.getMuzzleVector(%slot);
    %proj = createProjectile("GrenadeProjectile", %obj.arpgGrenade.launcherProjectile, %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);

    %proj.damageBuffFactor = %obj.damageBuffFactor;
    %obj.lastProjectile = %proj;
    %obj.client.projectile = %proj;
        
    return %proj;
}

datablock ItemData(AssaultRPGLauncher)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

	pickUpName = "abstract";
};

function AssaultRPGLauncher::onApply(%data, %obj)
{
    %obj.play3D("SensorDeploySound");
    %obj.mountImage(AssaultRPGImage, $ShoulderSlot);
    %obj.assaultRPG = true;
}

function AssaultRPGLauncher::onUnApply(%data, %obj)
{
    %obj.unmountImage($ShoulderSlot);
    %obj.assaultRPG = false;
}

Enhancement.registerModule("SM: RPG Launcher", "AssaultRPGLauncher");
