datablock ItemData(ClassBooster)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

	pickUpName = "abstract";
};

function ClassBooster::onApply(%data, %obj)
{

}

function ClassBooster::onUnApply(%data, %obj)
{

}

Enhancement.registerModule("Class Booster", "ClassBooster");
