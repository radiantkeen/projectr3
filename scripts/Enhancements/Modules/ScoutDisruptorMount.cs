datablock ShapeBaseImageData(ScoutDisruptorImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   offset = "-0.35 -0.175 0.25";
   rotation = "0 1 0 45";
   emap = true;
   mountPoint = 1;
   mass = 10;
   isShoulderWeapon = true;
   
   usesEnergy = true;
   fireEnergy = 50;
   minEnergy = 50;

   projectile = DisruptorBolt;
   projectileType = LinearFlareProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateSequence[1]                 = "Deploy";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSound[2]                    = ShockLanceReloadSound;
   
   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
   stateScript[3]                   = "onFire";
//   stateDirection[3]                = true;


   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 1.0;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function ScoutDisruptorImage::onFire(%data, %obj, %slot)
{
    %time = getSimTime();

    if(%obj.fireTimeout[%data, %data.item] > %time)
    {
        %obj.play3D(ElfFireWetSound);
        return;
    }

    %p = Parent::onFire(%data, %obj, %slot);

    if(%p)
    {
        %obj.play3D(SentryTurretFireSound);
        %obj.fireTimeout[%data, %data.item] = %time + (2000 / (1 + %obj.rateOfFire[%data.item]));
    }
    else
    {
        %obj.fireTimeout[%data, %data.item] = %time + (1000 / (1 + %obj.rateOfFire[%data.item]));
        %obj.play3D(ElfFireWetSound);
    }

    %obj.setImageTrigger(%slot, false);
}

function ScoutDisruptorImage::spawnProjectile(%data, %obj, %slot, %mode)
{
    %p = 0;

    for(%i = 0; %i < 6; %i++)
    {
        %p = createProjectile("LinearFlareProjectile", "DisruptorBolt", vectorSpread(%obj.getMuzzleVector(%slot), 16), %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
        %p.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];
    }

    return %p;
}

datablock ItemData(ScoutDisruptorMount)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

	pickUpName = "abstract";
};

function ScoutDisruptorMount::onApply(%data, %obj)
{
    %obj.play3D("ShockLanceSwitchSound");
    %obj.mountImage(ScoutDisruptorImage, $ShoulderSlot);

    schedule(32, %obj, "updateMeteorAmmo", %obj, ScoutDisruptorImage);

    %obj.shoulderWeapon = true;
}

function ScoutDisruptorMount::onUnApply(%data, %obj)
{
    %obj.unmountImage($ShoulderSlot);
    %obj.shoulderWeapon = false;
}

Enhancement.registerModule("SM: Disruptor Scattergun", "ScoutDisruptorMount");
