datablock ItemData(SubspaceRegenerator)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

	pickUpName = "abstract";
};

function SubspaceRegenerator::onTick(%this, %obj)
{
    if(!%obj.isDead && %obj.subspacing)
    {
        %obj.incHP(1);
        %this.schedule(2000, "onTick", %obj);
    }
}

function SubspaceRegenerator::onApply(%data, %obj)
{
    %obj.subspacing = true;
    %data.onTick(%obj);
}

function SubspaceRegenerator::onUnApply(%data, %obj)
{
    %obj.subspacing = false;
}

Enhancement.registerModule("Subspace Regenerator", "SubspaceRegenerator");
