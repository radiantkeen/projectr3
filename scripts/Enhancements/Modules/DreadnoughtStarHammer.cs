//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(StarHammerAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some star hammer shells";
};

datablock ShapeBaseImageData(StarHammerImage)
{
   className = WeaponImage;
   shapeFile = "turret_missile_large.dts";
   offset = "-0.5 -0.1 0.5";
   rotation = "0 1 0 180";
   emap = true;
   mountPoint = 1;
   mass = 10;
   
   isShoulderWeapon = true;
   
   projectile = StarHammerRocket;
   projectileType = LinearProjectile;

   usesEnergy = false;
   fireEnergy = -1;
   minEnergy = -1;
   ammo = StarHammerAmmo;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
   stateScript[3]                   = "onFire";
//   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 4.0;
   stateAllowImageChange[4]         = false;
   stateSound[4]                    = StarHammerReloadSound;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function StarHammerImage::onFire(%data, %obj, %slot)
{
    %time = getSimTime();
    
    if(%obj.fireTimeout[%data, %data.item] > %time)
    {
        %obj.play3D(MortarDryFireSound);
        return;
    }

    %p = Parent::onFire(%data, %obj, %slot);

    if(%p)
    {
        createRemoteProjectile("LinearFlareProjectile", "PowerDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);
        commandToClient(%obj.client, 'setInventoryHudAmount', 0, %obj.getInventory(%data.ammo));

        %obj.applyKick(-1500);
        %obj.play3D(StarHammerFireSound);
        %obj.fireTimeout[%data, %data.item] = %time + (5000 / (1 + %obj.rateOfFire[%data.item]));
    }
    else
    {
        %obj.fireTimeout[%data, %data.item] = %time + (2000 / (1 + %obj.rateOfFire[%data.item]));
        %obj.play3D(MortarDryFireSound);
    }

    %obj.setImageTrigger(%slot, false);
}

function updateHammerAmmo(%obj, %data)
{
    %obj.setInventory(%data.ammo, 999);
    commandToClient(%obj.client, 'setInventoryHudAmount', 0, %obj.getInventory(%data.ammo));
}

datablock ItemData(DreadStarHammer)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

	pickUpName = "abstract";
};

function DreadStarHammer::onApply(%data, %obj)
{
    %obj.play3D("MILSwitchSound");
    %obj.mountImage(StarHammerImage, $ShoulderSlot);

    schedule(256, %obj, "updateHammerAmmo", %obj, StarHammerImage);

    %obj.shoulderWeapon = true;
}

function DreadStarHammer::onUnApply(%data, %obj)
{
    %obj.unmountImage($ShoulderSlot);
    %obj.shoulderWeapon = false;
}

Enhancement.registerModule("SM: Star Hammer", "DreadStarHammer");
