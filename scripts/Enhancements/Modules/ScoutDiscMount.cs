//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(ScoutDiscAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some scout shoulder discs";
};

datablock ShapeBaseImageData(ScoutDiscMountImage)
{
   className = WeaponImage;
   shapeFile = "weapon_disc.dts";
   offset = "-0.325 -0.2 0.2";
   rotation = "0 1 0 90";
   emap = true;
   mountPoint = 1;
   mass = 10;
   ammo = ScoutDiscAmmo;
   isShoulderWeapon = true;
   
   usesEnergy = false;
   fireEnergy = -1;
   minEnergy = -1;
   
   projectile = DiscProjectile;
   projectileType = LinearProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSequence[1]                 = "Activated";
   stateSound[1]                    = DiscSwitchSound;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "DiscSpin";
   stateSound[2]                    = DiscLoopSound;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.5;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.5; // 0.25 load, 0.25 spinup
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = DiscReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = DiscDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function ScoutDiscMountImage::onFire(%data, %obj, %slot)
{
    %time = getSimTime();

    if(%obj.fireTimeout[%data, %data.item] > %time)
    {
        %obj.play3D(DiscDryFireSound);
        return;
    }

    %p = Parent::onFire(%data, %obj, %slot);

    if(%p)
    {
        commandToClient(%obj.client, 'setInventoryHudAmount', 0, %obj.getInventory(%data.ammo));

        %obj.fireTimeout[%data, %data.item] = %time + (2000 / (1 + %obj.rateOfFire[%data.item]));
    }
    else
    {
        %obj.fireTimeout[%data, %data.item] = %time + (1000 / (1 + %obj.rateOfFire[%data.item]));
        %obj.play3D(DiscDryFireSound);
    }

    %obj.setImageTrigger(%slot, false);
}

function updateDMAmmo(%obj, %data)
{
    %obj.setInventory(%data.ammo, 999);
    commandToClient(%obj.client, 'setInventoryHudAmount', 0, %obj.getInventory(%data.ammo));
}

datablock ItemData(ScoutDiscMount)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

	pickUpName = "abstract";
};

function ScoutDiscMount::onApply(%data, %obj)
{
    %obj.play3D("PlasmaFireWetSound");
    %obj.mountImage(ScoutDiscMountImage, $ShoulderSlot);

    schedule(256, %obj, "updateDMAmmo", %obj, ScoutDiscMountImage);

    %obj.shoulderWeapon = true;
}

function ScoutDiscMount::onUnApply(%data, %obj)
{
    %obj.unmountImage($ShoulderSlot);
    %obj.shoulderWeapon = false;
}

Enhancement.registerModule("SM: Disc Mount", "ScoutDiscMount");
