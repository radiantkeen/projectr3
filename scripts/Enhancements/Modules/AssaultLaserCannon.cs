//--------------------------------------------------------------------------
// Assault Laser Cannon
//--------------------------------------

datablock SniperProjectileData(AssaultLaserBeam)
{
   directDamage        = 0.4;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Laser;

   maxRifleRange       = 300;
   rifleHeadMultiplier = 1.3;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Laser;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 50;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   hasFalloff = true;
   optimalRange = 200;
   falloffRange = 300;
   falloffDamagePct = 0.25;

   startBeamWidth		  = 0.25;
   endBeamWidth 	     = 0.4;
   pulseBeamWidth 	  = 0.5;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

datablock ShapeBaseImageData(AssaultLaserImage)
{
   className = WeaponImage;
   shapeFile = "weapon_plasma.dts";
   offset = "-0.4 -0.3 0.22";
   rotation = "0 1 0 32";
   emap = true;
   mountPoint = 1;
   mass = 15;
   isShoulderWeapon = true;
   
   projectile = AssaultLaserBeam;
   projectileType = SniperProjectile;

   usesEnergy = true;
   fireEnergy = 50;
   minEnergy = 50;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "ambient";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "activation";
   stateTimeoutValue[2]             = 0.5;
   stateSound[2]                    = ShockLanceReloadSound;
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.25;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
   stateScript[3]                   = "onFire";
//   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 1.25;
   stateAllowImageChange[4]         = false;
   stateSound[4]                    = "";
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function AssaultLaserImage::onFire(%data, %obj, %slot)
{
    %p = Parent::onFire(%data, %obj, %slot);

    if(%p)
    {
        %p.setEnergyPercentage(0.85);
        %obj.play3D(SniperRifleFireSound);
    }
    else
        %obj.play3D(ElfFireWetSound);

    %obj.setImageTrigger(%slot, false);
}

datablock ItemData(AssaultLaserCannon)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

	pickUpName = "abstract";
};

function AssaultLaserCannon::onApply(%data, %obj)
{
    %obj.play3D("SniperRifleSwitchSound");
    %obj.mountImage(AssaultLaserImage, $ShoulderSlot);
    %obj.shoulderWeapon = true;
}

function AssaultLaserCannon::onUnApply(%data, %obj)
{
    %obj.unmountImage($ShoulderSlot);
    %obj.shoulderWeapon = false;
}

Enhancement.registerModule("SM: Laser Cannon", "AssaultLaserCannon");
