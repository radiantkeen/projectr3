datablock ItemData(ShieldHardener)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

	pickUpName = "abstract";
};

function ShieldHardener::onApply(%data, %obj)
{
    %obj.shieldDamageFactor[$DamageGroupMask::Energy] *= 0.7;
    %obj.shieldDamageFactor[$DamageGroupMask::Explosive] *= 0.7;
    %obj.shieldDamageFactor[$DamageGroupMask::Kinetic] *= 0.7;
    %obj.shieldDamageFactor[$DamageGroupMask::Plasma] *= 0.7;
    %obj.shieldDamageFactor[$DamageGroupMask::Mitzi] *= 0.7;
}

function ShieldHardener::onUnApply(%data, %obj)
{
    // Automatically reset on inventory
}

Enhancement.registerModule("Shield Hardener", "ShieldHardener");
