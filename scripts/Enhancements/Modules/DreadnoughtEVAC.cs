datablock ItemData(DreadnoughtEVAC)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

	pickUpName = "abstract";
};

function DreadnoughtEVAC::onApply(%data, %obj)
{
    %obj.hasEVAC = true;
}

function DreadnoughtEVAC::onUnApply(%data, %obj)
{
    %obj.hasEVAC = false;
}

Enhancement.registerModule("Emergency EVAC", "DreadnoughtEVAC");
