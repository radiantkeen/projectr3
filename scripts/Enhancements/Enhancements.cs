// Enhancement System
function Enhancement::__construct(%this)
{
    Enhancement.Version = 1.0;
}

function Enhancement::__destruct(%this)
{
   // Thou shalt not spam
}

if(!isObject(Enhancement))
   System.addClass(Enhancement);

// Low flags: Prefs
$Enhancement::Stackable             = 1 << 0;
$Enhancement::EventListen           = 1 << 1;
$Enhancement::WeaponMode            = 1 << 2;

// High flags: Vehicles and Armors

$Enhancement::PlugsuitCompat        = 1 << 24;
$Enhancement::ScoutCompat           = 1 << 25;
$Enhancement::AssaultCompat         = 1 << 26;
$Enhancement::MagneticIonCompat     = 1 << 27;
$Enhancement::BattleAngelCompat     = 1 << 28;
$Enhancement::GearheadCompat        = 1 << 29;
$Enhancement::BrawlerCompat         = 1 << 30;

$Enhancement::AllArmorCompat        = $Enhancement::PlugsuitCompat | $Enhancement::ScoutCompat | $Enhancement::AssaultCompat | $Enhancement::MagneticIonCompat | $Enhancement::BattleAngelCompat | $Enhancement::GearheadCompat | $Enhancement::BrawlerCompat;

// Shortcut flags
$Enhancement::AllWeapons            = 2147483647;
$WeaponsList::AllWeapons            = 2147483647;
$Enhancement::All                   = 2147483647;

// Types
$EnhancementType::Armor             = 0;
$EnhancementType::Vehicle           = 1;
$EnhancementType::Weapon            = 2;
$EnhancementType::Pack              = 3;
$EnhancementType::Walker            = 4;

function Enhancement::registerEnhancement(%this, %type, %enh, %name, %desc, %flags, %wearMask)
{
    // No duplication!
    if(isObject(%enh))
        return;

    %newEnh = new ScriptObject(%enh)
    {
        class = %enh;
        superClass = Enhancement;
    };
    
    %newEnh.name = %name;
    %newEnh.description = %desc;
    %newEnh.wearableMask = %wearMask;
    %newEnh.prefFlags = %flags;
    %newEnh.onInit();
    
    // Store index of where this object is
    if(%this.enhancementCount[%type] $= "")
        %this.enhancementCount[%type] = 0;
        
    %this.enhancements[%type, %this.enhancementCount[%type]] = %newEnh;
    
    // Next data
    %this.enhancementCount[%type]++;
}

function Enhancement::enumerateClient(%this, %client)
{
    for(%e = 0; %e < 4; %e++) // change to 5 when building clientside support for walker enhs
    {
        for(%i = 0; %i < %this.enhancementCount[%e]; %i++)
        {
            %enh = %this.enhancements[%e, %i];
            
            commandToClient(%client, 'EnumEnhancements', %e, %enh.getName(), %enh.name, %enh.description, %enh.wearableMask, %enh.prefFlags);
        }
    }
}

function Enhancement::onInit(%this)
{
    // Set prefs here
}

function Enhancement::validateUse(%this, %obj)
{
    return true;
}

function Enhancement::applyEffect(%this, %obj, %item)
{
    // Do stuff to %obj here
    //%mul = %obj.enhancementMultiplier;
}

function Enhancement::onProjectileEnd(%this, %proj)
{
    // Call cleanup stuff
}

function Enhancement::onTriggerFire(%this, %obj, %data)
{
    // Used for weapon modes
}

function ShapeBase::getShortName(%obj)
{
    if(%obj.getDatablock().shortName !$= "")
        return %obj.getDatablock().shortName;
    else if(%obj.isWalker)
        return %obj.shortName;
    else if(%obj.isPlayer())
        return %obj.getArmorSize();
    else if(%obj.isVehicle())
        return detag(%obj.getDatablock().targetNameTag);
    else
        return "StaticShape";
}

function armorSizeToBlock(%size)
{
    return %size@"MaleHumanArmor";
}

// Player/vehicle level stuff here
function ShapeBase::installEnhancements(%obj, %type)
{
    %data = %obj.getDatablock();
//    %data.resetStats(%obj);
    %count = %data.enhancementSlots;

    if(%type == $EnhancementType::Vehicle)
    {
        %client = %obj.spawningClient;
        %shortName = %data.getName();
        
        for(%i = 0; %i < 8; %i++)
        {
            %client.installedEnhancement[%shortName, %i] = "";

            if(%i < %count)
            {
                if(%client.pendingEnhancement[%type, %shortName, %i] $= "")
                {
                    %enhid = $SCEnhLookup[%client.guid, %shortName, 0];

                    if(%enhid > 0)
                        %defaultEnh = $SCEnhData[%enhid, %i];
                    else
                        %defaultEnh = $DefaultVehicleEnh[%obj.vid, %i];
                        
                    %client.installedEnhancement[%shortName, %i] = %defaultEnh;
                    %client.pendingEnhancement[%type, %shortName, %i] = %defaultEnh;
                }
                else
                    %client.installedEnhancement[%shortName, %i] = %client.pendingEnhancement[%type, %shortName, %i];

                %client.installedEnhancement[%shortName, %i].applyEffect(%obj, %shortName);
            }
        }
    
        %data.schedule(32, "postInstallEnhancements", %obj);
    }
    else
    {
        %client = %obj.client;
        %shortName = %obj.getShortName();
        
        for(%i = 0; %i < 8; %i++)
        {
            %client.installedEnhancement[%shortName, %i] = "";

            if(%i < %count)
            {
                if(%client.pendingEnhancement[%type, %shortName, %i] $= "")
                {
                    %enhid = $SCEnhLookup[%client.guid, %shortName, 0];

                    if(%enhid > 0)
                        %defaultEnh = $SCEnhData[%enhid, %i];
                    else
                        %defaultEnh = "ArmorHPPlus10";
                        
                    %client.installedEnhancement[%shortName, %i] = %defaultEnh;
                    %client.pendingEnhancement[%type, %shortName, %i] = %defaultEnh;
                }
                else
                    %client.installedEnhancement[%shortName, %i] = %client.pendingEnhancement[%type, %shortName, %i];

                %client.installedEnhancement[%shortName, %i].applyEffect(%obj, %shortName);
            }
        }

        %data.schedule(32, "postInstallEnhancements", %obj);
    }
}

function GameConnection::updateDefaultEnhancement(%client, %type, %item, %index, %enh)
{
    %new = false;
    
    if($SCEnhCount[%client.guid] $= "")
        $SCEnhCount[%client.guid] = 0;

    %enhid = $SCEnhLookup[%client.guid, %item, 0];
    
    if(%enhid $= "")
    {
        %enhid = %client.guid @ $SCEnhCount[%client.guid]; // %trueid

        $SCEnhLookup[%client.guid, %item, 0] = %enhid;
        %new = true;
    }
    
    %ename = $SCEnhName[%enhid] !$= "" ? $SCEnhName[%enhid] : "Default";

    if($SCEnhTotal[%client.guid, %item] $= "")
        $SCEnhTotal[%client.guid, %item] = 0;

    $SCEnhData[%enhid, "type"] = %type;
    $SCEnhData[%enhid, "item"] = %item;
    $SCEnhData[%enhid, %index] = %enh;
    $SCEnhName[%enhid] = %ename;

    if(%new)
    {
        $SCEnhTotal[%client.guid, %item]++;
        $SCEnhCount[%client.guid]++;
    }
}

// Don't forget to clean this up ::onDrop
function GameConnection::installEnhancements(%client, %type, %item)
{
    %client.modeCount[%item] = 0;
    %count = %item.image.enhancementSlots;
    
    // Do nothing for items that don't have enhancements
    if(%count == 0 || %count $= "")
        return;
    
    // Reset weapon modes and other variables when installing enhancements
    %client.player.resetDefaultItemStats(%item);
    %client.totalInstalledEnhancements[%item] = 0;

    for(%i = 0; %i < 8; %i++)
    {
        %client.installedEnhancement[%item, %i] = "";

        if(%i < %count)
        {
            %curEnh = %client.pendingEnhancement[%type, %item, %i];

            if(%curEnh $= "" || %curEnh $= "DefaultWeaponEnh")
            {
                if(%type == $EnhancementType::Weapon)
                {
                    %enhid = $SCEnhLookup[%client.guid, %item, 0];
                    
                    if(%enhid > 0)
                        %defaultWEnh = $SCEnhData[%enhid, %i];
                    else
                        %defaultWEnh = $DefaultWeaponEnh[%item, %i];
                        
                    %useWEnh = %defaultWEnh $= "" ? "DefaultWeaponEnh" : %defaultWEnh;

                    %client.installedEnhancement[%item, %i] = %useWEnh;
                    %client.pendingEnhancement[%type, %item, %i] = %useWEnh;
                }
                else
                {
                    %client.installedEnhancement[%item, %i] = "DefaultPackEnh";
                    %client.pendingEnhancement[%type, %item, %i] = "DefaultPackEnh";
                }
            }
            
            %client.installedEnhancement[%item, %i] = %client.pendingEnhancement[%type, %item, %i];
            %thisEnh = %client.installedEnhancement[%item, %i];
            %thisEnh.applyEffect(%client.player, %item);
            
            %client.totalInstalledEnhancements[%item]++;
            
            if(%type == $EnhancementType::Weapon)
            {
                if(%thisEnh.prefFlags & $Enhancement::WeaponMode)
                {
                    // Mode 0 is always the base weapon, so any additional modes would be 1+

                    if(%client.modeCount[%item] > 0)
                        %client.modeCount[%item]++;
                    else
                        %client.modeCount[%item] = 1;

                    %client.weaponMode[%item, %client.modeCount[%item]] = %thisEnh;
                }
            }
        }
    }
}

function Enhancement::getInstalledEnhancement(%this, %client, %item, %enhName)
{
    for(%i = 0; %i < %client.totalInstalledEnhancements[%item]; %i++)
        if(%client.installedEnhancement[%item, %i].getName() $= %enhName)
            return %client.installedEnhancement[%item, %i];
    
    return false;
}

// Used on spawn and on inventory change
// todo: change to client-bound system
function Player::resetDefaultItemStats(%obj, %item)
{
    %obj.client.modeCount[%item] = 0;
    %obj.rateOfFire[%item] = 0;
    %obj.weaponBonusDamage[%item] = 0;
    %obj.energyEfficiency[%item] = 0;
    %obj.spreadFactor[%item] = 0;
    %obj.ammoCapacityBonus[%item.image.ammo] = 0;
}

function GameConnection::setPendingEnhancement(%client, %type, %item, %slot, %enh)
{
    %client.pendingEnhancement[%type, %item, %slot] = %enh;
}

function GameConnection::setDefaultEnhancements(%client, %type, %item)
{
    switch(%type)
    {
        case $EnhancementType::Armor:
            %count = armorSizeToBlock(%item).enhancementSlots;

            // Do nothing for items that don't have enhancements
            if(%count == 0 || %count $= "")
                return;

            for(%i = 0; %i < %count; %i++)
            {
                %enhid = $SCEnhLookup[%client.guid, %item, 0];

                if(%enhid > 0)
                    %defaultEnh = $SCEnhData[%enhid, %i];
                else
                    %defaultEnh = "ArmorHPPlus10";
                    
                %client.pendingEnhancement[%type, %item, %i] = %defaultEnh;
            }
            
            return;
        
        case $EnhancementType::Vehicle:
            return;

        case $EnhancementType::Weapon:
            %count = %item.image.enhancementSlots;

            // Do nothing for items that don't have enhancements
            if(%count == 0 || %count $= "")
                return;

            for(%i = 0; %i < %count; %i++)
            {
                %enhid = $SCEnhLookup[%client.guid, %item, 0];

                if(%enhid > 0)
                    %defaultWEnh = $SCEnhData[%enhid, %i];
                else
                    %defaultWEnh = $DefaultWeaponEnh[%item, %i];

                %client.pendingEnhancement[%type, %item, %i] = %defaultWEnh $= "" ? "DefaultWeaponEnh" : %defaultWEnh;
            }
            
            return;
            
        case $EnhancementType::Pack:
            %count = %item.image.enhancementSlots;

            // Do nothing for items that don't have enhancements
            if(%count == 0 || %count $= "")
                return;

            for(%i = 0; %i < %count; %i++)
                %client.pendingEnhancement[%type, %item, %i] = "DefaultPackEnh";
            return;
    }
}

exec("scripts/Enhancements/DefaultLoadouts.cs");
execDir("Enhancements/Armors");
execDir("Enhancements/Weapons");
execDir("Enhancements/Packs");
execDir("Enhancements/Vehicles");
// Place empty slot on the last of every list
exec("scripts/Enhancements/Default.cs");

// Load modules too
exec("scripts/Enhancements/Modules.cs");
