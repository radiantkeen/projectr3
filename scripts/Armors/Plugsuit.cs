// Plugsuit

// Plugsuit CSL (if applicable) here?
function triggerSpecialPlugsuit(%data, %obj, %imageData)
{
    %time = getSimTime();

    if(%obj.nextVectorport > %time)
        return;
    
    if(%obj.gyanLevel < 1)
        return;
    
    %gpuse = %obj.gyanLevel > 3 ? 3 : %obj.gyanLevel;
    %dist = 100 * %gpuse;
    %transform = "";
    %travelled = 0;
    %endpos = "";
    
    %hit = castRay(%obj.getWorldBoxCenter(), %obj.getMuzzleVector(0), %dist, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType, %obj);

    if(%hit)
    {
        if(%hit.hitObj.isPlayer())
        {
            if(!%hit.hitObj.isMounted())
            {
                %endpos = vectorProject(%hit.hitObj.getWorldBoxCenter(), %hit.hitObj.getForwardVector(), -1.5);
                %transform = %endpos SPC %hit.hitObj.getRotation();
            }
        }
        else
            %endpos = vectorProject(%hit.hitPos, %obj.getMuzzleVector(0), -0.5);
        
        %gpuse = mCeil(%hit.hitDist / 100);
        %travelled = mCeil(%hit.hitDist);
    }
    else
    {
        commandToClient(%obj.client, 'BottomPrint', "No VectorPort location in range ("@%dist@"m)", 5, 1);
        return;
    }

    if(%transform == 0)
        %transform = %endpos SPC %obj.getRotation();
    
    %obj.setVelocity("0 0 0");
    %obj.ejectFlag();
    %obj.setTransform(%transform);
    %obj.decGyan(%gpuse);

    InitContainerRadiusSearch(vectorAdd(%endPos, "0 0 0.5"), 0.1, $TypeMasks::StaticShapeObjectType);

    while(%found = containerSearchNext())
    {
        if(%found.getType() & $TypeMasks::StationObjectType)
            continue;
        else
        {
            %obj.gib();
            %obj.setDamageFlash(0.5 + %obj.getDamageFlash());
            %obj.getDatablock().damageObject(%obj, %obj, %endPos, 10000, $DamageType::VectorGlitch, "0 0 0", false);
            break;
        }
    }
    
    commandToClient(%obj.client, 'BottomPrint', "Distance travelled: "@ %travelled @"m - Used" SPC %gpuse SPC "GP - Vectorport cooling off, 30s", 5, 1);
    schedule(30000, %obj, "specialScoutCD", %obj);
    
    %obj.nextVectorport = %time + 30000;
}

function specialPlugsuitCD(%obj)
{
    if(!%obj.isDead && %obj.getDatablock().armorType == $ArmorType::Scout)
        commandToClient(%obj.client, 'BottomPrint', "VectorPort ready!", 5, 1);
}

datablock PlayerData(PlugsuitMaleHumanArmor) : ArmorDamageProfile
{
   emap = true;

   armorType = $ArmorType::Plugsuit;
   armorMask = $ArmorMask::Plugsuit;
   armorClass = $ArmorClassMask::Light;
   armorFlags = $ArmorFlags::CanPilot | $ArmorFlags::NoDeathFiring;
   enhancementMultiplier = 1.0;
   enhancementSlots = 0;
   
   armorSpecialCount = 1;
   armorSpecial[0] = "TargetingLaserFakeImage";
   armorSpecial[1] = "ArmorSpecialImage";
   
   className = Armor;
   shapeFile = "light_male.dts";
   cameraMaxDist = 3;
   computeCRC = true;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_ergbaricon";
   hudImageNameEnemy[0] = "gui/hud_ergbaricon";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;

   aiAvoidThis = true;

   minLookAngle = -1.5;
   maxLookAngle = 1.5;
   maxFreelookAngle = 3.0;

   mass = 75;
   drag = 0.134;
   maxdrag = 0.3;
   density = 19;
   maxDamage = 0.45;
   maxEnergy = 400;
   repairRate = 0.0033;
   energyPerDamagePoint = 75.0; // shield energy required to block one point of damage

   rechargeRate = 0;
   jetForce = 37.28 * 90;
   underwaterJetForce = 28.00 * 90 * 1.5;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = 0.45;
   underwaterJetEnergyDrain = 0.225;
   minJetEnergy = 9;
   maxJetHorizontalPercentage = 0.6;

   runForce = 55.20 * 90;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 18;
   maxBackwardSpeed = 15;
   maxSideSpeed = 15;

   maxUnderwaterForwardSpeed = 15;
   maxUnderwaterBackwardSpeed = 13;
   maxUnderwaterSideSpeed = 13;

   jumpForce = 8.34 * 90;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpDelay = 0;

   recoverDelay = 8;
   recoverRunForceScale = 1.4;

   minImpactSpeed = 45;
   speedDamageScale = 0.0022;

   jetSound = ArmorJetSound;
   wetJetSound = ArmorJetSound;
   jetEmitter = HumanArmorJetEmitter;
   jetEffect = HumanArmorJetEffect;

   boundingBox = "1.2 1.2 2.3";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = LightMaleFootprint;
   decalOffset = 0.25;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 70;
   jumpSurfaceAngle = 80;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;

   noFrictionOnSki = true;
   horizMaxSpeed = 500;
   horizResistSpeed = 48.74;
   horizResistFactor = 0;
   maxJetForwardSpeed = 30;

   upMaxSpeed = 52;
   upResistSpeed = 20.89;
   upResistFactor = 0.35;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 4.0; // takes 4 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootLightSoftSound;
   RFootSoftSound       = RFootLightSoftSound;
   LFootHardSound       = LFootLightHardSound;
   RFootHardSound       = RFootLightHardSound;
   LFootMetalSound      = LFootLightMetalSound;
   RFootMetalSound      = RFootLightMetalSound;
   LFootSnowSound       = LFootLightSnowSound;
   RFootSnowSound       = RFootLightSnowSound;
   LFootShallowSound    = LFootLightShallowSplashSound;
   RFootShallowSound    = RFootLightShallowSplashSound;
   LFootWadingSound     = LFootLightWadingSound;
   RFootWadingSound     = RFootLightWadingSound;
   LFootUnderwaterSound = LFootLightUnderwaterSound;
   RFootUnderwaterSound = RFootLightUnderwaterSound;
   LFootBubblesSound    = LFootLightBubblesSound;
   RFootBubblesSound    = RFootLightBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactLightSoftSound;
   impactHardSound      = ImpactLightHardSound;
   impactMetalSound     = ImpactLightMetalSound;
   impactSnowSound      = ImpactLightSnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactLightWaterEasySound;
   impactWaterMedium    = ImpactLightWaterMediumSound;
   impactWaterHard      = ImpactLightWaterHardSound;

   groundImpactMinSpeed    = 14.0;
   groundImpactShakeFreq   = "3.0 3.0 3.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.6;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterLightSound;

   maxWeapons = 2;           // Max number of different weapons the player can have
   maxGrenades = 1;          // Max number of different grenades the player can have
   maxMines = 0;             // Max number of different mines the player can have

   // Inventory restrictions
   max[RepairKit]          = 1;
   max[Mine]               = 0;
   max[Grenade]            = 1;
   max[Blaster]            = 0;
   max[HandDisruptor]      = 1;
   max[Plasma]             = 1;
   max[PlasmaAmmo]         = 80;
   max[Disc]               = 1;
   max[DiscAmmo]           = 5;
   max[SniperRifle]        = 0;
   max[GrenadeLauncher]    = 0;
   max[GrenadeLauncherAmmo]= 0;
   max[Mortar]             = 0;
   max[MortarAmmo]         = 0;
   max[MissileLauncher]    = 0;
   max[MissileLauncherAmmo]= 0;
   max[Chaingun]           = 1;
   max[ChaingunAmmo]       = 75;
   max[SubspaceMagnet]     = 0;
   max[SubspaceCapacitor]  = 0;
   max[RepairGun]          = 1;
   max[CloakingPack]       = 0;
   max[SensorJammerPack]   = 0;
   max[EnergyPack]         = 0;
   max[RepairPack]         = 0;
   max[ShieldPack]         = 0;
   max[AmmoPack]           = 0;
   max[SatchelCharge]      = 0;
   max[FlashGrenade]       = 1;
   max[ConcussionGrenade]  = 1;
   max[FlareGrenade]       = 1;
   max[TargetingLaser]     = 1;
   max[ELFGun]             = 1;
   max[ShockLance]         = 0;
   max[CameraGrenade]      = 1;
   max[Beacon]             = 3;
   
   max[SonicPulser] = 1;
   max[NapalmGrenade] = 1;
   max[EMPGrenade] = 1;
   max[CloakingMine] = 0;
   max[KoiMine] = 0;
   max[ConcussionMine] = 0;
   max[RepairPatchMine] = 0;
   
   max[MitziBlastCannon]   = 0;
   max[MitziCapacitor]     = 0;
   max[Sagittarius]        = 0;
   max[SagittariusAmmo]    = 0;
   max[Bolter]             = 0;
   max[BolterAmmo]         = 0;
   max[PlasmaCannon]       = 0;
   max[PlasmaCannonAmmo]   = 0;
   max[Ripper]             = 0;
   max[RipperAmmo]         = 0;
   max[ParticleBeamCannon] = 0;
   max[PBCAmmo]            = 0;
   max[MPDisruptor]        = 0;
   max[BlasterRifle]       = 0;
   
   max[ThrusterGrenade]   = 0;

   max[GravitronArmorPlating] = 0;
   max[EnduriumArmorPlating] = 0;
   max[DamageAmp] = 0;
   max[Nosferatu] = 0;
   max[SubspaceRegenerator] = 0;
   max[MagneticClamp] = 0;
   max[ExplosiveResistArmor] = 0;
   max[ElectroMagResistArmor] = 0;
   max[ThermalResistArmor] = 0;
   max[KineticResistArmor] = 0;
   max[SonicResistArmor] = 0;

   observeParameters = "0.5 4.5 4.5";
   shieldEffectScale = "0.7 0.7 1.0";
};

//----------------------------------------------------------------------------
datablock PlayerData(PlugsuitFemaleHumanArmor) : PlugsuitMaleHumanArmor
{
   shapeFile = "light_female.dts";
   waterBreathSound = WaterBreathFemaleSound;
   jetEffect =  HumanMediumArmorJetEffect;
};

//----------------------------------------------------------------------------
datablock PlayerData(PlugsuitMaleBiodermArmor) : PlugsuitMaleHumanArmor
{
   shapeFile = "bioderm_light.dts";
   jetEmitter = BiodermArmorJetEmitter;
   jetEffect =  BiodermArmorJetEffect;

   debrisShapeName = "bio_player_debris.dts";
   debris = BiodermDebris;
   
   //Foot Prints
   decalData   = LightBiodermFootprint;
   decalOffset = 0.3;

   waterBreathSound = WaterBreathBiodermSound;
};
