// YG Tek Gearhead Mk3

function setupGearhead(%data, %obj)
{
    if(isObject(%obj.currentAura))
        %obj.currentAura.destroy();

    %newAura = 0;
    
    if(%obj.subspacing)
        %newAura = Aura.create("GearheadSubspaceAura");
    else if(%obj.usingDamageAmp)
        %newAura = Aura.create("GearheadDamageAura");
        
    if(%newAura > 0)
    {
        %newAura.start();
        %newAura.attachToObject(%obj);
        
        %obj.currentAura = %newAura;
    }
    
    %obj.selectedBarrel = 0;
    %obj.selectedPainter = 0;
}

// Nano Burst - Heals everyone and everything within 30m of the Gearhead for 50 HP per GP (max 2), 10 second cooldown, ignores walls and cover
function triggerSpecialGearhead(%data, %obj, %imageData)
{
    %time = getSimTime();

    if(%obj.nextNanoBurst > %time)
        return;

    if(%obj.gyanLevel < 1)
        return;

    %gpuse = %obj.gyanLevel > 2 ? 2 : %obj.gyanLevel;

    %obj.decGyan(%gpuse);
    %obj.play3D("RepairPatchSound");
    
    InitContainerRadiusSearch(%obj.getWorldBoxCenter(), 30, $TypeMasks::StaticShapeObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType);
    
    while(%found = containerSearchNext())
    {
        %found.playShieldEffect("0.0 0.0 1.0");
        %found.incHP(%gpuse * 50);
    }

    commandToClient(%obj.client, 'BottomPrint', "Engaging Nano Burst, all objects within 30m repaired.", 5, 1);

    %obj.nextNanoBurst = %time + 10000;
}

datablock TargetProjectileData(PainterLaserProj)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 250;
   beamColor           	= "1.0 0.396078 0.0";

   startBeamWidth			= 0.20;
   pulseBeamWidth 	   = 0.15;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 6.0;
   pulseLength         	= 0.150;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = true;
};

datablock AudioProfile(PainterSwitchSound)
{
   filename    = "fx/weapons/generic_switch.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(PainterFireSound)
{
   filename    = "fx/weapons/targetinglaser_paint.wav";
   description = CloseLooping3d;
   preload = true;
};

datablock ShapeBaseImageData(LaserPainterImage)
{
   className = WeaponImage;

   shapeFile = "weapon_targeting.dts";
   item = TargetingLaser;
   offset = "0 0 0";

   projectile = "PainterLaserProj";
   projectileType = "TargetProjectile";
   customModeChange = true;

   enhancementSlots = 0;

   weaponDescName = "Laser Painter";
   defaultModeName = "Laser Painter";
   defaultModeDescription = "Point and paint, the Bombardment Satellite will lock on and fire the selected charge.";
   defaultModeSwitchSound = "BlasterDryFireSound";
   defaultModeFireSound = "";
   defaultModeFailSound = "BlasterDryFireSound";

    barrelCount = 4;

    barrelName[0] = "Artillery Painter";
    barrelDesc[0] = "Targets and locks a nearby (1000m) artillery turret to fire on the location";
    barrelCost[0] = 0;
    barrelLockTime[0] = 3000;
    
    barrelName[1] = "Stryker Kinetic Charge";
    barrelDesc[1] = "Fires a hard hitting shell at the painted target, affects targets through walls";
    barrelCost[1] = 6000;
    barrelLockTime[1] = 3000;

    barrelName[2] = "Ion Storm Lightning Strike";
    barrelDesc[2] = "Creates a localized lightning storm at the painted target";
    barrelCost[2] = 10000;
    barrelLockTime[2] = 3000;
    
    barrelName[3] = "Saturn CCC Rocket Barrage";
    barrelDesc[3] = "Launches a barrage of devistator-class rockets at the painted target (20/sec)";
    barrelCost[3] = 15000;
    barrelLockTime[3] = 4000;

    barrelName[4] = "Mitzi Orbital Strike Cannon";
    barrelDesc[4] = "Fires 3 high-powered Mitzi blasts at the painted target";
    barrelCost[4] = 25000;
    barrelLockTime[4] = 4000;
    
	usesEnergy = true;
	minEnergy = -1;
    fireEnergy = -1;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Activate";
	stateSound[0]                    = PainterSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
	stateEnergyDrain[3]              = 0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onFire";
   stateTransitionOnTriggerUp[3]    = "Deconstruction";
   stateTransitionOnNoAmmo[3]       = "Deconstruction";
   stateSound[3]                    = PainterFireSound;

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Deconstruction";
   stateScript[5]                   = "deconstruct";
   stateTransitionOnTimeout[5]      = "Ready";
};

function LaserPainterImage::selectFireMode(%this, %obj)
{
    %obj.selectedPainter++;

    if(%obj.selectedPainter > %this.barrelCount)
        %obj.selectedPainter = 0;

    bottomPrint(%obj.client, "<color:ffffff>[Weapon "@%obj.selectedPainter+1@"/"@%this.barrelCount+1@"] <color:42dbea>"@%this.barrelName[%obj.selectedPainter]@" {U: "@%this.barrelCost[%obj.selectedPainter]@" R:"@$ResourceCount[%obj.client.team]@"/"@$ResourceMax[%obj.client.team]@"}" NL %this.barrelDesc[%obj.selectedPainter], $WeaponMode::DisplayTime, 2);
    messageClient(%obj.client, 'MsgSPCurrentObjective2', "", 'Health: %1 | Weapon Mode: %2', %obj.internalHealth, %this.barrelName[%obj.selectedPainter]);
}

function validateLaserPainter(%obj, %image)
{
    %pass = false;
    
    if(%obj.selectedPainter $= "")
        %obj.selectedPainter = 0;
       
    if(%obj.selectedPainter > 0)
    {
        if(!$Satellite[%obj.client.team])
            bottomPrint(%obj.client, "<color:ff0000>Error!<color:42dbea> No Bombardment Satellite in orbit. Deploy one!", 5, 1);
        else if(%image.barrelCost[%obj.selectedPainter] > $ResourceCount[%obj.client.team])
            bottomPrint(%obj.client, "<color:ff0000>Error!<color:42dbea> Not enough resources to fire" SPC %image.barrelName[%obj.selectedPainter], 5, 1);
        else
            %pass = true;
    }
    else
    {
        InitContainerRadiusSearch(%obj.position, 1000, $TypeMasks::VehicleObjectType);

        while((%found = containerSearchNext()) != 0)
        {
            if(%found.deployed == 1)
            {
                if(%found.team == %obj.team)
                {
                    if(%found.artilleryTarget > 0)
                        continue;

                    %obj.selectedBMAT = %found;
                    %pass = true;
                    break;
                }
            }
        }
            
        if(!%pass)
        {
            %obj.selectedBMAT = 0;
            bottomPrint(%obj.client, "<color:ff0000>Error!<color:42dbea> No free Bombard Mobile Artillery within range (1000m)!", 5, 1);
        }
    }

    return %pass;
}

function LaserPainterImage::onFire(%this, %obj, %slot)
{
    %time = getSimTime();

    if(%obj.lastPaintTime > getSimTime())
    {
        %obj.setImageTrigger(%slot, false);
        return;
    }

    if(!validateLaserPainter(%obj, %this))
    {
        %obj.setImageTrigger(%slot, false);
        return;
    }

    %hit = castRay(%obj.getMuzzlePoint(%slot), %obj.getMuzzleVector(%slot), %this.projectile.maxRifleRange, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType);

    if(%hit)
    {
        %obj.lastPaintMode = %obj.selectedPainter;
        %obj.lastProjectile = createProjectile("TargetProjectile", "PainterLaserProj", %obj.getMuzzleVector(%slot), %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
        %obj.startPaintTime = %time;
        %obj.startPaintPos = %hit.hitPos;
        %obj.startPaintObj = %hit.hitObj;
        %obj.painterMode = 1;
        
        if(isObject(%hit.hitObj))
        {
            if(%hit.hitObj.team != %obj.team && (%hit.hitObj.isPlayer() || %hit.hitObj.isVehicle()))
                %obj.painterMode = 2;
        }
        
        %this.schedule(250, "acquireTarget", %obj, %slot);
        return;
    }
    else
        bottomPrint(%obj.client, "<color:ff0000>Error!<color:42dbea> No target in range.", 5, 1);

    %obj.lastPaintTime = %time + 500;
    %obj.setImageTrigger(%slot, false);
}

function LaserPainterImage::acquireTarget(%this, %obj, %slot)
{
    if(!%obj.getImageTrigger(%slot))
    {
        if(isObject(%obj.lastProjectile))
            %obj.lastProjectile.delete();
            
        return;
    }

    if(%obj.lastPaintMode != %obj.selectedPainter)
    {
        if(isObject(%obj.lastProjectile))
            %obj.lastProjectile.delete();

        bottomPrint(%obj.client, "<color:ff0000>Error!<color:42dbea> Changed targeting mode, deactivating.", 5, 1);
        return;
    }
    
    %obj.lastPaintMode = %obj.selectedPainter;

    %time = getSimTime();
    %pos = %obj.getMuzzlePoint(%slot);
    %end = vectorProject(%pos, %obj.getMuzzleVector(%slot), %this.projectile.maxRifleRange);
    %ray = containerRayCast(%pos, %end, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType, 0);

    if(!%ray)
    {
        bottomPrint(%obj.client, "<color:ff0000>Error!<color:42dbea> No target within lasering range (250m).", 5, 1);
        %obj.lastPaintTime = %time + 500;
        %obj.setImageTrigger(%slot, false);
        return;
    }

    if(%obj.painterMode == 1)
    {
        %hitPos = getWords(%ray, 1, 3);

        if(vectorDist(%hitPos, %obj.startPaintPos) > 1)
        {
            bottomPrint(%obj.client, "<color:ff0000>Error!<color:42dbea> Laser painter calibration drift, deactivating.", 5, 1);
            %obj.lastPaintTime = %time + 500;
            %obj.setImageTrigger(%slot, false);
            return;
        }
    }
    else if(%obj.painterMode == 2)
    {
        %hitObj = getWord(%ray, 0);
        
        if(!isObject(%hitObj) || %obj.startPaintObj != %hitObj)
        {
            bottomPrint(%obj.client, "<color:ff0000>Error!<color:42dbea> Laser painter target lost, deactivating.", 5, 1);
            %obj.lastPaintTime = %time + 500;
            %obj.setImageTrigger(%slot, false);
            return;
        }
    }
    
    %timeleftpct = ((%time - %obj.startPaintTime) / %this.barrelLockTime[%obj.selectedPainter]);
    
    if(%timeleftpct >= 1)
    {
        if(%obj.selectedPainter == 0)
            BMATFireOnPoint(%obj);
        else
        {
            if(validateLaserPainter(%obj, %this))
            {
                if(%obj.painterMode == 2)
                    %obj.startPaintPos = %obj.startPaintObj.getWorldBoxCenter();
                    
                $Satellite[%obj.client.team].getDatablock().fireSuperweapon(%obj, %this.barrelCost[%obj.selectedPainter]);
            }
        }
        
        bottomprint(%obj.client, ">>> TARGET LOCKED <<<", 5, 1);
        %obj.lastPaintTime = %time + 500;
        %obj.setImageTrigger(%slot, false);
    }
    else
    {
        %progress = createColorGraph("#", 40, %timeleftpct, "<color:FFFFFF>", "<color:FFFFFF>", "<color:FFFFFF>", "<color:777777>", 0.05, 0.1);
        bottomprint(%obj.client, ">>> FOCUSING OPTICAL TARGETING SENSORS <<<" NL %progress, 1, 2);
        %this.schedule(200, "acquireTarget", %obj, %slot);
    }
}

function BMATFireOnPoint(%player)
{
    if(!isObject(%player.selectedBMAT))
        return;
    
    %this = %player.selectedBMAT;

    for(%i = 0; %i < 5; %i++)
    {
        %part = %this.installedVehiclePart[(%i + 3)];

        if(isObject(%part))
            %part.reArm(%this, %this, %player, %i);
    }

    %targetPoint = vectorAdd(%player.startPaintPos, "0 0 0.25");
    
    %beacon = new WayPoint()
    {
        position = %targetPoint;
        rotation = "1 0 0 0";
        scale = "1 1 1";
        name = "Artillery Target";
        dataBlock = "WayPointMarker";
        lockCount = "0";
        homingCount = "0";
        team = %player.team;
    };

    MissionCleanup.add(%beacon);
    %beacon.schedule(20000, "delete");
    
    if(%player.painterMode == 2)
    {
        %player.startPaintPos = %player.startPaintObj.getWorldBoxCenter();
        %this.artilleryTarget = %player.startPaintObj;
        %time = getSimTime();

        schedule(20000, %this, "BMATStopFiring", %this);
        BMATTrackTarget(%this, %player.startPaintObj, %beacon, %time + 20000, %time);
        %this.play3d(SatelliteFireWarning);
        return;
    }
    
    schedule(20000, %this, "BMATStopFiring", %this);
    %this.artilleryTarget = %beacon;
    %this.play3d(SatelliteFireWarning);
}

function BMATTrackTarget(%this, %target, %beacon, %timeout, %start)
{
    if(getSimTime() < %start)
    {
        %beacon.position = %target.getWorldBoxCenter();
        
        schedule(32, %target, "BMATTrackTarget", %this, %target, %beacon, %timeout, %start);
    }
}

function BMATStopFiring(%veh)
{
    // Power cycle the turret, in a manner of speaking
    %veh.getMountNodeObject(1).setAutoFire(false);
    
    schedule(5000, %veh, "BMATResetFiring", %veh);
}

function BMATResetFiring(%veh)
{
    %veh.artilleryTarget = 0;
    %veh.getMountNodeObject(1).setAutoFire(true);
}

function LaserPainterImage::deconstruct(%data, %obj, %slot)
{
   if(isObject(%obj.lastProjectile))
   {
      %obj.lastProjectile.delete();
      %obj.lastProjectile = "";
   }
}

datablock PlayerData(GearheadMaleHumanArmor) : ArmorDamageProfile
{
   emap = true;

   armorType = $ArmorType::Gearhead;
   armorMask = $ArmorMask::Gearhead;
   armorClass = $ArmorClassMask::Medium;
   armorFlags = $ArmorFlags::CanPilot;
   enhancementMultiplier = 2.0;
   enhancementSlots = 3;
   
   armorSpecialCount = 4;
   armorSpecial[0] = "TargetingLaserFakeImage";
   armorSpecial[1] = "ArmorSpecialImage";
   armorSpecial[2] = "LaserPainterImage";
   armorSpecial[3] = "BarrelSwapperImage";
   armorSpecial[4] = "LightBarrelSwapperImage";
//   armorSpecial[5] = "ConstructorImage";
   
   className = Armor;
   shapeFile = "medium_male.dts";
   cameraMaxDist = 3;
   computeCRC = true;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy";
   hudRenderModulated[0] = true;
   hudRenderDistance[0] = true;
   
   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;

   aiAvoidThis = true;

   minLookAngle = -1.5;
   maxLookAngle = 1.5;
   maxFreelookAngle = 3.0;

   mass = 115;
   drag = 0.2;
   maxdrag = 0.4;
   density = 20;
   maxDamage = 2.0;
   maxEnergy = 130;
   repairRate = 0.0033;
   energyPerDamagePoint = 60.0; // shield energy required to block one point of damage

   rechargeRate = 0.408;
   jetForce = 34.52 * 130;
   underwaterJetForce = 29.03 * 130 * 1.5;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = 1.305;
   underwaterJetEnergyDrain = 0.774;
   minJetEnergy = 3;
   maxJetHorizontalPercentage = 0.6;

   runForce = 46 * 130;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 15;
   maxBackwardSpeed = 12;
   maxSideSpeed = 13;

   maxUnderwaterForwardSpeed = 12;
   maxUnderwaterBackwardSpeed = 10;
   maxUnderwaterSideSpeed = 10;

   recoverDelay = 5;
   recoverRunForceScale = 0.8;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 4.0; // takes 4 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.333; // takes 3.0 seconds of constant jet to get full heat sig.

   jumpForce = 8.5 * 130;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpDelay = 0;

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 70;
   jumpSurfaceAngle = 80;

   minJumpSpeed = 15;
   maxJumpSpeed = 25;

   noFrictionOnSki = true;
   horizMaxSpeed = 500;
   horizResistSpeed = 41.78;
   horizResistFactor = 0;
   maxJetForwardSpeed = 25;

   upMaxSpeed = 52;
   upResistSpeed = 20.89;
   upResistFactor = 0.23;

   minImpactSpeed = 45;
   speedDamageScale = 0.0023;

   jetSound = ArmorJetSound;
   wetJetSound = ArmorWetJetSound;

   jetEmitter = HumanArmorJetEmitter;
   jetEffect = HumanMediumArmorJetEffect;

   boundingBox = "1.45 1.45 2.4";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = MediumMaleFootprint;
   decalOffset = 0.35;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootMediumSoftSound;
   RFootSoftSound       = RFootMediumSoftSound;
   LFootHardSound       = LFootMediumHardSound;
   RFootHardSound       = RFootMediumHardSound;
   LFootMetalSound      = LFootMediumMetalSound;
   RFootMetalSound      = RFootMediumMetalSound;
   LFootSnowSound       = LFootMediumSnowSound;
   RFootSnowSound       = RFootMediumSnowSound;
   LFootShallowSound    = LFootMediumShallowSplashSound;
   RFootShallowSound    = RFootMediumShallowSplashSound;
   LFootWadingSound     = LFootMediumWadingSound;
   RFootWadingSound     = RFootMediumWadingSound;
   LFootUnderwaterSound = LFootMediumUnderwaterSound;
   RFootUnderwaterSound = RFootMediumUnderwaterSound;
   LFootBubblesSound    = LFootMediumBubblesSound;
   RFootBubblesSound    = RFootMediumBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactMediumSoftSound;
   impactHardSound      = ImpactMediumHardSound;
   impactMetalSound     = ImpactMediumMetalSound;
   impactSnowSound      = ImpactMediumSnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactMediumWaterEasySound;
   impactWaterMedium    = ImpactMediumWaterMediumSound;
   impactWaterHard      = ImpactMediumWaterHardSound;

   groundImpactMinSpeed    = 13.0;
   groundImpactShakeFreq   = "3.5 3.5 3.5";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.7;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterMediumSound;

   maxWeapons = 2;            // Max number of different weapons the player can have
   maxGrenades = 1;           // Max number of different grenades the player can have
   maxMines = 1;              // Max number of different mines the player can have

   // Inventory restrictions
   max[RepairKit]          = 3;
   max[Mine]               = 5;
   max[Grenade]            = 10;
   max[Blaster]            = 1;
   max[Plasma]             = 1;
   max[PlasmaAmmo]         = 350;
   max[Disc]               = 1;
   max[DiscAmmo]           = 20;
   max[SniperRifle]        = 0;
   max[GrenadeLauncher]    = 1;
   max[GrenadeLauncherAmmo]= 25; // z0dd - ZOD, 9/28/02. Was 12
   max[Mortar]             = 0;
   max[MortarAmmo]         = 0;
   max[MissileLauncher]    = 1;
   max[LaserMissileLauncher] = 1;
   max[RocketLauncher]     = 1;
   max[MissileLauncherAmmo]= 4;
   max[Chaingun]           = 1;
   max[ChaingunAmmo]       = 200;
   max[SubspaceMagnet]     = 0;
   max[SubspaceCapacitor]  = 0;
   max[RepairGun]          = 1;
   max[CloakingPack]       = 0;
   max[SensorJammerPack]   = 1;
   max[EnergyPack]         = 1;
   max[RepairPack]         = 1;
   max[ShieldPack]         = 1;
   max[AmmoPack]           = 1;
   
   max[SatchelCharge]      = 1;
   max[InventoryDeployable]= 0;
   max[MotionSensorDeployable]   = 1;
   max[PulseSensorDeployable] = 1;
   max[TurretOutdoorDeployable]     = 1;
   max[TurretIndoorDeployable]   = 1;

   max[FlashGrenade]       = 10;
   max[ConcussionGrenade]  = 10;
   max[FlareGrenade]       = 10;
   
   max[TargetingLaser]     = 1;
   max[ELFGun]             = 1;
   max[ShockLance]         = 0;
   max[CameraGrenade]      = 6;
   max[Beacon]             = 8;

   max[SynomicRegenerator] = 1;
   
   max[MitziBlastCannon]   = 1;
   max[MitziCapacitor]     = 40;
   max[SpikeRifle]         = 1;
   max[SpikeAmmo]          = 50;
   max[ThrusterGrenade]    = 1;
   max[BatteryGrenade]     = 2;
   
   max[Bolter]             = 0;
   max[BolterAmmo]         = 0;
   max[PlasmaCannon]       = 0;
   max[PlasmaCannonAmmo]   = 0;
   max[Ripper]             = 0;
   max[RipperAmmo]         = 0;
   max[ParticleBeamCannon] = 0;
   max[PBCAmmo]            = 0;
   max[MPDisruptor]        = 0;
   max[BlasterRifle]       = 1;

   max[SonicPulser] = 10;
   max[NapalmGrenade] = 10;
   max[EMPGrenade] = 10;
   max[CloakingMine] = 5;
   max[KoiMine] = 5;
   max[ConcussionMine] = 5;
   max[RepairPatchMine] = 5;

   max[Nosferatu] = 1;
   max[MagneticClamp] = 1;
   max[ShieldHardener] = 1;
   max[LifeSupport] = 1;
   max[NonNewtonianPadding] = 1;
   
   max[DeployableInvStationPack] = 1;
   max[TelePack] = 1;
   max[SensorBasePack] = 1;
   max[CommandBasePack] = 1;
   max[SatelliteBasePack] = 1;
   max[BlastWallPack] = 1;
   max[TurretBasePack] = 1;
   max[ShieldGeneratorPack] = 1;
   max[BussardCollectorPack] = 1;
   max[ResourceDrillerPack] = 1;
   max[ShieldNodeGeneratorPack] = 1;
   max[ResourceDepotPack] = 1;
   max[NodeSpawnFavsPack] = 1;
   max[NodeRegeneratorPack] = 1;
   max[NodeRepulsorPack] = 1;
   max[NodeShieldPack] = 1;
   max[NodeCloakPack] = 1;
   max[ResourceHarvester] = 1;
   max[ClassBooster] = 1;

   observeParameters = "0.5 4.5 4.5";
   shieldEffectScale = "0.7 0.7 1.0";
};

//----------------------------------------------------------------------------
datablock PlayerData(GearheadFemaleHumanArmor) : GearheadMaleHumanArmor
{
   shapeFile = "medium_female.dts";
   waterBreathSound = WaterBreathFemaleSound;
   jetEffect =  HumanArmorJetEffect;
};

//----------------------------------------------------------------------------
datablock PlayerData(GearheadMaleBiodermArmor) : GearheadMaleHumanArmor
{
   shapeFile = "bioderm_medium.dts";
   jetEmitter = BiodermArmorJetEmitter;
   jetEffect =  BiodermArmorJetEffect;

   debrisShapeName = "bio_player_debris.dts";
   debris = BiodermDebris;
   
   //Foot Prints
   decalData   = MediumBiodermFootprint;
   decalOffset = 0.35;

   waterBreathSound = WaterBreathBiodermSound;
};


