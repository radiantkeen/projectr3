datablock ItemData(ArmorWeight)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

   image = "ArmorWeightP5";
	pickUpName = "abstract";
};

datablock ShapeBaseImageData(ArmorWeightP5)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 5;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP10)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 10;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP15)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 15;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP20)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 20;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP25)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 25;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP30)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 30;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP35)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 35;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP40)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 40;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP45)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 45;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP50)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 50;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP75)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 75;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightP100)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = 100;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM5)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -5;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM10)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -10;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM15)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -15;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM20)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -20;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM25)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -25;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM30)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -30;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM35)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -35;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM40)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -40;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM45)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -45;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM50)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -50;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM75)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -75;
   subImage = true;
};

datablock ShapeBaseImageData(ArmorWeightM100)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = ArmorWeight;
   mountPoint = 1;
   mass = -100;
   subImage = true;
};
