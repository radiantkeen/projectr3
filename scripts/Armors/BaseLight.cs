//SOUNDS ----- LIGHT ARMOR--------
datablock AudioProfile(LFootLightSoftSound)
{
   filename    = "fx/armor/light_LF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootLightSoftSound)
{
   filename    = "fx/armor/light_RF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootLightHardSound)
{
   filename    = "fx/armor/light_LF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootLightHardSound)
{
   filename    = "fx/armor/light_RF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootLightMetalSound)
{
   filename    = "fx/armor/light_LF_metal.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootLightMetalSound)
{
   filename    = "fx/armor/light_RF_metal.wav";
   description = AudioClose3d;
   preload = true;
};
datablock AudioProfile(LFootLightSnowSound)
{
   filename    = "fx/armor/light_LF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootLightSnowSound)
{
   filename    = "fx/armor/light_RF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootLightShallowSplashSound)
{
   filename    = "fx/armor/light_LF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootLightShallowSplashSound)
{
   filename    = "fx/armor/light_RF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootLightWadingSound)
{
   filename    = "fx/armor/light_LF_wade.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootLightWadingSound)
{
   filename    = "fx/armor/light_RF_wade.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootLightUnderwaterSound)
{
   filename    = "fx/armor/light_LF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootLightUnderwaterSound)
{
   filename    = "fx/armor/light_RF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootLightBubblesSound)
{
   filename    = "fx/armor/light_LF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootLightBubblesSound)
{
   filename    = "fx/armor/light_RF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactLightSoftSound)
{
   filename    = "fx/armor/light_land_soft.wav";
   description = AudioClose3d;
   preload = true;
   effect = ImpactSoftEffect;
};

datablock AudioProfile(ImpactLightHardSound)
{
   filename    = "fx/armor/light_land_hard.wav";
   description = AudioClose3d;
   preload = true;
   effect = ImpactHardEffect;
};

datablock AudioProfile(ImpactLightMetalSound)
{
   filename    = "fx/armor/light_land_metal.wav";
   description = AudioClose3d;
   preload = true;
   effect = ImpactMetalEffect;
};

datablock AudioProfile(ImpactLightSnowSound)
{
   filename    = "fx/armor/light_land_snow.wav";
   description = AudioClosest3d;
   preload = true;
   effect = ImpactSnowEffect;
};

datablock AudioProfile(ImpactLightWaterEasySound)
{
   filename    = "fx/armor/general_water_smallsplash2.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactLightWaterMediumSound)
{
   filename    = "fx/armor/general_water_medsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactLightWaterHardSound)
{
   filename    = "fx/armor/general_water_bigsplash.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ExitingWaterLightSound)
{
   filename    = "fx/armor/general_water_exit2.wav";
   description = AudioClose3d;
   preload = true;
};

//----------------------------------------------------------------------------
datablock DecalData(LightMaleFootprint)
{
   sizeX       = 0.125;
   sizeY       = 0.25;
   textureName = "special/footprints/L_male";
};

// Vectorport - Teleports you forward 200m using 2 GP, 30 sec cooldown
function triggerSpecialScout(%data, %obj, %imageData)
{
    %time = getSimTime();

    if(%obj.nextVectorport > %time)
        return;
    
    if(%obj.gyanLevel < 1)
        return;
    
    %dist = 200;
    %transform = "";
    %travelled = 0;
    %endpos = "";
    
    %hit = castRay(%obj.getWorldBoxCenter(), %obj.getMuzzleVector(0), %dist, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType, %obj);

    if(%hit)
    {
        if(%hit.hitObj.isPlayer())
        {
            if(!%hit.hitObj.isMounted())
            {
                %endpos = vectorProject(%hit.hitObj.getWorldBoxCenter(), %hit.hitObj.getForwardVector(), -1.5);
                %transform = %endpos SPC %hit.hitObj.getRotation();
            }
        }
        else
            %endpos = vectorProject(%hit.hitPos, %obj.getMuzzleVector(0), -0.5);
        
        %travelled = mCeil(%hit.hitDist);
    }
    else
    {
        commandToClient(%obj.client, 'BottomPrint', "No VectorPort location in range ("@%dist@"m)", 5, 1);
        return;
    }

    if(%transform == 0)
        %transform = %endpos SPC %obj.getRotation();
    
    %obj.setVelocity("0 0 0");
    %obj.ejectFlag();
    %obj.setTransform(%transform);
    %obj.decGyan(2);

    InitContainerRadiusSearch(vectorAdd(%endPos, "0 0 0.5"), 0.1, $TypeMasks::StaticShapeObjectType);

    while(%found = containerSearchNext())
    {
        if(%found.getType() & $TypeMasks::StationObjectType)
            continue;
        else
        {
            %obj.gib();
            %obj.setDamageFlash(0.5 + %obj.getDamageFlash());
            %obj.getDatablock().damageObject(%obj, %obj, %endPos, 10000, $DamageType::VectorGlitch, "0 0 0", false);
            break;
        }
    }
    
    commandToClient(%obj.client, 'BottomPrint', "Distance travelled: "@ %travelled @"m - Used 2 GP - Vectorport cooling off, 30s", 5, 1);
    schedule(30000, %obj, "specialScoutCD", %obj);
    
    %obj.nextVectorport = %time + 30000;
}

function specialScoutCD(%obj)
{
    if(!%obj.isDead && %obj.getDatablock().armorType == $ArmorType::Scout)
        commandToClient(%obj.client, 'BottomPrint', "VectorPort ready!", 5, 1);
}

datablock PlayerData(LightMaleHumanArmor) : ArmorDamageProfile
{
   emap = true;

   armorType = $ArmorType::Scout;
   armorMask = $ArmorMask::Scout;
   armorClass = $ArmorClassMask::Light;
   armorFlags = $ArmorFlags::CanPilot;
   enhancementMultiplier = 1.0;
   enhancementSlots = 3;
   
   armorSpecialCount = 1;
   armorSpecial[0] = "TargetingLaserFakeImage";
   armorSpecial[1] = "ArmorSpecialImage";
   
   className = Armor;
   shapeFile = "light_male.dts";
   cameraMaxDist = 3;
   computeCRC = true;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_camera";
   hudImageNameEnemy[0] = "gui/hud_camera";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;

   aiAvoidThis = true;

   minLookAngle = -1.5;
   maxLookAngle = 1.5;
   maxFreelookAngle = 3.0;

   mass = 90;
   drag = 0.134;
   maxdrag = 0.3;
   density = 19;
   maxDamage = 1.0;
   maxEnergy = 75;
   repairRate = 0.0033;
   energyPerDamagePoint = 75.0; // shield energy required to block one point of damage

   rechargeRate = 0.256;
   jetForce = 37.28 * 90;
   underwaterJetForce = 28.00 * 90 * 1.5;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = 0.90;
   underwaterJetEnergyDrain = 0.7;
   minJetEnergy = 3;
   maxJetHorizontalPercentage = 0.6;

   runForce = 55.20 * 90;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 15;
   maxBackwardSpeed = 13;
   maxSideSpeed = 13;

   maxUnderwaterForwardSpeed = 13;
   maxUnderwaterBackwardSpeed = 11;
   maxUnderwaterSideSpeed = 11;

   jumpForce = 8.34 * 90;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpDelay = 0;

   recoverDelay = 8;
   recoverRunForceScale = 1.4;

   minImpactSpeed = 45;
   speedDamageScale = 0.0022;

   jetSound = ArmorJetSound;
   wetJetSound = ArmorJetSound;
   jetEmitter = HumanArmorJetEmitter;
   jetEffect = HumanArmorJetEffect;

   boundingBox = "1.2 1.2 2.3";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = LightMaleFootprint;
   decalOffset = 0.25;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 70;
   jumpSurfaceAngle = 80;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;

   noFrictionOnSki = true;
   horizMaxSpeed = 500;
   horizResistSpeed = 48.74;
   horizResistFactor = 0;
   maxJetForwardSpeed = 30;

   upMaxSpeed = 52;
   upResistSpeed = 20.89;
   upResistFactor = 0.35;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 4.0; // takes 4 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootLightSoftSound;
   RFootSoftSound       = RFootLightSoftSound;
   LFootHardSound       = LFootLightHardSound;
   RFootHardSound       = RFootLightHardSound;
   LFootMetalSound      = LFootLightMetalSound;
   RFootMetalSound      = RFootLightMetalSound;
   LFootSnowSound       = LFootLightSnowSound;
   RFootSnowSound       = RFootLightSnowSound;
   LFootShallowSound    = LFootLightShallowSplashSound;
   RFootShallowSound    = RFootLightShallowSplashSound;
   LFootWadingSound     = LFootLightWadingSound;
   RFootWadingSound     = RFootLightWadingSound;
   LFootUnderwaterSound = LFootLightUnderwaterSound;
   RFootUnderwaterSound = RFootLightUnderwaterSound;
   LFootBubblesSound    = LFootLightBubblesSound;
   RFootBubblesSound    = RFootLightBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactLightSoftSound;
   impactHardSound      = ImpactLightHardSound;
   impactMetalSound     = ImpactLightMetalSound;
   impactSnowSound      = ImpactLightSnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactLightWaterEasySound;
   impactWaterMedium    = ImpactLightWaterMediumSound;
   impactWaterHard      = ImpactLightWaterHardSound;

   groundImpactMinSpeed    = 14.0;
   groundImpactShakeFreq   = "3.0 3.0 3.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.6;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterLightSound;

   maxWeapons = 3;           // Max number of different weapons the player can have
   maxGrenades = 1;          // Max number of different grenades the player can have
   maxMines = 1;             // Max number of different mines the player can have

   // Inventory restrictions
   max[RepairKit]          = 1;
   max[Mine]               = 3;
   max[Grenade]            = 5;
   max[Blaster]            = 1;
   max[Plasma]             = 1;
   max[PlasmaAmmo]         = 250;
   max[Disc]               = 1;
   max[DiscAmmo]           = 15;
   max[SniperRifle]        = 1;
   max[GrenadeLauncher]    = 1;
   max[GrenadeLauncherAmmo]= 15;
   max[Mortar]             = 0;
   max[MortarAmmo]         = 0;
   max[MissileLauncher]    = 0;
   max[MissileLauncherAmmo]= 0;
   max[Chaingun]           = 1;
   max[ChaingunAmmo]       = 150;
   max[SubspaceMagnet]     = 1;
   max[SubspaceCapacitor]  = 10;
   max[RepairGun]          = 1;
   max[CloakingPack]       = 1;
   max[SensorJammerPack]   = 1;
   max[EnergyPack]         = 1;
   max[RepairPack]         = 1;
   max[ShieldPack]         = 1;
   max[AmmoPack]           = 1;
   max[SatchelCharge]      = 1;
   max[FlashGrenade]       = 5;
   max[ConcussionGrenade]  = 5;
   max[FlareGrenade]       = 5;
   max[TargetingLaser]     = 1;
   max[ELFGun]             = 1;
   max[ShockLance]         = 1;
   max[CameraGrenade]      = 2;
   max[Beacon]             = 3;
   
   max[SonicPulser] = 4;
   max[NapalmGrenade] = 4;
   max[EMPGrenade] = 4;
   max[CloakingMine] = 2;
   max[KoiMine] = 3;
   max[ConcussionMine] = 2;
   max[RepairPatchMine] = 2;
   
   max[SynomicRegenerator] = 1;

   max[MitziBlastCannon]    = 1;
   max[MitziCapacitor]      = 25;
   max[Sagittarius]         = 1;
   max[SagittariusAmmo]     = 10;
   max[ScoutDisruptorMount] = 1;
   max[ScoutDiscMount]      = 1;
   max[ScoutDiscAmmo]       = 10;
   max[PlasmaCannon]        = 0;
   max[PlasmaCannonAmmo]    = 0;
   max[Ripper]              = 0;
   max[RipperAmmo]          = 0;
   max[ParticleBeamCannon]  = 0;
   max[PBCAmmo]             = 0;
   max[MPDisruptor]         = 0;
   max[SpikeRifle]          = 1;
   max[SpikeAmmo]           = 50;
   max[BlasterRifle]        = 1;
   
   max[ThrusterGrenade]     = 1;
   max[BatteryGrenade]     = 1;
   
   max[Nosferatu] = 1;
   max[SubspaceRegenerator] = 1;
   max[MagneticClamp] = 1;
   max[ScoutLaserFocuser] = 1;
   max[LifeSupport] = 1;
   max[NonNewtonianPadding] = 1;
   max[ShieldHardener] = 1;
   max[ResourceHarvester] = 1;
   max[ClassBooster] = 1;
   
   observeParameters = "0.5 4.5 4.5";
   shieldEffectScale = "0.7 0.7 1.0";
};

//----------------------------------------------------------------------------
datablock PlayerData(LightFemaleHumanArmor) : LightMaleHumanArmor
{
   shapeFile = "light_female.dts";
   waterBreathSound = WaterBreathFemaleSound;
   jetEffect =  HumanMediumArmorJetEffect;
};

//----------------------------------------------------------------------------
datablock DecalData(LightBiodermFootprint)
{
   sizeX       = 0.25;
   sizeY       = 0.25;
   textureName = "special/footprints/L_bioderm";
};

datablock PlayerData(LightMaleBiodermArmor) : LightMaleHumanArmor
{
   shapeFile = "bioderm_light.dts";
   jetEmitter = BiodermArmorJetEmitter;
   jetEffect =  BiodermArmorJetEffect;

   debrisShapeName = "bio_player_debris.dts";
   debris = BiodermDebris;
   
   //Foot Prints
   decalData   = LightBiodermFootprint;
   decalOffset = 0.3;

   waterBreathSound = WaterBreathBiodermSound;
};
