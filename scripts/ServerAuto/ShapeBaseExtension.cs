//-----------------------------------------------------------------------------
// ShapeBase Extension
//--------------------------------------

// HP conversion stuff
function ShapeBase::updateHPLevel(%obj)
{
    if(%obj.isDead)
        return;

    if(%obj.isPlayer() && %obj.hitPoints < 1 && !%obj.scriptKilled)
    {
        %rem = %obj.internalHealth + %obj.hitPoints;

        if(%rem > 0)
            %obj.hitPoints = 1;
            
        %obj.setInternalHealth(%rem);
    }

    %obj.setDamageLevel((1 - (%obj.hitPoints / %obj.maxHitPoints)) * %obj.getDatablock().maxDamage);
}

function ShapeBase::getHPLevel(%obj) // getDamagePercent()
{
    %data = %obj.getDatablock();
    %health = %data.maxDamage - %obj.getDamageLevel();
    %healthPct = %health / %data.maxDamage;
    
    return mCeil(%obj.maxHitPoints * %healthPct);
}

function ShapeBase::calculateHP(%obj)
{
    if(%obj.maxHitPoints $= "")
        %obj.maxHitPoints = mCeil(%obj.getDatablock().maxDamage * 100);

    %obj.hitPoints = %obj.getHPLevel();
}

function ShapeBase::setHPLevel(%obj, %amount)
{
    %amount = mCeil(%amount);
    %obj.hitPoints = %amount;

    if(%obj.hitPoints > %obj.maxHitPoints)
        %obj.hitPoints = %obj.maxHitPoints;
        
    %obj.updateHPLevel();
}

function ShapeBase::decHP(%obj, %amount)
{
    %amount = mCeil(%amount);

    if(%amount == 0)
        return;

    %obj.hitPoints -= %amount;
    %obj.updateHPLevel();
}

function ShapeBase::incHP(%obj, %amount)
{
    %amount = mCeil(%amount);
    %obj.hitPoints += %amount;

    if(%obj.hitPoints > %obj.maxHitPoints)
        %obj.hitPoints = %obj.maxHitPoints;

    %obj.updateHPLevel();
}

function ShapeBase::applyHPDamage(%obj, %amount)
{
    %obj.decHP(%amount);
    %obj.getDatablock().onDamage(%obj);
}

function ShapeBase::setInternalHealth(%obj, %amount)
{
    %obj.internalHealth = %amount;

    %obj.applyInternalDamage(%obj, 0);
}

function ShapeBase::applyInternalDamage(%obj, %amount)
{
    if(%obj.isDead)
        return;

    %obj.internalHealth -= %amount;
    
    if(%obj.internalHealth < 0)
        %obj.internalHealth = 0;

    %obj.showInternalHealth();

    if(%obj.internalHealth < 1 && !%obj.scriptKilled)
    {
        if(%obj.repAegis == true && %obj.reppack == true)
        {
            if(%obj.hitPoints < 1)
                %obj.hitPoints = 1;
                
            %obj.internalHealth = 1;
            
            triggerRepPackAegis(%obj);
        }
        else if(%obj.lifeSupport == true)
        {
            if(%obj.hitPoints < 1)
                %obj.hitPoints = 1;
                
            %obj.internalHealthMax = 30;
            %obj.internalHealth = 30;
            
            triggerLifeSupportCD(%obj);
        }
    }
}

function ShapeBase::doInternalDamage(%targetObject, %sourceObject, %amount, %damageType, %element, %srcProj)
{
    %targetObject.lastHitFlags |= $Projectile::InternalDamage;
    %targetObject.getDatablock().damageObject(%targetObject, %sourceObject, %targetObject.position, %amount, %damageType, "0 0 0", %sourceObject.client, %srcProj, %element);
}

function ShapeBase::setPowerLevel(%obj, %amount)
{
    %energyPct = %amount / %obj.maxPower;
    %obj.setEnergyLevel(%healthPct * %obj.getDatablock().maxEnergy);
}

function ShapeBase::getPowerLevel(%obj)
{
    return mCeil(%obj.getEnergyLevel() * 100);
}

function ShapeBase::playHitIndicator(%obj, %enemy)
{
    %client = 0;
    
    if(%obj.client > 0)
        %client = %obj.client;
    else if((%cl = %obj.getControllingClient()))
        %client = %cl;
        
    if(%client && !%client.isAIControlled())
    {
        if(%enemy)
            messageClient(%client, 'MsgClientHit', %client.playerHitWav);

        %client.displayHitIndicator(%enemy);
    }
}

function GameConnection::displayHitIndicator(%client, %enemy)
{
    if(%client.hitDisplay > 0)
        cancel(%client.hitDisplay);
    
    if(%enemy)
        activateDeploySensorGrn(%client.player);
    else
        activateDeploySensorRed(%client.player);

    %client.hitDisplay = %client.schedule(256, "clearHitIndicator");
}

function GameConnection::clearHitIndicator(%client)
{
    %client.hitDisplay = 0;
    deactivateDeploySensor(%client.player);
}

function ShapeBaseData::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);
	// if it's a deployed object, schedule the ambient thread to play in a little while
   if(%data.deployAmbientThread)
	   %obj.schedule(750, "playThread", $AmbientThread, "ambient");
	// check for ambient animation that should always be played
	if(%data.alwaysAmbient)
		%obj.playThread($AmbientThread, "ambient");

    // keen: MD3 initial stuff
    %obj.isWet = false;
    %obj.isFragmented = false;
    %obj.isWalker = false;

    %data.resetStats(%obj);
}

function ShapeBaseData::resetStats(%data, %obj)
{
    // Enhancement System
    %obj.enhancementMultiplier = %data.enhancementMultiplier $= "" ? 1 : %data.enhancementMultiplier;
    %obj.enhancementSlots = %data.enhancementSlots !$= "" ? %data.enhancementSlots : 0;
    
    %obj.damageReduceFactor = 1.0;
    %obj.damageBuffFactor = 1.0;
    %obj.energyEfficiencyFactor = 1.0;
    %obj.shieldFactor = 1.0;
    %obj.bonusWeight = 0;
    %obj.jetEnergyFactor = 1.0;
    %obj.jetForceFactor = 1.0;
    %obj.spreadFactorBase = 1.0;
    %obj.rateOfFire = 1.0;
    %obj.maxAmmoCapacityFactor = 1.0;
    %obj.rechargeRateFactor = 1.0;
    %obj.shieldRechargeFactor = 1.0;
    %obj.damageReduction = 0;

    %obj.armorDamageFactor[$DamageGroupMask::Misc] = 1.0;
    %obj.armorDamageFactor[$DamageGroupMask::Energy] = 0.75;
    %obj.armorDamageFactor[$DamageGroupMask::Explosive] = 1.25;
    %obj.armorDamageFactor[$DamageGroupMask::Kinetic] = 1.1;
    %obj.armorDamageFactor[$DamageGroupMask::Plasma] = 1.0;
    %obj.armorDamageFactor[$DamageGroupMask::Mitzi] = 1.0;
    %obj.armorDamageFactor[$DamageGroupMask::Poison] = 1.0;
    %obj.armorDamageFactor[$DamageGroupMask::Sonic] = 1.0;
    %obj.armorDamageFactor[$DamageGroupMask::Psionic] = 1.0;
    %obj.armorDamageFactor[$DamageGroupMask::ExternalMeans] = 1.0;

    %obj.shieldDamageFactor[$DamageGroupMask::Misc] = 1.0;
    %obj.shieldDamageFactor[$DamageGroupMask::Energy] = 1.5;
    %obj.shieldDamageFactor[$DamageGroupMask::Explosive] = 0.5;
    %obj.shieldDamageFactor[$DamageGroupMask::Kinetic] = 1.0;
    %obj.shieldDamageFactor[$DamageGroupMask::Plasma] = 1.1;
    %obj.shieldDamageFactor[$DamageGroupMask::Mitzi] = 1.0;
    %obj.shieldDamageFactor[$DamageGroupMask::Poison] = 1.0;
    %obj.shieldDamageFactor[$DamageGroupMask::Sonic] = 1.0;
    %obj.shieldDamageFactor[$DamageGroupMask::Psionic] = 1.0;
    %obj.shieldDamageFactor[$DamageGroupMask::ExternalMeans] = 1.0;
    
   // HP system
   %obj.maxHitPoints = mCeil(%data.maxDamage * 100);
   %obj.hitPoints = %obj.maxHitPoints;
   %obj.baseHitPoints = %obj.maxHitPoints;
   %obj.maxPower = mCeil(%data.maxEnergy * 100);
}

function GameBaseData::onAdd(%data, %obj)
{
   if(%data.targetTypeTag !$= "")
   {
      // use the name given to the object in the mission file
      if(%obj.nameTag !$= "")
      {
         %obj.nameTag = addTaggedString(%obj.nameTag);
         %nameTag = %obj.nameTag;
      }
      else
         %nameTag = %data.targetNameTag;

   	%obj.target = createTarget(%obj, %nameTag, "", "", %data.targetTypeTag, 0, 0);
   }
   else
      %obj.target = -1;
}

function ShapeBase::damage(%this, %sourceObject, %position, %amount, %damageType, %srcProj, %element)
{
    if(%this.isLinkedNode == true)
    {
        if(%this.flags & $LC::IndependantDamage)
            %this.getDataBlock().damageObject(%this, %sourceObject, %position, %amount, %damageType, "0 0 0", %srcProj.instigator.client, %srcProj, %element);
        else
            %this.parentNode.getDataBlock().damageObject(%this.parentNode, %sourcedObject, %position, %amount, %damageType, "0 0 0", %srcProj.instigator.client, %srcProj, %element);
    }
    else
        %this.getDataBlock().damageObject(%this, %sourceObject, %position, %amount, %damageType, "0 0 0", %srcProj.instigator.client, %srcProj, %element);
}

function ShapeBaseData::checkShields(%data, %targetObject, %position, %amount, %damageType, %element)
{
    if(%targetObject.lastHitFlags & $Projectile::ArmorOnly)
        %amount = 0;
    else if(%targetObject.lastHitFlags & $Projectile::ShieldsOnly)
        %amount *= 2;
    else if(%targetObject.lastHitFlags & $Projectile::IgnoreShields)
        return %amount;

   %energy = %targetObject.getEnergyLevel();
   
   // keen: force calculations for low shields, don't run extra code on negligable shield impact
   if(%energy <= 10)
      return %amount;
   
   %strength = %energy / %data.energyPerDamagePoint;
   %shieldScale = %data.shieldDamageScale[%damageType];
   if(%shieldScale $= "")
      %shieldScale = 1;

   if (%amount * %shieldScale <= %strength) {
      // Shield absorbs all
      %lost = %amount * %shieldScale * %data.energyPerDamagePoint;
      %energy -= %lost;
      %targetObject.setEnergyLevel(%energy);

      %normal = "0.0 0.0 1.0";
      %targetObject.playShieldEffect( %normal );

      return 0;
   }
   // Shield exhausted
   %targetObject.setEnergyLevel(0);
   
    if(%targetObject.lastHitFlags & $Projectile::ShieldsOnly)
        return 0;
    else
        return %amount - %strength / %shieldScale;
}

function StaticShapeData::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %element)
{
   // if this is a non-team mission type and the object is "protected", don't damage it
   if(%data.noIndividualDamage && Game.allowsProtectedStatics())
      return;

   // if this is a Siege mission and this object shouldn't take damage (e.g. vehicle stations)
   if(%data.noDamageInSiege && Game.class $= "SiegeGame")
      return;

   if(%data.invulnerable)
      return;

   if(%element $= "")
      %element = $DamageGroupMask::Misc;

   %amount = %amount * 0.01; //mRound(%amount); // keen: todo fix

   if(%sourceObject && %targetObject.isEnabled())
   {
      if(%sourceObject.client)
      {
        %targetObject.lastDamagedBy = %sourceObject.client;
        %targetObject.lastDamagedByTeam = %sourceObject.client.team;
        %targetObject.damageTimeMS = GetSimTime();
      }
      else
      {
        %targetObject.lastDamagedBy = %sourceObject;
        %targetObject.lastDamagedByTeam = %sourceObject.team;
        %targetObject.damageTimeMS = GetSimTime();
      }
   }

   // Scale damage type & include shield calculations...
   if (%data.isShielded)
      %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);

   %damageScale = %data.damageScale[%damageType];
   if(%damageScale !$= "")
      %amount *= %damageScale;

    //if team damage is off, cap the amount of damage so as not to disable the object...
    if (!$TeamDamage && !%targetObject.getDataBlock().deployedObject)
    {
       // -------------------------------------
       // z0dd - ZOD, 6/24/02. Console spam fix
       if(isObject(%sourceObject))
       {
          //see if the object is being shot by a friendly
          if(%sourceObject.isVehicle())
             %attackerTeam = getVehicleAttackerTeam(%sourceObject);
          else
             %attackerTeam = %sourceObject.team;
      }
      if ((%targetObject.getTarget() != -1) && isTargetFriendly(%targetObject.getTarget(), %attackerTeam))
      {
         %curDamage = %targetObject.getDamageLevel();
         %availableDamage = %targetObject.getDataBlock().disabledLevel - %curDamage - 0.05;
         if (%amount > %availableDamage)
            %amount = %availableDamage;
      }
    }

   %targetObject.lastSourceObject = %sourceObject;
   %targetObject.lastDamageType = %damageType;

   // if there's still damage to apply
   if(%amount > 0)
   {
 //     %targetObject.applyDamage(%amount);

       // compute delta HP damage
       %hpDmgAmt = %amount * 100;
       %maxDmg = %targetObject.getMaxDamage();
       %hpDmgPct = %hpDmgAmt / %targetObject.maxHitPoints;
       %healthDelta = %hpDmgPct * %maxDmg;

       %targetObject.hitPoints =- %hpDmgAmt;
       %targetObject.applyDamage(%healthDelta);
       
       // PoP adjusting power output
       if(%targetObject.powering == true)
            %targetObject.getGroup().updatePowerState();
       
       if(%targetObject.isLinkedNode == true && %targetObject.flags & $LC::RootNode)
           %targetObject.propagateDamageLevel(%targetObject.getDamageLevel());
   }
}

function StaticShapeData::onDamage(%this,%obj)
{
   // Set damage state based on current damage level
   %damage = %obj.getDamageLevel();
   if(%damage >= %this.destroyedLevel)
   {
      if(%obj.getDamageState() !$= "Destroyed")
      {
         %obj.setDamageState(Destroyed);
         // if object has an explosion damage radius associated with it, apply explosion damage
         if(%this.expDmgRadius)
            RadiusExplosion(%obj, %obj.getWorldBoxCenter(), %this.expDmgRadius, %this.expDamage, %this.expImpulse, %obj, $DamageType::Explosion);
         %obj.setDamageLevel(%this.maxDamage);
         
         if(%obj.isLinkedNode == true && %obj.flags & $LC::RootNode)
            %obj.destroyNode();
         else if(%obj.flags & $LC::DestroysParent)
            %obj.parentNode.damage(%obj.parentNode.lastSourceObject, %obj.parentNode.position, 100000, %obj.parentNode.lastDamageType, 0, $DamageGroupMask::ExternalMeans);
      }
   }
   else
   {
      if(%damage >= %this.disabledLevel)
      {
         if(%obj.getDamageState() !$= "Disabled")
            %obj.setDamageState(Disabled);
      }
      else
      {
         if(%obj.getDamageState() !$= "Enabled")
            %obj.setDamageState(Enabled);
      }
   }
}
