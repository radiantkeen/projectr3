// Generate name
function AIGenerateRandomName()
{
     %prefix = getRandom(1);
     %suffix = getRandom(1);
     %name = "";
     %tries = 0;

     if(%prefix)
          %name = $RandomBotGenPrefix[getRandom($RandomBotGenPrefixCount)];

     %rnd = getRandom($RandomBotGenNameCount);

     // Make sure we don't have any duplicates
     if($RandomBotNameAllocated[%rnd])
     {
          // Choose another unused bot name until we've found a free one...
          // If this fails (runs out of tries) it will be inevitable and use the
          // last generated one
          while($RandomBotNameAllocated[%rnd] && (%tries < $RandomBotGenNameCount))
          {
               %rnd = getRandom($RandomBotGenNameCount);
               %tries++;
          }

          // If we do overflow, just start over
          if(%tries == $RandomBotGenNameCount)
               AIClearUsedNamePool();
     }

     // Mark this name as taken
     $RandomBotNameAllocated[%rnd] = true;

     %mainName = $RandomBotGenName[%rnd];

     // Sanity check - somehow name is blank, this solves the problem although
     // potentially counter-productive...
     if(%mainName $= "")
          %mainName = $RandomBotGenName[getRandom($RandomBotGenNameCount)];

     %name = %name $= "" ? %mainName : %name SPC %mainName;

     if(%suffix)
          %name = %name SPC $RandomBotGenSuffix[getRandom($RandomBotGenSuffixCount)];

     // DarkDragonDX: Hopefully fix weird prepended spaces in bot names
     return trim(%name);
}

function AIClearUsedNamePool()
{
     %count = 0;

     while(%count < $RandomBotGenNameCount)
     {
          $RandomBotNameAllocated[%count] = false;
          %count++;
     }
}

function AIConnection::onAIConnect(%client, %name, %team, %skill, %offense, %voice, %voicePitch)
{
   if(%skill $= "")
      %skill = getRandom();

   %name = AIGenerateRandomName();

   // Sex/Race defaults
   %client.race = getRandom(1) ? "Human" : "Bioderm";

   if(%client.race $= "Human")
        %client.sex = getRandom(1) ? "Male" : "Female";
   else
        %client.sex = "Male";

   %client.armor = getRandom(1) ? "Light" : "Medium";

   if(%client.race $= "Human" && %client.sex $= "Male")
        %voice = getRandom(1) ? "Bot1" : "Male"@getRandom(1,5);
   else if(%client.race $= "Human" && %client.sex $= "Female")
        %voice = getRandom(1) ? "Bot1" : "Fem"@getRandom(1,5);
   else
        %voice = getRandom(1) ? "Bot1" : "Derm"@getRandom(1,3);

   %client.voice = %voice;
   %client.voiceTag = addTaggedString(%voice);

//   if (%voicePitch $= "" || %voicePitch < 0.5 || %voicePitch > 2.0)
//      %voicePitch = 1.0;
   %voicePitch = 1 - ((getRandom(20) - 10)/100);
   %client.voicePitch = %voicePitch;

   %client.name = addTaggedString( "\cp\c9" @ %name @ "\co" );
   %client.nameBase = %name;

   echo("BOTADD:" SPC %client.name SPC %client SPC %client.getAddress());
   $HostGamePlayerCount++;

   //set the initial team - Game.assignClientTeam() should be called later on...
   %client.team = %team;

   if(%voice $= "Bot1")
        %skin = %client.team == 1 ? "basebot" : "basebbot";
   else if(%client.race $= "Human")
        %skin = %client.team == 1 ? "beagle" : "swolf";
   else
        %skin = %client.team == 1 ? "base" : "baseb";

//   if ( %client.team & 1 )
   %client.skin = addTaggedString(%skin);

	//setup the target for use with the sensor net, etc...
   %client.target = allocClientTarget(%client, %client.name, %client.skin, %client.voiceTag, '_ClientConnection', 0, 0, %client.voicePitch);

   if($currentMissionType $= "SinglePlayer")
		messageAllExcept(%client, -1, 'MsgClientJoin', "", %name, %client, %client.target, true);
   else
	   messageAllExcept(%client, -1, 'MsgClientJoin', '\c1%1 joined the game.', %name, %client, %client.target, true);

	//assign the skill
	%client.setSkillLevel(%skill);

	//assign the affinity
   %client.offense = getRandom(1) == 1 ? true : false;

   //clear any flags
   %client.stop(); // this will clear the players move state
   %client.clearStep();
   %client.lastDamageClient = -1;
   %client.lastDamageTurret = -1;
   %client.setEngageTarget(-1);
   %client.setTargetObject(-1);
   %client.objective = "";

	//clear the defaulttasks flag
	%client.defaultTasksAdded = false;

	//if the mission is already running, spawn the bot
   if ($missionRunning)
      %client.startMission();
      
   schedule(0, 0, "updatePlayerCounts");
}

function RndAIAssignClientTeam(%game, %client, %respawn)
{
//error("DefaultGame::assignClientTeam");
   // this function is overwritten in non-team mission types (e.g. DM)
   // so these lines won't do anything
   //if(!%game.numTeams)
   //{
   //   setTargetSkin(%client.target, %client.skin);
   //   return;
   //}

   //  camera is responsible for creating a player
   //  - counts the number of players per team
   //  - puts this player on the least player count team
   //  - sets the client's skin to the servers default

   %numPlayers = ClientGroup.getCount();
   for(%i = 0; %i <= %game.numTeams; %i++)
      %numTeamPlayers[%i] = 0;

   for(%i = 0; %i < %numPlayers; %i = %i + 1)
   {
      %cl = ClientGroup.getObject(%i);
      if(%cl != %client)
         %numTeamPlayers[%cl.team]++;
   }
   %leastPlayers = %numTeamPlayers[1];
   %leastTeam = 1;
   for(%i = 2; %i <= %game.numTeams; %i++)
   {
      if( (%numTeamPlayers[%i] < %leastPlayers) ||
         ( (%numTeamPlayers[%i] == %leastPlayers) &&
         ($teamScore[%i] < $teamScore[%leastTeam] ) ))
      {
         %leastTeam = %i;
         %leastPlayers = %numTeamPlayers[%i];
      }
   }

   %client.team = %leastTeam;
   %client.lastTeam = %team;

   // Assign the team skin: - disabled here so bots don't get weird skins on mission change - keen
//   if ( %client.isAIControlled() )
//   {
//      if ( %leastTeam & 1 )
//      {
//         %client.skin = addTaggedString( "basebot" );
//         setTargetSkin( %client.target, 'basebot' );
//      }
//      else
//      {
//         %client.skin = addTaggedString( "basebbot" );
//         setTargetSkin( %client.target, 'basebbot' );
//      }
//   }
//   else
      setTargetSkin( %client.target, %game.getTeamSkin(%client.team) );
      //setTargetSkin( %client.target, %client.skin );

   // might as well standardize the messages
   //messageAllExcept( %client, -1, 'MsgClientJoinTeam', '\c1%1 joined %2.', %client.name, $teamName[%leastTeam], %client, %leastTeam );
   //messageClient( %client, 'MsgClientJoinTeam', '\c1You joined the %2 team.', $client.name, $teamName[%client.team], %client, %client.team );
   messageAllExcept( %client, -1, 'MsgClientJoinTeam', '\c1%1 joined %2.', %client.name, %game.getTeamName(%client.team), %client, %client.team );
   messageClient( %client, 'MsgClientJoinTeam', '\c1You joined the %2 team.', %client.name, %game.getTeamName(%client.team), %client, %client.team );

   updateCanListenState( %client );

//   logEcho(%client.nameBase@" (cl "@%client@") joined team "@%client.team);
}

function onAIRespawn(%client)
{
   %markerObj = Game.pickPlayerSpawn(%client, true);
   Game.createPlayer(%client, %markerObj);

	//make sure the player object is the AI's control object - even during the mission warmup time
	//the function AISystemEnabled(true/false) will control whether they actually move...
	%client.setControlObject(%client.player);

   if (%client.objective)
      error("ERROR!!! " @ %client @ " is still assigned to objective: " @ %client.objective);

   //clear the objective and choose a new one
	AIUnassignClient(%client);
   %client.stop();
   %client.clearStep();
   %client.lastDamageClient = -1;
   %client.lastDamageTurret = -1;
   %client.shouldEngage = -1;
   %client.setEngageTarget(-1);
   %client.setTargetObject(-1);
	%client.pilotVehicle = false;

	//set the spawn time
	%client.spawnTime = getSimTime();
	%client.respawnThread = "";

	//timeslice the objective reassessment for the bots
	if (!isEventPending(%client.objectiveThread))
	{
		%curTime = getSimTime();
		%remainder = %curTime % 5000;
		%schedTime = $AITimeSliceReassess - %remainder;
		if (%schedTime <= 0)
			%schedTime += 5000;
		%client.objectiveThread = schedule(%schedTime, %client, "AIReassessObjective", %client);

		//set the next time slice "slot"
		$AITimeSliceReassess += 300;
		if ($AITimeSliceReassess > 5000)
			$AITimeSliceReassess -= 5000;
	}

    // MD3: Init player
    %client.player.onSpawnInstance();

	//call the game specific spawn function
	Game.onAIRespawn(%client);
}

function AIProcessBuyInventory(%client)
{
	//get some vars
	%player = %client.player;
	if (!isObject(%player))
		return "Failed";

	%closestInv = %client.invToUse;
	%inventorySet = %client.invBuyList;
	%buyingSet = %client.buyingSet;

	//make sure it's still valid, enabled, and on our team
	if (! (%closestInv > 0 && isObject(%closestInv) &&
		(%closestInv.team <= 0 || %closestInv.team == %client.team) && %closestInv.isEnabled()))
	{
		//reset the state machine
		%client.buyInvTime = 0;
		return "InProgress";
	}

	//make sure the inventory station is not blocked
	%invLocation = %closestInv.getWorldBoxCenter();
   InitContainerRadiusSearch(%invLocation, 2, $TypeMasks::PlayerObjectType);
   %objSrch = containerSearchNext();
	if (%objSrch == %client.player)
		%objSrch = containerSearchNext();

	//the closestInv is busy...
	if (%objSrch > 0)
	{
		//have the AI range the inv
		if (%client.seekingInv $= "" || %client.seekingInv != %closestInv)
		{
			%client.invWaitTime = "";
			%client.seekingInv = %closestInv;
		   %client.stepRangeObject(%closestInv, "DefaultRepairBeam", 5, 10);
		}

		//inv is still busy - see if we're within range
		else if (%client.getStepStatus() $= "Finished")
		{
			//initialize the wait time
			if (%client.invWaitTime $= "")
				%client.invWaitTime = getSimTime() + 5000 + (getRandom() * 10000);

			//else see if we've waited long enough
			else if (getSimTime() > %client.invWaitTime)
			{
			   schedule(250, %client, "AIPlayAnimSound", %client, %objSrch.getWorldBoxCenter(), "vqk.move", -1, -1, 0);
				%client.invWaitTime = getSimTime() + 5000 + (getRandom() * 10000);
			}
		}
		else
		{
			//in case we got bumped, and are ranging the target again...
			%client.invWaitTime = "";
		}
	}

	//else if we've triggered the inv, automatically give us the equipment...
	else if (isObject(%closestInv) && isObject(%closestInv.trigger) && VectorDist(%closestInv.trigger.getWorldBoxCenter(), %player.getWorldBoxCenter()) < 1.5)
	{
		//first stop...
		%client.stop();

	   %index = 0;
		if (%buyingSet)
		{
			//first, clear the players inventory
			%player.clearInventory();
			%item = $AIEquipmentSet[%inventorySet, %index];
		}
		else
			%item = getWord(%inventorySet, %index);


		//armor must always be bought first
	   if (%item $= "Light" || %item $= "Medium" || %item $= "Heavy")
	   {
	      %player.setArmor(%item);

          // MD3: update bots here
          StatusEffect.stopAllEffects(%client.player);
          %client.player.onSpawnInstance();
          
	      %index++;
	   }

		//set the data block after the armor had been upgraded
      %playerDataBlock = %player.getDataBlock();

		//next, loop through the inventory set, and buy each item
		if (%buyingSet)
			%item = $AIEquipmentSet[%inventorySet, %index];
		else
			%item = getWord(%inventorySet, %index);
		while (%item !$= "")
		{
			//set the inventory amount to the maximum quantity available
			if (%player.getInventory(AmmoPack) > 0)
				%ammoPackQuantity = AmmoPack.max[%item];
			else
				%ammoPackQuantity = 0;

         %quantity = %player.getDataBlock().max[%item] + %ammoPackQuantity;
			if ($InvBanList[$CurrentMissionType, %item])
				%quantity = 0;
         %player.setInventory(%item, %quantity);

			//get the next item
			%index++;
			if (%buyingSet)
				%item = $AIEquipmentSet[%inventorySet, %index];
			else
				%item = getWord(%inventorySet, %index);
		}

		//put a weapon in the bot's hand...
		%player.cycleWeapon();

		//return a success
		return "Finished";
	}

	//else, keep moving towards the inv station
	else
	{
		if (isObject(%closestInv) && isObject(%closestInv.trigger))
		{
			//quite possibly we may need to deal with what happens if a bot doesn't have a path to the inv...
			//the current premise is that no inventory stations are "unpathable"...
			//if (%client.isSeekingInv)
			//{
			//   %dist = %client.getPathDistance(%closestInv.trigger.getWorldBoxCenter());
			//	if (%dist < 0)
			//		error("DEBUG Tinman - still need to handle bot stuck trying to get to an inv!");
			//}

			%client.stepMove(%closestInv.trigger.getWorldBoxCenter(), 1.5);
			%client.isSeekingInv = true;
		}
		return "InProgress";
	}
}

function AIChooseObjectWeapon(%client, %targetObject, %distToTarg, %mode, %canUseEnergyStr, %environmentStr)
{
   //get our inventory
   %player = %client.player;
	if (!isObject(%player))
		return;

	if (!isObject(%targetObject))
		return;

	%canUseEnergy = (%canUseEnergyStr $= "true");
	%inWater = (%environmentStr $= "water");
   %hasBlaster = (%player.getInventory("Blaster") > 0) && %canUseEnergy;
   %hasPlasma = (%player.getInventory("Plasma") > 0) && (%player.getInventory("PlasmaAmmo") > 0) && !%inWater;
   %hasChain = (%player.getInventory("Chaingun") > 0) && (%player.getInventory("ChaingunAmmo") > 0);
   %hasDisc = (%player.getInventory("Disc") > 0) && (%player.getInventory("DiscAmmo") > 0);
   %hasGrenade = (%player.getInventory("GrenadeLauncher") > 0) && (%player.getInventory("GrenadeLauncherAmmo") > 0);
   %hasMortar = (%player.getInventory("Mortar") > 0) && (%player.getInventory("MortarAmmo") > 0);
   %hasRepairPack = (%player.getInventory("RepairPack") > 0) && %canUseEnergy;
   %hasTargetingLaser = (%player.getInventory("TargetingLaser") > 0) && %canUseEnergy;
   %hasMissile = (%player.getInventory("MissileLauncher") > 0) && (%player.getInventory("MissileLauncherAmmo") > 0);

   //see if we're destroying the object
   if (%mode $= "Destroy")
   {
		if ((%targetObject.getDataBlock().getClassName() $= "TurretData" ||
			  %targetObject.getDataBlock().getName() $= "MineDeployed") && %distToTarg < 50)
		{
         if (%hasPlasma)
            %useWeapon = "Plasma";
         else if (%hasDisc)
            %useWeapon = "Disc";
         else if (%hasBlaster)
            %useWeapon = "Blaster";
         else if (%hasChain)
            %useWeapon = "Chaingun";
         else
            %useWeapon = "NoAmmo";
		}
      else if (%distToTarg < 40)
      {
         if (%hasPlasma)
            %useWeapon = "Plasma";
         else if (%hasChain)
            %useWeapon = "Chaingun";
         else if (%hasBlaster)
            %useWeapon = "Blaster";
         else if (%hasDisc)
            %useWeapon = "Disc";
         else
            %useWeapon = "NoAmmo";
      }
      else
         %useWeapon = "NoAmmo";
   }

   //else See if we're repairing the object
   else if (%mode $= "Repair")
   {
      if (%hasRepairPack)
         %useWeapon = "RepairPack";
      else
         %useWeapon = "NoAmmo";
   }

   //else see if we're lazing the object
   else if (%mode $= "Laze")
   {
      if (%hasTargetingLaser)
         %useWeapon = "TargetingLaser";
      else
         %useWeapon = "NoAmmo";
   }

   //else see if we're mortaring the object
   else if (%mode $= "Mortar")
   {
      if (%hasMortar)
         %useWeapon = "Mortar";
      else
         %useWeapon = "NoAmmo";
   }

   //else see if we're rocketing the object
   else if (%mode $= "Missile" || %mode $= "MissileNoLock")
   {
      if (%hasMissile)
         %useWeapon = "MissileLauncher";
      else
         %useWeapon = "NoAmmo";
   }

   //now select the weapon
   switch$ (%useWeapon)
   {
      case "Blaster":
         %client.player.use("Blaster");
         %client.setWeaponInfo("EnergyBolt", 25, 50, 1, 0.1);

      case "Plasma":
         %client.player.use("Plasma");
         %client.setWeaponInfo("PlasmaBolt", 25, 50);

      case "Chaingun":
         %client.player.use("Chaingun");
         %client.setWeaponInfo("ChaingunBullet", 30, 75, 150);

      case "Disc":
         %client.player.use("Disc");
         %client.setWeaponInfo("DiscProjectile", 30, 75);

      case "GrenadeLauncher":
         %client.player.use("GrenadeLauncher");
         %client.setWeaponInfo("AutoCannonHEShell", 40, 75);

      case "Mortar":
         %client.player.use("Mortar");
         %client.setWeaponInfo("MortarShot", 100, 350);

      case "RepairPack":
			if (%player.getImageState($BackpackSlot) $= "Idle")
	         %client.player.use("RepairPack");
         %client.setWeaponInfo("DefaultRepairBeam", 40, 75, 300, 0.1);

      case "TargetingLaser":
         %client.player.use("TargetingLaser");
         %client.setWeaponInfo("BasicTargeter", 20, 300, 300, 0.1);

      case "MissileLauncher":
         %client.player.use("MissileLauncher");
         %client.setWeaponInfo("ShoulderMissile", 80, 300);

      case "NoAmmo":
         %client.setWeaponInfo("NoAmmo", 30, 75);
   }
}
