//------------------------------------------------------------------------------
// Vehicle handling code
System.vehicleFrameUpdateMS = 3000;

// Special - 0-1
$VehicleTargeterSlot = 0;
$VehicleWeightSlot = 1;

// Secondary - 2-3

// Primary - 4-7

// function findVehiclePlayerSlot(%veh, %player)
function VehicleData::getMountObjectNode(%data, %obj)
{
    for(%i = 0; %i < 16; %i++)
    {
        %mObj = %veh.getMountNodeObject(%i);

        if(isObject(%mObj))
            if(%mObj == %obj)
                return %i;
    }

    return -1;
}

// Vehicle Missile handling code
datablock AudioProfile(LargeRocketFireSound)
{
     filename = "fx/Bonuses/high-level4-blazing";
     description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(LargeExplosionSound)
{
     filename = "fx/Bonuses/down_passback3_rocket.wav";
     description = AudioBIGExplosion3d;
   preload = true;
};

function ShapeBase::addMissile(%vehicle, %slot, %image, %deploy, %proj)
{
    if(%vehicle.missileCount $= "")
        %vehicle.missileCount = 0;

    if(%vehicle.missileReady[%slot] == true)
        return;
        
    %vehicle.missileReady[%slot] = true;
    %vehicle.missileDeploy[%slot] = %deploy;
    %vehicle.missileProj[%slot] = %proj;
    %vehicle.armedMissiles[%vehicle.missileCount] = %slot;
        %vehicle.missileCount++;
    
    if(%image !$= "")
        %vehicle.mountImage(%image, %slot);
        
    %vehicle.lastMissileFired = getSimTime();
}

function ShapeBase::launchMissile(%vehicle)
{
    %time = getSimTime();
    
    if(%vehicle.missileCount < 1)
    {
        %vehicle.playAudio(2, BomberBombDryFireSound);

        return;
    }
    else if(%time < %vehicle.missileTimeout)
    {
        %vehicle.playAudio(1, MissileReloadSound);

        return;
    }
    
    %slot = %vehicle.armedMissiles[%vehicle.missileCount - 1];
    %player = %vehicle.getMountNodeObject(0);
    %fwdVec = %vehicle.getForwardVector();
    %point = %vehicle.getMuzzlePoint(%slot);
    %p = createProjectile("BombProjectile", %vehicle.missileDeploy[%slot], %fwdVec, %point, %vehicle, %slot, %player);
    %vehicle.missileReady[%slot] = false;
    
    %target = %vehicle.getLockedTarget();
    %p.fwdDir = %fwdVec;

    if(%target)
        %p.target = %target;
    else if(%vehicle.isLocked())
        %p.lockedPos = %vehicle.getLockedPosition();

    %p.missileProj = %vehicle.missileProj[%slot];

    %p.getDatablock().schedule(800, fireMissile, %p);
    %vehicle.unmountImage(%slot);
    %vehicle.missileCount--;
    %vehicle.missileTimeout = %time + 6000;
    
    commandToClient(%player.client, 'setAmmoHudCount', "Missiles:" SPC %vehicle.missileCount);
    %vehicle.playAudio(0, BomberTurretDryFireSound);
}

function ProjectileData::fireMissile(%data, %proj)
{
    if(%data.bombOnly)
        return;
        
    %proj.play3D(LargeRocketFireSound);
    %proj.schedule(32, delete);

    %point = getFXPoint();
    %point.setPosition(%proj.position);

    if(isObject(%proj.target) || %proj.lockedPos !$= "")
       %m = createProjectile("SeekerProjectile", %proj.missileProj, %proj.fwdDir, %proj.position, %point, 0, %proj.instigator);
    else
       %m = createProjectile("LinearProjectile", %proj.missileProj@"Dumbfire", %proj.fwdDir, %proj.position, %point, 0, %proj.instigator);
       
    MissileSet.add(%m);

    %point.setTransform($g_FXOrigin);

    %m.vehicleObject    = %proj.vehicleObject;
    %m.damageBuffFactor = %proj.damageBuffFactor;

    if(isObject(%proj.target))
    {
        %m.target = %proj.target;
        %m.setObjectTarget(%proj.target);
    }
    else if(%proj.lockedPos !$= "")
        %m.setPositionTarget(%proj.lockedPos);
//    else
//        %m.setNoTarget();
}

function ShapeBaseImageData::onBayLaunch(%data,%obj,%slot,%proj)
{
    %projData = %proj.getDatablock();
    %target = %obj.getLockedTarget();
    %proj.fwdDir = %obj.getMuzzleVector(0);

    if(%target)
        %proj.target = %target;
    else if(%obj.isLocked())
        %proj.lockedPos = %vehicle.getLockedPosition();

    %proj.missileProj = %projData.missileNameBase;

    %projData.schedule(800, fireMissile, %proj);
}

datablock AudioProfile(DebrisFXExplosionSound)
{
   filename = "fx/explosions/deployables_explosion.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock DebrisData(HitDebrisFX)
{
   explodeOnMaxBounce = false;

   elasticity = 0.35;
   friction = 0.5;

   lifetime = 12.0;
   lifetimeVariance = 4.0;

   minSpinSpeed = 20;
   maxSpinSpeed = 800;

   numBounces = 5;
   bounceVariance = 3;

   staticOnMaxBounce = true;
   gravModifier = 1.0;

   useRadiusMass = true;
   baseRadius = 1;

   velocity = 14.0;
   velocityVariance = 7.0;
};

datablock ExplosionData(DebrisFXExplosion)
{
   soundProfile = DebrisFXExplosionSound;
   faceViewer = true;

   explosionShape = "effect_plasma_explosion.dts";
   sizes[0] = "0.2 0.2 0.2";
   sizes[1] = "0.3 0.3 0.3";
};

datablock StaticShapeData(DebrisExplosive) : StaticShapeDamageProfile
{
   shapeFile      = "turret_muzzlepoint.dts";
   explosion      = DebrisFXExplosion;

   maxDamage      = 1;
   destroyedLevel = 1;
   disabledLevel  = 1;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

   debrisShapeName = "debris_generic.dts";
   debris = HitDebrisFX;
};

datablock StaticShapeData(SmallDebrisExplosive) : StaticShapeDamageProfile
{
   shapeFile      = "turret_muzzlepoint.dts";
   explosion      = DebrisFXExplosion;

   maxDamage      = 1;
   destroyedLevel = 1;
   disabledLevel  = 1;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

   debrisShapeName = "debris_generic_small.dts";
   debris = HitDebrisFX;
};

function createHitDebris(%pos)
{
   %debris = getRandom(1) ? "DebrisExplosive" : "SmallDebrisExplosive";

   %d = new StaticShape()
   {
      dataBlock = %debris;
   };

   MissionCleanup.add(%d);

   %d.setPosition(%pos);
   %d.schedule(32, setDamageState, "Destroyed");
}

function VehicleData::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);
   
   if((%data.sensorData !$= "") && (%obj.getTarget() != -1))
      setTargetSensorData(%obj.getTarget(), %data.sensorData);

   if(%obj.disableMove)
      %obj.immobilized = true;

   if(%obj.deployed)
   {
      if($countDownStarted)
      {
         // spawning on mission load
         %data.installParts(%obj, 0);
         %data.schedule(($Host::WarmupTime * 1000) / 2, "vehicleDeploy", %obj, 0, 1);
      }
      else
      {
         $VehiclesDeploy[$NumVehiclesDeploy] = %obj;
         $NumVehiclesDeploy++;
      }
   }

   if(%obj.mountable || %obj.mountable $= "")
      %data.isMountable(%obj, true);
   else
      %data.isMountable(%obj, false);
      
    %data.schedule(32, "startReactor", %obj);
}

function VehicleData::startReactor(%data, %obj)
{
    %obj.setSelfPowered();
    %obj.setRechargeRate(%obj.rechargeTick);
    %obj.setEnergyLevel(%data.maxEnergy);
    
    %data.onTick(%obj);
}

function VehicleData::onRemove(%this, %obj)
{
   // if there are passengers/driver, kick them out
   for(%i = 0; %i < %obj.getDatablock().numMountPoints; %i++)
   {
      %pl = %obj.getMountNodeObject(%i);
      
      if(isObject(%pl) && %pl.isPlayer())
         %pl.unmount();
   }

   %this.deleteAllMounted(%obj);
   
   vehicleListRemove(%obj.getDataBlock(), %obj);
   
   if(%obj.lastPilot.lastVehicle == %obj)
      %obj.lastPilot.lastVehicle = "";

   if(isObject(%obj.frame))
      %obj.frame.delete();

   if(isObject(%obj.sensor))
      %obj.sensor.delete();
      
   if(isObject(%obj.shieldSource))
      %obj.shieldSource.delete();
      
   Parent::onRemove(%this, %obj);
}

function VehicleData::mountDriver(%data, %obj, %player)
{
   if(isObject(%obj) && %obj.getDamageState() !$= "Destroyed")
   {
      %player.startFade(1000, 0, true);
      schedule(1000, 0, "testVehicleForMount", %player, %obj);
      %player.schedule(1500,"startFade",1000, 0, false);
   }
}

function VehicleData::updateAmmoCount(%data, %obj, %client, %ammo)
{
    // the trick here is to go 3rd person and back, and then the ammo hud updates
     commandToClient(%client, 'setAmmoHudCount', %obj.ammoCache[%ammo]);
}

function TurretData::updateAmmoCount(%data, %obj, %client, %ammo)
{
    // keen: reticle ammo count not reflecting in turrets, use bottomprint
//    commandToClient(%client, 'setAmmoHudCount', %obj.getObjectMount().ammoCache[%ammo]);
    commandToClient(%client, 'BottomPrint', "Ammo:" SPC %obj.getObjectMount().ammoCache[%ammo], 2, 1);
}

function testVehicleForMount(%player, %obj)
{
   if(isObject(%obj) && %obj.getDamageState() !$= "Destroyed" && !%player.teleporting && !%player.client.inInv)
      %player.getDataBlock().onCollision(%player, %obj, 0);
}

// Use this to hide shield generators and/or any other statics
function VehicleData::playerDismounted(%data, %obj, %player)
{
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, true );

   setTargetSensorGroup(%obj.getTarget(), %obj.team);

   %player.setCloaked(false);

   // if there is a turret, set its team as well.
   if( %obj.turretObject > 0 )
      setTargetSensorGroup(%obj.turretObject.getTarget(), %obj.team);
}

// todo: modify so that it sets the damage level for all slots (0-15) if type ^ player
function VehicleData::onDamage(%this,%obj)
{
   %damage = %obj.getDamageLevel();
   if (%damage >= %this.destroyedLevel)
   {
      if(%obj.getDamageState() !$= "Destroyed")
      {
         if(%obj.respawnTime !$= "")
            %obj.marker.schedule = %obj.marker.data.schedule(%obj.respawnTime, "respawn", %obj.marker);
         %obj.setDamageState(Destroyed);
      }
   }
   else
   {
      if(%obj.getDamageState() !$= "Enabled")
         %obj.setDamageState(Enabled);
   }
}

function VehicleData::onDestroyed(%data, %obj, %prevState)
{
    if(%obj.lastDamagedBy)
    {
        %destroyer = %obj.lastDamagedBy;
        game.vehicleDestroyed(%obj, %destroyer);
    }

	radiusVehicleExplosion(%data, %obj);

   if(%obj.turretObject > 0)
      if(%obj.turretObject.getControllingClient())
         %obj.turretObject.getDataBlock().playerDismount(%obj.turretObject);
         
   for(%i = 0; %i < %obj.getDatablock().numMountPoints; %i++)
   {
      if (%obj.getMountNodeObject(%i)) {
         %flingee = %obj.getMountNodeObject(%i);
         %flingee.getDataBlock().doDismount(%flingee, true);
         %xVel = 250.0 - (getRandom() * 500.0);
         %yVel = 250.0 - (getRandom() * 500.0);
         %zVel = (getRandom() * 100.0) + 50.0;
         %flingVel = %xVel @ " " @ %yVel @ " " @ %zVel;
         %flingee.applyImpulse(%flingee.getTransform(), %flingVel);
//         echo("got player..." @ %flingee.getClassName());
         %flingee.damage(0, %obj.getPosition(), 125, $DamageType::Crash, 0, $DamageGroupMask::Kinetic);
      }
   }

   %data.deleteAllMounted(%obj);
   
   // keen: anti-vehicle run over fix
   %obj.setFrozenState(true);
   %obj.getDatablock().schedule(256, "hideAndSeek", %obj);
}

function VehicleData::hideAndSeek(%data, %obj)
{
    %obj.setPosition(VectorScale(vectorRand(), -10000));
    %obj.schedule(2048, "delete");
    
    // delete the other objects that might be hanging around on the vehicle
    for(%i = 0; %i < 16; %i++)
    {
        %something = %obj.getMountNodeObject(%i);

        if(isObject(%something))
            %something.schedule(1000, "delete");
    }
}

function VehicleData::deleteAllMounted(%obj)
{
    // was thinking this was a smart way to solve this problem, but noooo, T2 console spams...
//    for(%i = 0; %i < 16; %i++)
//    {
//        %thing = %obj.getMountNodeObject(%i);
        
//        if(isObject(%thing))
//        {
//            %thing.startFade(1, 0, true);
//            %thing.schedule(256, "delete");
//        }
//    }
}

function radiusVehicleExplosion(%data, %vehicle)
{
	// this is a modified version of RadiusExplosion() from projectiles.cs
	%position = %vehicle.getPosition();
   InitContainerRadiusSearch(%position, %data.explosionRadius, $TypeMasks::PlayerObjectType      |
                                                 $TypeMasks::VehicleObjectType     |
                                                 $TypeMasks::MoveableObjectType    |
                                                 $TypeMasks::StaticShapeObjectType |
                                                 $TypeMasks::ForceFieldObjectType  |
                                                 $TypeMasks::TurretObjectType      |
                                                 $TypeMasks::ItemObjectType);

   %numTargets = 0;
   while ((%targetObject = containerSearchNext()) != 0)
   {
		if(%targetObject == %vehicle)
			continue;

      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %data.explosionRadius)
         continue;

      if (%targetObject.isMounted())
      {
         %mount = %targetObject.getObjectMount();
			if(%mount == %vehicle)
				continue;

         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if (%mount.getDataBlock().isProtectedMountPoint[%found] && (%mount != %vehicle))
               continue;
         }
      }

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      %coverage = calcExplosionCoverage(%position, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType));
      if (%coverage == 0)
         continue;

      %amount = (1.0 - (%dist / %data.explosionRadius)) * %coverage * %data.explosionDamage * 100;
      %targetData = %targetObject.getDataBlock();

      %momVec = "0 0 1";

      if(%amount > 0)
         %targetData.damageObject(%targetObject, %sourceObject, %position, %amount, $DamageType::Explosion, %momVec, 0, 0, $DamageGroupMask::Explosive);
   }
}

//--------------------------------------------------------------
// NUMBER OF PURCHASEABLE VEHICLES PER TEAM
//--------------------------------------------------------------

$VehicleRespawnTime        = 15000;
$VehicleMax[ScoutVehicle]     = 4;
$VehicleMax[AssaultVehicle]   = 2;
$VehicleMax[MobileBaseVehicle]  = 2;
$VehicleMax[ScoutFlyer]       = 3;
$VehicleMax[BomberFlyer]      = 2;
$VehicleMax[HAPCFlyer]        = 2;
$VehicleMax[Stormguard]       = 2;
$VehicleMax[Outlaw]           = 2;
$VehicleMax[Bulldog]          = 2;
$VehicleMax[Fury]             = 2;

// todo: switch to automatic method
function clearVehicleCount(%team)
{
   $VehicleTotalCount[%team, ScoutVehicle]      = 0;
   $VehicleTotalCount[%team, AssaultVehicle]    = 0;
   $VehicleTotalCount[%team, MobileBaseVehicle] = 0;
   $VehicleTotalCount[%team, ScoutFlyer]        = 0;
   $VehicleTotalCount[%team, BomberFlyer]       = 0;
   $VehicleTotalCount[%team, HAPCFlyer]         = 0;
   $VehicleTotalCount[%team, Stormguard]        = 0;
   $VehicleTotalCount[%team, Outlaw]            = 0;
   $VehicleTotalCount[%team, Bulldog]           = 0;
   $VehicleTotalCount[%team, Fury]              = 0;
}

// todo: switch to bitmask method
function findEmptySeat(%vehicle, %player, %forceNode)
{
   %minNode = 1;
   %node = -1;
   %dataBlock = %vehicle.getDataBlock();
   %dis = %dataBlock.minMountDist;
   %playerPos = getWords(%player.getTransform(), 0, 2);
   %message = "";
   
   if(%player.getDatablock().armorFlags & $ArmorFlags::CanPilot)
   {
       if(%dataBlock.lightOnly)
       {
          if(%player.getDatablock().armorClass == $ArmorClassMask::Light)
             %minNode = 0;
          else
             %message = '\c2This vehicle can only be used by light frame armors.~wfx/misc/misc.error.wav';
       }
       else
          %minNode = 0;
   }
   else
      %minNode = findFirstHeavyNode(%dataBlock);
      
   if(%forceNode !$= "")
      %node = %forceNode;
   else
   {
      for(%i = 0; %i < %dataBlock.numMountPoints; %i++)
         if(!%vehicle.getMountNodeObject(%i))
         {
            %seatPos = getWords(%vehicle.getSlotTransform(%i), 0, 2);
            %disTemp = VectorLen(VectorSub(%seatPos, %playerPos));
            if(%disTemp <= %dis)
            {
               %node = %i;
               %dis = %disTemp;
            }
         }
    }
   if(%node != -1 && %node < %minNode)
   {
      if(%message $= "")
      {
         if(%node == 0)
            %message = '\c2Only Scout or Assault Armors can pilot this vehicle.~wfx/misc/misc.error.wav';
         else
            %message = '\c2Only Scout or Assault Armors can use that position.~wfx/misc/misc.error.wav';
      }

      if(!%player.noSitMessage)
      {
         %player.noSitMessage = true;
         %player.schedule(2000, "resetSitMessage");
         messageClient(%player.client, 'MsgArmorCantMountVehicle', %message);
      }
      %node = -1;
   }
   return %node;
}

function findFirstHeavyNode(%data)
{
   for(%i = 0; %i < %data.numMountPoints; %i++)
      if(%data.mountPose[%i] $= "")
         return %i;
   return %data.numMountPoints;
}

function VehicleData::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %theClient, %srcProj, %element)
{
   if(%element $= "")
      %element = $DamageGroupMask::Misc;

   %amount = mRound(%amount);
   
   if(%proj !$= "")
   {
      if(%amount > 0 && %targetObject.lastDamageProj !$= %proj)
      {
         %targetObject.lastDamageProj = %proj;
         %targetObject.lastDamageAmount = %amount;
      }
      else if(%targetObject.lastDamageAmount < %amount)
         %amount = %amount - %targetObject.lastDamageAmount;
      else
         return;
   }

   // check for team damage
   %sourceClient = %sourceObject ? %sourceObject.getOwnerClient() : 0;
   %targetTeam = getTargetSensorGroup(%targetObject.getTarget());

   if(%sourceClient)
      %sourceTeam = %sourceClient.getSensorGroup();
   else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   {
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      %sourceClient = %sourceObject.getControllingClient(); // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle with a controlled turret
   }
   else
   {
      %sourceTeam = %sourceObject ? getTargetSensorGroup(%sourceObject.getTarget()) : -1;
      // Client is allready defined and this spams console - ZOD
      //%sourceClient = %sourceObject.getControllingClient(); // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle from a vehicle
   }

    // vehicles no longer obey team damage -JR
//    if(!$teamDamage && (%targetTeam == %sourceTeam) && %targetObject.getDamagePercent() > 0.5)
//       return;
    //but we do want to track the destroyer
    if(%sourceObject)
    {
        %targetObject.lastDamagedBy = %sourceObject;
        %targetObject.lastDamageType = %damageType;
    }
    else
        %targetObject.lastDamagedBy = 0;

   // ----------------------------------------------------------------------------------
   // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle
   // keen: removed as hitsound alreads plays with reticle
//   if(%sourceClient && %sourceClient.vehicleHitSound)
//   {
//      if(%targetTeam != %sourceTeam)
//      {
//         if ((%damageType > 0  && %damageType < 11) ||
//             (%damageType == 13)                    ||
//             (%damageType > 15 && %damageType < 24) ||
//             (%damageType > 25 && %damageType < 32))
//         {
//            messageClient(%sourceClient, 'MsgClientHit', %sourceClient.vehicleHitWav);
//         }
//      }
//   }
   // ----------------------------------------------------------------------------------

   // Scale damage type & include shield calculations...
   %amount *= %targetObject.damageReduceFactor;
   %amount *= %data.damageScale[%damageType];
   %amount *= %targetObject.armorDamageFactor[%element];
   
   if(%targetObject.isShielded)
       %amount = %targetObject.shieldSource.getDatablock().onShieldDamage(%targetObject.shieldSource, %targetObject, %position, %amount, %damageType, %element); //installedVehiclePart[$VehiclePartType::Shield].onShieldDamage(%targetObject, %position, %amount, %damageType);

   if(%targetObject.damageReduction > 0 && %amount > 0 && %targetObject.hitPoints > 0)
   {
      if(%targetObject.hitPoints > %targetObject.damageReduction)
         %dr = %targetObject.hitPoints;

      if(%dr > 0)
      {
         if(%amount >= %dr)
            %amount -= %dr;
         else
            %amount = 0;
      }
   }
       
   if(%amount == 0)
      return;
      
    if(%targetObject.lastHitFlags & $Projectile::ArmorOnly)
        %amount *= 2;
      
   // compute delta HP damage
//   %hpDmgAmt = mCeil(%amount * 100);
   %targetObject.applyHPDamage(mRound(%amount));

   if(getRandom() < (0.2 + ((%targetObject.getDamageLevel() / %data.maxDamage) * 0.15)))
      createHitDebris(%position);

   if(%targetObject.getDamageState() $= "Destroyed" )
   {
      if( %momVec !$= "")
         %targetObject.setMomentumVector(%momVec);
   }
}

// Trigger rerouting to vehicle
function VehicleData::triggerPack(%data, %obj, %player)
{
//    %module = %obj.installedVehiclePart[$VehiclePartType::Module];

    if(%obj.installedModule == true)
    {
        %time = getSimTime();
        %module = %obj.installedVehiclePart[$VehiclePartType::Module];

        if(%obj.moduleTimeout[%module] > %time)
            return;

        if(%module.triggerType == $VMTrigger::Push)
            %module.triggerPush(%data, %obj, %player);
        else if(%module.triggerType == $VMTrigger::Toggle)
        {
            %obj.moduleTriggerState = !%obj.moduleTriggerState;
            
            %module.triggerToggle(%data, %obj, %player, %obj.moduleTriggerState);
        }
        
        %obj.moduleTimeout[%module] = %time + %module.cooldownTime;
    }
}

function VehicleData::triggerModuleOff(%data, %obj)
{
    if(%obj.installedModule == true)
    {
        %module = %obj.installedVehiclePart[$VehiclePartType::Module];

        if(%module.triggerType == $VMTrigger::Toggle)
        {
            %obj.moduleTriggerState = false;

            %module.triggerToggle(%data, %obj, %obj.getMountNodeObject(0), %obj.moduleTriggerState);
        }
    }
}

function VehicleData::turretTriggerPack(%data, %obj, %player, %turret)
{
//    echo("TurretTriggerPack" SPC %data SPC %obj SPC %player SPC %turret);
}

function VehicleData::triggerGrenade(%data, %vehicle, %player)
{
    %time = getSimTime();

    if(%time < %vehicle.nextRestockTime)
    {
        messageClient(%player.client, 'MsgVehNoReloadCD', '\c2Vehicle cannot restock yet, rearranging internal ammo cache.');
        return;
    }
    
    if(vectorLen(%vehicle.getVelocity()) > 5)
    {
        messageClient(%player.client, 'MsgVehRTooFast', '\c2Vehicle is moving too fast to restock, please land before attempting again.');
        return;
    }
    
    InitContainerRadiusSearch(%vehicle.getWorldBoxCenter(), 25, $TypeMasks::StaticShapeObjectType);

    while((%hit = containerSearchNext()) != 0)
    {
        if(%hit.team == %vehicle.team && %hit.getDatablock().shapeFile $= "vehicle_pad.dts")
        {
            // Reload vehicle
            %vehicle.play3d(MissileReloadSound);
            messageClient(%player.client, 'MsgVehReload', '\c2Vehicle landed on pad, ammo restocked.');

            for(%i = 0; %i < 5; %i++)
            {
                %part = %vehicle.installedVehiclePart[(%i + 3)];

                if(isObject(%part))
                    %part.reArm(%data, %vehicle, %player, %i);
            }

            %vehicle.nextRestockTime = %time + 120000;
            return;
        }
    }

    messageClient(%player.client, 'MsgVehNoReload', '\c2Vehicle cannot restock, not landed on a friendly vehicle pad.');
}

function VehicleData::triggerMine(%data, %obj, %player, %slot, %state)
{
    if(%state)
        return;
        
    if(%slot > 0 && %obj.invSystem == true)
    {
        %time = getSimTime();
        
        if(%time < %obj.slotInvTimeout[%slot])
        {
            %player.playAudio(0, "StationAccessDeniedSound");
            return;
        }

        %obj.slotInvTimeout[%slot] = %time + 45000;
        %player.lastWeapon = ( %player.getMountedImage($WeaponSlot) == 0 ) ? "" : %player.getMountedImage($WeaponSlot).item;
        
        %player.unmountImage($WeaponSlot);
        %player.playAudio(0, "MobileBaseInventoryActivateSound");
        %player.setCloaked(true);
        %player.schedule(500, "setCloaked", false);

        buyFavorites(%player.client, 0);				// second variable is heal trigger +soph
        messageClient(%player.client, 'MsgVehInvActivate', '\c2Inventory module has been activated. System cooling and will be ready in 45 seconds.');
    }
    else
        %data.useSpecialFunction(%obj, %player);
}

function VehicleData::useSpecialFunction(%data, %veh, %player)
{
    // do the mario here
}

function VehicleData::handleMineOption(%data, %obj, %player, %slot, %state)
{
     if( %obj.getDataBlock().vehicleType == $VehicleMask::Nightshade )		// special circumstances for nightshade
          %isNightShade = true;							// +soph
     if(%obj.moduleID == $VehicleModule::Inventory || %isNightshade )		// nightshade always has it +soph
     {
          if(isObject(%obj.getMountNodeObject(0)) && %slot != 0 )		// && %player != %obj.getMountNodeObject(0)) -soph
          {
               %time = getSimTime();

               if( %time > %player.lastVInvTime && %time > %obj.invSlotTimeout[%slot] )	// (%time > %player.lastVInvTime) -soph
               {
                    %player.lastWeapon = ( %player.getMountedImage($WeaponSlot) == 0 ) ? "" : %player.getMountedImage($WeaponSlot).item;
                    %player.unmountImage($WeaponSlot);
                    %player.play3D("StationInventoryActivateSound");
                    %player.setCloaked(true);
                    %player.schedule(500, "setCloaked", false);

                    buyFavorites( %player.client , 0 );				// second variable is heal trigger +soph

                   // %player.setEnergyLevel(%player.getDatablock().maxEnergy);	// -soph
                    %player.lastVInvTime = %time + 45000;			// %player.lastVInvTime = %time + 10000; -soph
                    resetInvPTimeout( %player );				// +soph
                    %obj.invSlotTimeout[%slot] = %isNightshade ? %time + 20000 : %time + 45000;
                    if( %state )
                         messageClient(%player.client, 'MsgVehInvModActivate', '\c2Inventory Module has provided your selected loadout.  System will be ready in %1 seconds.' , %isNightshade ? 20 : 45 ); // +soph
                    schedule(500, %player, vInvEffectStart, %player);
               }
               else
                    if( %state )						// +[soph]
                    {
                         if( %obj.invSlotTimeout[%slot] > %player.lastVInvTime )
                              messageClient(%player.client, 'MsgVehInvModFail', '\c2Vehicle Inventory Module not ready: please wait %1 seconds.', ( 1 + mFloor ( ( %obj.invSlotTimeout[%slot] - %time ) / 1000 ) ) ) ;
                         else
                              messageClient(%player.client, 'MsgVehInvModFail', '\c2Please wait %1 seconds before using this vehicle\'s Inventory Module.', ( 1 + mFloor ( ( %player.lastVInvTime - %time ) / 1000 ) ) ) ;
                    }								// +[/soph]
          }
     }
}

/// Vehicle HUD

function VehicleHud::updateHud(%obj, %client, %tag)
{
   %station = %client.player.station;
   %obj.vstag = %tag;
   %menu = %client.currentVehicleMenu;

   %station.vehicle[scoutVehicle] = true;
   %station.vehicle[scoutFlyer] = true;
   %station.vehicle[AssaultVehicle] = true;
   %station.vehicle[bomberFlyer] = true;
   %station.vehicle[hapcFlyer] = true;
   %station.vehicle[mobileBaseVehicle] = true;
   %station.vehicle[Outlaw] = true;
   %station.vehicle[Bulldog] = true;
   %station.vehicle[Fury] = true;
   %station.vehicle[Stormguard] = true;

   %team = %client.getSensorGroup();
   %count = 0;

   if(%menu == 1)
   {
      if (%station.vehicle[scoutVehicle])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Shadow Jetbike", "", ScoutVehicle, vehicleLeft("ScoutVehicle", %team) );
         %count++;
      }
      if (%station.vehicle[scoutFlyer])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Skycutter Interceptor", "", ScoutFlyer, vehicleLeft("ScoutFlyer", %team) );
         %count++;
      }
      if (%station.vehicle[mobileBaseVehicle])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Bombard Mobile Artillery", "", MobileBaseVehicle, vehicleLeft("MobileBaseVehicle", %team) );
         %count++;
      }
      if(%station.vehicle[Fury]) // todo: set up mechs
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Fury Scout Walker", "", Fury, vehicleLeft("Fury", %team) ) ;
         %count++;
      }
      if(%station.vehicle[Stormguard]) // todo: set up mechs
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Stormguard Assault Walker", "", Stormguard, vehicleLeft("Stormguard", %team) ) ;
         %count++;
      }
      
      messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", "B", 1 );
      %count++;
   }
   else if(%menu == 2)
   {
      if (%station.vehicle[Outlaw])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Outlaw Hovercar", "", Outlaw, vehicleLeft("Outlaw", %team) );
         %count++;
      }
      if (%station.vehicle[bomberFlyer])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Firestorm VTOL Escort", "", BomberFlyer, vehicleLeft("BomberFlyer", %team) );
         %count++;
      }
      if (%station.vehicle[Bulldog])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Bulldog VTOL APC", "", Bulldog, vehicleLeft("Bulldog", %team) );
         %count++;
      }
      if (%station.vehicle[hapcFlyer])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Imperator Sky Fortress", "", HAPCFlyer, vehicleLeft("HAPCFlyer", %team) );
         %count++;
      }
      if (%station.vehicle[AssaultVehicle])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Trebuchet HoverTank", "", AssaultVehicle, vehicleLeft("AssaultVehicle", %team) );
         %count++;
      }

      messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", "B", 1 );
      %count++;
   }
   else
   {
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Single Seat Vehicles", "", "F", 1337 );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Multi Seat Vehicles", "", "G", 1337 );
        %count++;
   }
   
   %station.lastCount = %count;
}

function vehicleLeft(%blockName, %team)
{
    return $VehicleMax[%blockName] - $VehicleTotalCount[%team, %blockName];
}

function vehicleCheck(%blockName, %team)
{
   if(vehicleLeft(%blockName, %team) > 0)
      return true;

   return false;
}

function actuallyBuyingVehicle(%client, %blockName)
{
    %wellAreYou = false;
    %menu = 0;
    
    switch$(%blockName)
    {
        case "B":
            %menu = 0;

        case "F":
            %menu = 1;

        case "G":
            %menu = 2;
            
        default:
            %wellAreYou = true;
    }

    commandToClient(%client, 'StationVehicleHideHud');
    %client.currentVehicleMenu = %menu;
    
    if(%wellAreYou)
        return true;
    
    commandToClient(%client, 'StationVehicleShowHud');
    
    return false;
}

function serverCmdBuyVehicle(%client, %blockName)
{
    if(!actuallyBuyingVehicle(%client, %blockName))
        return;
   
   %team = %client.getSensorGroup();
   if(vehicleCheck(%blockName, %team))
   {
      %st = %client.player.station;
      %station = %st.pad;
      
    if(!%st.selfPower)
    {
        if(%st.getPowerPct() < 0.8 && (%blockName.numMountPoints > 1 || %blockName.isWalker))
        {
            MessageClient(%client, "", "Large vehicle creation failed: Base power below 80%.~wfx/misc/misc.error.wav");
            return;
        }
    }

      if( (%station.ready) && (%station.station.vehicle[%blockName]) )
      {
         %trans = %station.getTransform();
         %pos = getWords(%trans, 0, 2);
         %matrix = VectorOrthoBasis(getWords(%trans, 3, 6));
         %yrot = getWords(%matrix, 3, 5);
         %p = vectorAdd(%pos,vectorScale(%yrot, -3));
         %p =  getWords(%p,0, 1) @ " " @ getWord(%p,2) + 4;

//         error(%blockName);
//         error(%blockName.spawnOffset);

         %p = vectorAdd(%p, %blockName.spawnOffset);
         %rot = getWords(%trans, 3, 5);
         %angle = getWord(%trans, 6) + 3.14;
         %mask = $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType |
                 $TypeMasks::StationObjectType | $TypeMasks::TurretObjectType;
	      InitContainerRadiusSearch(%p, %blockName.checkRadius, %mask);

	      %clear = 1;
         for (%x = 0; (%obj = containerSearchNext()) != 0; %x++)
         {
            if((%obj.getType() & $TypeMasks::VehicleObjectType) && (%obj.getDataBlock().checkIfPlayersMounted(%obj)))
            {
               %clear = 0;
               break;
            }
            else if(%obj.isWalker && isObject(%obj.pilotObject))
            {
                MessageClient(%client, "", 'Can\'t create vehicle. A player is on the creation pad.');
                return;
            }
            else
               %removeObjects[%x] = %obj;
         }
         if(%clear)
         {
            %fadeTime = 0;
            for(%i = 0; %i < %x; %i++)
            {
               if(%removeObjects[%i].isPlayer() && !%removeObjects[%i].isWalker)
               {
                  %pData = %removeObjects[%i].getDataBlock();
                  %pData.damageObject(%removeObjects[%i], 0, "0 0 0", 1000, $DamageType::VehicleSpawn);
               }
               else
               {
                  %removeObjects[%i].mountable = 0;
                  %removeObjects[%i].startFade( 1000, 0, true );
                  %removeObjects[%i].schedule(1001, "delete");
                  %fadeTime = 1500;
               }
            }
            schedule(%fadeTime, 0, "createVehicle", %client, %station, %blockName, %team , %p, %rot, %angle);
         }
         else
            MessageClient(%client, "", 'Can\'t create vehicle. A player is on the creation pad.');
      }
      //--------------------------------------------------------------------------------------
      // z0dd - ZOD, 4/25/02. client tried to quick purchase a vehicle that isn't on this map
      else
      {
      	 messageClient(%client, "", "~wfx/misc/misc.error.wav");
      }
      //--------------------------------------------------------------------------------------
   }
   //--------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 4/25/02. client tried to quick purchase vehicle when max vehicles of this type are already in use
   else
   {
      messageClient(%client, "", "~wfx/misc/misc.error.wav");
   }
   //--------------------------------------------------------------------------------------------------------------
}

function createVehicle(%client, %station, %blockName, %team, %pos, %rot, %angle)
{
   if($VehicleWalkerID[%blockName] > 0)
   {
      %station.ready = false;

      if(%client.player.lastVehicle)
      {
         %client.player.lastVehicle.lastPilot = "";
         vehicleAbandonTimeOut(%client.player.lastVehicle);
         %client.player.lastVehicle = "";
      }
      
      spawnWalker(%client, %blockName, %pos SPC %rot SPC %angle);

      %station.playThread($ActivateThread,"activate2");
      %station.playAudio($ActivateSound, ActivateVehiclePadSound);

      // play the FX
      %fx = new StationFXVehicle()
      {
         dataBlock = VehicleInvFX;
         stationObject = %station;
      };
   }
   else if((%obj = %blockName.create(%team)))
   {
      //-----------------------------------------------
      // z0dd - ZOD, 4/25/02. MPB Teleporter.
      // keen: repurpose this for portal exit point
//      if ( %blockName $= "MobileBaseVehicle" )
//      {
//         %station.station.teleporter.MPB = %obj;
//         %obj.teleporter = %station.station.teleporter;
//      }
      //-----------------------------------------------
      %station.ready = false;
      %obj.team = %team;
      %obj.useCreateHeight(true);
      %obj.schedule(5500, "useCreateHeight", false);
      %obj.getDataBlock().isMountable(%obj, false);
      %obj.getDataBlock().schedule(6500, "isMountable", %obj, true);

      %station.playThread($ActivateThread,"activate2");
      %station.playAudio($ActivateSound, ActivateVehiclePadSound);

      vehicleListAdd(%blockName, %obj);
      MissionCleanup.add(%obj);

      %turret = %obj.getMountNodeObject(10);
      if(%turret > 0)
      {
         %turret.setCloaked(true);
         %turret.schedule(4800, "setCloaked", false);
      }

      %obj.setCloaked(true);
      %obj.setTransform(%pos @ " " @ %rot @ " " @ %angle);

      %obj.schedule(3700, "playAudio", 0, VehicleAppearSound);
      %obj.schedule(4800, "setCloaked", false);

      if(%client.player.lastVehicle)
      {
         %client.player.lastVehicle.lastPilot = "";
         vehicleAbandonTimeOut(%client.player.lastVehicle);
         %client.player.lastVehicle = "";
      }
      %client.player.lastVehicle = %obj;
      %obj.lastPilot = %client.player;

      // play the FX
      %fx = new StationFXVehicle()
      {
         dataBlock = VehicleInvFX;
         stationObject = %station;
      };

      if ( %client.isVehicleTeleportEnabled() )
         %obj.getDataBlock().schedule(5000, "mountDriver", %obj, %client.player);
         
       %blockName.installParts(%obj, %client.player);
       
       if(%obj.getTarget() != -1)
          setTargetSensorGroup(%obj.getTarget(), %client.getSensorGroup());
   }
}

function vehicleAbandonTimeOut(%vehicle)
{
   if(%vehicle.getDatablock().cantAbandon $= "" && %vehicle.lastPilot $= "")
   {
      if(%vehicle.isWalker)
      {
        // don't fade if someone else captured it
        if(isObject(%vehicle.pilotObject))
            return;
            
        $VehicleTotalCount[%vehicle.originalteam, %vehicle.shortName]--;
        %vehicle.mountable = 0;
        %vehicle.startFade(1000, 0, true);
        %vehicle.schedule(1001, "delete");
        return;
      }

      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
         if (%vehicle.getMountNodeObject(%i))
         {
            %passenger = %vehicle.getMountNodeObject(%i);
            if(%passenger.lastVehicle !$= "")
               schedule(15000, %passenger.lastVehicle,"vehicleAbandonTimeOut", %passenger.lastVehicle);
            %passenger.lastVehicle = %vehicle;
            %vehicle.lastPilot = %passenger;
            return;
         }

      if(%vehicle.respawnTime !$= "")
         %vehicle.marker.schedule = %vehicle.marker.data.schedule(%vehicle.respawnTime, "respawn", %vehicle.marker);
      %vehicle.mountable = 0;
      %vehicle.startFade(1000, 0, true);
      %vehicle.schedule(1001, "delete");
   }
}

function clearVehicleCount(%team)
{
   $VehicleTotalCount[%team, ScoutVehicle]      = 0;
   $VehicleTotalCount[%team, AssaultVehicle]    = 0;
   $VehicleTotalCount[%team, MobileBaseVehicle] = 0;
   $VehicleTotalCount[%team, ScoutFlyer]        = 0;
   $VehicleTotalCount[%team, BomberFlyer]       = 0;
   $VehicleTotalCount[%team, HAPCFlyer]         = 0;
   $VehicleTotalCount[%team, StormguardWalker]  = 0;
   $VehicleTotalCount[%team, Outlaw]            = 0;
   $VehicleTotalCount[%team, Bulldog]           = 0;
   $VehicleTotalCount[%team, Fury]              = 0;
   $VehicleTotalCount[%team, Stormguard]        = 0;
}

// Used on maps that alter the count of vehicles - default vehicles only
function refreshVehicleCounts()
{
     if($VehicleMax[ScoutVehicle] > 4)
        $VehicleMax[ScoutVehicle] = 4;

     if($VehicleTotalCount[1, ScoutVehicle] < 0)
        $VehicleTotalCount[1, ScoutVehicle] = 0;

     if($VehicleTotalCount[2, ScoutVehicle] < 0)
        $VehicleTotalCount[2, ScoutVehicle] = 0;

     if($VehicleMax[AssaultVehicle] > 2)
        $VehicleMax[AssaultVehicle] = 2;

     if($VehicleTotalCount[1, AssaultVehicle] < 0)
        $VehicleTotalCount[1, AssaultVehicle] = 0;

     if($VehicleTotalCount[2, AssaultVehicle] < 0)
        $VehicleTotalCount[2, AssaultVehicle] = 0;

     if($VehicleMax[MobileBaseVehicle] > 1)
        $VehicleMax[MobileBaseVehicle] = 1;

     if($VehicleTotalCount[1, MobileBaseVehicle] < 0)
        $VehicleTotalCount[1, MobileBaseVehicle] = 0;

     if($VehicleTotalCount[2, MobileBaseVehicle] < 0)
        $VehicleTotalCount[2, MobileBaseVehicle] = 0;

     if($VehicleMax[ScoutFlyer] > 3)
        $VehicleMax[ScoutFlyer] = 3;

     if($VehicleTotalCount[1, ScoutFlyer] < 0)
        $VehicleTotalCount[1, ScoutFlyer] = 0;

     if($VehicleTotalCount[2, ScoutFlyer] < 0)
        $VehicleTotalCount[2, ScoutFlyer] = 0;

     if($VehicleMax[BomberFlyer] > 2)
        $VehicleMax[BomberFlyer] = 2;

     if($VehicleTotalCount[1, BomberFlyer] < 0)
        $VehicleTotalCount[1, BomberFlyer] = 0;

     if($VehicleTotalCount[2, BomberFlyer] < 0)
        $VehicleTotalCount[2, BomberFlyer] = 0;

     if($VehicleMax[HAPCFlyer] > 2)
        $VehicleMax[HAPCFlyer] = 2;

     if($VehicleTotalCount[1, HAPCFlyer] < 0)
        $VehicleTotalCount[1, HAPCFlyer] = 0;

     if($VehicleTotalCount[2, HAPCFlyer] < 0)
        $VehicleTotalCount[2, HAPCFlyer] = 0;
}

function serverCmdSetVehicleWeapon(%client, %num)
{
    %obj = %client.player.getControlObject();
    %data = %obj.getDataBlock();
    %bIsVehicle = %obj.isVehicle();
    
    if(%data.numWeapons < %num)
        return;

    %obj.selectedWeapon = %num;
    %obj.selectedWeaponObject = "";
//    %vehicle = %bIsVehicle ? %obj : %obj.getObjectMount();
    %wep = %obj.weapon[%num];
    
    if(isObject(%wep))
    {
        %obj.selectedWeaponObject = %wep;
        %wep.onSwitchWeapon(%obj, %client, %num);
        
        if(%wep.usesAmmo)
            %data.schedule(500, "updateAmmoCount", %obj, %client, %wep.ammo);
    }
          
    if(!%bIsVehicle)
    {
        %client.setVWeaponsHudActive(%num);
        %client.setWeaponsHudActive("Blaster");
        
        if(%obj.selectedWeaponObject !$= "")
            commandToClient(%client, 'BottomPrint', %wep.name NL %wep.description, 5, 2);
    }

    // set the active image on the client's obj
    %image = %num * 2;
    %client.setObjectActiveImage(%obj, %image);
    
    // if firing then set the proper image trigger
    if(%obj.fireTrigger)
    {
        %obj.setImageTrigger(2, false);
        %obj.setImageTrigger(4, false);
        %obj.setImageTrigger(6, false);

        if(%obj.getImageTrigger(6))
            ShapeBaseImageData::deconstruct(%obj.getMountedImage(6), %obj);
            
        %obj.setImageTrigger(%image, true);
    }
}

function VehicleData::onTick(%data, %obj)
{
    if(!isObject(%obj))
        return false;
        
    if(%obj.getRepairRate() > 0)
        %obj.updateHPLevel();

//    if(%obj.tickFunction !$= "")
//        call(%obj.tickFunction, %this, %obj);

    if(%data.updateFrames)
    {
        %time = getSimTime();

        if(%time > %obj.nextFrameUpdate)
        {
            %obj.nextFrameUpdate = %time + System.vehicleFrameUpdateMS;
            %data.processFrameUpdate(%obj);
        }
    }

    if(%data.checkMinVelocity > 0)
    {
        %velVec = %obj.getVelocity();
        %vel = vectorLen(%velVec);
        %fvel = vectorLen(getWords(%velVec, 0, 1) SPC "0");
        %nVec = "0 0 0";
        %doImpulse = false;
        %doStall = false;
        
        if(%data.isMovingBackwards(%obj))
        {
            if(%vel > %data.checkMinReverseSpeed)
            {
                %force = %vel + (%obj.totalWeight * 3.5);
                %nVec = vectorScale(%obj.getForwardVector(), %force);
                %doImpulse = true;
            }
        }

        if(%vel < %data.checkMinVelocity)
        {
            if(!%data.findGround(%obj))
            {
                %force = %vel + (%obj.totalWeight * 3.5);
                %nVec = vectorScale("0 0 -1", %force);
                %doImpulse = true;
                %doStall = true;
            }
        }

        if(%doImpulse)
            %obj.applyImpulse(%obj.getTransform(), %nVec);
            
        %obj.stallState = %doStall;
    }
    
    %pilot = %obj.getMountNodeObject(0);

    if(isObject(%pilot) && (%obj.tickCount % 8 == 0))
        %data.updatePilotStatus(%obj, %pilot);
        
    %pct = %obj.getDamagePct();
    
    if(%pct >= 0.7 && (%obj.tickCount % 48 == 0))
        %obj.playAudio(3, EngineAlertSound);
        
    if(%pct >= 0.8 && (%obj.tickCount % 12 == 0))
    {
        if(getRandom() > 0.9)
        {
            %vel = vectorLen(%obj.getVelocity());
            %force = %vel + (%pct * 500);
            %stVec = VectorRand();
            %nVec = vectorScale(%stVec, %force);
            %obj.applyImpulse(%obj.getTransform(), %nVec);
            
            %dmgFXPos = vectorProject(%obj.position, %stVec, 1.5);
            createHitDebris(%dmgFXPos);
        }
    }

    %obj.tickCount++;
    %data.schedule($g_TickTime, "onTick", %obj);

    return true;
}

function VehicleData::processFrameUpdate(%data, %obj)
{
   if(isObject(%obj.frame))
   {
      %obj.mountObject(%obj.frame, %data.frameMountPoint);
      %obj.frame.setDamageLevel(%obj.getDamageLevel());
   }
}

function VehicleData::findGround(%data, %obj)
{
   %dist = %data.checkMinHeight * -1;
   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType | $TypeMasks::WaterObjectType;
   %pos = %obj.position;
   %end = VectorAdd(%pos, "0 0" SPC %dist);
   %result = containerRayCast(%pos, %end, %mask, %obj);
   %target = getWord(%result, 0);

   if(isObject(%target))
      return true;
   else
      return false;
}

function VehicleData::isMovingBackwards(%data, %obj)
{
    %forwardVec = vectorNormalize(%obj.getVelocity());
//    %objPos = %obj.position;
    %dif = %obj.getForwardVector();
    %dot = VectorDot(%dif, %forwardVec);

    if(%dot >= mCos(1.05))
        return false;
    else
        return true;
}

function throwVehicleFlare(%obj, %vehicle, %flare)
{
     if(%obj.getInventory(%flare))
        %obj.decInventory(%flare, 1);
     else
        return false;

      %p = new FlareProjectile()
      {
         dataBlock        = FlareGrenadeProj;
         initialDirection = "0 0 -1";
         initialPosition  = %vehicle.position;
         sourceObject     = %obj;
         sourceSlot       = 0;
      };
      
      FlareSet.add(%p);
      MissionCleanup.add(%p);
      
      %obj.play3d(GrenadeThrowSound);
      %p.schedule(6000, "delete");
      
      // miscellaneous grenade-throwing cleanup stuff
      %obj.lastThrowTime[%data] = getSimTime();
      %obj.throwStrength = 0;

      return true;
}

datablock AudioProfile(EngineAlertSound)
{
   filename    = "gui/vote_nopass.WAV";
   description = AudioExplosion3d;
   preload = true;
};

datablock TurretImageData(SeekingParamImage) // ShapeBaseImageData
{
   mountPoint = 4;
   offset = "0 0.1 -0.3";
   rotation = "0 1 0 180";
   shapeFile = "turret_muzzlepoint.dts";

   isSeeker     = true;
   seekRadius   = 250;
   maxSeekAngle = 30;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 15;
   
   // Turret parameters
   activationMS      = 1000;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 200;
   degPerSecTheta    = 720;
   degPerSecPhi      = 720;
};

function VehicleData::onPilotSeated(%data, %obj, %player)
{
   if(%obj.selectedWeapon $= "")
      %obj.selectedWeapon = 1;

   %wep = %obj.weapon[%obj.selectedWeapon];

   if(!isObject(%wep))
       return;

   %client = %obj.getMountNodeObject(0).client;

   if(isObject(%wep) && %wep.name !$= "")
   {
//        commandToClient(%client, 'BottomPrint', %wep.name NL %wep.description, 4, 2);

        if(%wep.ammo !$= "")
            %data.updateAmmoCount(%obj, %client, %wep.ammo);
   }

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function VehicleData::onGunnerSeated(%data, %obj, %player, %turretNode)
{
    %turret = %obj.getMountNodeObject(%turretNode);
    %player.vehicleTurret = %turret;
    %player.setTransform("0 0 0 0 0 1 0");
    %player.lastWeapon = %player.getMountedImage($WeaponSlot);
    %player.unmountImage($WeaponSlot);

    if(!%player.client.isAIControlled())
    {
        %player.setControlObject(%turret);
        %player.client.setObjectActiveImage(%turret, 0);
    }
    %turret.turreteer = %player;
    %turret.owner = %player.client;

    commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
    commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);
    commandToClient(%player.client, 'setRepairReticle');

    %data.updateGunnerStatus(%obj, %player, %turretNode);
}

function VehicleData::updateGunnerStatus(%data, %obj, %player, %turretNode)
{
    %turret = %obj.getMountNodeObject(%turretNode);

    if(%turret.selectedWeapon $= "")
        %turret.selectedWeapon = 1;

    %wep = %turret.weapon[%turret.selectedWeapon];
    %weapon = "None";

    if(isObject(%wep) && %wep.name !$= "")
        %weapon = %wep.name;

    commandToClient(%player.client, 'BottomPrint', "Now using" SPC %wep.name, 5, 1);
}

function VehicleData::updatePilotStatus(%data, %obj, %pilot)
{
    %hpPct = %obj.getDamageLeftPct();
    %shieldPct = %obj.shieldSource.strength / %obj.shieldSource.maxStrength;
    %hpGraph = createColorGraph("|", 20, %hpPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6);
    %shGraph = createColorGraph("|", 20, %shieldPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6);

    if(%obj.selectedWeapon $= "")
        %obj.selectedWeapon = 1;

    %wep = %obj.weapon[%obj.selectedWeapon];
    %weapon = "None";
    %hasWep = false;
    
    if(isObject(%wep) && %wep.name !$= "")
    {
        %weapon = %wep.name;
        %hasWep = true;
    }

//     %module = %obj.moduleInstalled ? %obj.moduleName : "N/A";

//     if(%client.hasMD2Client)
//          commandToClient(%client, 'GraphVehHUD', %hpPct, %shGraph, %weapon);
//     else

    if(%obj.stallState)
        commandToClient(%pilot.client, 'BottomPrint', "Hull: "@%hpGraph@" ("@mCeil(%hpPct * 100)@"%) | Shield: "@%shGraph@" ("@mCeil(%shieldPct * 100)@"%)\nCurrent Weapon: "@%weapon NL "<color:FF6633>>> STALLING, INCREASE SPEED <<", 1, 3);
    else if(%hasWep && %obj.getMountedImage(0).pilotHeadTracking)
        commandToClient(%pilot.client, 'BottomPrint', "Hull: "@%hpGraph@" ("@mCeil(%hpPct * 100)@"%) | Shield: "@%shGraph@" ("@mCeil(%shieldPct * 100)@"%)\nCurrent Weapon [Mouselook]: "@%weapon, 1, 2);
    else
        commandToClient(%pilot.client, 'BottomPrint', "Hull: "@%hpGraph@" ("@mCeil(%hpPct * 100)@"%) | Shield: "@%shGraph@" ("@mCeil(%shieldPct * 100)@"%)\nCurrent Weapon: "@%weapon, 1, 2);
        
    %ammoStr = "Ammo:";

    for(%i = 0; %i < %obj.wepCount; %i++)
    {
        %ammo = %obj.ammoCache[%obj.weapon[(%i + 1)].ammo];

        if(%ammo $= "")
            %ammo = 0;

        %ammoStr = %ammoStr SPC (%i + 1) @ ":" SPC %ammo;
    }

    messageClient(%pilot.client, 'MsgSPCurrentObjective2', "", %ammoStr);
}

function VehicleData::installTurret(%data, %obj, %turretData, %slot)
{
   %turret = TurretData::create(%turretData);
   %turret.selectedWeapon = 1;
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %turret.schedule(5500, "setCloaked", false);
   %turret.vehicle = %obj;
   MissionCleanup.add(%turret);

   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %obj.mountObject(%turret, %slot);

   return %turret;
}

datablock TurretImageData(UBTurretController)
{
   shapeFile            = "turret_muzzlepoint.dts";
   mountPoint           = 3;

   projectile           = MBlasterAABolt;

   // Turret parameters
   activationMS         = 1000;
   deactivateDelayMS    = 1500;
   thinkTimeMS          = 200;
   degPerSecTheta       = 500;
   degPerSecPhi         = 800;

   attackRadius         = 75;

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;

   minEnergy = 0;
   fireEnergy = 0;

   stateName[0]                     = "Activate";
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "Ready";

   stateName[1]                     = "Ready";
   stateTransitionOnTriggerDown[1]  = "Trigger";

   stateName[2]                     = "Trigger";
   stateAllowImageChange[2]         = false;
   stateScript[2]                   = "onTriggerDown";
   stateTransitionOnTriggerUp[2]    = "Release";

   stateName[3]                     = "Release";
   stateScript[3]                   = "onTriggerUp";
   stateTransitionOnTimeout[3]      = "Ready";
};

function UBTurretController::onTriggerDown(%data, %obj, %slot)
{
    %data.triggerLoop(%obj, %slot);
}

function UBTurretController::onTriggerUp(%data, %obj, %slot)
{

}

function UBTurretController::triggerLoop(%data, %obj, %slot)
{
    if(!%obj.fireTrigger)
    {
        cancel(%obj.turretFireThread);
        return;
    }

    if(isEventPending(%obj.turretFireThread))
        cancel(%obj.turretFireThread);

    %time = getSimTime();
    %nextFireTime = 32;

    if(%time >= %obj.nextFireTime)
    {
        if(%obj.selectedWeapon == 1)
        {
            %obj.barrelFlip1 = %obj.barrelFlip1 == 2 ? 3 : 2;
            %nextFireTime = %obj.getMountedImage(%obj.barrelFlip1).fireTimeout;

            %obj.setImageTrigger(%obj.barrelFlip1, true);
            %obj.schedule(32, "setImageTrigger", %obj.barrelFlip1, false);
        }
        else if(%obj.selectedWeapon == 2)
        {
            %obj.barrelFlip2 = %obj.barrelFlip2 == 4 ? 5 : 4;
            %nextFireTime = %obj.getMountedImage(%obj.barrelFlip2).fireTimeout;

            %obj.setImageTrigger(%obj.barrelFlip2, true);
            %obj.schedule(32, "setImageTrigger", %obj.barrelFlip2, false);
        }
        else
        {
            %obj.setImageTrigger(2, false);
            %obj.setImageTrigger(3, false);
            %obj.setImageTrigger(4, false);
            %obj.setImageTrigger(5, false);

            %nextFireTime = 500;
        }

        %obj.nextFireTime = %time + %nextFireTime;
    }

    %obj.turretFireThread = %data.schedule(%nextFireTime, "triggerLoop", %obj, %slot);
}

// Skycutter Interceptor
function ScoutFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %frame = new StaticShape()
   {
          dataBlock = SkycutterFrame;
          scale = "0.999998 0.999998 0.999998";
   };
   %obj.mountObject(%frame, 7);
   MissionCleanup.add(%frame);

   %frame.parent = %obj;
   %obj.frame = %frame;
   %frame.team = %obj.teamBought;
   %obj.lastRemountTime = getSimTime();
   
   setTargetSensorGroup(%frame.getTarget(), %frame.team);
   setTargetNeverVisMask(%frame.getTarget(), 0xffffffff);

   %obj.frame.mountImage(SkycutterWingsImage, 0);
   %obj.frame.mountImage(SkycutterEngineImage, 1);
   %obj.frame.setCloaked(true);
   %obj.frame.schedule(5000, setCloaked, false);
   
//   %obj.mountImage(SkycutterWingsImage, 1);
   %obj.schedule(4500, "playThread", $ActivateThread, "activate");
}

function ScoutFlyer::onTrigger(%data, %obj, %trigger, %state)
{
//    error("ScoutFlyer::onTrigger" SPC %data SPC %obj SPC %trigger);
    
    if(%trigger == 0)
    {
        if(%obj.selectedWeapon == 1)
        {
            %wep = %obj.installedVehiclePart[3];
            
            if(isObject(%wep) && %wep.firesMissiles !$= "")
            {
                %obj.launchMissile();
                %state = 0;
            }

            %obj.setImageTrigger(4, %state);
            %obj.setImageTrigger(5, %state);
            %obj.setImageTrigger(6, %state);
            %obj.setImageTrigger(7, %state);
        }
        else
        {
            %wep = %obj.installedVehiclePart[4];

            if(isObject(%wep) && %wep.firesMissiles !$= "")
            {
                %obj.launchMissile();
                %state = 0;
            }
            
            %obj.setImageTrigger(2, %state);
            %obj.setImageTrigger(3, %state);
        }
    }
    else if(%trigger == 4 && %state)
        %data.triggerGrenade(%obj, %obj.getMountNodeObject(0));
    else if(%trigger == 5)
        %data.triggerMine(%obj, %obj.getMountNodeObject(0), 0, %state);
}

function ScoutFlyer::playerMounted(%data, %obj, %player, %node)
{
   // scout flyer == SUV (single-user vehicle)
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Shrike", %node);
   commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
   
   %data.onPilotSeated(%obj, %player);
      
   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function ScoutFlyer::playerDismounted(%data, %obj, %player)
{
   %obj.fireWeapon = false;

   for(%i = 0; %i < 8; %i++)
      %obj.setImageTrigger(%i, false);

   setTargetSensorGroup(%obj.getTarget(), %obj.team);

   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, true );
}

// Disabled for now: weird two-animation issue
function ScoutFlyer::onTick(%data, %obj)
{
    if(Parent::onTick(%data, %obj) && isObject(%obj.frame))
    {
        %speed = vectorLen(%obj.getVelocity());

        if(%speed > 15)
            %obj.frame.setImageTrigger(1, true);
        else
            %obj.frame.setImageTrigger(1, false);

        if(getSimTime() > %obj.lastRemountTime)
        {
            %obj.mountObject(%obj.frame, 7);
            %obj.lastRemountTime += 10000;
        }
    }
}

function ScoutVehicle::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   
   %obj.mountImage(HBEngineParam, 2);
   %obj.mountImage(HBDiscImage, 4);
   %obj.mountImage(HBDecal1, 5);
   %obj.mountImage(HBDecal2, 6);
   %obj.mountImage(HBEngine1, 3);
   %obj.mountImage(HBEngine2, 7);
   
   %obj.schedule(5500, "playThread", $ActivateThread, "activate");
}

function ScoutVehicle::onTick(%data, %obj)
{
    if(Parent::onTick(%data, %obj))
    {
        %speed = vectorLen(%obj.getVelocity());
        
        if(%speed > 50)
        {
            %obj.setImageTrigger(3, true);
            %obj.setImageTrigger(7, true);
        }
        else
        {
            %obj.setImageTrigger(3, false);
            %obj.setImageTrigger(7, false);
        }
    }
}

function ScoutVehicle::onTrigger(%data, %obj, %trigger, %state)
{
    if(%trigger == 0)
    {
        %wep = %obj.installedVehiclePart[3];

        if(isObject(%wep) && %wep.firesMissiles !$= "")
        {
            %obj.launchMissile();
            %state = 0;
        }
        
        %obj.setImageTrigger(0, %state);
    }
    else if(%trigger == 4 && %state)
        %data.triggerGrenade(%obj, %obj.getMountNodeObject(0));
    else if(%trigger == 5)
        %data.triggerMine(%obj, %obj.getMountNodeObject(0), 0, %state);
//    if(%trigger == 3)
//        %obj.setImageTrigger(2, %state);
}

function ScoutVehicle::playerMounted(%data, %obj, %player, %node)
{
   // scout vehicle == SUV (single-user vehicle)
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Hoverbike", %node);

   // z0dd - ZOD, 5/14/02. Create a weapon hud and reticle
   commandToClient(%player.client, 'ShowVehicleWeapons', "Hoverbike");

   %data.onPilotSeated(%obj, %player);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function ScoutVehicle::playerDismounted(%data, %obj, %player)
{
   %obj.setImageTrigger(0, false);
   %obj.setImageTrigger(2, false);
   %player.setCloaked(false);
   
   setTargetSensorGroup(%obj.getTarget(), %obj.team);
   
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, true );
}

function HAPCFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   
   %obj.mountImage(HavocPilotSection, 7);
   %obj.turretR = %this.installTurret(%obj, "ImperatorTurret", 2);
   %obj.turretL = %this.installTurret(%obj, "ImperatorTurret", 5);
   %obj.gunshipBeacon = 0;

   %sensor = new StaticShape()
   {
       dataBlock = ImperatorSensor;
       scale = "1 1 1";
   };
   
   %obj.mountObject(%sensor, 1);
   MissionCleanup.add(%sensor);

   %sensor.parent = %obj;
   %obj.sensor = %sensor;
   %sensor.team = %obj.teamBought;

   setTargetSensorGroup(%sensor.getTarget(), %sensor.team);
   setTargetNeverVisMask(%sensor.getTarget(), 0xffffffff);

   %obj.sensor.setCloaked(true);
   %obj.sensor.schedule(4500, "playThread", $ActivateThread, "ambient");
   %obj.sensor.schedule(5000, setCloaked, false);
//   %obj.schedule(6000, "playThread", $ActivateThread, "activate");

//    echo(%obj SPC "imperator active");
}

function HAPCFlyer::onTick(%data, %obj)
{
    if(Parent::onTick(%data, %obj))
    {
        %speed = vectorLen(%obj.getVelocity());

        if(%speed > 12)
            %obj.setImageTrigger(7, true);
        else
            %obj.setImageTrigger(7, false);
    }
}

function HAPCFlyer::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);
//      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
      commandToClient(%player.client, 'setRepairReticle');
      
      setTargetSensorGroup(%obj.getMountNodeObject(2).getTarget(), %player.team);
      setTargetSensorGroup(%obj.getMountNodeObject(5).getTarget(), %player.team);
   }
   else if(%node == 3)
      %data.onGunnerSeated(%obj, %player, 2);
   else if(%node == 4)
      %data.onGunnerSeated(%obj, %player, 5);
   else
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "HAPC", %node);
   
   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 0 1 1 0 ")
   %passString = buildPassengerString(%obj);

   // send the string of passengers to all mounted players
   for(%i = 0; %i < %data.numMountPoints; %i++)
   {
      %seat = %obj.getMountNodeObject(%i);

      if(%seat > 0 && %seat.isPlayer())
         commandToClient(%seat.client, 'checkPassengers', %passString);
   }

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function Bulldog::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   %obj.schedule(6000, "playThread", $ActivateThread, "activate");
   %obj.invSystem = true;
}

function Bulldog::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
      commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);
   else
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "HAPC", %node);

   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 0 1 1 0 ")
   %passString = buildPassengerString(%obj);

   // send the string of passengers to all mounted players
   for(%i = 0; %i < %data.numMountPoints; %i++)
   {
      %seat = %obj.getMountNodeObject(%i);

      if(%seat > 0 && %seat.isPlayer())
         commandToClient(%seat.client, 'checkPassengers', %passString);
   }

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function AssaultVehicle::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %turret = TurretData::create(AssaultPlasmaTurret);
   %turret.selectedWeapon = 1;
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 10);
//   %turret.mountImage(AssaultPlasmaTurretBarrel, 2);
//   %turret.mountImage(AssaultMortarTurretBarrel, 4);
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   %obj.turretObject = %turret;

   //vehicle turrets should not auto fire at targets
   %turret.setAutoFire(false);

   //Needed so we can set the turret parameters..
   %turret.mountImage(AssaultTurretParam, 0);
   %obj.schedule(6000, "playThread", $ActivateThread, "activate");

   // Pilot gun mount
   %obj.mountImage(GuardianPilotTurret, 2);

   // set the turret's target info
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
}

function AssaultVehicle::onTrigger(%data, %obj, %trigger, %state)
{
    if(%trigger == 0)
    {
        %wep = %obj.installedVehiclePart[3];

        if(isObject(%wep) && %wep.firesMissiles !$= "")
        {
            %obj.launchMissile();
            %state = 0;
        }

        %obj.setImageTrigger(0, %state);
    }
    else if(%trigger == 4 && %state)
        %data.triggerGrenade(%obj, %obj.getMountNodeObject(0));
}

function AssaultVehicle::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0) {
      // driver position
      // is there someone manning the turret?
      //%turreteer = %obj.getMountedNodeObject(1);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Assault", %node);
       commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
       commandToClient(%player.client, 'setRepairReticle');
       %data.updateAmmoCount(%obj, %player.client, %obj.getMountedImage(2).ammo);
//       %data.onPilotSeated(%obj, %player);
   }
   else if(%node == 1)
   {
      // turreteer position
      %turret = %obj.getMountNodeObject(10);
      %player.vehicleTurret = %turret;
      %player.setTransform("0 0 0 0 0 1 0");
      %player.lastWeapon = %player.getMountedImage($WeaponSlot);
      %player.unmountImage($WeaponSlot);
      if(!%player.client.isAIControlled())
      {
         %player.setControlObject(%turret);
         %player.client.setObjectActiveImage(%turret, 2);
      }
      %turret.turreteer = %player;
      // if the player is the turreteer, show vehicle's weapon icons
      //commandToClient(%player.client, 'showVehicleWeapons', %data.getName());
      //%player.client.setVWeaponsHudActive(1); // plasma turret icon (default)

      $aWeaponActive = 0;
      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
      %obj.getMountNodeObject(10).selectedWeapon = 1;
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Assault", %node);
       %data.updateGunnerStatus(%obj, %player, 10);
   }

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );

   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 ")
   %passString = buildPassengerString(%obj);
	// send the string of passengers to all mounted players
	for(%i = 0; %i < %data.numMountPoints; %i++)
		if(%obj.getMountNodeObject(%i) > 0)
		   commandToClient(%obj.getMountNodeObject(%i).client, 'checkPassengers', %passString);
}

function Outlaw::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %turret = TurretData::create(OutlawTurret);
   %turret.selectedWeapon = 1;
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 10);
//   %turret.mountImage(AssaultPlasmaTurretBarrel, 2);
//   %turret.mountImage(AssaultMortarTurretBarrel, 4);
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   %obj.turretObject = %turret;

   //vehicle turrets should not auto fire at targets
   %turret.setAutoFire(false);

   //Needed so we can set the turret parameters..
   %turret.mountImage(AssaultTurretParam, 0);
//   %obj.schedule(6000, "playThread", $ActivateThread, "activate");

   // set the turret's target info
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
}

function Outlaw::onTrigger(%data, %obj, %trigger, %state)
{
    if(%trigger == 0)
    {
        %wep = %obj.installedVehiclePart[3];

        if(isObject(%wep) && %wep.firesMissiles !$= "")
        {
            %obj.launchMissile();
            %state = 0;
        }

        %obj.setImageTrigger(2, %state);
        %obj.setImageTrigger(3, %state);
    }
    else if(%trigger == 4 && %state)
        %data.triggerGrenade(%obj, %obj.getMountNodeObject(0));
}

function Outlaw::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0) {
      // driver position
      // is there someone manning the turret?
      //%turreteer = %obj.getMountedNodeObject(1);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Assault", %node);
       commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
       commandToClient(%player.client, 'setRepairReticle');
       %data.updateAmmoCount(%obj, %player.client, %obj.getMountedImage(2).ammo);
//       %data.onPilotSeated(%obj, %player);
   }
   else if(%node == 1)
   {
      // turreteer position
      %turret = %obj.getMountNodeObject(10);
      %player.vehicleTurret = %turret;
      %player.setTransform("0 0 0 0 0 1 0");
      %player.lastWeapon = %player.getMountedImage($WeaponSlot);
      %player.unmountImage($WeaponSlot);
      if(!%player.client.isAIControlled())
      {
         %player.setControlObject(%turret);
         %player.client.setObjectActiveImage(%turret, 2);
      }
      %turret.turreteer = %player;
      // if the player is the turreteer, show vehicle's weapon icons
      //commandToClient(%player.client, 'showVehicleWeapons', %data.getName());
      //%player.client.setVWeaponsHudActive(1); // plasma turret icon (default)

      $aWeaponActive = 0;
      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
      %obj.getMountNodeObject(10).selectedWeapon = 1;
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Assault", %node);
       %data.updateGunnerStatus(%obj, %player, 10);
   }

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );

   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 ")
   %passString = buildPassengerString(%obj);
	// send the string of passengers to all mounted players
	for(%i = 0; %i < %data.numMountPoints; %i++)
		if(%obj.getMountNodeObject(%i) > 0)
		   commandToClient(%obj.getMountNodeObject(%i).client, 'checkPassengers', %passString);
}

function Outlaw::deleteAllMounted(%data, %obj)
{
   %turret = %obj.getMountNodeObject(10);
   if(!%turret)
      return;

   if(%client = %turret.getControllingClient())
   {
      %client.player.setControlObject(%client.player);
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
      %client.player.mountVehicle = false;
   }
   %turret.schedule(2000, delete);
}

function BomberFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   %obj.isBombing = false;
   
   %turret = TurretData::create(BomberTurret);
   %turret.team = %obj.teamBought;
   %turret.selectedWeapon = 1;
   %turret.setSelfPowered();
   %turret.mountImage(AssaultTurretParam, 0); // AIAimingTurretBarrel
   %turret.mountImage(UBTurretController, 1);
   %turret.mountImage(BomberTargetingImage, 6);
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   %turret.vehicleMounted = %obj;
   %turret.setAutoFire(false);
    
   %obj.mountObject(%turret, 10);
   %obj.turretObject = %turret;

   MissionCleanup.add(%turret);

   %obj.mountImage(FirestormPilotTurret, 3);
   
   // setup the turret's target info
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
}

function BomberFlyer::onTrigger(%data, %obj, %trigger, %state)
{
    if(%trigger == 0)
    {
        %wep = %obj.installedVehiclePart[3];

        if(isObject(%wep) && %wep.firesMissiles !$= "")
        {
            %obj.launchMissile();
            %state = 0;
        }
        
        %obj.setImageTrigger(2, %state);
    }
    else if(%trigger == 4 && %state)
        %data.triggerGrenade(%obj, %obj.getMountNodeObject(0));
}

function BomberTurret::onTrigger(%data, %obj, %trigger, %state)
{
   switch(%trigger)
   {
      case 0:
         %obj.fireTrigger = %state;
         
         if(%obj.selectedWeapon == 1 || %obj.selectedWeapon == 2)
         {
            if(%obj.getImageTrigger(6))
            {
               %obj.setImageTrigger(6, false);
               ShapeBaseImageData::deconstruct(%obj.getMountedImage(6), %obj);
            }
            
            %obj.setImageTrigger(1, %state);
         }
         else
         {
            %obj.setImageTrigger(1, false);

            if(%state)
               %obj.setImageTrigger(6, true);
            else
            {
               %obj.setImageTrigger(6, false);
               BomberTargetingImage::deconstruct(%obj.getMountedImage(6), %obj);
            }
         }

      case 2:
         if(%state)
         {
            %obj.getDataBlock().playerDismount(%obj);
         }
      case 4:
        if(%state)
            %data.triggerGrenade(%obj, %obj.getMountNodeObject(0));
   }
}


function BomberFlyer::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
      %player.setPilot(true);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);
       commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
       commandToClient(%player.client, 'setRepairReticle');
   }
   else if(%node == 1)
   {
      // bombardier position
      %turret = %obj.getMountNodeObject(10);
      %player.vehicleTurret = %turret;
      %player.setTransform("0 0 0 0 0 1 0");
      %player.lastWeapon = %player.getMountedImage($WeaponSlot);
      %player.unmountImage($WeaponSlot);
      if(!%player.client.isAIControlled())
      {
         %player.setControlObject(%turret);
         %player.client.setObjectActiveImage(%turret, 2);
      }
      
      if(%obj.isBombing)
          commandToClient(%player.client, 'startBomberSight');
          
      %turret.bomber = %player;
//      $bWeaponActive = 0;
      %obj.getMountNodeObject(10).selectedWeapon = 1;
      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);
       %data.updateGunnerStatus(%obj, %player, 10);
      %player.isBomber = true;
   }
   else
   {
      // tail gunner position
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "Bomber", %node);
   }
   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 0 ")
   %passString = buildPassengerString(%obj);
	// send the string of passengers to all mounted players
	for(%i = 0; %i < %data.numMountPoints; %i++)
		if(%obj.getMountNodeObject(%i) > 0)
		   commandToClient(%obj.getMountNodeObject(%i).client, 'checkPassengers', %passString);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

//----------------------------
// JERICHO FORWARD BASE (Mobile Point Base)
//----------------------------

function MobileBaseVehicle::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   %obj.station = "";
   %obj.turret = "";
   %obj.beacon = "";

   %obj.schedule(5000, "playThread", $AmbientThread, "ambient");
}

//----------------------------
// JERICHO FORWARD BASE
//----------------------------

function MobileBaseVehicle::deleteAllMounted(%data, %obj)
{
   if(isObject(%obj.station)) // z0dd - ZOD, 4/25/02. Console spam fix.
   {
      %obj.station.getDataBlock().onLosePowerDisabled(%obj.station);
      %obj.unmountObject(%obj.station);
      %obj.station.trigger.schedule(2000, delete);
      %obj.station.schedule(2000, delete);
   }
   if(isObject(%obj.turret)) // z0dd - ZOD, 4/25/02. Console spam fix.
   {
      %obj.turret.getDataBlock().onLosePowerDisabled(%obj.turret);
      %obj.unmountObject(%obj.turret);
      %obj.turret.schedule(2000, delete);
   }
   //--------------------------------------------------------------------------
   // z0dd - ZOD, 4/25/02. MPB Teleporter.
//   if (isObject(%obj.teleporter))
//   {
//      %obj.teleporter.setThreadDir($ActivateThread, FALSE);
//      %obj.teleporter.playThread($ActivateThread,"activate");
//      %obj.teleporter.playAudio($ActivateSound, StationVehicleDeactivateSound);
//   }
   //--------------------------------------------------------------------------
   if(isObject(%obj.shield))
      %obj.shield.schedule(2000, delete);

   if(isObject(%obj.beacon))
   {
      %obj.beacon.schedule(0, delete);
   }
}

//----------------------------
// JERICHO FORWARD BASE
//----------------------------

function MobileBaseVehicle::playerMounted(%data, %obj, %player, %node)
{
   // MPB vehicle == SUV (single-user vehicle)
   commandToClient(%player.client, 'setHudMode', 'Pilot', "MPB", %node);
   if(%obj.deploySchedule)
   {
      %obj.deploySchedule.clear();
      %obj.deploySchedule = "";
   }

   if(%obj.deployed !$= "" && %obj.deployed == 1)
   {
      %obj.setThreadDir($DeployThread, false);
      %obj.playThread($DeployThread,"deploy");
      %obj.playAudio($DeploySound, MobileBaseUndeploySound);
      %obj.station.setThreadDir($DeployThread, false);
      %obj.station.getDataBlock().onLosePowerDisabled(%obj.station);
      %obj.station.clearSelfPowered();
      %obj.station.goingOut=false;
      %obj.station.notDeployed = 1;
      %obj.station.playAudio($DeploySound, MobileBaseStationUndeploySound);

      if((%turretClient = %obj.turret.getControllingClient()) !$= "")
      {
         CommandToServer( 'resetControlObject', %turretClient );
      }

      %obj.turret.setThreadDir($DeployThread, false);
      %obj.turret.clearTarget();
      %obj.turret.setTargetObject(-1);

      %obj.turret.playAudio($DeploySound, MobileBaseTurretUndeploySound);
      %obj.shield.open();
      %obj.shield.schedule(1000,"delete");
      %obj.deploySchedule = "";
      //-----------------------------------------------------------------------
      // z0dd - ZOD, 4/25/02. MPB Teleporter.
//      %obj.teleporter.setThreadDir($ActivateThread, FALSE);
//      %obj.teleporter.playThread($ActivateThread,"activate");
//      %obj.teleporter.playAudio($ActivateSound, StationVehicleDeactivateSound);
      //-----------------------------------------------------------------------
      %obj.fullyDeployed = 0;

      %obj.noEnemyControl = 0;
   }
   %obj.deployed = 0;

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function MobileBaseVehicle::playerDismounted(%data, %obj, %player)
{
   %obj.schedule(500, "deployVehicle", %data, %player);
   Parent::playerDismounted( %data, %obj, %player );
}

function WheeledVehicle::deployVehicle(%obj, %data, %player)
{
   if (!%data.vehicleDeploy(%obj, %player))
      %obj.schedule(500, "deployVehicle", %data, %player);
}

//**************************************************************
//* JERICHO DEPLOYMENT and UNDEPLOYMENT
//**************************************************************

function MobileBaseVehicle::vehicleDeploy(%data, %obj, %player, %force)
{
   if(VectorLen(%obj.getVelocity()) <= 0.1 || %force)
   {
      %deployMessage = "";
      if( (%deployMessage = %data.checkTurretDistance(%obj)) $= "" || %force)
      {
         if(%obj.station $= "")
         {
            if( (%deployMessage = %data.checkDeploy(%obj)) $= "" || %force)
            {
               %obj.station = new StaticShape() {
                  scale = "1 1 1";
                  dataBlock = "MobileInvStation";
                  lockCount = "0";
                  homingCount = "0";
                  team = %obj.team;
                  vehicle = %obj;
               };
               %obj.station.startFade(0,0,true);
               %obj.mountObject(%obj.station, 2);
               %obj.station.getDataBlock().createTrigger(%obj.station);
               %obj.station.setSelfPowered();
               %obj.station.playThread($PowerThread,"Power");
               %obj.station.playAudio($HumSound,StationHumSound);
               %obj.station.vehicle = %obj;
               %obj.turret = new turret() {
                  scale = "1 1 1";
                  dataBlock = "MobileTurretBase";
                  lockCount = "0";
                  homingCount = "0";
                  team = %obj.team;
               };
               %obj.turret.setDamageLevel(%obj.getDamageLevel());
               %obj.mountObject(%obj.turret, 1);
               %obj.turret.setSelfPowered();
               %obj.turret.playThread($PowerThread,"Power");

               //%obj.turret.mountImage(MissileBarrelLarge, 0 ,false);
               // -----------------------------------------------------
               // z0dd - ZOD, 4/25/02. modular MPB turret
               if(%obj.barrel !$= "")
               {
                  %obj.turret.mountImage(%obj.barrel, 0 ,false);
               }
               else if(%obj.turret.barrel !$= "")
               {
                  %obj.turret.mountImage(%obj.turret.barrel, 0 ,false);
               }
               else
               {
                  %obj.turret.mountImage(MissileBarrelLarge, 0 ,false);
               }
               // -----------------------------------------------------

//               %obj.beacon = new BeaconObject() {
//                  dataBlock = "DeployedBeacon";
//                  position = %obj.position;
 //                 rotation = %obj.rotation;
  //                team = %obj.team;
//               };
//               %obj.beacon.setBeaconType(friend);
//               %obj.beacon.setTarget(%obj.team);

               checkSpawnPos(%obj, 20);
            }
         }
         else
         {
            %obj.station.setSelfPowered();
            %obj.station.playThread($PowerThread,"Power");
            %obj.turret.setSelfPowered();
            %obj.turret.playThread($PowerThread,"Power");
         }
         if(%deployMessage $= "" || %force)
         {
//            if(%obj.turret.getTarget() == -1)
//            {
//               %obj.turret.setTarget(%obj.turret.target);
//            }
            %obj.turret.setThreadDir($DeployThread, true);
            %obj.turret.playThread($DeployThread,"deploy");
            %obj.turret.playAudio($DeploySound, MobileBaseTurretDeploySound);

            %obj.station.notDeployed = 1;
            %obj.setThreadDir($DeployThread, true);
            %obj.playThread($DeployThread,"deploy");
            %obj.playAudio($DeploySound, MobileBaseDeploySound);
            %obj.deployed = 1;
            %obj.deploySchedule = "";
            %obj.disableMove = true;
            %obj.setFrozenState(true);
            if(isObject(%obj.shield))
               %obj.shield.delete();

            %obj.shield = new forceFieldBare()
            {
               scale = "1.22 1.8 1.1";
               dataBlock = "defaultTeamSlowFieldBare";
               team = %obj.team;
            };
            %obj.shield.open();
            setTargetSensorData(%obj.getTarget(), MPBDeployedSensor);
         }
      }
      if(%deployMessage !$= "")
         messageClient(%player.client, '', %deployMessage);

      return true;
   }
   else
   {
      return false;
   }
}

function MobileTurretBase::selectTarget(%this, %turret)
{
    %veh = %turret.getObjectMount();

    if(%veh.artilleryTarget > 0)
        %turret.setTargetObject(%veh.artilleryTarget);
    else
        %turret.clearTarget();
}

function MobileBaseVehicle::onEndSequence(%data, %obj, %thread)
{
   if(%thread == $DeployThread && !%obj.deployed)
   {
      %obj.unmountObject(%obj.station);
      %obj.station.trigger.delete();
      %obj.station.delete();
      %obj.station = "";

	  %obj.beacon.delete();

      %obj.unmountObject(%obj.turret);
      %obj.turret.delete();
      %obj.turret = "";

      if(!%obj.immobilized)
      {
         %obj.disableMove = false;
         %obj.setFrozenState(false);
      }
      setTargetSensorData(%obj.getTarget(), %data.sensorData);
   }
   else
   {
      %obj.station.startFade(0,0,false);
      %obj.station.setThreadDir($DeployThread, true);
      %obj.station.playThread($DeployThread,"deploy");
      %obj.station.playAudio($DeploySound, MobileBaseStationDeploySound);
      %obj.station.goingOut = true;
      %obj.shield.setTransform(%obj.getSlotTransform(3));
      %obj.shield.close();
      %obj.isDeployed = true;
      %obj.noEnemyControl = 1;
   }

   Parent::onEndSequence(%data, %obj, %thread);
}

function MobileInvStation::onEndSequence(%data, %obj, %thread)
{
   if(!%obj.goingOut)
      %obj.startFade(0,0,true);
   else
   {
      %obj.notDeployed = 0;
      %obj.vehicle.fullyDeployed = 1;
      //--------------------------------------------------------------------------------
      // z0dd - ZOD, 4/25/02. MPB Teleporter.
//      if(isObject(%obj.vehicle.teleporter))
//      {
//         %obj.vehicle.teleporter.setThreadDir($ActivateThread, TRUE);
//         %obj.vehicle.teleporter.playThread($ActivateThread,"activate");
//         %obj.vehicle.teleporter.playAudio($ActivateSound, StationVehicleAcitvateSound);
//      }
      //--------------------------------------------------------------------------------
   }
   Parent::onEndSequence(%data, %obj, %thread);
}

// MPB capacitor block issue
function MobileBaseVehicle::checkDeploy(%data, %obj)
{
   %mask = $TypeMasks::VehicleObjectType | $TypeMasks::MoveableObjectType |
           $TypeMasks::StaticShapeObjectType | $TypeMasks::ForceFieldObjectType |
           $TypeMasks::ItemObjectType | $TypeMasks::PlayerObjectType |
           $TypeMasks::TurretObjectType | //$TypeMasks::StaticTSObjectType |
           $TypeMasks::InteriorObjectType;

   //%slot 1 = turret   %slot 2 = station
   %height[1] = 0;
   %height[2] = 0;
   %radius[1] = 2.4;
   %radius[2] = 2.4;
   %stationFailed = false;
   %turretFailed = false;

   for(%x = 2; %x < 3; %x++)
   {
      %posXY = getWords(%obj.getSlotTransform(%x), 0, 1);
      %posZ = (getWord(%obj.getSlotTransform(%x), 2) + %height[%x]);
      InitContainerRadiusSearch(%posXY @ " " @ %posZ, %radius[%x], %mask);

      while ((%objFound = ContainerSearchNext()) != 0)
      {
         if(%objFound != %obj)
         {
            if(%x == 1)
               %turretFailed = true;
            else
               %stationFailed = true;
            break;
         }
      }
   }

   //If turret, station or both fail the send back the error message...
   if(%turretFailed &&  %stationFailed)
      return "Both Turret and Station are blocked and unable to deploy.";
   if(%turretFailed)
      return "Turret is blocked and unable to deploy.";
   if(%stationFailed)
      return "Station is blocked and unable to deploy.";

   //Check the station for collision with the Terrain
   %mat = %obj.getTransform();
   for(%x = 1; %x < 7; %x+=2)
   {
      %startPos = MatrixMulPoint(%mat, %data.stationPoints[%x]);
      %endPos = MatrixMulPoint(%mat, %data.stationPoints[%x+1]);

      %rayCastObj = containerRayCast(%startPos, %endPos, $TypeMasks::TerrainObjectType, 0);
      if(%rayCastObj)
         return "Station is blocked by terrain and unable to deploy.";
   }

   return "";
}

function MobileBaseVehicle::checkTurretDistance(%data, %obj)
{
   %pos = getWords(%obj.getTransform(), 0, 2);
   InitContainerRadiusSearch(%pos, 10, $TypeMasks::TurretObjectType); // z0dd - ZOD, 6/21/02. Allow closer deploy. Was 100 //  | $TypeMasks::InteriorObjectType
   while ((%objFound = ContainerSearchNext()) != 0)
   {
      if(%objFound.getType() & $TypeMasks::TurretObjectType)
      {
         if(%objFound.getDataBlock().className $= "TurretBase")
            return "Turret is within 10m of artillery. Unable to deploy.";
      }
//      else
//      {
//         %subStr = getSubStr(%objFound.interiorFile, 1, 4);
//         if(%subStr !$= "rock" && %subStr !$= "spir" && %subStr !$= "misc")
//            return "Building is in the area. Unable to deploy.";
//      }
   }
   return "";
}

function serverCmdControlObject(%client, %targetId)
{
   // match started:
   if(!$MatchStarted)
   {
      commandToClient(%client, 'ControlObjectResponse', false, "mission has not started.");
      return;
   }

   // object:
   %obj = getTargetObject(%targetId);
   if(%obj == -1)
   {
      commandToClient(%client, 'ControlObjectResponse', false, "failed to find target object.");
      return;
   }

   // shapebase:
   if(!(%obj.getType() & $TypeMasks::ShapeBaseObjectType))
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object cannot be controlled.");
      return;
   }

   // can control:
   if(!%obj.getDataBlock().canControl || %obj.getMountedImage(0).getName() $= "MissileBarrelLarge") // z0dd - ZOD 4/18/02. Prevent missile barrels from being controlled
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object cannot be controlled.");
      return;
   }

   // check damage:
   if(%obj.getDamageState() !$= "Enabled")
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object is " @ %obj.getDamageState());
      return;
   }

   // powered:
   if(!%obj.isPowered())
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object is not powered.");
      return;
   }

   // controlled already:
   %control = %obj.getControllingClient();
   if(%control)
   {
      if(%control == %client)
         commandToClient(%client, 'ControlObjectResponse', false, "you are already controlling that object.");
      else
         commandToClient(%client, 'ControlObjectResponse', false, "someone is already controlling that object.");
      return;
   }

   // same team?
   if(getTargetSensorGroup(%targetId) != %client.getSensorGroup())
   {
      commandToClient(%client, 'ControlObjectResonse', false, "cannot control enemy objects.");
      return;
   }

   // dead?
   if(%client.player == 0)
   {
      commandToClient(%client, 'ControlObjectResponse', false, "dead people cannot control objects.");
      return;
   }

   //mounted in a vehicle?
   if (%client.player.isMounted() || isObject(%client.walker))
   {
      commandToClient(%client, 'ControlObjectResponse', false, "can't control objects while mounted in a vehicle.");
      return;
   }

   %client.setControlObject(%obj);
   commandToClient(%client, 'ControlObjectResponse', true, getControlObjectType(%obj));

   // --------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/12/02. Change turrets name to controllers name.
   if((%obj.getType() & $TypeMasks::TurretObjectType) && (!%client.isAIControlled()))
   {
      // Set this varible on the client so we can reset turret nameTag when client is done.
      %client.TurretControl = %obj;

      if(%obj.nameTag !$= "") // Get the name tag for storage, this is created in the *.mis file.
      {
         %obj.oldTag = getTaggedString(%obj.nameTag); // Store this nameTag in a var on the turret.
         removeTaggedString(%obj.nameTag); // Reset the turrets nameTag.
         %obj.nameTag = "";
      }
      else // This is either a deployed turret or the *.mis file has no nameTag for it.
      {
         %obj.oldTag = ""; // No nameTag to store on turret (paranoia).

         // Reset the turrets targetNameTag. This may cause problems - ZOD
         //removeTaggedString(%obj.getDataBlock().targetNameTag);
         //%obj.getDataBlock().targetNameTag = "";
      }

      // Reset the turrets target
      freeTarget(%obj.getTarget());

      // Set the turrets target and new nameTag.
      %obj.nameTag = addTaggedString(%client.nameBase @ " controlling ");
      %obj.target = createTarget(%obj, %obj.nameTag, "", "", %obj.getDatablock().targetTypeTag, %obj.team, 0);
      setTargetSensorGroup(%obj.target, %obj.team);
   }
   // --------------------------------------------------------------------------------------------------------
}

function resetControlObject(%client)
{
   if( isObject( %client.comCam ) )
      %client.comCam.delete();

   if(isObject(%client.player) && !%client.player.isDestroyed() && $MatchStarted)
   {
      if(isObject(%client.walker))
         %client.setControlObject(%client.walker);
      else
         %client.setControlObject(%client.player);
   }
   else
      %client.setControlObject(%client.camera);

   // -----------------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/12/02. Reset the turrets nameTag back to its original.
   if(%client.TurretControl !$= "")
      %turret = %client.TurretControl;
   else
      return;

   if(isObject(%turret))
   {
      // Reset the turrets target and nameTag
      removeTaggedString(%turret.nameTag);
      %turret.nameTag = "";
      freeTarget(%turret.getTarget());

      // Set the turrets target and new nameTag
      if(%turret.oldTag !$= "")
         %turret.nameTag = addTaggedString(%turret.oldTag);
      else
         //%turret.nameTag = addTaggedString(getTaggedString(%turret.getDataBlock().targetNameTag));
         %turret.nameTag = %turret.getDataBlock().targetNameTag; // This should allready be a tagged string

      %turret.target = createTarget(%turret, %turret.nameTag, "", "", %turret.getDatablock().targetTypeTag, %turret.team, 0);
      setTargetSensorGroup(%turret.target, %turret.team);

      // Reset the varible set on the client and turret
      %turret.oldTag = "";
      %client.TurretControl = "";
   }
   // -----------------------------------------------------------------------------------------------------------------------
}

// 1-6 keys
function VehicleData::onTriggerKeys(%data, %veh, %pl, %targetBlock)
{
//    echo("onTriggerKeys vehicle" SPC %data SPC %veh SPC %pl SPC %targetBlock);
}

//----------------------------------------------------------------------------
function ShapeBase::cycleWeapon( %this, %data )
{
   if(%this.isWalker)
   {
        %this.walkerCycleWeapon(%data);
        return;
   }
   
   if ( %this.weaponSlotCount == 0 )
      return;
      
   %slot = -1;
   if ( %this.getMountedImage($WeaponSlot) != 0 )
   {
      %curWeapon = %this.getMountedImage($WeaponSlot).item.getName();
      for ( %i = 0; %i < %this.weaponSlotCount; %i++ )
      {
         //error("curWeaponName == " @ %curWeaponName);
         if ( %curWeapon $= %this.weaponSlot[%i] )
         {
            %slot = %i;
            break;
         }
      }
   }

   if ( %data $= "prev" )
   {
      // Previous weapon...
      if ( %slot == 0 || %slot == -1 )
      {
         %i = %this.weaponSlotCount - 1;
         %slot = 0;
      }
      else
         %i = %slot - 1;
   }
   else
   {
      // Next weapon...
      if ( %slot == ( %this.weaponSlotCount - 1 ) || %slot == -1 )
      {
         %i = 0;
         %slot = ( %this.weaponSlotCount - 1 );
      }
      else
         %i = %slot + 1;
   }

   %newSlot = -1;
   while ( %i != %slot )
   {
      if ( %this.weaponSlot[%i] !$= ""
        && %this.hasInventory( %this.weaponSlot[%i] )
        && %this.hasAmmo( %this.weaponSlot[%i] ) )
      {
         // player has this weapon and it has ammo or doesn't need ammo
         %newSlot = %i;
         break;
      }

      if ( %data $= "prev" )
      {
         if ( %i == 0 )
            %i = %this.weaponSlotCount - 1;
         else
            %i--;
      }
      else
      {
         if ( %i == ( %this.weaponSlotCount - 1 ) )
            %i = 0;
         else
            %i++;
      }
   }

   if ( %newSlot != -1 )
      %this.use( %this.weaponSlot[%newSlot] );
}

//----------------------------------------------------------------------------
function ShapeBase::selectWeaponSlot( %this, %data )
{
   if(%this.isWalker)
   {
        %this.walkerSelectKey(%data);
        return;
   }
   
   if ( %data < 0 || %data > %this.weaponSlotCount
     || %this.weaponSlot[%data] $= "" || %this.weaponSlot[%data] $= "TargetingLaser" )
      return;

   %this.use( %this.weaponSlot[%data] );
}
