//-----------------------------------------------------------------------------
// Projectile Library
//--------------------------------------
// MA Tracking - distance from ground
$g_MACountHeight = 15;

function createProjectile(%data, %proj, %vector, %pos, %sourceObject, %slot, %instigator)
{
   %p = new (%data)()
   {
      dataBlock          = %proj;
      initialDirection   = %vector;
      initialPosition    = %pos;
      sourceObject       = %sourceObject;
      instigator         = %instigator;
      sourceSlot         = %slot;
      vehicleObject      = %sourceObject.getObjectMount();
      damageBuffFactor   = 1;
      hitSomething       = false;
      forceIgnoreReflect = false;
      lastReflectedFrom  = 0;
      cloneCount         = 0;
   };

   %p.flags = %proj.flags $= "" ? 0 : %proj.flags;

   if(%proj.ticking == true)
      %proj.initTick(%p);

   MissionCleanup.add(%p);
   return %p;
}

function createRemoteProjectile(%data, %proj, %vector, %pos, %slot, %instigator)
{
    %point = getFXPoint();
    %point.setPosition(%pos);

    %p = createProjectile(%data, %proj, %vector, %pos, %point, %slot, %instigator);

    %point.setTransform($g_FXOrigin);
    return %p;
}

function createLance(%proj, %vector, %pos, %sourceObject, %slot, %instigator, %target)
{
   %p = new ShockLanceProjectile()
   {
      dataBlock        = %proj;
      initialDirection = %vector;
      initialPosition  = %pos;
      sourceObject     = %sourceObject;
      instigator       = %instigator;
      sourceSlot       = %slot;
      vehicleObject    = %sourceObject.getObjectMount();
      targetId         = %target;
      damageBuffFactor = 1;
      hitSomething     = %target;
   };

   %p.flags = %proj.flags $= "" ? 0 : %proj.flags;

   MissionCleanup.add(%p);
   return %p;
}

// console spam fix/weird bug fix?
function ShockLanceProjectile::hasTarget(%proj)
{
    return isObject(%proj.targetId);
}

function createRemoteLance(%proj, %vector, %pos, %slot, %instigator, %target)
{
    %point = getFXPoint();
    %point.setPosition(%pos);

    %p = createLance(%proj, %vector, %pos, %point, %slot, %instigator, %target);

    %point.setTransform($g_FXOrigin);
    return %p;
}

function projectileTrail(%data, %proj, %vector, %pos, %slot, %instigator, %radius)
{
    if(%radius > 0)
        %pos = VectorAdd(VectorScale(VectorRand(), %radius), %pos);

    createRemoteProjectile(%data, %proj, %vector, %pos, %slot, %instigator);
}

function transformProjectile(%source, %class, %block, %newPos, %newVec)
{
    %point = getFXPoint();
    %point.setPosition(%newPos);

    %p = new(%class)()
    {
        dataBlock          = %block;
        initialDirection   = %newVec;
        initialPosition    = %newPos;
        sourceObject       = %point;
        sourceSlot         = %source.sourceSlot;
        instigator         = %source.instigator;
        vehicleObject      = %source.vehicleObject;
        flags              = %source.flags;
        currentFireMode    = %source.currentFireMode;
        damageBuffFactor   = %source.damageBuffFactor;
        ticking            = %source.ticking;
        hitSomething       = false;
        forceIgnoreReflect = %source.forceIgnoreReflect;
        lastReflectedFrom  = %source.lastReflectedFrom;
        cloneCount         = %source.cloneCount++;
    };

    MissionCleanup.add(%p);
    %point.setTransform($g_FXOrigin);
    %source.hitSomething = true;
    %source.schedule(32, "delete");

    return %p;
}

function cloneProjectile(%source, %newPos, %newVec)
{
    %p = transformProjectile(%source, %source.getClassName(), %source.getDatablock().getName(), %newPos, %newVec);

    %source.getDatablock().onClone(%source, %p);

    return %p;
}

function absorbProjectile(%source)
{
    if(!%source.getDatablock().canReflect(%source))
        return "None 0 1";

    %source.schedule(0, "delete");
    return %source.getDatablock().getName() SPC %source.instigator SPC %source.damageBuffFactor;
}

function ProjectileData::onClone(%this, %source, %newProj)
{
    // Abstract
}

function PrecipitationData::canReflect(%data, %proj) // this actually counts as a projectile!
{
    return false;
}

function ProjectileData::canReflect(%data, %proj)
{
    if(%proj.hitSomething || %proj.cloneCount > 7 || %proj.forceIgnoreReflect || %proj.flags & $Projectile::IgnoreReflectorField)
        return false;

    return true;
}

function ELFProjectileData::canReflect(%data, %proj)
{
    return false;
}

function TargetProjectileData::canReflect(%data, %proj)
{
    return false;
}

function RepairProjectileData::canReflect(%data, %proj)
{
    return false;
}

function SniperProjectileData::canReflect(%data, %proj)
{
    return false;
}

// keen: prevent locked missiles from being reflected
function SeekerProjectileData::canReflect(%data, %proj)
{
    if(isObject(%proj.target))
        return false;

    return Parent::canReflect(%data, %proj);
}

function FFReflectProjectile(%proj, %ff, %normal)
{
    if(!%proj.getDatablock().canReflect(%proj))
        return;

    %pVec = VectorNormalize(%proj.initialDirection);
    %flip = VectorDot(%normal, %pVec);
    %flip = VectorScale(%normal, %flip);
    %newVec = VectorAdd(%pVec, VectorScale(%flip, -2));
    %newPos = VectorAdd(%proj.position, %newVec);

    if(%proj.getClassName() $= "BombProjectile")
        %newVec = vectorScale(%newVec, 175);

    if(isObject(%ff.fieldSource))
    {
        %data = %proj.getDatablock();

        if(%data.hasDamageRadius && %data.indirectDamage > (2 * %data.directDamage))
            %ff.fieldSource.applyDamage(%data.indirectDamage);
        else
            %ff.fieldSource.applyDamage(%data.directDamage);
    }

    return cloneProjectile(%proj, %newPos, %newVec);
}

function ForcefieldBare::isMounted(){   return false;   }
function ForcefieldBare::shouldApplyImpulse(){   return false;   }

function ProjectileData::initTick(%this, %proj, %tickTimeMult)
{
    if(%tickTimeMult $= "")
        %tickTimeMult = 1;

    %proj.hitSomething = false;
    %proj.tickTime = $g_TickTime * %tickTimeMult;
    %proj.lifeTime = getSimTime() + %this.lifetimeMS;
    %proj.tickCount = 1;

    %this.onTick(%proj);
}

function ProjectileData::onTick(%this, %proj)
{
    if(!%proj.hitSomething && %proj.lifeTime > getSimTime())
    {
        %proj.tickCount++;
        %this.schedule(%proj.tickTime, "onTick", %proj);
    }
}

function ProjectileData::onExplode(%data, %proj, %pos, %mod)
{
    %proj.hitSomething = true;

    if(%data.isVehicleMissile !$= "")
    {
        if(isObject(%proj.target) && (%proj.target.isPlayer() || %proj.target.isVehicle()))
        {
            // keen: attempt to stop missile lock
            %proj.target.homingCount--;
            %proj.setNoTarget();
        }
    }

    if(%data.hasDamageRadius)
       RadiusExplosion(%proj, %pos, %data.damageRadius, %data.indirectDamage * %proj.damageBuffFactor, %data.kickBackStrength, %proj.instigator, %data.radiusDamageType);
}

function ProjectileData::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    %projectile.hitSomething = true;

    if(isObject(%projectile.modeReference))
        %projectile.modeReference.onProjectileEnd(%projectile);
    
    if(isObject(%targetObject))
    {
        // bounce off physical zones (forcefields)
        if(isObject(%targetObject.pz))
        {
            FFReflectProjectile(%projectile, %targetObject, %normal);
            return 0;
        }

        %modifier = 1;

        if(%targetObject.isPlayer())
        {
            if(%data.mdPiercing == true && %targetObject.isShielded != true && !%targetObject.nonNewtonian)
                %targetObject.applyInternalDamage(mFloor(5 * %projectile.damageBuffFactor), %projectile.instigator, $DamageType::Disc, true);
                
            if(%projectile.flags & $Projectile::CanHeadshot)
            {
                if(firstWord(%targetObject.getDamageLocation(%position)) $= "head")
                {
                    %targetObject.getOwnerClient().headShot = 1;
                    %modifier = %targetObject.getDatablock().armorClass == $ArmorClassMask::Heavy ? %data.headshotMultiplier * 0.7 : %data.headshotMultiplier;
                }
                else
                {
                    %modifier = 1;
                    %targetObject.getOwnerClient().headShot = 0;
                }
            }

            // Set flag here, deal with it in player scripts (more points for MA kills too)
            if(%projectile.flags & $Projectile::CountMAs)
                checkMAHit(%projectile, %position, %targetObject);
        }

        %targetObject.lastHitFlags = %data.flags;

        if(%data.mdEnable == true)
        {
            for(%c = 0; %c < %data.mdDamageTypeCount; %c++)
            {
                if(%data.mdDamageRadius[%c])
                    continue;
                    
                %amount = %data.useProjDamage == true ? %projectile.smDamage : %data.mdDamageAmount[%c];
                %element = %data.useProjDamage == true ? %projectile.smElement : %data.mdDamageElement[%c];
                %targetObject.lastHitFlags = %data.flags $= "" ? 0 : %data.flags;
                
                if(%data.flags & $Projectile::PlaysHitSound && %targetObject.getType() & ($TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType))
                    %projectile.instigator.playHitIndicator((%targetObject.team != %projectile.instigator.team));
                
                %targetObject.damage(%projectile.instigator, %position, %amount * %modifier * %projectile.damageBuffFactor, %data.mdDeathMessageSet, %projectile, %element);
            }
        }
        else
            %targetObject.damage(%projectile.instigator, %position, %data.directDamage * %modifier * %projectile.damageBuffFactor, %data.directDamageType);
    }
}

function checkMAHit(%proj, %pos, %col)
{
    %time = getSimTime();
    %ground = castRay(%col.position, "0 0 -1", $g_MACountHeight, $TypeMasks::StaticObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::BuildingObjectType);

    if(%ground)
        return;

    %col.client.MAVicim = true;
    %dist = mFloor(vectorDist(%col.position, %proj.instigator.position));

    if(%col.lastMATime > %time)
        commandToClient(%proj.instigator.client, 'BottomPrint', ">>> MID AIR HIT <<<\nDistance:" SPC %dist@"m", 5, 2); // Play MA sound

    %col.lastMATime = %time + 1500;
    schedule(32, %col, "resetMAHit", %col);
}

function resetMAHit(%col)
{
    %col.client.MAVicim = false;
}

function ProjectileData::calculateFalloff(%data, %obj, %srcPos, %endPos)
{
    %distMod = 1.0;

    if(%data.hasFalloff == true)
    {
        %dist = vectorDist(%srcPos, %endPos);
        %optimalRange = %data.optimalRange + (%obj.laserFocus == true ? 500 : 0);

        if(%dist > %optimalRange)
        {
            if(%dist > %data.falloffRange)
                %distMod = %data.falloffDamagePct;
            else
            {
                %remainingRange = %dist - %data.optimalRange;
                %falloffPart = %data.falloffRange - %data.optimalRange;
                %distMod = 1 - (%remainingRange / %falloffPart);
            }
        }
    }

    return %distMod;
}

function SniperProjectileData::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   %projectile.hitSomething = true;
   %distMod = 1.0;

   if(isObject(%targetObject)) // z0dd - ZOD, 4/24/02. Console spam fix.
   {
      // calculate distance, adjust for falloff
      %distMod = %data.calculateFalloff(%projectile.instigator, %projectile.instigator.getPosition(), %targetObject.getPosition());

      if(%targetObject.isPlayer() && %projectile.flags & $Projectile::CanHeadshot)
      {
         %damLoc = firstWord(%targetObject.getDamageLocation(%position));

         if(%damLoc $= "head")
         {
            %targetObject.getOwnerClient().headShot = 1;
            %modifier = %targetObject.getDatablock().armorClass == $ArmorClassMask::Heavy ? %data.headshotMultiplier * 0.7 : %data.headshotMultiplier;
         }
         else
         {
            %modifier = 1;
            %targetObject.getOwnerClient().headShot = 0;
         }
      }

        if(%targetObject.isPlayer() && %projectile.flags & $Projectile::CountMAs)
            checkMAHit(%projectile, %position, %targetObject);

      %targetObject.lastHitFlags = %data.flags;
      
        if(%data.mdEnable == true)
        {
            for(%c = 0; %c < %data.mdDamageTypeCount; %c++)
            {
                if(%data.mdDamageRadius[%c])
                    continue;

                %targetObject.lastHitFlags = %data.flags $= "" ? 0 : %data.flags;

                if(%data.flags & $Projectile::PlaysHitSound && %targetObject.getType() & ($TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType))
                    %projectile.instigator.playHitIndicator((%targetObject.team != %projectile.instigator.team));
                
                %targetObject.damage(%projectile.instigator, %position, %modifier * %data.mdDamageAmount[%c] * %projectile.damageBuffFactor * %distMod, %data.mdDeathMessageSet, %projectile, %data.mdDamageElement[%c]);
            }
        }
        else
            %targetObject.damage(%projectile.instigator, %position, %modifier * %data.directDamage * %projectile.damageBuffFactor * %distMod, %data.directDamageType);

      if(%data.kickBackStrength !$= "")
      {
          %p = %targetObject.getWorldBoxCenter();
          %momVec = VectorSub(%p, %position);
          %momVec = VectorNormalize(%momVec);
          %impulseVec = VectorScale(%momVec, %data.kickBackStrength);

          %targetObject.applyImpulse(%p, %impulseVec);
      }
   }
}

function RadiusExplosion(%explosionSource, %position, %radius, %damage, %impulse, %sourceObject, %damageType)
{
//    trace(1);
//    schedule(0, 0, "trace", 0);
    
   InitContainerRadiusSearch(%position, %radius, $TypeMasks::PlayerObjectType      |
                                                 $TypeMasks::VehicleObjectType     |
                                                 $TypeMasks::StaticShapeObjectType |
                                                 $TypeMasks::TurretObjectType      |
                                                 $TypeMasks::CorpseObjectType      |
                                                 $TypeMasks::ItemObjectType);
   %statEffect = false;
   %projData = 0;
   %numTargets = 0;

   while((%targetObject = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %radius)
         continue;

      if (%targetObject.isMounted())
      {
         %mount = %targetObject.getObjectMount();
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if (%mount.getDataBlock().isProtectedMountPoint[%found])
            {
               continue;
            }
         }
      }

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   // keen: check for projectile datablock stuff here
//   if(%numTargets)
//   {
      %projData = %explosionSource.getDatablock();
      %statEffect = %projData.radialStatusEffect !$= "" ? true : false;
//   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      %coverage = calcExplosionCoverage(%position, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType |
                                         $TypeMasks::VehicleObjectType));
      if (%coverage == 0)
         continue;

      %data = %targetObject.getDataBlock();
      %className = %data.className;

      if (%impulse && %data.shouldApplyImpulse(%targetObject))
      {
         %p = %targetObject.getWorldBoxCenter();
         %momVec = VectorSub(%p, %position);
         %momVec = VectorNormalize(%momVec);

         //------------------------------------------------------------------------------

         %impulseVec = VectorScale(%momVec, %impulse * (1.0 - (%dist / %radius)));
         %doImpulse = true;
      }
      else if(%targetObject.isVehicle()) //if( %className $= FlyingVehicleData || %className $= HoverVehicleData ) // Removed WheeledVehicleData. z0dd - ZOD, 4/24/02. Do not allow impulse applied to MPB, conc MPB bug fix.
      {
         %p = %targetObject.getWorldBoxCenter();
         %momVec = VectorSub(%p, %position);
         %momVec = VectorNormalize(%momVec);

         %impulseVec = VectorScale(%momVec, %impulse * (1.0 - (%dist / %radius)));

         if( getWord( %momVec, 2 ) < -0.5 )
            %momVec = "0 0 1";

         // Add obj's velocity into the momentum vector
         %velocity = %targetObject.getVelocity();
         //%momVec = VectorNormalize( vectorAdd( %momVec, %velocity) );
         %doImpulse = true;
      }
      else
      {
         %momVec = "0 0 1";
         %doImpulse = false;
      }

    if(%projData.mdEnable == true)
    {
        for(%c = 0; %c < %projData.mdDamageTypeCount; %c++)
        {
            if(!%projData.mdDamageRadius[%c])
                continue;

            %dmg = %projData.useProjDamage == true ? %explosionSource.smDamage : %projData.mdDamageAmount[%c];
            %element = %projData.useProjDamage == true ? %explosionSource.smElement : %projData.mdDamageElement[%c];
            %amount = (1.0 - (%dist / %radius)) * %coverage * %dmg;
            
            if(%amount > 0)
            {
                %targetObject.lastHitFlags = %projData.flags $= "" ? 0 : %projData.flags;

                if(%projData.flags & $Projectile::PlaysHitSound && %targetObject.getType() & ($TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType))
                    %sourceObject.playHitIndicator((%targetObject.team != %sourceObject.team));
                    
                %data.damageObject(%targetObject, %sourceObject, %position, %amount, %projData.mdDeathMessageSet, %momVec, %explosionSource.theClient, %explosionSource, %element);
            }
        }
    }
    else
    {
        %amount = (1.0 - (%dist / %radius)) * %coverage * %damage;
        
        if(%amount > 0)
            %data.damageObject(%targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %explosionSource.theClient, %explosionSource);
    }

    if(%amount == 0 && %projData.doesConcussion == true && %targetObject.isPlayer())
    {
        %data.applyConcussion( %dist, %radius, %sourceObject, %targetObject );

   	  	if(!$teamDamage && %sourceObject != %targetObject && %sourceObject.client.team == %targetObject.client.team)
            messageClient(%targetObject.client, 'msgTeamConcussionGrenade', '\c1You were hit by %1\'s concussion grenade.', getTaggedString(%sourceObject.client.name));
    }

    if(%statEffect && %amount > 0 && (%targetObject.getType() & %projData.statusEffectMask) && getRandom() <= %projData.statusEffectChance)
        StatusEffect.applyEffect(%projData.radialStatusEffect, %targetObject, %sourceObject);

      //-------------------------------------------------------------------------------
      // z0dd - ZOD, 4/16/02. Tone done the how much bomber & HPC flip out when damaged
      if( %doImpulse )
      {
      	 %vehName = %data.getName();
         if ((%vehName $= "BomberFlyer") || (%vehName $= "HAPCFlyer"))
         {
              %bomberimp = VectorScale(%impulseVec, 0.6);
              %impulseVec = %bomberimp;
         }
         %targetObject.applyImpulse(%position, %impulseVec);
      }
      //if( %doImpulse )
      //   %targetObject.applyImpulse(%position, %impulseVec);
      //-------------------------------------------------------------------------------
   }
}

function RadiusRadiationExplosion(%explosionSource, %position, %radius, %damage, %impulse, %sourceObject, %poison)
{
   InitContainerRadiusSearch(%position, %radius, $TypeMasks::PlayerObjectType);
   %projData = 0;
   %numTargets = 0;

   if(%poison == true)
       %damage *= 1.5;

   while ((%targetObject = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %radius)
         continue;

      if (%targetObject.isMounted())
      {
         %mount = %targetObject.getObjectMount();
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if (%mount.getDataBlock().isProtectedMountPoint[%found])
            {
               continue;
            }
         }
      }

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      %coverage = calcExplosionCoverage(%position, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType |
                                         $TypeMasks::VehicleObjectType));
      if (%coverage == 0)
         continue;

      %amount = mCeil((1.0 - (%dist / %radius)) * %coverage * %damage);

      %data = %targetObject.getDataBlock();
      %className = %data.className;

      if (%impulse && %data.shouldApplyImpulse(%targetObject))
      {
         %p = %targetObject.getWorldBoxCenter();
         %momVec = VectorSub(%p, %position);
         %momVec = VectorNormalize(%momVec);

         //------------------------------------------------------------------------------

         %impulseVec = VectorScale(%momVec, %impulse * (1.0 - (%dist / %radius)));
         %doImpulse = true;
      }
      else
      {
         %momVec = "0 0 1";
         %doImpulse = false;
      }

      if(%amount > 0)
      {
          if(%targetObject.isShielded != true && !%targetObject.lifeSupport)
          {
              if(%poison == true)
                  %targetObject.doInternalDamage(%sourceObject, %amount, $DamageType::Poison, $DamageGroupMask::ExternalMeans, %explosionSource); // element, srcproj
              else
                  %targetObject.doInternalDamage(%sourceObject, %amount, $DamageType::MitziBlast, $DamageGroupMask::ExternalMeans, %explosionSource); // element, srcproj

              playPain(%targetObject);
          }
      }

      if( %doImpulse )
         %targetObject.applyImpulse(%position, %impulseVec);
   }
}

function ELFProjectileData::zapTarget(%data, %projectile, %target, %targeter)
{
   %oldERate = %target.getRechargeRate();
   %target.teamDamageStateOnZap = $teamDamage;
   %teammates = %target.client.team == %targeter.client.team;

   if( %target.teamDamageStateOnZap || !%teammates )
      %target.setRechargeRate(%oldERate - %data.drainEnergy);
   else
      %target.setRechargeRate(%oldERate);

   %projectile.checkELFStatus(%data, %target, %targeter);
}

function ELFProjectileData::unzapTarget(%data, %projectile, %target, %targeter)
{
	cancel(%projectile.ELFrecur);
	%target.stopAudio($ELFZapSound);
	%targeter.stopAudio($ELFFireSound);
	%target.zapSound = false;
	%targeter.zappingSound = false;
   %teammates = %target.client.team == %targeter.client.team;

	if(!%target.isDestroyed())
	{
		%oldERate = %target.getRechargeRate();
		if( %target.teamDamageStateOnZap || !%teammates )
			%target.setRechargeRate(%oldERate + %data.drainEnergy);
		else
			%target.setRechargeRate(%oldERate);
	}
}

function ELFProjectileData::targetDestroyedCancel(%data, %projectile, %target, %targeter)
{
   cancel(%projectile.ELFrecur);
	%target.stopAudio($ELFZapSound);
   %targeter.stopAudio($ELFFireSound);
	%target.zapSound = false;
	%targeter.zappingSound = false;
	%projectile.delete();
}

function ELFProjectile::checkELFStatus(%this, %data, %target, %targeter)
{
   if(isObject(%target))
   {
		if(%target.getDamageState() $= "Destroyed")
		{
			%data.targetDestroyedCancel(%this, %target, %targeter);
			return;
		}

      if((%target.getType() & ($TypeMasks::StaticObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType)) && %data.beamType != 1)
          %target.getDataBlock().damageObject(%target, %targeter, %target.position, 1, %data.directDamageType, "0 0 0", %targeter.client, %this, $DamageGroupMask::Energy);

      if(%data.beamType == 1)
        %this.ELFrecur = %this.schedule(32, tickGrapple, %data, %target, %targeter);

      %this.ELFrecur = %this.schedule(32, checkELFStatus, %data, %target, %targeter);

      %targeter.playAudio($ELFFireSound, ELFGunFireSound);
      if(!%target.zapSound)
		{
         %target.playAudio($ELFZapSound, ELFHitTargetSound);
			%target.zapSound = true;
			%targeter.zappingSound = true;
		}
   }
   // -------------------------------------------------------
   // z0dd - ZOD, 5/27/02. Stop firing if there is no target,
   // fixes continuous fire bug.
	//else if(%targeter.zappingSound)
	//{
	//	%targeter.stopAudio($ELFFireSound);
	//	%targeter.zappingSound = false;
	//}
   else
   {
      if(%targeter.zappingSound)
      {
         %targeter.stopAudio($ELFFireSound);
         %targeter.zappingSound = false;
      }
      %data.targetDestroyedCancel(%this, %target, %targeter);
      return;
   }
   // End z0dd - ZOD
   // -------------------------------------------------------
}
