//------------------------------------------------------------------------------

function displayArmorStatistics(%obj)
{
    %client = %obj.client;
    %data = %obj.getDataBlock();
    %rRate = %obj.getRechargeRate() * ($g_tickDelta * 1000);
    
    if(%obj.maxHitPoints < %obj.baseHitPoints)
         %colorFlag = $HudTextColors::Red;
    else if(%obj.maxHitPoints > %obj.baseHitPoints)
         %colorFlag = $HudTextColors::Green;
    else
         %colorFlag = $HudTextColors::White;

    bottomPrint(%client, $HudTextColors::Default@""@$ArmorsListData[$ArmorsListID[%client.armor], "name"] SPC "Slots:" SPC %data.enhancementSlots NL "Armor:"@%colorFlag SPC %obj.maxHitPoints SPC "<color:42dbea>HP | Energy:<color:FFFFFF>" SPC %data.maxEnergy SPC "<color:42dbea>KW Rate:" SPC %rRate SPC "KW\\s | Run Speed:" SPC convertMS(%data.maxForwardSpeed, "kph") SPC "KPH" NL $ArmorsListData[$ArmorsListID[%client.armor], "desc"], 300, 3);
//    bottomPrint(%client, $HudTextColors::Default@""@$ArmorsListData[$ArmorsListID[%client.armor], "name"] SPC "Slots:" SPC %data.enhancementSlots SPC "Installed Module:" SPC %obj.currentArmorMod.displayName NL "Armor:"@%colorFlag SPC %obj.maxHitPoints SPC "<color:42dbea>HP | Energy:<color:FFFFFF>" SPC %data.maxEnergy SPC "<color:42dbea>KW | Charge Rate:" SPC %rRate SPC "KW\s | Run Speed:" SPC convertMS(%data.maxForwardSpeed, "kph") SPC "KPH" NL $ArmorsListData[$ArmorsListID[%client.armor], "desc"], 300, 3);
}

//------------------------------------------------------------------------------
function stationTrigger::onEnterTrigger(%data, %obj, %colObj)
{
	//make sure it's a player object, and that that object is still alive
   if(!%colObj.isPlayer() || %colObj.isDead || %colObj.isWalker)
      return;

   // z0dd - ZOD, 7/13/02 Part of hack to keep people from mounting
   // vehicles in disallowed armors.
   if(%obj.station.getDataBlock().getName() !$= "StationVehicle")
      %colObj.client.inInv = true;

   %colObj.inStation = true;
   commandToClient(%colObj.client,'setStationKeys', true);
   if(Game.stationOnEnterTrigger(%data, %obj, %colObj)) {
      //verify station.team is team associated and isn't on player's team
      if((%obj.mainObj.team != %colObj.client.team) && (%obj.mainObj.team != 0))
      {
         //%obj.station.playAudio(2, StationAccessDeniedSound);
         messageClient(%colObj.client, 'msgStationDenied', '\c2Access Denied -- Wrong team.~wfx/powered/station_denied.wav');
      }
      else if(%obj.disableObj.isDisabled())
      {
         messageClient(%colObj.client, 'msgStationDisabled', '\c2Station is disabled.');
      }
      else if(!%obj.mainObj.isPowered())
      {
         messageClient(%colObj.client, 'msgStationNoPower', '\c2Station is not powered.');
      }
      else if(%obj.station.notDeployed)
      {
         messageClient(%colObj.client, 'msgStationNotDeployed', '\c2Station is not deployed.');
      }
      else if(%colObj.juggMode == true)
      {
         messageClient(%colObj.client, 'msgNoJuggMode', '\c2Cannot use inventory station while in Juggernaut mode.~wfx/powered/station_denied.wav');
      }
      else if(%obj.station.triggeredBy $= "")
      {
         if(%obj.station.getDataBlock().setPlayersPosition(%obj.station, %obj, %colObj))
         {
            messageClient(%colObj.client, 'CloseHud', "", 'inventoryScreen');
            commandToClient(%colObj.client, 'TogglePlayHuds', true);
            %obj.station.triggeredBy = %colObj;
            %obj.station.getDataBlock().stationTriggered(%obj.station, 1);
            %colObj.station = %obj.station;
            %colObj.lastWeapon = ( %colObj.getMountedImage($WeaponSlot) == 0 ) ? "" : %colObj.getMountedImage($WeaponSlot).getName().item;
            %colObj.unmountImage($WeaponSlot);
            %colObj.getDatablock().stopTick(%colObj);
         }
      }
   }
}

function StationInventory::onDestroyed(%this, %obj, %prevState)
{
    Parent::onDestroyed(%this, %obj, %prevState);

    if(%obj.isRootNode())
    {
        %obj.destroyNode();
        %obj.trigger.delete();
        
        // root node handles this code, so this is pretending to be a root node
        // handle it here too
        $TeamDeployedCount[%obj.team, "DeployableInvStationPack"]--;
    }
}

//------------------------------------------------------------------------------
function ItemData::onInventory(%data,%shape,%value)
{
   if(%data.catagory $= "ArmorMod") // prevent inventory from auto-killing mounted images from modules
      return;

   if (!%value) {
      // If we don't have any more of these items, make sure
      // we don't have an image mounted.
      %slot = %shape.getMountSlot(%data.image);
      if (%slot != -1)
         %shape.unmountImage(%slot);
   }
}

function buyFavorites(%client)
{
   // don't forget -- for many functions, anything done here also needs to be done
   // below in buyDeployableFavorites !!!
   %client.player.clearInventory();
   %client.setWeaponsHudClearAll();
   %cmt = $CurrentMissionType;
   %client.player.ejectFlag();
   
   %curArmor = %client.player.getDatablock();
   %curDmgPct = getDamagePercent(%curArmor.maxDamage, %client.player.getDamageLevel());
   %client.player.unmountImage($ShoulderSlot);
   
   // armor
   %client.armor = $NameToInv[%client.favorites[0]];
   %client.player.setArmor( %client.armor );
   %newArmor = %client.player.getDataBlock();

   %client.player.setDamageLevel(0);
   %client.player.internalHealthMax = 10; // + rank
   %client.player.internalHealth = %client.player.internalHealthMax;
   %client.player.shieldCap = "";
   StatusEffect.stopAllEffects(%client.player);
   
   %weaponCount = 0;

   // keen: install armor enhancements here
   %client.player.onSpawnInstance(true);

   // module
   %amCh = $NameToInv[%client.favorites[%client.armorModIndex]];

   if(%client.player.currentArmorMod !$= "")
      %client.player.currentArmorMod.onUnApply(%client.player);

   if(%amCh !$= "")
   {
      %client.player.setInventory(%amCh, 1);
      %client.player.currentArmorMod = %amCh;
      %client.player.currentArmorMod.onApply(%client.player);
   }
   else
      %client.player.currentArmorMod = "";
      
   %client.player.installEnhancements($EnhancementType::Armor, %client.player);
   
   // weapons
   for(%i = 0; %i < getFieldCount( %client.weaponIndex ); %i++)
   {
      %inv = $NameToInv[%client.favorites[getField( %client.weaponIndex, %i )]];

      if( %inv !$= "" )
      {
         %weaponCount++;
         %client.player.setInventory( %inv, 1 );

         // keen: install weapon enhancements here
         %client.installEnhancements($EnhancementType::Weapon, %inv);
      }
      
      // ----------------------------------------------------
      // z0dd - ZOD, 4/24/02. Code optimization.
      if(%inv.image.ammo !$= "")
         %client.player.setInventory(%inv.image.ammo, 999);
      // ----------------------------------------------------
   }
   %client.player.weaponCount = %weaponCount;

   // pack
   %pCh = $NameToInv[%client.favorites[%client.packIndex]];
   if ( %pCh $= "" )
      %client.clearBackpackIcon();
   else
      %client.player.setInventory( %pCh, 1 );

   // if this pack is a deployable that has a team limit, warn the purchaser
	// if it's a deployable turret, the limit depends on the number of players (deployables.cs)
	if(%pCh $= "TurretIndoorDeployable" || %pCh $= "TurretOutdoorDeployable")
		%maxDep = countTurretsAllowed(%pCh);
	else
	   %maxDep = $TeamDeployableMax[%pCh];

   if(%maxDep !$= "")
   {
      %depSoFar = $TeamDeployedCount[%client.player.team, %pCh];
      %packName = %client.favorites[%client.packIndex];

      if(Game.numTeams > 1)
         %msTxt = "Your team has "@%depSoFar@" of "@%maxDep SPC %packName@"s deployed.";
      else
         %msTxt = "You have deployed "@%depSoFar@" of "@%maxDep SPC %packName@"s.";

      messageClient(%client, 'MsgTeamDepObjCount', %msTxt);
   }

   // grenades
   for ( %i = 0; %i < getFieldCount( %client.grenadeIndex ); %i++ )
   {
      if ( !($InvBanList[%cmt, $NameToInv[%client.favorites[getField( %client.grenadeIndex, %i )]]]) )
        %client.player.setInventory( $NameToInv[%client.favorites[getField( %client.grenadeIndex,%i )]], 30 );
   }

    %client.player.lastGrenade = $NameToInv[%client.favorites[getField( %client.grenadeIndex,%i )]];

   // if player is buying cameras, show how many are already deployed
   if(%client.favorites[%client.grenadeIndex] $= "Deployable Camera")
   {
      %maxDep = $TeamDeployableMax[DeployedCamera];
      %depSoFar = $TeamDeployedCount[%client.player.team, DeployedCamera];
      if(Game.numTeams > 1)
         %msTxt = "Your team has "@%depSoFar@" of "@%maxDep@" Deployable Cameras placed.";
      else
         %msTxt = "You have placed "@%depSoFar@" of "@%maxDep@" Deployable Cameras.";
      messageClient(%client, 'MsgTeamDepObjCount', %msTxt);
   }
   
   // mines
   // -----------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 4/24/02. Old code did not check to see if mines are banned, fixed.
   for ( %i = 0; %i < getFieldCount( %client.mineIndex ); %i++ )
   {
      if ( !($InvBanList[%cmt, $NameToInv[%client.favorites[getField( %client.mineIndex, %i )]]]) )
        %client.player.setInventory( $NameToInv[%client.favorites[getField( %client.mineIndex,%i )]], 30 );
   }
   // -----------------------------------------------------------------------------------------------------
   // miscellaneous stuff -- Repair Kit, Beacons, Targeting Laser
   if ( !($InvBanList[%cmt, RepairKit]) )
      %client.player.setInventory( RepairKit, 10 );
   if ( !($InvBanList[%cmt, Beacon]) )
      %client.player.setInventory( Beacon, 20 ); // z0dd - ZOD, 4/24/02. 400 was a bit much, changed to 20
   if ( !($InvBanList[%cmt, TargetingLaser]) )
      %client.player.setInventory( TargetingLaser, 1 );

   // ammo pack pass -- hack! hack!
   if( %pCh $= "AmmoPack" )
      invAmmoPackPass(%client);
      
   // Doing this immediately for some reason will cause it only to display
   // proper stats half the time, the reason for which eludes and scares me...
   schedule(256, %client.player, "displayArmorStatistics", %client.player);
   %client.player.getDatablock().schedule(128, "initTick", %client.player);
}

//------------------------------------------------------------------------------
function buyDeployableFavorites(%client)
{
   %player = %client.player;
	%prevPack = %player.getMountedImage($BackpackSlot);
   %player.clearInventory();
   %client.setWeaponsHudClearAll();
   %cmt = $CurrentMissionType;

   // players cannot buy armor from deployable inventory stations
	%weapCount = 0;
   for ( %i = 0; %i < getFieldCount( %client.weaponIndex ); %i++ )
   {
      %inv = $NameToInv[%client.favorites[getField( %client.weaponIndex, %i )]];
      if ( !($InvBanList[DeployInv, %inv]) )
      {
         %player.setInventory( %inv, 1 );
			// increment weapon count if current armor can hold this weapon
         if(%player.getDatablock().max[%inv] > 0)
				%weapCount++;
      // ---------------------------------------------
      // z0dd - ZOD, 4/24/02. Code streamlining.
      if ( %inv.image.ammo !$= "" )
         %player.setInventory( %inv.image.ammo, 999 );
      // ---------------------------------------------
      
          // keen: install weapon enhancements here
          %client.installEnhancements($EnhancementType::Weapon, %inv);
         
			if(%weapCount >= %player.getDatablock().maxWeapons)
				break;
      }
   }
   %player.weaponCount = %weapCount;
   // give player the grenades and mines they chose, beacons, and a repair kit
   for ( %i = 0; %i < getFieldCount( %client.grenadeIndex ); %i++)
   {
      %GInv = $NameToInv[%client.favorites[getField( %client.grenadeIndex, %i )]];
      %client.player.lastGrenade = %GInv;
      if ( !($InvBanList[DeployInv, %GInv]) )
         %player.setInventory( %GInv, 30 );

   }

   // if player is buying cameras, show how many are already deployed
   if(%client.favorites[%client.grenadeIndex] $= "Deployable Camera")
   {
      %maxDep = $TeamDeployableMax[DeployedCamera];
      %depSoFar = $TeamDeployedCount[%client.player.team, DeployedCamera];
      if(Game.numTeams > 1)
         %msTxt = "Your team has "@%depSoFar@" of "@%maxDep@" Deployable Cameras placed.";
      else
         %msTxt = "You have placed "@%depSoFar@" of "@%maxDep@" Deployable Cameras.";
      messageClient(%client, 'MsgTeamDepObjCount', %msTxt);
   }

   for ( %i = 0; %i < getFieldCount( %client.mineIndex ); %i++ )
   {
      %MInv = $NameToInv[%client.favorites[getField( %client.mineIndex, %i )]];
      if ( !($InvBanList[DeployInv, %MInv]) )
         %player.setInventory( %MInv, 30 );
   }
   if ( !($InvBanList[DeployInv, Beacon]) && !($InvBanList[%cmt, Beacon]) )
      %player.setInventory( Beacon, 20 ); // z0dd - ZOD, 4/24/02. 400 was a bit much, changed to 20.
   if ( !($InvBanList[DeployInv, RepairKit]) && !($InvBanList[%cmt, RepairKit]) )
      %player.setInventory( RepairKit, 1 );
   if ( !($InvBanList[DeployInv, TargetingLaser]) && !($InvBanList[%cmt, TargetingLaser]) )
      %player.setInventory( TargetingLaser, 1 );

   // players cannot buy deployable station packs from a deployable inventory station
   %packChoice = $NameToInv[%client.favorites[%client.packIndex]];
   if ( !($InvBanList[DeployInv, %packChoice]) )
      %player.setInventory( %packChoice, 1 );

   // if this pack is a deployable that has a team limit, warn the purchaser
	// if it's a deployable turret, the limit depends on the number of players (deployables.cs)
	if(%packChoice $= "TurretIndoorDeployable" || %packChoice $= "TurretOutdoorDeployable")
		%maxDep = countTurretsAllowed(%packChoice);
	else
	   %maxDep = $TeamDeployableMax[%packChoice];
   if((%maxDep !$= "") && (%packChoice !$= "InventoryDeployable"))
   {
      %depSoFar = $TeamDeployedCount[%client.player.team, %packChoice];
      %packName = %client.favorites[%client.packIndex];

      if(Game.numTeams > 1)
         %msTxt = "Your team has "@%depSoFar@" of "@%maxDep SPC %packName@"s deployed.";
      else
         %msTxt = "You have deployed "@%depSoFar@" of "@%maxDep SPC %packName@"s.";

      messageClient(%client, 'MsgTeamDepObjCount', %msTxt);
   }

	if(%prevPack > 0)
	{
		// if player had a "forbidden" pack (such as a deployable inventory station)
		// BEFORE visiting a deployed inventory station AND still has that pack chosen
		// as a favorite, give it back
		if((%packChoice $= %prevPack.item) && ($InvBanList[DeployInv, %packChoice]))
	      %player.setInventory( %prevPack.item, 1 );
	}

   if(%packChoice $= "AmmoPack")
      invAmmoPackPass(%client);
}

//
function buyInvPackFavorites(%client)
{
   %player = %client.player;
	%prevPack = %player.getMountedImage($BackpackSlot);
   %player.clearInventory();
   %client.setWeaponsHudClearAll();
   %cmt = $CurrentMissionType;

   // players cannot buy armor from deployable inventory stations
	%weapCount = 0;
   for ( %i = 0; %i < getFieldCount( %client.weaponIndex ); %i++ )
   {
      %inv = $NameToInv[%client.favorites[getField( %client.weaponIndex, %i )]];
      if ( !($InvBanList[DeployInv, %inv]) )
      {
         %player.setInventory( %inv, 1 );
			// increment weapon count if current armor can hold this weapon
         if(%player.getDatablock().max[%inv] > 0)
				%weapCount++;
      // ---------------------------------------------
      // z0dd - ZOD, 4/24/02. Code streamlining.
      if ( %inv.image.ammo !$= "" )
         %player.setInventory( %inv.image.ammo, 999 );
      // ---------------------------------------------

          // keen: install weapon enhancements here
          %client.installEnhancements($EnhancementType::Weapon, %inv);

			if(%weapCount >= %player.getDatablock().maxWeapons)
				break;
      }
   }
   %player.weaponCount = %weapCount;
   // give player the grenades and mines they chose, beacons, and a repair kit
   for ( %i = 0; %i < getFieldCount( %client.grenadeIndex ); %i++)
   {
      %GInv = $NameToInv[%client.favorites[getField( %client.grenadeIndex, %i )]];
      %client.player.lastGrenade = %GInv;
      if ( !($InvBanList[DeployInv, %GInv]) )
         %player.setInventory( %GInv, 30 );

   }

   // if player is buying cameras, show how many are already deployed
   if(%client.favorites[%client.grenadeIndex] $= "Deployable Camera")
   {
      %maxDep = $TeamDeployableMax[DeployedCamera];
      %depSoFar = $TeamDeployedCount[%client.player.team, DeployedCamera];
      if(Game.numTeams > 1)
         %msTxt = "Your team has "@%depSoFar@" of "@%maxDep@" Deployable Cameras placed.";
      else
         %msTxt = "You have placed "@%depSoFar@" of "@%maxDep@" Deployable Cameras.";
      messageClient(%client, 'MsgTeamDepObjCount', %msTxt);
   }

   for ( %i = 0; %i < getFieldCount( %client.mineIndex ); %i++ )
   {
      %MInv = $NameToInv[%client.favorites[getField( %client.mineIndex, %i )]];
      if ( !($InvBanList[DeployInv, %MInv]) )
         %player.setInventory( %MInv, 30 );
   }
   if ( !($InvBanList[DeployInv, Beacon]) && !($InvBanList[%cmt, Beacon]) )
      %player.setInventory( Beacon, 20 ); // z0dd - ZOD, 4/24/02. 400 was a bit much, changed to 20.
   if ( !($InvBanList[DeployInv, RepairKit]) && !($InvBanList[%cmt, RepairKit]) )
      %player.setInventory( RepairKit, 1 );
   if ( !($InvBanList[DeployInv, TargetingLaser]) && !($InvBanList[%cmt, TargetingLaser]) )
      %player.setInventory( TargetingLaser, 1 );
      
   // keen: removed pack choice from here
   %player.setInventory(%prevPack.item, 1);
   invAmmoPackPass(%client);
}

//function Station::onGainPowerEnabled(%data, %obj)
function Station::onGainPowerEnabled(%data, %obj)
{
    Parent::onGainPowerEnabled(%data, %obj);
    
    %obj.lpRepSpawned = false;
    
    if(isObject(%obj.lpRepPack))
        %obj.lpRepPack.delete();
}

function Station::onLosePowerDisabled(%data, %obj)
{
    Parent::onLosePowerDisabled(%data, %obj);

    if(getSimTime() > 30000 && %obj.getDamageState() $= "Enabled")
    {
     if(%obj.lpRepSpawned != true)
     {
         %reppack = new Item()
         {
             dataBlock = "RepairPack";
             static = true;
             rotate = false;
         };

         %reppack.setPosition(%obj.getWorldBoxCenter());
         %obj.lpRepPack = %reppack;
         %obj.lpRepSpawned = true;
     }
    }
    
	// check to see if a player was using this station
	%occupied = %obj.triggeredBy;
	if(%occupied > 0)
	{
		if(%data.doesRepair)
         %data.endRepairing(%obj);
		// if it's a deployed station, stop "flashing panels" thread
		if(%data.getName() $= DeployedStationInventory)
		   %obj.stopThread($ActivateThread);
		// reset some attributes
      %occupied.setCloaked(false);
		%occupied.station = "";
		%occupied.inStation = false;
		%obj.triggeredBy = "";
		// restore "toggle inventory hud" key
	   commandToClient(%occupied.client,'setStationKeys', false);
		// re-mount last weapon or weapon slot 0
		if(%occupied.getMountedImage($WeaponSlot) == 0)
      {
         if(%occupied.inv[%occupied.lastWeapon])
            %occupied.use(%occupied.lastWeapon);
         if(%occupied.getMountedImage($WeaponSlot) == 0)
            %occupied.selectWeaponSlot( 0 );
      }
	}
}

// Inventory functions
function ShapeBase::maxInventory(%this, %data)
{
    return %this.getDatablock().max[%data.getName()];
}

function ShapeBase::incInventory(%this,%data,%amount)
{
   %max = %this.maxInventory(%data);
   %cv = %this.inv[%data.getName()];
   if (%cv < %max) {
      if (%cv + %amount > %max)
         %amount = %max - %cv;
      %this.setInventory(%data,%cv + %amount);
      %data.incCatagory(%this); // Inc the players weapon count
      return %amount;
   }
   return 0;
}

function ShapeBase::decInventory(%this,%data,%amount)
{
   %name = %data.getName();
   %cv = %this.inv[%name];
   if (%cv > 0) {
      if (%cv < %amount)
         %amount = %cv;
      %this.setInventory(%data,%cv - %amount, true);
      %data.decCatagory(%this); // Dec the players weapon count
      return %amount;
   }
   return 0;
}

// Repair kit overrides
function RepairPatch::onCollision(%data,%obj,%col)
{
   if(%col.isPlayer() && !%col.isMounted() && !%col.isDead && %col.getDamageLevel() > 0)
   {
      %col.playAudio(0, RepairPatchSound);
      
      if(%col.lifeSupportTicking)
      {
            zapEffect(%col, "FXHealGreen");
            StatusEffect.stopEffect("BurnEffect", %col);
            StatusEffect.stopEffect("CorrosiveHitEffect", %col);
            StatusEffect.stopEffect("EMPulseEffect", %col);
            StatusEffect.stopEffect("PoisonEffect", %col);

            %col.internalHealth += 10;
            messageClient(%col.client, 'MsgItemPickupRPLS', '\c2Applying temporary patch to extend life support');
            
            if(%obj.isTemporary != true)
                %obj.respawn();
            else
                %obj.schedule(32, "delete");

            return;
      }
      
      %col.incHP(60);
      zapEffect(%col, "FXHealGreen");
      
      // Mine thrown ones will have this flag
      if(%obj.isTemporary != true)
          %obj.respawn();
      else
          %obj.schedule(32, "delete");

      if(%col.client > 0)
         messageClient(%col.client, 'MsgItemPickupRP', '\c0Repair patch activated, restoring 60 HP.');
   }
}

function RepairKit::onUse(%data, %obj)
{
    if(%obj.lifeSupportTicking)
    {
        %obj.decInventory(%data, 1);
        %obj.internalHealth = 30;
        zapEffect(%obj, "FXHealGreen");
        StatusEffect.stopEffect("CorrosiveHitEffect", %obj);
        StatusEffect.stopEffect("PoisonEffect", %obj);

        messageClient(%obj.client, 'MsgRepairKitLSUsed', '\c2Injected stimulants to extend life support');
        return;
    }
    
    if(%obj.getDamagePercent() == 0)
    {
        messageClient(%obj.client, 'MsgMaxRepairKit', "\c2Armor integrity at 100%, no repair necessary.");
        return;
    }
    else if(%obj.tickHealing == true)
        return;

    %obj.tickHealing = true;
    %obj.maxHealTicks = %obj.maxHitPoints / 5;
    %obj.tickHeals = 0;

    %obj.decInventory(%data, 1);
    %data.schedule(200, "tickHeal", %obj);
    StatusEffect.stopEffect("CorrosiveHitEffect", %obj);
    StatusEffect.stopEffect("PoisonEffect", %obj);
    
    messageClient(%obj.client, 'MsgRepairKitUsed', '\c2Initiating armor rebuild, please remain motionless for maximum repair efficiency.');
}

function RepairKit::tickHeal(%data, %obj)
{
    if(!isObject(%obj))
        return;
    
    %tickTime = 200;
    %vel = vectorLen(%obj.getVelocity());
    
    if(%obj.isDead)
        return;
    else if(%vel > 0.1)
    {
        if(%vel > 20)
            %vel = 20;
            
        %tickTime += %vel * 50;
    }

    if(%obj.tickHeals >= %obj.maxHealTicks)
    {
        messageClient(%obj.client, 'MsgRepairKitEnd', "\c2Repair Kit power exhausted.");
        %obj.tickHealing = false;
        return;
    }
    else if(%obj.getDamagePercent() == 0)
    {
        messageClient(%obj.client, 'MsgRepairKitFinish', "\c2Armor integrity at 100%, repairs complete.");
        %obj.tickHealing = false;
        return;
    }

    %obj.incHP(5);
    %obj.tickHeals++;
//    %obj.playShieldEffect("0 0 1");

    if(%obj.tickHeals % 5 == 0)
        zapEffect(%obj, "FXHealGreen");
        
    %data.schedule(%tickTime, "tickHeal", %obj);
}
