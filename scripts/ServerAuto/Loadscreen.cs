//--------------------------------------------------------------------------
// Loading GUI
//--------------------------------------

$MD3LoadScreenTip[0] = "Grenade fires your shoulder weapon if you have one.";
$MD3LoadScreenTip[1] = "Health kits restore health over time depending on your speed.";
$MD3LoadScreenTip[2] = "Most vehicles require you to keep moving to keep airborne.";
$MD3LoadScreenTip[3] = "Enhancements allow you to augment your armor and weapons to your taste.";

$MD3LoadScreenTipCount = 3;

//------------------------------------------------------------------------------
function sendLoadInfoToClient(%client)
{
     if(%client.loadScreened)
          return;

     %client.loadScreened = true;

     // Switch screens if the option is selected
//     if(%client.defaultLoadScreen)
          sendChatInfoToClient(%client);
//     else
//          sendModInfoToClient(%client);

//          schedule(1000, %client, messageClient, %client, 'MsgGameOver', "");
}

function sendModInfoToClient(%client)
{
   %snd[0] = '~wfx/misc/nexus_cap.wav';
   %snd[1] = '~wfx/misc/switch_taken.wav';

   %sndCnt = 1;

   %launchSnd = %snd[getRandom(0, %sndCnt)];
   %tip = $MD3LoadScreenTip[getRandom(0, $MD3LoadScreenTipCount)];

   %singlePlayer = $CurrentMissionType $= "SinglePlayer";
   messageClient(%client, 'MsgLoadInfo', "", $CurrentMission, "", "");

   %nmis = "<font:verdana bold:12><color:33CCCC>* Mission: <color:FFFFFF>" SPC $MissionDisplayName SPC "("@$MissionTypeDisplayName@")";

   messageClient(%client, 'MsgLoadQuoteLine', %launchSnd, "<spush><font:sui generis:22><color:EEEE33><just:center>Meltdown 3 <color:FFFFFF>Beta<spop>");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<spush><font:times new roman:20><color:fe2322><just:center>Game Concept<spop>");
   messageClient(%client, 'MsgLoadQuoteLine', "", "");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<spush><font:verdana bold:16><color:33CCCC>Version: <color:FFFFFF>v"@System.Version@" <color:33CCCC>Developer: <color:FFFFFF><a:PLAYER\tKeen>Keen</a><spop>");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<spush><font:verdana bold:16><color:33CCCC>With contributions from: <color:FFFFFF>Bahke, DarkDragonDX<spop>");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<font:verdana bold:16><color:33CCCC>For more information, visit <color:ffffff><a:wwwlink	forums.radiantalpha.com/>http://forums.radiantalpha.com</a>.");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<font:verdana bold:16><color:33CCCC>Press F2 to configure your account.");
   messageClient(%client, 'MsgLoadQuoteLine', "", "");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<font:verdana bold:16><color:33CCCC>Tip: <color:FFFFFF>"@%tip );
   messageClient(%client, 'MsgLoadQuoteLine', "", "");
   messageClient(%client, 'MsgLoadQuoteLine', "", "");
   messageClient(%client, 'MsgLoadQuoteLine', "", "");

   // Send server info:
   messageClient( %client, 'MsgLoadRulesLine', "", "<font:verdana:16>" @ $Host::GameName, false );
   messageClient( %client, 'MsgLoadRulesLine', "", $Host::Info, false );
   messageClient( %client, 'MsgLoadRulesLine', "", %nmis );
   messageClient( %client, 'MsgLoadRulesLine', "", "<color:33CCCC>* Time limit: <color:FFFFFF>" @ $Host::TimeLimit, false );
   messageClient( %client, 'MsgLoadRulesLine', "", "<color:33CCCC>* Team damage: <color:FFFFFF>" @ ($TeamDamage ? "On" : "Off") );
   messageClient( %client, 'MsgLoadRulesLine', "", "<color:33CCCC>* Smurfs: <color:FFFFFF>" @ ($Host::NoSmurfs ? "No" : "Yes") );
//   messageClient( %client, 'MsgLoadRulesLine', "", "<color:FFFFFF>" @ $Host::LoadScreenMessage, false );

   messageClient(%client, 'MsgLoadInfoDone');
}

function sendChatInfoToClient(%client)
{
   messageClient(%client, 'MsgGameOver', "");
   messageClient(%client, 'MsgClearDebrief', "");

   %snd[0] = '~wfx/misc/nexus_cap.wav';
   %snd[1] = '~wfx/misc/switch_taken.wav';

   %sndCnt = 1;

   %launchSnd = %snd[getRandom(0, %sndCnt)];
   %tip = $MD3LoadScreenTip[getRandom(0, $MD3LoadScreenTipCount)];

   %singlePlayer = $CurrentMissionType $= "SinglePlayer";
   messageClient(%client, 'MsgLoadInfo', "", $CurrentMission, "", "");

   %nmis = "<font:verdana bold:12><color:33CCCC>* Mission: <color:FFFFFF>" SPC $MissionDisplayName SPC "("@$MissionTypeDisplayName@")";

   // Server name
   messageClient(%client, 'MsgDebriefResult', "", '<just:center>%1', $Host::GameName);

   messageClient(%client, 'MsgLoadQuoteLine', %launchSnd, "");
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:sui generis:22><color:EEEE33><just:center>Meltdown 3 <color:FFFFFF>Beta<spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:times new roman:20><color:fe2322><just:center>Game Concept<spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "");
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:verdana bold:16><color:33CCCC>Version: <color:FFFFFF>v"@System.Version@" <color:33CCCC>Developer: <color:FFFFFF><a:PLAYER\tKeen>Keen</a><spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:verdana bold:16><color:33CCCC>With contributions from: <color:FFFFFF>Bahke, DarkDragonDX<spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "<font:verdana bold:16><color:33CCCC>For more information, visit <color:ffffff><a:wwwlink	forums.radiantalpha.com>http://forums.radiantalpha.com</a>.");
   messageClient(%client, 'MsgDebriefAddLine', "", "<font:verdana bold:16><color:33CCCC>Press F2 to configure your account.");
   messageClient(%client, 'MsgDebriefAddLine', "", "");
   messageClient(%client, 'MsgDebriefAddLine', "", "<font:verdana bold:16><color:33CCCC>Tip: <color:FFFFFF>"@%tip );
   messageClient(%client, 'MsgDebriefAddLine', "", "");
   messageClient(%client, 'MsgDebriefAddLine', "", "");

   // Send server info:
   messageClient(%client, 'MsgDebriefAddLine', "", %nmis);
   messageClient(%client, 'MsgDebriefAddLine', "", $Host::Info);
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:FFFFFF>" @ $Host::LoadScreenMessage);
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:33CCCC>* Time limit: <color:FFFFFF>" @ $Host::TimeLimit, false );
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:33CCCC>* Team damage: <color:FFFFFF>" @ ($TeamDamage ? "On" : "Off"));
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:33CCCC>* Smurfs: <color:FFFFFF>" @ ($Host::NoSmurfs ? "No" : "Yes"));
//   messageClient(%client, 'MsgDebriefAddLine', "", "<color:FFFFFF>Load screen message here.", false ); // @ $Host::LoadScreenMessage
}
