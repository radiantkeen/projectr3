// Separate code for superweapon definitions and datablocks

// Strings
//------------------------------------------------------------------------------
$SWBeaconCount = 0;

$SWBeaconStrings[$SWBeaconCount] = "Uh oh...";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "You may feel a slight tingling sensation...";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "This is going to hurt in the morning";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "One parking lot, coming up!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "I'd move if I were you...";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Once the pin is out, Mr. Handgrenade is no longer your friend";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Death(tm) coming soon!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Hey what's that bright light...";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "GAME OVER";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Call your mother, you're not coming home";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "C-C-C-C-C-C-COMBO BREAKER!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "It was at this moment that they looked up and welcomed death";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Ooh! Aah! Pretty! Target destroyed!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Boom goes the dynamite!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "I lost a bomb, do you have it?";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "This will be messy";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "There goes the neighborhood!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Today is a good day to die";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Special Delivery!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "There goes the neighborhood!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Burn in righteous fire!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Say hello to my little friend!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Tag! You're it!";
$SWBeaconCount++;
$SWBeaconStrings[$SWBeaconCount] = "Anything special I should tell your next of kin?";
$SWBeaconCount++;

// Handler
//------------------------------------------------------------------------------
function BSNode::fireSuperweapon(%data, %player, %cost)
{
    %this = $Satellite[%player.client.team];
    %sourcePoint = %this.getWorldBoxCenter();
    %targetPoint = vectorAdd(%player.startPaintPos, "0 0 1");
    %targetVector = VectorFromPoints(%targetPoint, %sourcePoint);
    %beaconname = $SWBeaconStrings[getRandom($SWBeaconCount)];
    %disttime = 500 + vectorDist(%sourcePoint, %targetPoint);
    
    %beacon = new WayPoint()
    {
        position = %targetPoint;
        rotation = "1 0 0 0";
        scale = "1 1 1";
        name = %beaconname;
        dataBlock = "WayPointMarker";
        lockCount = "0";
        homingCount = "0";
        team = %obj.team;
    };

    MissionCleanup.add(%beacon);
    %beacon.schedule(15000 + %disttime, "delete");
    
    switch(%player.selectedPainter)
    {
        case 1: // Stryker Kinetic Charge
            createRemoteProjectile("SeekerProjectile", "StrykerKineticCharge", %targetVector, vectorAdd(%sourcePoint, "0 0 -10"), 0, %player);
            
        case 2: // Ion Storm Lightning Strike
            schedule(%disttime, 0, "createSWLightning", %targetPoint);

        case 3: // Saturn CCC Rocket Barrage
            %ceiling = vectorProject(%targetPoint, "0 0 1", 1000);
            %delaytime = 1000 + %disttime;
            
            for(%i = 0; %i < 300; %i++)
                schedule((%i * 50) + %delaytime, 0, "createSaturnRain", %player, %ceiling);

        case 4: // Mitzi Orbital Strike Cannon
            for(%i = 0; %i < 3; %i++)
            {
                %slightlyOff = vectorSpread(%targetVector, 2);
                schedule((%i * 333), 0, "createRemoteProjectile", "EnergyProjectile", "MitziOrbitalBlast", %slightlyOff, vectorAdd(%sourcePoint, "0 0 -10"), 0, %player);
            }
    }
    
    $ResourceCount[%this.team] -= %cost;
    %this.play3d(SatelliteFireWarning);
}

function createSWLightning(%targetPoint)
{
    %storm = createLightning(%targetPoint, 100, 120, 75, false);
    %storm.schedule(12000, "delete");
}

function createSaturnRain(%player, %point)
{
    %offset = getWord(%point, 0) + getRandomT(100) SPC getWord(%point, 1) + getRandomT(100) SPC getWord(%point, 2);
    
    createRemoteProjectile("SeekerProjectile", "Saturn300Missile", "0 0 -1", %offset, 0, %player);
}

// Datablocks
//------------------------------------------------------------------------------
datablock AudioProfile(SatelliteFireWarning)
{
    filename = "fx/misc/red_alert.wav";
    description = AudioBIGExplosion3d;
    preload = true;
};

datablock SeekerProjectileData(Saturn300Missile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 1000;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion           = "LargeMissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 30000;
   muzzleVelocity      = 500.0;
   maxVelocity         = 500.0;
   turningSpeed        = 500.0;
   acceleration        = 500.0;

   proximityRadius = 1;

   terrainAvoidanceSpeed         = 90;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 10;

   flareDistance = 50;
   flareAngle    = 15;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 32;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = true;
};

datablock SeekerProjectileData(StrykerKineticCharge) : Saturn300Missile
{
   scale = "10.0 10.0 10.0";
   projectileShapeName = "bomb.dts";
   indirectDamage      = 25;
   damageRadius        = 25.0;
   radiusDamageType    = $DamageType::Railgun;
   kickBackStrength    = 7500;
   lifetimeMS          = 30000;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Railgun;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 2500;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion = SatchelMainExplosion;
   underwaterExplosion = UnderwaterSatchelMainExplosion;
};

