// Weapons Class Extension

$WeaponMode::DisplayTime = 4;

function processSpawnCloakInvuln(%data, %obj)
{
   if( %obj.station $= "" && %obj.isCloaked() )
   {
      if( %obj.respawnCloakThread !$= "" )
      {
         Cancel(%obj.respawnCloakThread);
         %obj.setCloaked( false );
         %obj.respawnCloakThread = "";
      }
      else
      {
         if( %obj.getEnergyLevel() > 20 )
         {
            %obj.setCloaked( false );
            %obj.reCloak = %obj.schedule( 500, "setCloaked", true );
         }
      }
   }

   if( %obj.client > 0 )
   {
      %obj.setInvincibleMode(0 ,0.00);
      %obj.setInvincible( false ); // fire your weapon and your invincibility goes away.
   }
}

function ShapeBase::hasAmmo(%this, %weapon)
{
    if(%this.disableSwitch)
        return false;
     
    if(%weapon.dontCycleTo)
        return false;
     
    if(%weapon.image.usesEnergy)
        return true;
    else
        return %weapon.image.hasAmmo(%this);
}

function ShapeBaseImageData::hasAmmo(%data, %obj)
{
    return %obj.getInventory(%data.ammo);
}

function ShapeBaseImageData::validateFire(%data, %obj, %vehicleID)
{
   // ---------------------------------------------------------------------------
    // determine draw source
    %bCap = false;
    %energyUse = 0;
    %ammoUse = 0;
    %gyanUse = 0;
    
    if(%obj.isPlayer())
    {
       // ---------------------------------------------------------------------------
       // z0dd - ZOD, 9/3/02. Anti rapid fire mortar/missile fix.
       // keen: Optimization for these two weapons only
//       if(%obj.cantFire !$= "")
//          return false;

//       if(%data.rapidFireGlitch !$= "")
//       {
//          %obj.cantFire = 1;
//          %preventTime = %data.stateTimeoutValue[4];
//          %obj.reloadSchedule = schedule(%preventTime * 1000, %obj, resetFire, %obj);
//          return false;
//       }
       
        %drawSource = %obj;

        if(%obj.client.selectedFireMode[%data.item] > 0)
            %modeStats = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]].validateUse(%obj, %data);
        else
            %modeStats = %data.validateFireMode(%obj);

        if(%modeStats $= "f")
            return false;

        %energyUse = getWord(%modeStats, 0);
        %ammoUse = getWord(%modeStats, 1);
        %gyanUse = getWord(%modeStats, 2);
    }
    else
    {
        %drawSource = %obj;
        
        if(%data.usesEnergy)
        {
            if(%data.useMountEnergy && %drawSource.getEnergyLevel() < %data.fireEnergy)
                return false;
            else if(%data.useCapacitor && %data.usesEnergy && %vehicleID.turretObject.getCapacitorLevel() > %data.fireEnergy)
            {
                %drawSource = %vehicleID.turretObject;
                %bCap = true;
            }
            else if(%drawSource.getEnergyLevel() < %data.fireEnergy)
                return false;

            %energyUse = %data.fireEnergy;
        }
        
        if(%data.ammo !$= "")
        {
            if(%data.hasAmmo(%obj) > 0)
                %ammoUse = 1;
            else
                return false;
        }
    }

    // use draw resources
    if(%energyUse)
    {
       %energyUse *= %drawSource.energyEfficiencyFactor + %drawSource.energyEfficiency[%data.item];
       
       if(%bCap)
           %drawSource.useCapEnergy(%energyUse);
       else
           %drawSource.useEnergy(%energyUse);
    }

    if(%ammoUse)
        %obj.decInventory(%data.ammo, %ammoUse);

    if(%gyanUse)
        %obj.decGyan(%gyanUse);
        
    return true;
}

// Default fire mode
function ShapeBaseImageData::validateFireMode(%data, %obj)
{
    %ammoUse = 0;
    
    if(%data.usesEnergy)
        if(%obj.getEnergyLevel() < %data.fireEnergy)
            return "f";

    if(%data.ammo !$= "")
        if(%data.hasAmmo(%obj) > 0)
            %ammoUse = 1;
        else
            return "f";

    if(%data.gyanUse > 0)
        if(%obj.gyanLevel < %data.gyanUse)
        {
            bottomPrint(%obj.client, "Not enough GP ("@%data.gyanUse@") to fire this mode", 5, 1);
            return "f";
        }

    %gyanUse = %data.gyanUse > 0 ? %data.gyanUse : 0;
    %energyUse = %data.fireEnergy > 0 ? %data.fireEnergy : 0;

    return %energyUse SPC %ammoUse SPC %gyanUse;
}

//  %thegreatcircle = %obj.getControllingClient().getControlObject();
//  vehicle|turret.getControllingClient()->clientconnection.getControlObject()
function ShapeBaseImageData::spawnProjectile(%data, %obj, %slot, %mode)
{
    if(%data.useForwardVector == true)
        %vector = %data.projectileSpread > 0 ? vectorSpread(%spawningObject.getEyeVector(), %data.projectileSpread * (%obj.spreadFactorBase + %obj.spreadFactor[%data.item])) : %obj.getEyeVector();
    else if(%data.pilotHeadTracking == true)
    {
        %vect = %obj.isVehicle() ? %obj.getMountNodeObject(0).getEyeVector() : %obj.getMuzzleVector(%slot);
        %vector = %data.projectileSpread > 0 ? vectorSpread(%vect, %data.projectileSpread * (%obj.spreadFactorBase + %obj.spreadFactor[%data.item])) : %vect;
    }
    else
        %vector = %data.projectileSpread > 0 ? vectorSpread(%obj.getMuzzleVector(%slot), %data.projectileSpread * (%obj.spreadFactorBase + %obj.spreadFactor[%data.item])) : %obj.getMuzzleVector(%slot);

    if(%mode !$= "")
        %proj = %mode.spawnProjectile(%data, %obj, %slot, %vector, %obj.getMuzzlePoint(%slot));
    else
        %proj = createProjectile(%data.projectileType, %data.projectile, %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);

    %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];

    if(%data.deleteLastProjectile)
    {
        if(isObject(%obj.lastProjectile))
            %obj.lastProjectile.delete();

        %obj.deleteLastProjectile = %data.deleteLastProjectile;
    }

    %obj.lastProjectile = %proj;

    // AI hook
    if(%obj.client)
        %obj.client.projectile = %proj;

    if(%data.updatePilotAmmo == true)
        %obj.getDatablock().updateAmmoCount(%obj, %obj.getControllingClient(), %data.ammo);

    if(%data.projectileType $= "SniperProjectile")
        %proj.setEnergyPercentage(0.85);
        
    return %proj;
}

function ShapeBaseImageData::onFire(%data, %obj, %slot)
{
    if(%obj.isCloaked() && %obj.cloakjammed == true)
    {
        %data.misFire(%obj, %slot);
        return;
    }
        
    if(%data.sharedResourcePool == true)
        return VehicleImage::onFire(%data, %obj, %slot);

    %mode = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]];
    
    if(isObject(%mode))
    {
        if(%mode.hasTrigger == true && isObject(%obj.lastProjectile) && !%obj.lastProjectile.hitSomething)
        {
            %mode.onTriggerFire(%obj, %data);
            return;
        }
            
        if(%obj.fireTimeout[%data, %mode] > getSimTime())
            return;
    }

    %mount = %obj.getObjectMount();
    %turret = isObject(%mount) ? %mount : 0;
    
    if(!%data.validateFire(%obj, %turret))
    {
        %data.misFire(%obj, %slot);
        return;
    }

    if(%data.staggerCount !$= "")
        return %data.staggerFire(%obj, %slot, %data.staggerCount, 0);
    else
    {
        %p = %data.spawnProjectile(%obj, %slot, %mode);

        %p.currentFireMode = %obj.selectedFireMode[%data.item];
        %fireSound = isObject(%mode) && %mode.fireSound !$= "" ? %mode.fireSound : %data.defaultModeFireSound;

        if(%fireSound !$= "" || %fireSound !$= "n")
            %obj.play3D(%fireSound);
            
        if(%data.isLaser)
            %p.setEnergyPercentage(%data.laserOpacity);
    }
    
    return %p;
}

function ShapeBaseImageData::staggerFire(%data, %obj, %slot, %total, %iteration)
{
    if(%iteration > %total)
        return;
        
    %mode = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]];
    %p = %data.spawnProjectile(%obj, %slot, %mode);

    %p.currentFireMode = %obj.selectedFireMode[%data.item];
    %fireSound = isObject(%mode) && %mode.fireSound !$= "" ? %mode.fireSound : %data.defaultModeFireSound;

    if(%fireSound !$= "" || %fireSound !$= "n")
        %obj.play3D(%fireSound);

    if(%data.isLaser)
        %p.setEnergyPercentage(%data.laserOpacity);
            
    %data.schedule(%data.staggerDelay, "staggerFire", %obj, %slot, %total, %iteration++);
    
    return %p;
}

function ShapeBaseImageData::misFire(%data, %obj, %slot)
{
    if(%data.defaultModeFailSound !$= "")
        %obj.play3D(%data.defaultModeFailSound);
}

function ShapeBaseImageData::playSoundIfAmmo(%data, %obj, %sound)
{
    if(isObject(%obj))
        if(%obj.getMountedImage(0) == %data)
            if(%obj.getInventory(%data.ammo) > 0)
                %obj.play3d(%sound);
}

// Functions for vehicle-specific weaponry - not sure if a new script class
// _needs_ to be created for it, so borrowing here
function VehicleImage::onFire(%data, %obj, %slot)
{
    %vtest = %obj.getObjectMount();
    %vehicle = isObject(%vtest) ? %vtest : %obj;
    
    if(!VehicleImage::validateFire(%data, %vehicle, %slot))
    {
        %data.misFire(%obj, %slot);
        return;
    }

    if(%data.staggerCount !$= "")
        return %data.staggerFire(%obj, %slot, %data.staggerCount, 0);
    else
        %p = %data.spawnProjectile(%obj, %slot, %mode);

    return %p;
}

function VehicleImage::validateFire(%data, %drawSource, %slot)
{
    %energyUse = 0;
    %ammoUse = 0;
    
    if(%drawSource.isCloaked())
        return false;

    if(%data.usesEnergy)
    {
        if(%drawSource.getEnergyLevel() < %data.fireEnergy)
            return false;

        %energyUse = %data.fireEnergy;
    }

    if(%data.ammo !$= "")
    {
        if(%drawSource.ammoCache[%data.ammo] > 0)
            %ammoUse = 1;
        else
            return false;
    }

    // DarkDragonDX: Prevent pilot head tracking weapons from hitting their own vehicles
    if(%data.pilotHeadTracking)
    {
        %ray = ContainerRayCast(%drawSource.getMuzzlePoint(%slot), vectorProject(%drawSource.getEyeVector(), 10), -1);

        if(getWord(%ray, 0) == %drawSource)
            return false;
    }
    
    // use draw resources
    if(%energyUse)
    {
       %energyUse *= %drawSource.energyEfficiencyFactor;
       %drawSource.useEnergy(%energyUse);
    }

    if(%ammoUse)
    {
        %drawSource.ammoCache[%data.ammo]--;
        
        if(%drawSource.ammoCache[%data.ammo] == 0)
            %drawSource.setImageAmmo(%slot, false);
    }
    
    return true;
}

// keen: provide facilities for alternate weapon images (ex. +/- Main RoF images)
function Weapon::onUse(%data, %obj)
{
    if(Game.weaponOnUse(%data, %obj))
        if(%obj.isPlayer())
            %obj.mountImage(%data.image, $WeaponSlot);
}

function Player::updateReticleAmmo(%obj, %weapon, %targetSlot)
{
    if(%targetSlot $= "")
        %targetSlot = 0;
        
    if(%weapon $= "")
        %weapon = %obj.getMountedImage(%targetSlot);

    if(%weapon.ammo !$= "")
        %obj.client.setAmmoHudCount(%obj.getInventory(%weapon.ammo) @"\t\t\tGP:" SPC %obj.gyanLevel);
    else
        %obj.client.setAmmoHudCount("\t\t\tGP:" SPC %obj.gyanLevel);
}

function Player::updateWeaponMode(%obj, %slot, %bp)
{
    if(%slot $= "")
        %slot = 0;
        
    %this = %obj.getMountedImage(%slot);
    
    if(%obj.client.selectedFireMode[%this.item] $= "" || %obj.client.selectedFireMode[%this.item] > %obj.client.modeCount[%this.item])
        %obj.client.selectedFireMode[%this.item] = 0;

    %modeName = "Standard";
    %modeDescription = "Default fire mode";

    if(%obj.client.selectedFireMode[%this.item] > 0) // show modes total
    {
        %modeName = %obj.client.weaponMode[%this.item, %obj.client.selectedFireMode[%this.item]].name;
        %modeDescription = %obj.client.weaponMode[%this.item, %obj.client.selectedFireMode[%this.item]].description;
//        %modeDPS =
    }
    else if(%this.barrelCount !$= "")
    {
        %modeName = %this.barrelName[%obj.selectedPainter];
        %modeDescription = %this.barrelDesc[%obj.selectedPainter];
    }
    else if(%this.defaultModeName !$= "")
    {
        %modeName = %this.defaultModeName;
        %modeDescription = %this.defaultModeDescription;
    }
    
    if(%bp)
        bottomPrint(%obj.client, "Now using" SPC %this.weaponDescName SPC "-" SPC %modeName NL %modeDescription, $WeaponMode::DisplayTime, 2);
        
    messageClient(%obj.client, 'MsgSPCurrentObjective2', "", 'Health: %1 | Weapon Mode: %2', %obj.internalHealth, %modeName);
}

function WeaponImage::onMount(%this, %obj, %slot)
{
    if(!%obj.isPlayer())
        return;

    if(%this.subImage)
    {
        %obj.setImageAmmo(%slot, true);
        return;
    }
        
    //messageClient(%obj.client, 'MsgWeaponMount', "", %this, %obj, %slot);
    // Looks arm position
    if(%this.armthread $= "")
       %obj.setArmThread(look);
    else
       %obj.setArmThread(%this.armThread);

    %tps = %data.triggerProxySlot !$= "" ? %data.triggerProxySlot : 0;
    %targetSlot = %tps ? %tps : $WeaponSlot;
    
    // Initial ammo state
    if(%obj.getMountedImage(%targetSlot).ammo !$= "")
        if(%obj.getInventory(%this.ammo))
            %obj.setImageAmmo(%slot, true);
    
    %obj.client.setWeaponsHudActive(%this.item);
    %obj.updateReticleAmmo(%this, %targetSlot);
    %obj.updateWeaponMode(%slot, true);
        
    // check for sub-images
    if(%this.subImage1 !$= "")
        %obj.mountImage(%this.subImage1, $WeaponSubImage1);

    if(%this.subImage2 !$= "")
        %obj.mountImage(%this.subImage2, $WeaponSubImage2);

    if(%this.subImage3 !$= "")
        %obj.mountImage(%this.subImage3, $WeaponSubImage3);
}

function updateReticle(%client, %item, %guiTex)
{
    %client.setWeaponsHudActive(%item, %guiTex, true);
}

function WeaponImage::onUnmount(%this,%obj,%slot)
{
   if(%obj.isVehicle())
   {
       Parent::onUnmount(%this, %obj, %slot);
       return;
   }
   
   %obj.client.setWeaponsHudActive(%this.item, 1);
   %obj.client.setAmmoHudCount(-1);
   
   commandToClient(%obj.client,'removeReticle');
   
   // try to avoid running around with sniper/missile arm thread and no weapon
   %obj.setArmThread(look);
   
    // check for sub-images
    if(%this.subImage1 !$= "")
        %obj.unmountImage($WeaponSubImage1);

    if(%this.subImage2 !$= "")
        %obj.unmountImage($WeaponSubImage2);

    if(%this.subImage3 !$= "")
        %obj.unmountImage($WeaponSubImage3);
        
   Parent::onUnmount(%this, %obj, %slot);
}

function Ammo::onInventory(%this,%obj,%amount)
{
   // Loop through and make sure the images using this ammo have
   // their ammo states set.
   for (%i = 0; %i < 8; %i++) {
      %image = %obj.getMountedImage(%i);
      if (%image > 0)
      {
         if (isObject(%image.ammo) && %image.ammo.getId() == %this.getId())
            %obj.setImageAmmo(%i,%amount != 0);
      }
   }
   ItemData::onInventory(%this,%obj,%amount);
   // Uh, don't update the hud ammo counters if this is a corpse...that's bad.
   if ( %obj.isPlayer() && %obj.getState() !$= "Dead" )
   {
      %obj.client.setWeaponsHudAmmo(%this.getName(), %amount);

      if(%obj.getMountedImage($WeaponSlot).ammo $= %this.getName())
         %obj.client.setAmmoHudCount(%amount @"\t\t\tGP:" SPC %obj.gyanLevel);
   }
}

function Weapon::onInventory(%this,%obj,%amount)
{
   if(Game.weaponOnInventory(%this, %obj, %amount))
   {
      // Do not update the hud if this object is a corpse:
      if ( %obj.getState() !$= "Dead" )
         %obj.client.setWeaponsHudItem(%this.getName(), 0, 1);
      ItemData::onInventory(%this,%obj,%amount);
      // if a player threw a weapon (which means that player isn't currently
      // holding a weapon), set armthread to "no weapon"
		// MES - taken out to avoid v-menu animation problems (bug #4749)
      //if((%amount == 0) && (%obj.getClassName() $= "Player"))
      //   %obj.setArmThread(looknw);
   }
}

function Weapon::onPickup(%this, %obj, %shape, %amount)
{
   // If player doesn't have a weapon in hand, use this one...
   if ( %shape.getClassName() $= "Player"
     && %shape.getMountedImage( $WeaponSlot ) == 0 )
      %shape.use( %this.getName() );
}

// Weapon Modes
function Beacon::onUse(%data, %obj)
{
   // look for 3 meters along player's viewpoint for interior or terrain
   %searchRange = 3.0;
   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::PlayerObjectType;
   // get the eye vector and eye transform of the player
   %eyeVec = %obj.getEyeVector();
   %eyeTrans = %obj.getEyeTransform();
   // extract the position of the player's camera from the eye transform (first 3 words)
   %eyePos = posFromTransform(%eyeTrans);
   // normalize the eye vector
   %nEyeVec = VectorNormalize(%eyeVec);
   // scale (lengthen) the normalized eye vector according to the search range
   %scEyeVec = VectorScale(%nEyeVec, %searchRange);
   // add the scaled & normalized eye vector to the position of the camera
   %eyeEnd = VectorAdd(%eyePos, %scEyeVec);
   // see if anything gets hit
   %searchResult = containerRayCast(%eyePos, %eyeEnd, %mask, %obj);

   %allowTargBeac = (Game.class $= "PracticeCTFGame"); // z0dd - ZOD, 10/5/02. Targeting beacons only allowed in Practice mode
   %newBeacType = (%allowTargBeac ? TargetBeacon : MarkerBeacon);

   if(!%searchResult)
   {
      %obj.selectFireMode();
      return 0;
   }
   else
   {
      %searchObj = GetWord(%searchResult, 0);

      if(%searchObj.isWalker == true)
      {
          walkerMountPlayer(%searchObj, %obj.client);
          return 0;
      }
      else if(%searchObj.getType() & ($TypeMasks::StaticShapeObjectType | $TypeMasks::ForceFieldObjectType) )
      {
         // if there's already a beacon where player is aiming, switch its type
         // otherwise, player can't deploy a beacon there
         if((%searchObj.getDataBlock().getName() $= DeployedBeacon))
         {
            if(%allowTargBeac) // z0dd - ZOD, 10/5/02. Targeting beacons only allowed in Practice mode
               switchBeaconType(%searchObj);
         }
         else
            messageClient(%obj.client, 'MsgBeaconNoSurface', '\c2Cannot place beacon. Not a valid surface.');
         return 0;
      }
      else if(%obj.inv[%data.getName()] <= 0)
         return 0;
   }
   // newly deployed beacons default to "target" type.
   if($TeamDeployedCount[%obj.team, %newBeacType] >= $TeamDeployableMax[%newBeacType]) // z0dd - ZOD, 10/5/02. Targeting beacons only allowed in Practice mode
   {
      messageClient(%obj.client, 'MsgDeployFailed', '\c2Your team\'s control network has reached its capacity for this item.~wfx/misc/misc.error.wav');
      return 0;
   }
   %terrPt = posFromRaycast(%searchResult);
   %terrNrm = normalFromRaycast(%searchResult);

   %intAngle = getTerrainAngle(%terrNrm);  // getTerrainAngle() function found in staticShape.cs
   %rotAxis = vectorNormalize(vectorCross(%terrNrm, "0 0 1"));
   if (getWord(%terrNrm, 2) == 1 || getWord(%terrNrm, 2) == -1)
      %rotAxis = vectorNormalize(vectorCross(%terrNrm, "0 1 0"));
   %rotation = %rotAxis @ " " @ %intAngle;

   %obj.decInventory(%data, 1);
   %depBeac = new BeaconObject() {
      dataBlock = "DeployedBeacon";
      position = VectorAdd(%terrPt, VectorScale(%terrNrm, 0.05));
      rotation = %rotation;
   };

   // --------------------------------------------------------------------
   // z0dd - ZOD, 10/5/02. Targeting beacons only allowed in Practice mode
   if (!%allowTargBeac)
   {
      %depBeac.setBeaconType(friend);
   }
   $TeamDeployedCount[%obj.team, %newBeacType]++;
   // --------------------------------------------------------------------

   %depBeac.playThread($AmbientThread, "ambient");
   %depBeac.team = %obj.team;
   %depBeac.sourceObject = %obj;

   // give it a team target
   %depBeac.setTarget(%depBeac.team);
   MissionCleanup.add(%depBeac);
}

function Player::selectFireMode(%obj)
{
    if(%obj.getMountedImage(0) > 0)
    {
        %client = %obj.client;
        %image = %obj.getMountedImage(0);
        %weapon = %image.item;

        if(%image.customModeChange == true)
        {
            %image.selectFireMode(%client.player);
            return;
        }

        if(%client.modeCount[%weapon] > 0)
        {
            %client.selectedFireMode[%weapon]++;

            if(%client.selectedFireMode[%weapon] > %client.modeCount[%weapon])
                %client.selectedFireMode[%weapon] = 0;
                
            %modeName = "Standard";
            %modeDescription = "Default fire mode";
            %switchSound = "ChaingunDryFireSound";

            if(%client.selectedFireMode[%weapon] > 0)
            {
                %switchSound = %client.weaponMode[%weapon, %client.selectedFireMode[%weapon]].modeSwitchSound $= "" ? %image.defaultModeSwitchSound : %client.weaponMode[%weapon, %client.selectedFireMode[%weapon]].modeSwitchSound;
                %modeName = %client.weaponMode[%weapon, %client.selectedFireMode[%weapon]].name;
                %modeDescription = %client.weaponMode[%weapon, %client.selectedFireMode[%weapon]].description;
            }
            else if(%image.defaultModeName !$= "")
            {
                %modeName = %image.defaultModeName;
                %modeDescription = %image.defaultModeDescription;
                %switchSound = %image.defaultModeSwitchSound;
            }

            bottomPrint(%obj.client, "<color:ffffff>[Mode "@%client.selectedFireMode[%weapon]+1@"/"@%client.modeCount[%weapon]+1@"] <color:42dbea>"@%image.weaponDescName@" -> "@%modeName NL %modeDescription, $WeaponMode::DisplayTime, 2);
            %obj.play3D(%switchSound);
            messageClient(%obj.client, 'MsgSPCurrentObjective2', "", 'Health: %1 | Weapon Mode: %2', %obj.internalHealth, %modeName);
            %weapon.onChangeMode(%obj, %client.weaponMode[%weapon, %client.selectedFireMode[%weapon]]);
        }
        else
            bottomPrint(%obj.client, "This weapon does not have any fire modes.", $WeaponMode::DisplayTime, 1);
    }
}

function Weapon::onChangeMode(%weapon, %obj, %mode)
{
    if(%mode.subImage !$= "")
    {
         %obj.unmountImage($WeaponSubImage1);
         %obj.mountImage(%mode.subImage, $WeaponSubImage1);
    }
}

function HandInventory::onUse(%data, %obj)
{
   // %obj = player  %data = datablock of what's being thrown
   if(Game.handInvOnUse(%data, %obj))
   {
      //AI HOOK - If you change the %throwStren, tell Tinman!!!
      //Or edit aiInventory.cs and search for: use(%grenadeType);

      // z0dd - ZOD, 6/11/02. Toss grenades and your invincibility and cloaking goes away.
      if(%obj.station $= "" && %obj.isCloaked())
      {
         if( %obj.respawnCloakThread !$= "" )
         {
            Cancel(%obj.respawnCloakThread);
            %obj.setCloaked( false );
            %obj.respawnCloakThread = "";
         }
      }
      if( %obj.client > 0 )
      {
         %obj.setInvincibleMode(0, 0.00);
         %obj.setInvincible( false );
      }

      %tossTimeout = getSimTime() - %obj.lastThrowTime[%data];
      if(%tossTimeout < $HandInvThrowTimeout)
         return;

      %throwStren = %obj.throwStrength;

      %obj.decInventory(%data, 1);
      
      if(%obj.assaultRPG)
      {
         %obj.arpgGrenade = %data.thrownItem;
         %obj.lastThrowTime[%data] = getSimTime();

         %obj.setImageTrigger($ShoulderSlot, true);
         return;
      }
      
      %thrownItem = new Item()
      {
         dataBlock = %data.thrownItem;
         sourceObject = %obj;
      };
      MissionCleanup.add(%thrownItem);

      // throw it
      %eye = %obj.getEyeVector();
      %vec = vectorScale(%eye, (%throwStren * 20.0));

      // add a vertical component to give it a better arc
      %dot = vectorDot("0 0 1", %eye);
      if(%dot < 0)
         %dot = -%dot;
      %vec = vectorAdd(%vec, vectorScale("0 0 10", 1 - %dot)); // z0dd - ZOD, 8/4/02. Add a higher arc to the toss. was 0 0 4

      // add player's velocity
      %vec = vectorAdd(%vec, vectorScale(%obj.getVelocity(), 0.65)); // z0dd - ZOD, 8/4/02. Add more of the players vel to the toss. was 0.4
      %pos = getBoxCenter(%obj.getWorldBox());

      %thrownItem.sourceObject = %obj;
      %thrownItem.team = %obj.team;
      %thrownItem.setTransform(%pos);

      %thrownItem.applyImpulse(%pos, %vec);
      %thrownItem.setCollisionTimeout(%obj);
      serverPlay3D(GrenadeThrowSound, %pos);
      %obj.lastThrowTime[%data] = getSimTime();

      %thrownItem.getDataBlock().onThrow(%thrownItem, %obj);
      %obj.throwStrength = 0;
   }
}

//function Weapon::displayCurrentMode(%weapon, %obj, %mode)
//{
//
//}

function TurretData::selectTarget(%this, %turret)
{
   %turretTarg = %turret.getTarget();

   if(%turretTarg == -1)
      return;

//   %veh = %turret.getObjectMount();
   
//   if(isObject(%veh))
//   {
//      if(%obj.gunshipBeacon > 0)
//      {
//         echo("gunship!");
//         return;
//      }
//   }
//
   // if the turret isn't on a team, don't fire at anyone
   if(getTargetSensorGroup(%turretTarg) == 0)
   {
      %turret.clearTarget();
      return;
   }

   // stop firing if turret is disabled or if it needs power and isn't powered
   if((!%turret.isPowered()) && (!%turret.needsNoPower))
   {
      %turret.clearTarget();
      return;
   }

   %TargetSearchMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;

   InitContainerRadiusSearch(%turret.getMuzzlePoint(0),
                             %turret.getMountedImage(0).attackRadius,
                             %TargetSearchMask);

   while ((%potentialTarget = ContainerSearchNext()) != 0)
   {
      %potTargTarg = %potentialTarget.getTarget();
      if (%turret.isValidTarget(%potentialTarget) && (getTargetSensorGroup(%turretTarg) != getTargetSensorGroup(%potTargTarg)))
      {
         %turret.setTargetObject(%potentialTarget);
         return;
      }
   }
}
