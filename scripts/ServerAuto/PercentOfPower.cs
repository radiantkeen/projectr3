// Percent of Power implementation

function SimGroup::calcPowerPct(%this)
{
    if(%this.providesPower)
        return 1;
    else if((%grp = %this.getGroup()).generators !$= "")
        return %grp.powerPct;
    else if(%this.generators !$= "" && (%count = %this.generators.getCount()) > 0)
    {
        %dmgAvg = 0;
        %dmgPct = 0;
        
        for(%i = 0; %i < %count; %i++)
        {
            %gen = %this.generators.getObject(%i);
            
            if(%gen.getDamageState() $= "Enabled")
                %dmgAvg += (1 - %gen.getDamagePct());
        }
        
        %dmgPct = %dmgAvg / %count;
        
        return %dmgPct;
    }
    else
        return 0;
}

function SimGroup::updatePowerState(%this)
{
    %this.powerPct = %this.calcPowerPct();
    %count = %this.getCount();
    
    for(%i = 0; %i < %count; %i++)
        %this.getObject(%i).updatePowerState();
}

function SimGroup::getPowerPct(%this)
{
    return %this.powerPct;
}

function SimObject::getPowerPct(%this)
{
    // Reduce console spam
}

function GameBase::getPowerPct(%this)
{
    %grp = %this.getGroup();
    
    return %grp == -1 ? 0 : %grp.powerPct;
}

function GameBase::updatePowerState(%this)
{
    %data = %this.getDatablock();
    %pct = %this.getPowerPct();
    %lowPower = 0.5;
    %offPower = 0.2;
    
    if(%data.lowPowerPct !$= "")
        %lowPower = %data.lowPowerPct;
        
    if(%data.offPowerPct !$= "")
        %offPower = %data.offPowerPct;

//    echo(%this SPC %data.getName() SPC %pct);

	if(%pct >= %offPower)
	   %data.gainPower(%this);
	else
	   %data.losePower(%this);
}

function SimObject::updatePowerState(%this)
{
    // Reduce console spam
}

function SimGroup::addGenerator(%this, %gen)
{
    if(%this.generators $= "")
        %this.generators = createRandomSimSet();
        
    if(!%this.generators.isMember(%gen))
    {
        %this.generators.add(%gen);
        
        %gen.powering = true;
    }

    %this.updatePowerState();
}

function SimGroup::delGenerator(%this, %gen)
{
    if(%this.generators $= "")
        return;

    if(%this.generators.isMember(%gen))
    {
        %gen.powering = false;
        
        %this.generators.remove(%gen);
        %this.updatePowerState();
    }
}

function SimGroup::powerInit(%this, %powerCount)
{
    // For maps without generators
    if(%this.providesPower)
        %powerCount++;
        
    %count = %this.getCount();
    
    for(%i = 0; %i < %count; %i++)
    {
        %obj = %this.getObject(%i);
        
        if(%obj.getType() & $TypeMasks::GameBaseObjectType)
        {
            if(%obj.getDatablock().isPowering(%obj))
            {
                if(%this.generators $= "")
                    %this.generators = createRandomSimSet();

                %this.generators.add(%obj);
                %obj.powering = true;
                %powerCount++;
            }
        }
    }
    
    %this.powerCount = %powerCount;
    %this.updatePowerState();

    for(%i = 0; %i < %this.getCount(); %i++)
    {
        %obj = %this.getObject(%i);
        %obj.powerInit(%powerCount);
    }
}

function GameBase::powerInit(%this, %powerCount)
{
    %this.updatePowerState();
//	if(%powerCount)
//	   %this.getDatablock().gainPower(%this);
//	else
//	   %this.getDataBlock().losePower(%this);
}

function Generator::isPowering(%data, %obj)
{
    return !%obj.isDisabled();
}

// Backwards compatibility
function GameBase::powerCheck(%this, %powerCount)
{
    %pct = %this.getGroup().powerPct;
    
    if(%pct > 0.2 || %this.selfPower)
        %this.getDatablock().gainPower(%this);
    else
        %this.getDatablock().losePower(%this);
}

function GameBase::isPowered(%this)
{
   return %this.selfPower || %this.getGroup().getPowerPct() > 0.2;
}
