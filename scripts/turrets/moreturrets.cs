// Flamethrower Turret

datablock GrenadeProjectileData(FlametongueTurretBolt) : DenseFireballProjectile
{
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Plasma;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   grenadeElasticity = 0.01;
   grenadeFriction   = 0.9;
   armingDelayMS     = 0;
   muzzleVelocity    = 100.0;
   gravityMod        = 1.9;
};

datablock TurretImageData(FlametongueBarrel)
{
   shapeFile = "turret_fusion_large.dts";
   item      = PlasmaBarrelPack; // z0dd - ZOD, 4/25/02. Was wrong: PlasmaBarrelLargePack

   projectile = FlametongueTurretBolt;
   projectileType = GrenadeProjectile;
   usesEnergy = true;
   fireEnergy = 15;
   minEnergy = 15;
   emap = true;

   // Turret parameters
   activationMS      = 700; // z0dd - ZOD, 3/27/02. Was 1000. Amount of time it takes turret to unfold
   deactivateDelayMS = 1500;
   thinkTimeMS       = 140; // z0dd - ZOD, 3/27/02. Was 200. Amount of time before turret starts to unfold (activate)
   degPerSecTheta    = 300;
   degPerSecPhi      = 500;
   attackRadius      = 100;

   projectileSpread = 3.0;
   
   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Activate";
   stateSound[1]                 = PBLSwitchSound;
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.3;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = PlasmaFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.4;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Reload";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

datablock ExplosionData(HeavyBlueBoltExplosion)
{
   soundProfile   = GrenadeExplosionSound;

   emitter[0] = UnderwaterHandGrenadeExplosionSmokeEmitter;
   emitter[1] = UnderwaterHandGrenadeSparkEmitter;
//   emitter[2] = HandGrenadeExplosionBubbleEmitter;

   subExplosion[0] = UnderwaterHandGrenadeSubExplosion1;
   subExplosion[1] = UnderwaterHandGrenadeSubExplosion2;

   shakeCamera = true;
   camShakeFreq = "12.0 13.0 11.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 1.0;
   camShakeRadius = 15.0;
};

datablock LinearFlareProjectileData(HeavyBlueBolt)
{
   scale               = "5.0 5.0 5.0";
   directDamage        = 0.4;
   directDamageType    = $DamageType::BlueBlaster;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disruptor;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 75;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::ShieldsOnly;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "HeavyBlueBoltExplosion";

   dryVelocity       = 250.0;
   wetVelocity       = 250.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 100;

   numFlares         = 20;
   size              = 1;
   flareColor        = "0.25 0.25 1.0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = ShrikeBlasterProjectile;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "0.25 0.25 1.0";
};

datablock TurretImageData(HeavyBlueDBarrel)
{
   shapeFile = "turret_elf_large.dts";
   item      = ELFBarrelPack; // z0dd - ZOD, 4/25/02. Was wrong: ELFBarrelLargePack

   projectile = HeavyBlueBolt;
   projectileType = LinearFlareProjectile;
   usesEnergy = true;
   fireEnergy = 40.0;
   minEnergy = 40.0;

   // Turret parameters
   activationMS      = 500; // z0dd - ZOD, 3/27/02. Was 500. Amount of time it takes turret to unfold
   deactivateDelayMS = 250;
   thinkTimeMS       = 100; // z0dd - ZOD, 3/27/02. Was 100. Amount of time before turret starts to unfold (activate)
   degPerSecTheta    = 580;
   degPerSecPhi      = 960;
   attackRadius      = 150;

   // State transiltions
   stateName[0]                        = "Activate";
   stateTransitionOnNotLoaded[0]       = "Dead";
   stateTransitionOnLoaded[0]          = "ActivateReady";

   stateName[1]                        = "ActivateReady";
   stateSequence[1]                    = "Activate";
   stateSound[1]                       = EBLSwitchSound;
   stateTimeoutValue[1]                = 1;
   stateTransitionOnTimeout[1]         = "Ready";
   stateTransitionOnNotLoaded[1]       = "Deactivate";
   stateTransitionOnNoAmmo[1]          = "NoAmmo";

   stateName[2]                        = "Ready";
   stateTransitionOnNotLoaded[2]       = "Deactivate";
   stateTransitionOnTriggerDown[2]     = "Fire";
   stateTransitionOnNoAmmo[2]          = "NoAmmo";

   stateName[3]                        = "Fire";
   stateFire[3]                        = true;
   stateRecoil[3]                      = LightRecoil;
   stateAllowImageChange[3]            = false;
   stateSequence[3]                    = "Fire";
   stateSound[3]                       = BomberTurretFireSound;
   stateScript[3]                      = "onFire";
   stateTransitionOnTriggerUp[3]       = "Reload";
   stateTimeoutValue[3]                = 1.4;
   stateTransitionOnNoAmmo[3]          = "NoAmmo";

   stateName[4]                        = "Reload";
   stateTimeoutValue[4]                = 0.01;
   stateAllowImageChange[4]            = false;
   stateSequence[4]                    = "Reload";
   stateTransitionOnTimeout[4]         = "Ready";
   stateTransitionOnNotLoaded[4]       = "Deactivate";
   stateTransitionOnNoAmmo[4]          = "NoAmmo";

   stateName[5]                        = "Deactivate";
   stateSequence[5]                    = "Activate";
   stateDirection[5]                   = false;
   stateTimeoutValue[5]                = 1;
   stateTransitionOnLoaded[5]          = "ActivateReady";
   stateTransitionOnTimeout[5]         = "Dead";

   stateName[6]                        = "Dead";
   stateTransitionOnLoaded[6]          = "ActivateReady";

   stateName[7]                        = "NoAmmo";
   stateTransitionOnAmmo[7]            = "Reload";
   stateSequence[7]                    = "NoAmmo";

   stateName[8]                       = "Deconstruction";
   stateScript[8]                     = "deconstruct";
   stateTransitionOnNoAmmo[8]         = "NoAmmo";
   stateTransitionOnTimeout[8]        = "Reload";
   stateTimeoutValue[8]               = 0.1;
};

datablock TracerProjectileData(TurretBolterBolt)
{
   doDynamicClientHits = true;

   directDamage        = 0.2;
   directDamageType    = $DamageType::Bolter;
   explosion           = "BolterExplosion";
   splash              = ChaingunSplash;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Bolter;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 15;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   kickBackStrength  = 0.0;
   sound 				= ChaingunProjectile;

   dryVelocity       = 350.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   tracerLength    = 12.0;
   tracerAlpha     = false;
   tracerMinPixels = 8;
   tracerColor     = 224.0/255.0 @ " " @ 227.0/255.0 @ " " @ 159.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.20;
   crossSize       = 0.45;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;
};

datablock TurretImageData(BolterBarrel)
{
   shapeFile = "TR2weapon_chaingun.dts";
   item      = PlasmaBarrelPack; // z0dd - ZOD, 4/25/02. Was wrong: PlasmaBarrelLargePack

   projectile = TurretBolterBolt;
   projectileType = TracerProjectile;
   usesEnergy = true;
   fireEnergy = 5;
   minEnergy = 5;
   emap = true;

   // Turret parameters
   activationMS      = 700; // z0dd - ZOD, 3/27/02. Was 1000. Amount of time it takes turret to unfold
   deactivateDelayMS = 1500;
   thinkTimeMS       = 140; // z0dd - ZOD, 3/27/02. Was 200. Amount of time before turret starts to unfold (activate)
   degPerSecTheta    = 300;
   degPerSecPhi      = 500;
   attackRadius      = 150;

   casing              = ShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 6.0;

   projectileSpread = 3.0; // z0dd - ZOD, 8/6/02. Was: 8.0 / 1000.0

   //--------------------------------------
   stateName[0]             = "Activate";
   stateTransitionOnNotLoaded[0]       = "Dead";
   stateTransitionOnLoaded[0]          = "Ready";
   stateSound[0]            = MBLSwitchSound;

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   stateTimeoutValue[1]                = 0.5;
   stateTransitionOnNotLoaded[1]       = "Deactivate";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.25;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = AssaultChaingunFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.2;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.5;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
   
   stateName[8]                = "Deactivate";
   stateSequence[8]            = "Activate";
   stateDirection[8]           = false;
   stateTimeoutValue[8]        = 1;
   stateTransitionOnLoaded[8]  = "Ready";
   stateTransitionOnTimeout[8] = "Dead";

   stateName[9]               = "Dead";
   stateTransitionOnLoaded[9] = "Ready";
};

datablock LinearProjectileData(TurretPowerDisc)
{
   projectileShapeName = "disc.dts";
   scale               = "1.5 1.5 1.5";
   emitterDelay        = -1;
   directDamage        = 0.5;
   directDamageType    = $DamageType::Disc;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Disc;
   kickBackStrength    = 3500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disc;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 150;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 50;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot;
   ticking             = false;
   headshotMultiplier  = 1.25;

   sound 				= discProjectileSound;
   explosion           = "LrgDiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   dryVelocity       = 125;
   wetVelocity       = 75;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2500;
   lifetimeMS        = 2500;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = 32;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

datablock TurretImageData(HeavyDiscBarrel)
{
   shapeFile = "TR2weapon_disc.dts";
   item      = PlasmaBarrelPack; // z0dd - ZOD, 4/25/02. Was wrong: PlasmaBarrelLargePack

   projectile = TurretPowerDisc;
   projectileType = LinearProjectile;
   usesEnergy = true;
   fireEnergy = 50;
   minEnergy = 50;
   emap = true;

   // Turret parameters
   activationMS      = 700; // z0dd - ZOD, 3/27/02. Was 1000. Amount of time it takes turret to unfold
   deactivateDelayMS = 1500;
   thinkTimeMS       = 400; // z0dd - ZOD, 3/27/02. Was 200. Amount of time before turret starts to unfold (activate)
   degPerSecTheta    = 300;
   degPerSecPhi      = 500;
   attackRadius      = 125;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Activation";
   stateSound[1]                 = RipperMountSound;
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateSequence[2]                = "discSpin";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 1.2;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = RipperFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.5;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Reload";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};
