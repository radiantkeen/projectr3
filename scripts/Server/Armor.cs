// Armor-specific variables
$InvincibleTime = 6;

//Damage Rate for entering Liquid
$DamageLava       = 0.0325;
$DamageHotLava    = 0.0325;
$DamageCrustyLava = 0.0325;

// Armor state flags
$ArmorFlags::CanPilot           = 1 << 0;
$ArmorFlags::NoFlagTouching     = 1 << 1;
$ArmorFlags::ExplodeOnDeath     = 1 << 2;
$ArmorFlags::NoDeathFiring      = 1 << 3;

// Armor Types
$ArmorType::Corpse              = 0;
$ArmorType::Plugsuit            = 1;
$ArmorType::BattleAngel         = 2;
$ArmorType::Scout               = 3;
$ArmorType::Gearhead            = 4;
$ArmorType::Assault             = 5;
$ArmorType::Brawler             = 6;
$ArmorType::MagneticIon         = 7;
$ArmorType::Walker              = 8;

// Armor Bitmasks
$ArmorMask::Corpse              = 1 << 0;
$ArmorMask::Plugsuit            = 1 << 1;
$ArmorMask::BattleAngel         = 1 << 2;
$ArmorMask::Scout               = 1 << 3;
$ArmorMask::Gearhead            = 1 << 4;
$ArmorMask::Assault             = 1 << 5;
$ArmorMask::Brawler             = 1 << 6;
$ArmorMask::MagneticIon         = 1 << 7;
$ArmorMask::Fury                = 1 << 8;
$ArmorMask::Stormguard          = 1 << 9;

// Armor Class
$ArmorClassMask::None           = 0;
$ArmorClassMask::Light          = 1;
$ArmorClassMask::Medium         = 2;
$ArmorClassMask::Heavy          = 3;
$ArmorClassMask::Walker         = 4;

// Speed limit
$g_THRUSTER_LIMIT               = 50; // 180 KPH

//----------------------------------------------------------------------------

function Armor::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);

// recharge rate set below
//   %obj.setRechargeRate(%data.rechargeRate * %obj.rechargeRateFactor);
   %obj.setRepairRate(0);
//   %obj.setSelfPowered(); // keen: necessary? not really sure, part of base player code
   %obj.selfPower = true; // ^ if the above is needed, this should really be the only necessary part from it
   %obj.internalHealth = 10;
   %obj.internalHealthMax = 10; // + rank

   %obj.mountVehicle = true;
   // MD3 stuff here down
   %obj.flags = %data.armorFlags;
   %obj.tickCount = 0;
   %obj.gyanLevel = 0;
   %obj.gyanMax = 10;
   %obj.isDead = false;
   %obj.deathTriggered = false;
   %obj.stationTickInterupt = false;
   %obj.transient = false;
   %obj.scriptKilled = false;
}

function Armor::resetStats(%data, %obj)
{
    if(%obj.bonusWeight != 0)
        %obj.unmountImage($FlagSlot);

    Parent::resetStats(%data, %obj);

    %obj.armorSpecials = -1;
    %obj.gyanMax = 10;

    if(%obj.gyanLevel > %obj.gyanMax)
        %obj.gyanLevel = %obj.gyanMax;

    %obj.shieldFlash = 0;

    %obj.internalHealthMax = 10 + mFloor(%obj.client.currentRank / 2);

    if(%obj.internalHealth > %obj.internalHealthMax)
        %obj.internalHealth = %obj.internalHealthMax;

    // Enhancement-specific stuff here
    %obj.laserFocus = false;
    %obj.rlLaserPointer = false;
    %obj.noFlagTouch = (%data.armorFlags & $ArmorFlags::NoFlagTouching) ? true : false;
    %obj.diffuseShield = false;
    %obj.phasedCloak = false;
    %obj.energyPackOverdrive = false;
    %obj.turbocharger = false;
    %obj.ultrasafeCloak = false;
    %obj.shieldExtended = false;
    %obj.repAegis = false;
    %obj.repSubspace = false;
    %obj.weaponJammed = false;
    %obj.omniJammer = false;
    %obj.ammoInventory = false;
    %obj.smPrecharge = false;
    %obj.repRifle = false;
    %obj.hasEVAC = false;
    %obj.assaultRPG = false;
    %obj.shoulderWeapon = false;
    %obj.nonNewtonian = false;
    %obj.lifeSupport = false;
    %obj.lifeSupportTicking = false;
    %obj.rsrcharvest = false;

    %obj.unmountImage($ShoulderSlot);
}

function Armor::postInstallEnhancements(%data, %obj)
{
    %obj.setHPLevel(%obj.maxHitPoints);
    %obj.setRechargeRate(%data.rechargeRate * %obj.rechargeRateFactor);

    if(%obj.bonusWeight != 0)
    {
        %obj.noFlagTouch = true;
        %weight = "";

        if(%obj.bonusWeight > 0)
        {
            if(%obj.bonusWeight <= 5)
                %weight = "ArmorWeightP5";
            else if(%obj.bonusWeight <= 10)
                %weight = "ArmorWeightP10";
            else if(%obj.bonusWeight <= 15)
                %weight = "ArmorWeightP15";
            else if(%obj.bonusWeight <= 20)
                %weight = "ArmorWeightP20";
            else if(%obj.bonusWeight <= 25)
                %weight = "ArmorWeightP25";
            else if(%obj.bonusWeight <= 30)
                %weight = "ArmorWeightP30";
            else if(%obj.bonusWeight <= 35)
                %weight = "ArmorWeightP35";
            else if(%obj.bonusWeight <= 40)
                %weight = "ArmorWeightP40";
            else if(%obj.bonusWeight <= 45)
                %weight = "ArmorWeightP45";
            else if(%obj.bonusWeight <= 50)
                %weight = "ArmorWeightP50";
            else if(%obj.bonusWeight <= 75)
                %weight = "ArmorWeightP75";
            else
                %weight = "ArmorWeightP100";
        }
        else if(%obj.bonusWeight < 0)
        {
            if(%obj.bonusWeight >= -5)
                %weight = "ArmorWeightM5";
            else if(%obj.bonusWeight >= -10)
                %weight = "ArmorWeightM10";
            else if(%obj.bonusWeight >= -15)
                %weight = "ArmorWeightM15";
            else if(%obj.bonusWeight >= -20)
                %weight = "ArmorWeightM20";
            else if(%obj.bonusWeight >= -25)
                %weight = "ArmorWeightM25";
            else if(%obj.bonusWeight >= -30)
                %weight = "ArmorWeightM30";
            else if(%obj.bonusWeight >= -35)
                %weight = "ArmorWeightM35";
            else if(%obj.bonusWeight >= -40)
                %weight = "ArmorWeightM40";
            else if(%obj.bonusWeight >= -45)
                %weight = "ArmorWeightM45";
            else if(%obj.bonusWeight >= -50)
                %weight = "ArmorWeightM50";
            else if(%obj.bonusWeight >= -75)
                %weight = "ArmorWeightM75";
            else
                %weight = "ArmorWeightM100";
        }

        %obj.mountImage(%weight, $flagslot);
    }

    if(%data.armorType == $ArmorType::Gearhead)
        setupGearhead(%data, %obj);
}

// Called on spawn and on inventory change
function Player::onSpawnInstance(%obj, %bSkipArmor)
{
    %data = %obj.getDatablock();

    %data.resetStats(%obj);
    %data.initTick(%obj);

    if(%bSkipArmor != true)
        %obj.installEnhancements($EnhancementType::Armor, %obj);
}


// Necessary because T2 is being retarded
function Player::getClassSize(%obj)
{
    %size = %obj.enhancementMultiplier;

    if(%obj.getInventory("ClassBooster") > 0)
        %size += 1;

    return %size;
}

// Shield Flash time (sec)
$g_ShieldFlashTime = 0.5;

function Player::shieldFlash(%obj)
{
//    if(%obj.shieldFlash > 0)
//        cancel(%obj.shieldFlash);

    %obj.setInvincibleMode(0.333, 0.05);

//    %obj.shieldFlash = %obj.schedule($g_ShieldFlashTime * 1000, "endShieldFlash");
}

function Player::endShieldFlash(%obj)
{
    if(%obj.shieldFlash > 0)
        cancel(%obj.shieldFlash);

    %obj.shieldFlash = 0;
}

function Armor::initTick(%data, %obj)
{
    if(%obj.ticking == true)
        return;

    %obj.ticking = true;
    %obj.tickThread = 0;
    %obj.isBot = %obj.client.isAIControlled();
    %data.schedule($g_TickTime, "onTick", %obj);
}

function Armor::onTick(%this, %obj)
{
    if(!isObject(%obj) || %obj.isDead)
        return false;

    if(%obj.getRepairRate() > 0)
        %obj.updateHPLevel();

    if(%obj.isWalker)
        %this.walkerTick(%obj);
    else if(%obj.tickCount % 32 == 0)
    {
        if(%obj.transient)
            %obj.setPosition(%obj.initialBuriedPos);

        %this.updateTeamStats(%obj);
    }

    %obj.tickCount++;
    %obj.tickThread = %this.schedule($g_TickTime, "onTick", %obj);

    return true;
}

function Armor::stopTick(%data, %obj)
{
    if(%obj.tickThread == 0)
        return;
        
    cancel(%obj.tickThread);
    %obj.tickThread = 0;
    %obj.ticking = false;
}

function Armor::updateTeamStats(%data, %obj)
{
    if(%obj.wearingShieldPack == true)
    {
        %shieldAmt = mFloor((%obj.shieldCap / %obj.shieldMax) * 100)@"%";
        
        messageClient(%obj.client, 'MsgSPCurrentObjective1', "", 'Shield: %6 | %2: %3 | %4: %5 | R: %1', $ResourceCount[%obj.client.team], $teamName[1], $teamScore[1], $teamName[2], $teamScore[2], %shieldAmt);
    }
    else
        messageClient(%obj.client, 'MsgSPCurrentObjective1', "", '%2: %3 | %4: %5 | Resources: %1', $ResourceCount[%obj.client.team], $teamName[1], $teamScore[1], $teamName[2], $teamScore[2]);
}

function Player::gib(%obj)
{
    if(%obj.isFragmented)
        return;

    %obj.isFragmented = true;
    %obj.isDead = true;
    %obj.blowUp();
}

function Player::ionDeathSequence(%obj)
{
    if(%obj.deathSequenceCount $= "")
    {
        %obj.isDead = true;
        %obj.deathSequenceMax = getRandom(24, 32);
        %obj.deathSequenceCount = 0;
    }

    if(%obj.deathSequenceCount > %obj.deathSequenceMax)
    {
        createRemoteProjectile("LinearFlareProjectile", "MagIonOLDeathCharge", vectorRand(), %obj.getWorldBoxCenter(), 0, %obj);
        %obj.gib();
    }
    else
    {
        projectileTrail("LinearFlareProjectile", "MagIonOLAreaCharge", vectorRand(), %obj.getWorldBoxCenter(), 0, %obj, 4);

        %obj.deathSequenceCount++;
        %obj.schedule(250, "ionDeathSequence");
    }
}

function deathTrigger(%player)
{
    %player.deathTriggered = !%player.deathTriggered;
    %player.setImageTrigger(0, %player.deathTriggered);

    if(%player.deathTriggered)
        schedule(5000 + getRandom(5000), %player, "deathTrigger", %player);
}

function Player::ejectFlag(%obj)
{
   if(%obj.holdingFlag)
   {
      %flag = %obj.holdingFlag;
      %flag.setVelocity("0 0 0");
      %flag.setTransform(%obj.getWorldBoxCenter());
      %flag.setCollisionTimeout(%obj);
      %obj.throwObject(%flag);
   }
}

function Armor::onDisabled(%this, %obj, %state)
{
    // keen: just for extra reinforcing
    %obj.isDead = true;

   if($MatchStarted)
   {
      %obj.startFade( 1000, $CorpseTimeoutValue - 1000, true );
      %obj.schedule( $CorpseTimeoutValue, "delete" );
   }
   else
      %obj.schedule( 150, "delete" );
}

function Armor::onRemove(%this, %obj)
{
   //Frohny asked me to remove this - all players are deleted now on mission cycle...
   //if(%obj.getState() !$= "Dead")
   //{
   //   error("ERROR PLAYER REMOVED WITHOUT DEATH - TRACE:");
   //   trace(1);
   //   schedule(0,0,trace,0);
   //}

   if (%obj.client.player == %obj)
      %obj.client.player = 0;
      
    if(%obj.isWalker == true)
        %this.unmountPilot(%obj);
}

function Player::liquidDamage(%obj, %data, %damageAmount, %damageType)
{
   if(%obj.getState() !$= "Dead")
   {
      %data.damageObject(%obj, 0, "0 0 0", %damageAmount, %damageType);
      %obj.lDamageSchedule = %obj.schedule(50, "liquidDamage", %data, %damageAmount, %damageType);

//      if(%obj.isWalker)
//        %obj.walkerInLava($DamageLava, $DamageType::Lava);
   }
   else
      %obj.lDamageSchedule = "";
}

function Armor::onEnterLiquid(%data, %obj, %coverage, %type)
{
   switch(%type)
   {
      case 0:
         //Water
        %obj.isWet = true;
      case 1:
         //Ocean Water
        %obj.isWet = true;
      case 2:
         //River Water
        %obj.isWet = true;
      case 3:
         //Stagnant Water
        %obj.isWet = true;
      case 4:
         //Lava
         %obj.liquidDamage(%data, $DamageLava, $DamageType::Lava);
      case 5:
         //Hot Lava
         %obj.liquidDamage(%data, $DamageHotLava, $DamageType::Lava);
      case 6:
         //Crusty Lava
         %obj.liquidDamage(%data, $DamageCrustyLava, $DamageType::Lava);
      case 7:
         //Quick Sand
   }
}

function Armor::onLeaveLiquid(%data, %obj, %type)
{
   switch(%type)
   {
      case 0:
         //Water
         %obj.isWet = false;
      case 1:
         //Ocean Water
         %obj.isWet = false;
      case 2:
         //River Water
         %obj.isWet = false;
      case 3:
         //Stagnant Water
         %obj.isWet = false;
      case 4:
         //Lava
      case 5:
         //Hot Lava
      case 6:
         //Crusty Lava
      case 7:
         //Quick Sand
   }

   if(%obj.lDamageSchedule !$= "")
   {
      cancel(%obj.lDamageSchedule);
      %obj.lDamageSchedule = "";
   }
}

function Armor::onMount(%this,%obj,%vehicle,%node)
{
   if(%obj.isWalker)
      return;

   if (%node == 0)
   {
      // Node 0 is the pilot's pos.
      %obj.setTransform("0 0 0 0 0 1 0");
      %obj.setActionThread(%vehicle.getDatablock().mountPose[%node],true,true);

      if(!%obj.inStation)
         %obj.lastWeapon = (%obj.getMountedImage($WeaponSlot) == 0 ) ? "" : %obj.getMountedImage($WeaponSlot).getName().item;

       %obj.unmountImage($WeaponSlot);

      if(!%obj.client.isAIControlled())
      {
         %obj.setControlObject(%vehicle);
         %obj.client.setObjectActiveImage(%vehicle, 2);
      }

      //E3 respawn...

      if(%obj == %obj.lastVehicle.lastPilot && %obj.lastVehicle != %vehicle)
      {
         schedule(15000, %obj.lastVehicle,"vehicleAbandonTimeOut", %obj.lastVehicle);
          %obj.lastVehicle.lastPilot = "";
      }
      if(%vehicle.lastPilot !$= "" && %vehicle == %vehicle.lastPilot.lastVehicle)
            %vehicle.lastPilot.lastVehicle = "";

      %vehicle.abandon = false;
      %vehicle.lastPilot = %obj;
      %obj.lastVehicle = %vehicle;

      // update the vehicle's team
      if((%vehicle.getTarget() != -1) && %vehicle.getDatablock().cantTeamSwitch $= "")
      {
         setTargetSensorGroup(%vehicle.getTarget(), %obj.client.getSensorGroup());
         if( %vehicle.turretObject > 0 )
            setTargetSensorGroup(%vehicle.turretObject.getTarget(), %obj.client.getSensorGroup());
      }

      // Send a message to the client so they can decide if they want to change view or not:
      commandToClient( %obj.client, 'VehicleMount' );

   }
   else
   {
      // tailgunner/passenger positions
      if(%vehicle.getDataBlock().mountPose[%node] !$= "")
         %obj.setActionThread(%vehicle.getDatablock().mountPose[%node]);
      else
         %obj.setActionThread("root", true);
   }
   // z0dd - ZOD, 6/27/02. announce to any other passengers that you've boarded
   if(%vehicle.getDatablock().numMountPoints > 1)
   {
      %nodeName = findNodeName(%vehicle, %node); // function in vehicle.cs
      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
      {
         if (%vehicle.getMountNodeObject(%i) > 0)
         {
            if(%vehicle.getMountNodeObject(%i).client != %obj.client)
            {
               %team = (%obj.team == %vehicle.getMountNodeObject(%i).client.team ? 'Teammate' : 'Enemy');
               messageClient( %vehicle.getMountNodeObject(%i).client, 'MsgShowPassenger', '\c2%3: \c3%1\c2 has boarded in the \c3%2\c2 position.', %obj.client.name, %nodeName, %team );
            }
            commandToClient( %vehicle.getMountNodeObject(%i).client, 'showPassenger', %node, true);
         }
      }
   }
   //make sure they don't have any packs active
//    if ( %obj.getImageState( $BackpackSlot ) $= "activate")
//       %obj.use("Backpack");
   if ( %obj.getImageTrigger( $BackpackSlot ) )
      %obj.setImageTrigger( $BackpackSlot, false );

   //AI hooks
   %obj.client.vehicleMounted = %vehicle;
   AIVehicleMounted(%vehicle);
   if(%obj.client.isAIControlled())
      %this.AIonMount(%obj, %vehicle, %node);
}

function Armor::onUnmount( %this, %obj, %vehicle, %node )
{
   if ( %node == 0 )
   {
      commandToClient( %obj.client, 'VehicleDismount' );
      commandToClient(%obj.client, 'removeReticle');

      if(%obj.inv[%obj.lastWeapon])
         %obj.use(%obj.lastWeapon);

      if(%obj.getMountedImage($WeaponSlot) == 0)
         %obj.selectWeaponSlot( 0 );

      //Inform gunner position when pilot leaves...
      //if(%vehicle.getDataBlock().showPilotInfo !$= "")
      //   if((%gunner = %vehicle.getMountNodeObject(1)) != 0)
      //      commandToClient(%gunner.client, 'PilotInfo', "PILOT EJECTED", 6, 1);
   }
   // z0dd - ZOD, 6/27/02. announce to any other passengers that you've left
   if(%vehicle.getDatablock().numMountPoints > 1)
   {
      %nodeName = findNodeName(%vehicle, %node); // function in vehicle.cs
      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
      {
         if (%vehicle.getMountNodeObject(%i) > 0)
         {
            if(%vehicle.getMountNodeObject(%i).client != %obj.client)
            {
               %team = (%obj.team == %vehicle.getMountNodeObject(%i).client.team ? 'Teammate' : 'Enemy');
               messageClient( %vehicle.getMountNodeObject(%i).client, 'MsgShowPassenger', '\c2%3: \c3%1\c2 has ejected from the \c3%2\c2 position.', %obj.client.name, %nodeName, %team );
            }
            commandToClient( %vehicle.getMountNodeObject(%i).client, 'showPassenger', %node, false);
         }
      }
   }
   //AI hooks
   %obj.client.vehicleMounted = "";
   if(%obj.client.isAIControlled())
      %this.AIonUnMount(%obj, %vehicle, %node);
}

function Armor::onCollision(%this,%obj,%col,%forceVehicleNode)
{
   if (%obj.getState() $= "Dead")
      return;

   if(%obj.isWalker)
   {
      %this.onWalkerCollision(%obj, %col);
      return;
   }

   %dataBlock = %col.getDataBlock();
   %className = %dataBlock.className;
   %client = %obj.client;
   // player collided with a vehicle
   %node = -1;

   if(%forceVehicleNode !$= "" || %col.isVehicle() && %obj.mountVehicle && %obj.getState() $= "Move" && %col.mountable && !%obj.inStation && %col.getDamageState() !$= "Destroyed")
   {
      //if the player is an AI, he should snap to the mount points in node order,
      //to ensure they mount the turret before the passenger seat, regardless of where they collide...
      if (%obj.client.isAIControlled())
      {
         %transform = %col.getTransform();

         //either the AI is *required* to pilot, or they'll pick the first available passenger seat
         if (%client.pilotVehicle)
         {
            //make sure the bot is in light armor
            if(%this.armorFlags & $ArmorFlags::CanPilot)
            {
               //make sure the pilot seat is empty
               if (!%col.getMountNodeObject(0))
                  %node = 0;
            }
         }
         else
            %node = findAIEmptySeat(%col, %obj);
      }
      else
         %node = findEmptySeat(%col, %obj, %forceVehicleNode);

      //now mount the player in the vehicle
      if(%node >= 0)
      {
         // players can't be pilots, bombardiers or turreteers if they have
         // "large" packs -- stations, turrets, turret barrels
         if(hasLargePack(%obj)) {
            // check to see if attempting to enter a "sitting" node
            if(nodeIsSitting(%datablock, %node)) {
               // send the player a message -- can't sit here with large pack
               if(!%obj.noSitMessage)
               {
                  %obj.noSitMessage = true;
                  %obj.schedule(2000, "resetSitMessage");
                  messageClient(%obj.client, 'MsgCantSitHere', '\c2Pack too large, can\'t occupy this seat.~wfx/misc/misc.error.wav');
               }
               return;
            }
         }
         if(%col.noEnemyControl && %obj.team != %col.team)
            return;

         commandToClient(%obj.client,'SetDefaultVehicleKeys', true);
         //If pilot or passenger then bind a few extra keys
         if(%node == 0)
            commandToClient(%obj.client,'SetPilotVehicleKeys', true);
         else
            commandToClient(%obj.client,'SetPassengerVehicleKeys', true);

         if(!%obj.inStation)
            %col.lastWeapon = ( %col.getMountedImage($WeaponSlot) == 0 ) ? "" : %col.getMountedImage($WeaponSlot).getName().item;
         else
            %col.lastWeapon = %obj.lastWeapon;

         %col.mountObject(%obj,%node);
         %col.playAudio(0, MountVehicleSound);
         %obj.mVehicle = %col;

			// if player is repairing something, stop it
			if(%obj.repairing)
				stopRepairing(%obj);

         //this will setup the huds as well...
         %dataBlock.playerMounted(%col,%obj, %node);
      }
   }
   else if (%className $= "Armor") {
      // player has collided with another player
      if(%col.getState() $= "Dead") {
         %gotSomething = false;
         // it's corpse-looting time!
         // weapons -- don't pick up more than you are allowed to carry!
         for(%i = 0; ( %obj.weaponCount < %obj.getDatablock().maxWeapons ) && $InvWeapon[%i] !$= ""; %i++)
         {
            %weap = $NameToInv[$InvWeapon[%i]];
            if ( %col.hasInventory( %weap ) )
            {
               if ( %obj.incInventory(%weap, 1) > 0 )
               {
                  %col.decInventory(%weap, 1);
                  %gotSomething = true;
                  messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %weap.pickUpName);
               }
            }
         }
         // targeting laser:
         if ( %col.hasInventory( "TargetingLaser" ) )
         {
            if ( %obj.incInventory( "TargetingLaser", 1 ) > 0 )
            {
               %col.decInventory( "TargetingLaser", 1 );
               %gotSomething = true;
               messageClient( %obj.client, 'MsgItemPickup', '\c0You picked up a targeting laser.' );
            }
         }
         // ammo
         for(%j = 0; $ammoType[%j] !$= ""; %j++)
         {
            %ammoAmt = %col.inv[$ammoType[%j]];
            if(%ammoAmt)
            {
               // incInventory returns the amount of stuff successfully grabbed
               %grabAmt = %obj.incInventory($ammoType[%j], %ammoAmt);
               if(%grabAmt > 0)
               {
                  %col.decInventory($ammoType[%j], %grabAmt);
                  %gotSomething = true;
                  messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', $ammoType[%j].pickUpName);
                  %obj.client.setWeaponsHudAmmo($ammoType[%j], %obj.getInventory($ammoType[%j]));
               }
            }
         }
         // figure out what type, if any, grenades the (live) player has
         %playerGrenType = "None";
         for(%x = 0; $InvGrenade[%x] !$= ""; %x++) {
            %gren = $NameToInv[$InvGrenade[%x]];
            %playerGrenAmt = %obj.inv[%gren];
            if(%playerGrenAmt > 0)
            {
               %playerGrenType = %gren;
               break;
            }
         }
         // grenades
         for(%k = 0; $InvGrenade[%k] !$= ""; %k++)
         {
            %gren = $NameToInv[$InvGrenade[%k]];
            %corpseGrenAmt = %col.inv[%gren];
            // does the corpse hold any of this grenade type?
            if(%corpseGrenAmt)
            {
               // can the player pick up this grenade type?
               if((%playerGrenType $= "None") || (%playerGrenType $= %gren))
               {
                  %taken = %obj.incInventory(%gren, %corpseGrenAmt);
                  if(%taken > 0)
                  {
                     %col.decInventory(%gren, %taken);
                     %gotSomething = true;
                     messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %gren.pickUpName);
                     %obj.client.setInventoryHudAmount(%gren, %obj.getInventory(%gren));
                  }
               }
               break;
            }
         }
         // figure out what type, if any, mines the (live) player has
         %playerMineType = "None";
         for(%y = 0; $InvMine[%y] !$= ""; %y++)
         {
            %mType = $NameToInv[$InvMine[%y]];
            %playerMineAmt = %obj.inv[%mType];
            if(%playerMineAmt > 0)
            {
               %playerMineType = %mType;
               break;
            }
         }
         // mines
         for(%l = 0; $InvMine[%l] !$= ""; %l++)
         {
            %mine = $NameToInv[$InvMine[%l]];
            %mineAmt = %col.inv[%mine];
            if(%mineAmt) {
               if((%playerMineType $= "None") || (%playerMineType $= %mine))
               {
                  %grabbed = %obj.incInventory(%mine, %mineAmt);
                  if(%grabbed > 0)
                  {
                     %col.decInventory(%mine, %grabbed);
                     %gotSomething = true;
                     messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %mine.pickUpName);
                     %obj.client.setInventoryHudAmount(%mine, %obj.getInventory(%mine));
                  }
               }
               break;
            }
         }
         // beacons
         %beacAmt = %col.inv[Beacon];
         if(%beacAmt)
         {
            %bTaken = %obj.incInventory(Beacon, %beacAmt);
            if(%bTaken > 0)
            {
               %col.decInventory(Beacon, %bTaken);
               %gotSomething = true;
               messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', Beacon.pickUpName);
               %obj.client.setInventoryHudAmount(Beacon, %obj.getInventory(Beacon));
            }
         }
         // repair kit
         %rkAmt = %col.inv[RepairKit];
         if(%rkAmt)
         {
            %rkTaken = %obj.incInventory(RepairKit, %rkAmt);
            if(%rkTaken > 0)
            {
               %col.decInventory(RepairKit, %rkTaken);
               %gotSomething = true;
               messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', RepairKit.pickUpName);
               %obj.client.setInventoryHudAmount(RepairKit, %obj.getInventory(RepairKit));
            }
         }
      }
      if(%gotSomething)
         %col.playAudio(0, CorpseLootingSound);
   }
}

// todo: dynamic method for vehicle to define sitting nodes
function nodeIsSitting(%vehDBlock, %node)
{
   // pilot == always a "sitting" node
   if(%node == 0)
      return true;
   else {
      switch$ (%vehDBlock.getName())
      {
         // note: for assault tank -- both nodes are sitting
         // for any single-user vehicle -- pilot node is sitting
         case "BomberFlyer":
            // bombardier == sitting; tailgunner == not sitting
            if(%node == 1)
               return true;
            else
               return false;
         case "HAPCFlyer":
            // only the pilot node is sitting
            return false;
         default:
            return true;
      }
   }
}

function Armor::onShieldDamage(%data, %obj, %position, %amount, %damageType, %element)
{
    if(%element == $DamageGroupMask::Sonic)
        return %amount;

    if(%obj.lastHitFlags & $Projectile::ArmorOnly)
        %amount = 0;
    else if(%obj.lastHitFlags & $Projectile::ShieldsOnly)
        %amount *= 2;

    %shieldScale = %data.shieldDamageScale[%damageType];

    if(%shieldScale $= "")
        %shieldScale = 1;
    
    %amount *= %obj.shieldFactor;
    %amount *= %shieldScale;
    %amount *= %obj.shieldDamageFactor[%element];
    %amount = mCeil(%amount);

    if(%amount < 1)
    {
        %obj.playShieldEffect("0.0 0.0 1.0");
        %obj.shieldFlash();
        return 0;
    }

    if(%obj.shieldRecharging)
    {
        messageClient(%obj.client, 'MsgShieldPackRecFail', '\c2Shield emitter recharging interrupted.');
        %obj.shieldRecharging = false;
    }

    if(%obj.shieldCap > %amount)
    {
        %obj.shieldCap -= %amount;
        %obj.playShieldEffect("0.0 0.0 1.0");
        %obj.shieldFlash();
        
        return 0;
    }

    if(%obj.shieldCap > 0)
    {
        messageClient(%obj.client, 'MsgShieldPackDepl', '\c2Shield capacitor depleted!');
        commandToClient(%obj.client, 'setShieldIconOff');
    }

    %amount -= %obj.shieldCap;
    %obj.shieldCap = 0;

    if(%obj.lastHitFlags & $Projectile::ShieldsOnly)
        %amount = 0;
    
    return %amount;
}

function Armor::onShieldDamageGP(%data, %obj, %position, %amount, %damageType, %element)
{
    if(%element == $DamageGroupMask::Sonic)
        return %amount;

    if(%obj.lastHitFlags & $Projectile::ArmorOnly)
        %amount = 0;
    else if(%obj.lastHitFlags & $Projectile::ShieldsOnly)
        %amount *= 2;
        
    %shieldScale = %data.shieldDamageScale[%damageType];

    if(%shieldScale $= "")
        %shieldScale = 1;

    %amount *= %obj.shieldFactor;
    %amount *= %shieldScale;
    %amount *= %obj.shieldDamageFactor[%element];
    %amount = mCeil(%amount);
    
    if(%obj.lastHitFlags & $Projectile::ShieldsOnly)
        %amount = 0;
    
    if(%amount < 1)
    {
        %obj.playShieldEffect("0.0 0.0 1.0");
        %obj.shieldFlash();
        return 0;
    }
    
    if(%obj.gyanShieldPool > %amount)
    {
        %obj.gyanShieldPool -= %amount;
        %obj.playShieldEffect("0.0 0.0 1.0");
        %obj.shieldFlash();

        return 0;
    }

    %amount -= %obj.gyanShieldPool;
    %obj.gyanShieldPool = 0;
    triggerSpecialMagneticIonE(%data, %obj, 0);

    return %amount;
}

function Armor::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC, %srcProj, %element)
{
   if(%targetObject.invincible || %targetObject.getState() $= "Dead") // magic constant time!
      return;

   if(%element $= "")
      %element = $DamageGroupMask::Misc;

   %amount = mRound(%amount);
   
   if(%targetObject.isWalker)
   {
      %data.damageWalker(%targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC, %srcProj, %element);
      return;
   }

   //----------------------------------------------------------------
   // z0dd - ZOD, 6/09/02. Check to see if this vehicle is destroyed,
   // if it is do no damage. Fixes vehicle ghosting bug. We do not
   // check for isObject here, destroyed objects fail it even though
   // they exist as objects, go figure.
   if(%damageType == $DamageType::Impact)
      if(%sourceObject.getDamageState() $= "Destroyed")
         return;

   if(%targetObject.isMounted() && !%targetObject.scriptKilled && ((%targetObject.lastHitFlags & $Projectile::Piercing) == 0))
   {
      %mount = %targetObject.getObjectMount();
      if(%mount.team == %targetObject.team)
      {
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if (%mount.getDataBlock().isProtectedMountPoint[%found])
            {
               %mount.getDataBlock().damageObject(%mount, %sourceObject, %position, %amount, %damageType);
               return;
            }
         }
      }
   }

   %targetClient = %targetObject.getOwnerClient();
   if(isObject(%mineSC))
      %sourceClient = %mineSC;
   else
      %sourceClient = isObject(%sourceObject) ? %sourceObject.getOwnerClient() : 0;

   %targetTeam = %targetClient.team;

   //if the source object is a player object, player's don't have sensor groups
   // if it's a turret, get the sensor group of the target
   // if its a vehicle (of any type) use the sensor group
   if (%sourceClient)
      %sourceTeam = %sourceClient.getSensorGroup();
   else if(%damageType == $DamageType::Suicide)
      %sourceTeam = 0;
   //--------------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 4/8/02. Check to see if this turret has a valid owner, if not clear the variable.
   else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   {
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      if(%sourceObject.owner !$="" && (%sourceObject.owner.team != %sourceObject.team || !isObject(%sourceObject.owner)))
      {
         %sourceObject.owner = "";
      }
   }
   //--------------------------------------------------------------------------------------------------------------------
   else if( isObject(%sourceObject) && %sourceObject.isVehicle())
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
   else
   {
      if (isObject(%sourceObject) && %sourceObject.getTarget() >= 0 )
      {
         %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      }
      else
      {
         %sourceTeam = -1;
      }
   }

   // if teamdamage is off, and both parties are on the same team
   // (but are not the same person), apply no damage
   if(!$teamDamage && (%targetClient != %sourceClient) && (%targetTeam == %sourceTeam))
      return;

    // Convert damage calculation here:
   if(%targetObject.isShielded && (%targetObject.lastHitFlags & $Projectile::IgnoreShields) == 0)
   {
      if(%targetObject.shieldSource > 0)
          %amount = %data.onShieldDamage(%targetObject.shieldSource, %position, %amount, %damageType, %element);
      else if(%targetObject.gyanShieldSeconds > 0)
          %amount = %data.onShieldDamageGP(%targetObject, %position, %amount, %damageType, %element);
      else
          %amount = %data.onShieldDamage(%targetObject, %position, %amount, %damageType, %element);
   }

       %amount *= %targetObject.damageReduceFactor;
       %amount *= %targetObject.armorDamageFactor[%element];

       //%damageScale = $InheritDamageProfile[%damageType] !$= "" ? %data.damageScale[$InheritDamageProfile[%damageType]] : %data.damageScale[%damageType];
       %damageScale = %data.damageScale[%damageType];

       if(%damageScale !$= "")
          %amount *= %damageScale;

       if(%targetObject.damageReduction > 0 && %amount > 0 && %targetObject.hitPoints > 0)
       {
          %dr = %targetObject.hitPoints > %targetObject.damageReduction ? %targetObject.hitPoints : %targetObject.damageReduction;

          if(%dr > 0)
          {
              if(%amount >= %dr)
                %amount -= %dr;
              else
                %amount = 0;
          }
       }
       
       if(%amount == 0 && %targetObject.internalHealth > 0)
          return;

        if(%targetObject.lastHitFlags & $Projectile::ArmorOnly)
            %amount *= 2;

//   if(%targetObject.ammoInventory == true && %targetObject.hasAmmoPack == true)
//   {
//      if(getRandom() > 0.96 )
//          createRemoteProjectile("LinearFlareProjectile", "MagIonOLDeathCharge", vectorRand(), %obj.getWorldBoxCenter(), 0, %obj);
//   }

        // Set the damage flash
        if(%amount > 3)
            %flash = %targetObject.getDamageFlash() + (%amount * 0.01);
        else
            %flash = %amount * 0.1;

        if(%flash > 0.75)
           %flash = 0.75;

        %previousDamage = %targetObject.getDamagePercent();
        %targetObject.setDamageFlash(%flash);

        if(%targetObject.lastHitFlags & $Projectile::InternalDamage)
            %targetObject.applyInternalDamage(mCeil(%amount), %sourceObject, %damageType);
        else
            %targetObject.applyHPDamage(mRound(%amount));            // compute delta HP damage

        if(%targetObject.internalHealth < 1)
            %targetObject.setDamageLevel(%data.maxDamage);

        Game.onClientDamaged(%targetClient, %sourceClient, %damageType, %sourceObject);

        if(%damagingClient != %targetClient.lastDamagedBy)
        {
            %targetClient.assistDamagedBy = %targetClient.lastDamagedBy;
            %targetClient.lastDamagedBy = %damagingClient;
        }

        %targetClient.lastDamaged = getSimTime();
        %targetClient.lastDamageType = %damageType;
    
   //now call the "onKilled" function if the client was... you know...
   if(%targetObject.getState() $= "Dead" || (%targetObject.hasEVAC && %targetObject.hitPoints < 1))
   {
      // prevent multi-looping death code (prevents straight-to-observer bugs)
      if(%targetObject.didDeathSequence == true)
        return;

      %targetObject.didDeathSequence = true;

      // Eject from walker if mounted in one (suicide/teamchange)
      if(isObject(%targetClient.walker))
         %targetClient.walker.getDatablock().unmountPilot(%targetClient.walker);
      
      // where did this guy get it?
      %damLoc = %targetObject.getDamageLocation(%position);

      // should this guy be blown apart?
      // keen: let projectile determine that
      if(%targetObject.getDatablock().armorFlags & $ArmorFlags::ExplodeOnDeath && (%targetObject.lastHitFlags & $Projectile::InternalDamage) == 0)
      {
        %targetObject.setMomentumVector(%momVec);
        %targetObject.ionDeathSequence();
      }
      else if(%targetObject.lastHitFlags & $Projectile::PlayerFragment)
      {
        %targetObject.setMomentumVector(%momVec);
        %targetObject.gib();
      }
      else if(%damageType == $DamageType::VehicleSpawn)
      {
         %targetObject.setMomentumVector("0 0 1");
         %targetObject.gib();
      }
      else if(getRandom() > 0.625)
          deathTrigger(%targetObject);

      // If we were killed, max out the flash
      %targetObject.setDamageFlash(0.75);

      if(isObject(%sourceObject) && %sourceObject.isPlayer() && (%sourceObject.client.team != %targetObject.client.team) && !isObject(%sourceObject.client.walker))
      {
          %sourceObject.incGyan(1);

          if(%sourceObject.nosferatu)
          {
             %sourceObject.incHP(mCeil(%sourceObject.maxHitPoints * 0.1));
             %sourceObject.useEnergy(%sourceObject.getDatablock().maxEnergy * -0.1);
          }
          else if(%sourceObject.rsrcharvest)
             $ResourceCount[%sourceObject.client.team] += 50 + (%sourceObject.client.currentRank * 3);
      }

      Game.onClientKilled(%targetClient, %sourceClient, %damageType, %sourceObject, %damLoc);
   }
//   else if ( %amount > 0.1 )
//   {
//      if( %targetObject.station $= "" && %targetObject.isCloaked() )
//      {
//         %targetObject.setCloaked( false );
//         %targetObject.reCloak = %targetObject.schedule( 500, "setCloaked", true );
//      }

    if(getRandom() > 0.85)
        playPain(%targetObject);

    // keen: reset so we don't get other damage bleedover
    %targetObject.lastHitFlags = 0;
//   }
}

function Armor::onImpact(%data, %playerObject, %collidedObject, %vec, %vecLen)
{
    if(%playerObject.nonNewtonian)
        return;
    else if(%playerObject.isShielded != true)
    {
        %playerObject.doInternalDamage(%playerObject, mCeil(%vecLen * 0.02071), $DamageType::Ground, $DamageGroupMask::ExternalMeans, 0); // element, srcproj
        playPain(%playerObject);
    }
    else
        %data.damageObject(%playerObject, 0, VectorAdd(%playerObject.getPosition(),%vec), %vecLen * %data.speedDamageScale, $DamageType::Ground, %vec, %collidedObject.client, 0, $DamageGroupMask::Kinetic);
}

function Player::showInternalHealth(%obj)
{
    if(%obj.client.isAIControlled())
        return;

    %hGraph = createColorGraph("#", %obj.internalHealthMax, (%obj.internalHealth / %obj.internalHealthMax), "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.333, 0.666);
    commandToClient(%obj.client, 'BottomPrint', "Health: "@%hGraph SPC mCeil(%obj.internalHealth)@" HP", 10, 1);
    %obj.updateWeaponMode();
}

function Player::incGyan(%obj, %gyan)
{
    %obj.gyanLevel += %gyan;

    updateStat(%obj.client.guid, "gpcollected", %gyan);

    if(%obj.gyanLevel > %obj.gyanMax)
        %obj.gyanLevel = %obj.gyanMax;

    %obj.updateReticleAmmo();
}

function Player::decGyan(%obj, %gyan)
{
    %obj.gyanLevel -= %gyan;

    if(%obj.gyanMax < 0)
        %obj.gyanMax = 0;

    %obj.updateReticleAmmo();
}

function Player::setGyan(%obj, %gyan)
{
    %obj.gyanLevel = %gyan;

    if(%obj.gyanLevel > %obj.gyanMax)
        %obj.gyanLevel = %obj.gyanMax;

    if(%obj.gyanLevel < 0)
        %obj.gyanLevel = 0;

    %obj.updateReticleAmmo();
}

function Player::setArmor(%this, %size)
{
   // Takes size as "Light","Medium", "Heavy"
   %client = %this.client;
   if (%client.race $= "Bioderm")
      // Only have male bioderms.
      %armor = %size @ "Male" @ %client.race @ Armor;
   else
      %armor = %size @ %client.sex @ %client.race @ Armor;
   //echo("Player::armor: " @ %armor);
   %this.setDataBlock(%armor);
   %client.armor = %size;
}

function Player::getArmorSize(%this)
{
    switch(%this.getDatablock().armorType)
    {
        case $ArmorType::Scout:
            return "Light";

        case $ArmorType::Assault:
            return "Medium";

        case $ArmorType::MagneticIon:
            return "Heavy";

        case $ArmorType::Gearhead:
            return "Gearhead";

        case $ArmorType::BattleAngel:
            return "BattleAngel";

        case $ArmorType::Plugsuit:
            return "Plugsuit";

        case $ArmorType::Brawler:
            return "Brawler";

        case $ArmorType::Walker:
            return "Walker";

        default:
            return "Unknown";
    }
}

function Player::getArmorType(%this)
{
   return %this.getDataBlock().armorMask;
}

function Player::getArmorClass(%this)
{
   return %this.getDataBlock().armorClass;
}

function Player::pickup(%this,%obj,%amount)
{
   %data = %obj.getDataBlock();
   // Don't pick up a pack if we already have one mounted
   if (%data.className $= Pack &&
         %this.getMountedImage($BackpackSlot) != 0)
      return 0;
	// don't pick up a weapon (other than targeting laser) if player's at maxWeapons
   else if(%data.className $= Weapon
     && %data.getName() !$= "TargetingLaser"  // Special case
     && %this.weaponCount >= %this.getDatablock().maxWeapons)
      return 0;
	// don't allow players to throw large packs at pilots (thanks Wizard)
   else if(%data.className $= Pack && %data.image.isLarge && %this.isPilot())
		return 0;
   return ShapeBase::pickup(%this,%obj,%amount);
}

function Player::isPilot(%this)
{
   %vehicle = %this.getObjectMount();
   // There are two "if" statements to avoid a script warning.
   if (%vehicle)
      if (%vehicle.getMountNodeObject(0) == %this)
         return true;
   return false;
}

function Player::isWeaponOperator(%this)
{
   %vehicle = %this.getObjectMount();
   if ( %vehicle )
   {
      %weaponNode = %vehicle.getDatablock().weaponNode;
      if ( %weaponNode > 0 && %vehicle.getMountNodeObject( %weaponNode ) == %this )
         return( true );
   }

   return( false );
}


function Player::use( %this,%data )
{
   // If player is in a station then he can't use any items
   if(%this.station !$= "")
      return false;

    if(%this.isWalker)
        return %this.walkerUse(%data);

   // todo: Link TL to Armor Speciality
//   if(%data $= "TargetingLaser")
//   {
//       return;
//   }

   // Convert the word "Backpack" to whatever is in the backpack slot.
   if(%data $= "Backpack")
   {
      if(%this.inStation)
         return false;

      // keen: assume we only mount vehicles
      if((%vehicle = %this.getObjectMount()))
      {
          if(%this.isPilot())
          {
                %vehicle.getDatablock().triggerPack(%vehicle, %this);
                return;
          }
          else if(%this.isWeaponOperator())
          {
                %vehicle.getDatablock().turretTriggerPack(%vehicle, %this, %this.getControlObject());
                return;
          }
      }
//      else if(%this.isWeaponOperator())
//      {
//            messageClient( %this.client, 'MsgCantUsePack', '\c2You can\'t use your pack while in a weaponry position.~wfx/misc/misc.error.wav' );
//            return false;
//      }

//         messageClient( %this.client, 'MsgCantUsePack', '\c2You can\'t use your pack while piloting.~wfx/misc/misc.error.wav' );
//         messageClient( %this.client, 'MsgCantUsePack', '\c2You can\'t use your pack while in a weaponry position.~wfx/misc/misc.error.wav' );

      %image = %this.getMountedImage($BackpackSlot);

      if(%image)
         %data = %image.item;
   }

   // Can't use some items when piloting or your a weapon operator
   if ( %this.isPilot() || %this.isWeaponOperator() )
      if ( %data.getName() !$= "RepairKit" )
         return false;

   return ShapeBase::use( %this, %data );
}

function Armor::onTrigger(%data, %player, %triggerNum, %val)
{
    if(%player.isWalker)
    {
        %data.walkerTrigger(%player, %triggerNum, %val);
        return;
    }

    if (%triggerNum == 4)
    {
      %shoulder = %player.getMountedImage($ShoulderSlot);
      
      if(%shoulder && %shoulder.isShoulderWeapon == true)
          %player.setImageTrigger($ShoulderSlot, %val);
      else if(%val && isObject((%veh = %player.getObjectMount())) && %veh.isVehicle() && %veh.getMountNodeObject(0) == %player)
      {
          %vehData = %veh.getDatablock();
          %vehData.triggerGrenade(%veh, %player);
          return;
      }
      else if (%val == 1) // Throw grenade
      {
         %player.grenTimer = 1;
      }
      else
      {
         if (%player.grenTimer == 0)
         {
            // Bad throw for some reason
         }
         else
         {
            for(%i = 0; %i < $GrenadeCount; %i++)
            {
                if(%player.getInventory($NameToInv[$InvGrenade[%i]]))
                {
                    %player.use($NameToInv[$InvGrenade[%i]]);
                    break;
                }
            }
//            %player.use(Grenade);
            %player.grenTimer = 0;
         }
      }
   }
   else if (%triggerNum == 5)
   {
      %veh = %player.getObjectMount();
      if(isObject(%veh) && %veh.isVehicle())
      {
//          if(%veh.getMountNodeObject(0) == %player)
//              return;

          %vehData = %veh.getDatablock();
          %vehData.triggerMine(%veh, %player, %vehData.getMountObjectNode(%veh, %player), %val);
          return;
      }

      // Throw mine
      if (%val == 1)
      {
         %player.mineTimer = 1;
      }
      else
      {
         if (%player.mineTimer == 0)
         {
            // Bad throw for some reason
         }
         else
         {
            for(%i = 0; %i < $MineCount; %i++)
            {
                if(%player.getInventory($NameToInv[$InvMine[%i]]))
                {
                    %player.use($NameToInv[$InvMine[%i]]);
                    break;
                }
            }

            %player.mineTimer = 0;
         }
      }
   }
   else if (%triggerNum == 3)
   {
      // val = 1 when jet key (LMB) first pressed down
      // val = 0 when jet key released
      // MES - do we need this at all any more?
      %player.isJetting = %val;
   }
}

function Armor::onDamage(%data, %obj)
{
   if(%obj.station !$= "" && %obj.getDamageLevel() == 0)
      %obj.station.getDataBlock().endRepairing(%obj.station);
}

function Armor::applyConcussion( %this, %dist, %radius, %sourceObject, %targetObject )
{
   if(%targetObject.magneticClamp == true || %targetObject.isWalker == true)
      return;

   %percentage = 1 - ( %dist / %radius );
   %random = getRandom();

   if( %sourceObject == %targetObject )
   {
      %flagChance = 1.0;
      %itemChance = 1.0;
   }
   else
   {
      %flagChance = 0.7;
      %itemChance = 0.7;
   }

   %probabilityFlag = %flagChance * %percentage;
   %probabilityItem = %itemChance * %percentage;

   if( %random <= %probabilityFlag )
      Game.applyConcussion( %targetObject );

   if(%random <= %probabilityItem)
   {
        %targetObject.throwPack();
        %targetObject.throwWeapon();
   }
}

function Player::maxInventory(%this, %data)
{
   %max = ShapeBase::maxInventory(%this, %data);
   %max = mCeil(%max * %this.maxAmmoCapacityFactor);
   %item = %data.getName();

   if(%this.ammoCapacityBonus[%item] !$= "")
    %max += %this.ammoCapacityBonus[%item];

  if (%this.getInventory(AmmoPack))
      %max += AmmoPack.max[%item];

   return %max;
}

// TL override for armor specials
function TargetingLaserImage::onMount(%this,%obj,%slot)
{
    %data = %obj.getDatablock();
    %obj.armorSpecials++;

    if(%obj.armorSpecials > %data.armorSpecialCount)
        %obj.armorSpecials = 0;

    %obj.unmountImage(%slot);
    %obj.mountImage(%data.armorSpecial[%obj.armorSpecials], %slot);

    if(%obj.armorSpecials > 0)
        commandToClient(%obj.client, 'setRepairReticle');
}

function TargetingLaserFakeImage::onFire(%data,%obj,%slot)
{
   %p = Parent::onFire(%data,%obj,%slot);
   %p.setTarget(%obj.team);
}

function serverCmdSelectWeaponSlot(%client, %data)
{
    // keen: context-based - switching back for now until an option is built in
    %pl = %client.player;
    
    if((%mount = %pl.getObjectMount()) > 0)
        %mount.getDatablock().onTriggerKeys(%mount, %pl, %data);
    else
        %client.getControlObject().selectWeaponSlot( %data );
//        %pl.getDatablock().onTriggerKeys(%pl, %data);
}

function Armor::onTriggerKeys(%data, %obj, %slot)
{
    if(%slot > %data.armorSpecialCount)
        return;

    %obj.unmountImage(0);
    %obj.mountImage(%data.armorSpecial[%slot], 0);

    commandToClient(%obj.client, 'setRepairReticle');
}

exec("scripts/Server/ArmorShared.cs");
execDir("Armors");
