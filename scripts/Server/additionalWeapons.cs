// Load MD3 weapons after all the base weapons are loaded

exec("scripts/Server/weaponSpecialFX.cs");
exec("scripts/weapons/MitziBlastCannon.cs");
exec("scripts/weapons/ThrustGrenade.cs");
exec("scripts/weapons/BatteryGrenade.cs");
exec("scripts/weapons/SpikeRifle.cs");
exec("scripts/weapons/Railgun.cs");
exec("scripts/weapons/Sagittarius.cs");
exec("scripts/weapons/SubspaceMagnet.cs");
exec("scripts/weapons/DualVulcan.cs");
exec("scripts/turrets/plasmaBarrelLarge.cs");
exec("scripts/weapons/PlasmaCannon.cs");
exec("scripts/weapons/Ripper.cs");
exec("scripts/weapons/DualDisruptor.cs");
exec("scripts/weapons/BlasterRifle.cs");
exec("scripts/weapons/PBC.cs");

exec("scripts/weapons/EMPGrenade.cs");
exec("scripts/weapons/NapalmGrenade.cs");
exec("scripts/weapons/SonicPulser.cs");
exec("scripts/weapons/KoiMine.cs");
exec("scripts/weapons/ConcussionMine.cs");
exec("scripts/weapons/RepairPatchMine.cs");
exec("scripts/weapons/CloakingMine.cs");
