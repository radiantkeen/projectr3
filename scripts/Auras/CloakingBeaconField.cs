function CloakingBeaconField::init(%this)
{
    %this.detectMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticObjectType;
    %this.blockMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;
    %this.radius = 30;
}

function CloakingBeaconField::validateAuraTick(%this)
{
    if(!isObject(%this.source))
    {
        %this.destroy();
        return false;
    }

    return true;
}

function CloakingBeaconField::onEnter(%this, %obj)
{
    // keen: bug to fix later
    if(%obj.getType() & %this.blockMask)
    {
        %this.removeFromAura(%obj);
        return;
    }
        
    if(%obj.isDead || %obj.team != %this.source.team || %obj == %this.source)
    {
        %this.removeFromAura(%obj);
        return;
    }

    %obj.cloakjammed = true;
    %obj.setCloaked(true);
}

function CloakingBeaconField::onLeave(%this, %obj)
{
    %obj.cloakjammed = false;
    %obj.setCloaked(false);
}

Aura.registerAura("CloakingBeaconField", $AuraType::Persistent);
