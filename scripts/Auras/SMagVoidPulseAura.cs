function SMagVoidPulseAura::init(%this)
{
    %this.detectMask = $TypeMasks::ProjectileObjectType;
    %this.blockMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType;
    %this.radius = 15;
}

function SMagVoidPulseAura::validateAuraTick(%this)
{
    if(%this.source.hitSomething == true || %this.source.smChargeLevel < 1)
    {
        %this.source.schedule(0, "delete");
        %this.destroy();
        return false;
    }

    return true;
}

function SMagVoidPulseAura::onEnter(%this, %obj)
{
    if(%obj.hitSomething != true)
    {
        %this.source.smChargeLevel -= 2;
        createLifeEmitter(%obj.position, "GreenReflectEmitter", 500);
        %obj.delete();
    }
}

function SMagVoidPulseAura::onLeave(%this, %obj)
{

}

Aura.registerAura("SMagVoidPulseAura", $AuraType::Persistent);
