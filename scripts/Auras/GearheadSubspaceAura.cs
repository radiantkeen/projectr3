function GearheadSubspaceAura::init(%this)
{
    %this.detectMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;
    %this.blockMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType;
    %this.radius = 25;
    
    %this.setTickRate(1000);
}

function GearheadSubspaceAura::validateAuraTick(%this)
{
    if(%this.source.isDead || %this.source.getDatablock().armorType != $ArmorType::Gearhead)
    {
        %this.destroy();
        return false;
    }

    if(%this.source.getEnergyPct() < 0.95)
        return false;
        
    return true;
}

function GearheadSubspaceAura::forEachInAura(%this, %obj)
{
    if(%obj.isDead == true || %obj == %this.source)
        return;

    %obj.incHP(1);
}

Aura.registerAura("GearheadSubspaceAura", $AuraType::Instance);
