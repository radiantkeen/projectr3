<?php
require "lib/sqlinterface.php";
require "lib/common.php";
?>
<!DOCTYPE html>
<html>
<head>
<title>Sol Conflict Stats - Rough</title>
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="lib/keenbasic.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="lib/jquery.tipsy.js"></script>
<script src="lib/keenbasic.js"></script>
</head>
<body>
<h1 class="centertext">Sol Conflict Stats - Quick & Dirty Lookup</h1>
<div class="st_blue st_dashed centerdiv centertext" style="width: 380px;">
<form action="quickstats.php" method="GET">
Lookup by GUID <input type="text" size="7" maxlength="7" name="tbGUID"> <button id="btnSubmit">Search</button></form>
</div>
<div class="st_grey st_thick centerdiv" style="width: 375px;">
Last 10 players updated:<br>
<?php
$dbc = newSQL("md3");
$query = $dbc->prepare("SELECT guid, basename FROM players ORDER BY lastupdated DESC LIMIT 10");
	
if(!$query->execute())
	die("SQL ERROR, please reload page.");
	
$result = $query->get_result();

if($result->num_rows > 0)
{	
	$list = "<ul>";
	
	while(($data = $result->fetch_array(MYSQLI_ASSOC)))
		$list .= sprintf('<li><a href="quickstats.php?guid=%d">%s</a></li>', $data["guid"], $data["basename"]);

	$list .= "</ul>";
	
	echo($list);
}
?>
</div><br>
<?php 
if(isset($_GET['guid']))
{
	$query = $dbc->prepare("SELECT p.guid, p.lastupdated, p.basename, s.servername AS 'Last Seen On', p.totalpoints, p.kills, p.deaths, p.assists, p.suicides, p.teamkills, p.pilotkills, p.vehiclekills, p.vehiclesdestroyed, p.vehiclecrashes, p.gamesplayed, p.gpcollected FROM players p LEFT JOIN servers s ON p.lastserver = s.serverid WHERE guid = ?");
	$query->bind_param("i", $_GET['guid']);
		
	if(!$query->execute())
		die("SQL ERROR, please reload page.");
		
	$result = $query->get_result();

	if($result->num_rows > 0)
	{	
		$firstrun = false;
		$table = '<table cellspacing="2" cellpadding="2" border="1" class="st_thin st_grey centertable"><tbody class="st_grey_data">';
		
		while(($player = $result->fetch_array(MYSQLI_ASSOC)))
		{			
			foreach($player as $key=>$value)
				$table .= "<tr><td>{$key}</td><td>{$value}</td></tr>";
		}
		
		$table .= "</tbody></table>";
		
		echo($table);
	}
	else
		echo("Player not found!");
}
?>
</body>
</html>