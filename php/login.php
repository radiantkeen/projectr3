<?php 
require "lib/sqlinterface.php";
require "lib/common.php";
$loginfailmsg = "LOGIN\tFAILED";
$sqlfaultmsg = "ERROR\t500 SQL SERVER FAULT";
$logindisabledmsg = "LOGIN\tDISABLED";
//error_reporting(E_ALL);

// login: update server IP address and name
if(isset($_POST['login']))
{
	if($_SERVER['HTTP_USER_AGENT'] !== "Tribes 2")
		die($loginfailmsg);
		
	$dbc = newSQL("md3");
	$query = $dbc->prepare("SELECT serverlogin, serverpass, servername, ipaddress, flags FROM servers WHERE serverid = ?");
	$query->bind_param("s", $_POST['sid']);
	
	if(!$query->execute())
		die($sqlfaultmsg);
	
	$result = $query->get_result();

	if($result->num_rows > 0)
	{	
		$data = $result->fetch_array(MYSQLI_ASSOC);
		
		if($data['flags'] & 1)
		{
			$pass = generatePassword($_POST['uid'], $_POST['pwd']);
			
			if($data['serverpass'] !== $pass)
				echo($loginfailmsg);
			else
			{
				$sip = getIPAddr();
				$query = $dbc->prepare("UPDATE servers SET ipaddress = ?, lastupdated = NOW() WHERE serverid = ?");
				$query->bind_param("ss", $sip, $_POST['sid']);
				
				if(!$query->execute())
					die($sqlfaultmsg);
					
				echo("LOGIN\tOK");
			}
		}
		else
			echo($logindisabledmsg);
	}
	else
		echo($loginfailmsg . "\tACCOUNT NOT FOUND");
}
else if(isset($_GET['playerdata'])) // Grab player stats
{
	if($_SERVER['HTTP_USER_AGENT'] !== "Tribes 2")
		die($loginfailmsg);
	
	$guid = (int)$_GET['guid'];
	
	$dbc = newSQL("md3");
	$query = $dbc->prepare("SELECT totalpoints FROM players WHERE guid = ?");
	$query->bind_param("i", $guid);
	
	if(!$query->execute())
		die($sqlfaultmsg);
	
	$result = $query->get_result();
	$header = "DATA\t" . $guid;
	$output = "";
	$numenh = 0;
	$numvenh = 0;
	
	if($result->num_rows > 0)
	{	
		$data = $result->fetch_array(MYSQLI_ASSOC);
		
		foreach($data as $field)
			$output .= "\t{$field}";
	}
	else
	{
		$result->close();
		
		$name = base64_decode($_GET['name']);
		$query = $dbc->prepare("INSERT INTO players (guid, lastupdated, basename, flags) VALUES (?, NOW(), ?, 1)");
		$query->bind_param("is", $guid, $name);
		
		if(!$query->execute())
			die($sqlfaultmsg);
			
		$query = $dbc->prepare("SELECT totalpoints FROM players WHERE guid = ?");
		$query->bind_param("i", $guid);
		
		if(!$query->execute())
			die($sqlfaultmsg);
		
		$result = $query->get_result();
		
		$data = $result->fetch_array(MYSQLI_ASSOC);
		
		foreach($data as $field)
			$output .= "\t{$field}";		
	}
	
	$output .= "\t";
	
	$result->close();
	
	$query = $dbc->prepare("SELECT * FROM enhloadouts WHERE guid = ?");
	$query->bind_param("i", $guid);
	
	if(!$query->execute())
		die($sqlfaultmsg);
	
	$result = $query->get_result();
	$numenh = $result->num_rows;
	
	if($result->num_rows > 0)
	{
		while($data = $result->fetch_array(MYSQLI_ASSOC))
		{
			$enhid = $guid . $data["enhid"];
			
			$output .= sprintf("%s\t%s\t%s\t", "enhtype", $enhid, $data["enhtype"]);
			$output .= sprintf("%s\t%s\t%s\t", "enhitem", $enhid, $data["item"]);
			$output .= sprintf("%s\t%s\t%s\t", "enhname", $enhid, $data["loadoutname"]);
			$output .= sprintf("%s\t%s\t%s\t", "eslot0", $enhid, $data["slot0"]);
			$output .= sprintf("%s\t%s\t%s\t", "eslot1", $enhid, $data["slot1"]);
			$output .= sprintf("%s\t%s\t%s\t", "eslot2", $enhid, $data["slot2"]);
			$output .= sprintf("%s\t%s\t%s\t", "eslot3", $enhid, $data["slot3"]);
			$output .= sprintf("%s\t%s\t%s\t", "eslot4", $enhid, $data["slot4"]);
			$output .= sprintf("%s\t%s\t%s\t", "eslot5", $enhid, $data["slot5"]);
		}
	}	
	
	$result->close();
	
	$query = $dbc->prepare("SELECT * FROM vehicleloadouts WHERE guid = ?");
	$query->bind_param("i", $guid);
	
	if(!$query->execute())
		die($sqlfaultmsg);
	
	$result = $query->get_result();
	$numvenh = $result->num_rows;
	
	if($result->num_rows > 0)
	{	
		while($data = $result->fetch_array(MYSQLI_ASSOC))
		{
			$vehid = $guid . $data["vehid"];
			
			$output .= sprintf("%s\t%s\t%s\t", "vehitem", $vehid, $data["vdata"]);
			$output .= sprintf("%s\t%s\t%s\t", "vehmask", $vehid, $data["firemask"]);
			$output .= sprintf("%s\t%s\t%s\t", "vehname", $vehid, $data["loadoutname"]);
			$output .= sprintf("%s\t%s\t%s\t", "vslot0", $vehid, $data["slot0"]);
			$output .= sprintf("%s\t%s\t%s\t", "vslot1", $vehid, $data["slot1"]);
			$output .= sprintf("%s\t%s\t%s\t", "vslot2", $vehid, $data["slot2"]);
			$output .= sprintf("%s\t%s\t%s\t", "vslot3", $vehid, $data["slot3"]);
			$output .= sprintf("%s\t%s\t%s\t", "vslot4", $vehid, $data["slot4"]);
			$output .= sprintf("%s\t%s\t%s\t", "vslot5", $vehid, $data["slot5"]);
			$output .= sprintf("%s\t%s\t%s\t", "vslot6", $vehid, $data["slot6"]);
			$output .= sprintf("%s\t%s\t%s\t", "vslot7", $vehid, $data["slot7"]);
			$output .= sprintf("%s\t%s\t%s\t", "venh0", $vehid, $data["enh0"]);
			$output .= sprintf("%s\t%s\t%s\t", "venh1", $vehid, $data["enh1"]);
			$output .= sprintf("%s\t%s\t%s\t", "venh2", $vehid, $data["enh2"]);
			$output .= sprintf("%s\t%s\t%s\t", "venh3", $vehid, $data["enh3"]);
			$output .= sprintf("%s\t%s\t%s\t", "venh4", $vehid, $data["enh4"]);
			$output .= sprintf("%s\t%s\t%s\t", "venh5", $vehid, $data["enh5"]);
		}
	}
	
	$output .= "END";
	$header .= "\t{$numenh} {$numvenh}";
	$output = $header . $output;
	
	echo($output);
}
else if(isset($_POST['updatestats'])) // Update player stats
{
	//if($_SERVER['HTTP_USER_AGENT'] !== "Tribes 2")
		//die($loginfailmsg);
		
	$dbc = newSQL("md3");
	$query = $dbc->prepare("SELECT ipaddress, flags FROM servers WHERE serverid = ?");
	$query->bind_param("s", $_POST['sid']);
	
	if(!$query->execute())
		die($sqlfaultmsg);
	
	$result = $query->get_result();

	if($result->num_rows > 0)
	{	
		$sip = getIPAddr();	
		$data = $result->fetch_array(MYSQLI_ASSOC);
		
		if(($data['flags'] & 1) == 0)
			die($logindisabledmsg);
			
		if($data['ipaddress'] !== $sip)
			die($loginfailmsg);
			
		$result->close();
		
		$query = $dbc->prepare("UPDATE players SET totalpoints = totalpoints + ?, kills = kills + ?, deaths = deaths + ?, assists = assists + ?, suicides = suicides + ?, teamkills = teamkills + ?, makills = makills + ?, pilotkills = pilotkills + ?, vehiclekills = vehiclekills + ?, vehiclesdestroyed = vehiclesdestroyed + ?, vehiclecrashes = vehiclecrashes + ?, gamesplayed = gamesplayed + ?, gpcollected = gpcollected + ?, lastserver = ?, lastupdated = NOW() WHERE guid = ?");
		$query->bind_param("iiiiiiiiiiiiisi", $_POST['pointsthismatch'], $_POST['kills'], $_POST['deaths'], $_POST['assists'], $_POST['suicides'], $_POST['teamkills'], $_POST['makills'], $_POST['pilotkills'], $_POST['vehiclekills'], $_POST['vehiclesdestroyed'], $_POST['vehiclecrashes'], $_POST['gamesplayed'], $_POST['gpcollected'], $_POST['sid'], $_POST['guid']);
				
		if(!$query->execute())
			die($sqlfaultmsg);

		$enhloadouts = json_decode($_POST['enh'], true);
		
		if(sizeof($enhloadouts) > 0)
		{
			$query = $dbc->prepare("DELETE FROM enhloadouts WHERE guid = ?");
			$query->bind_param("i", $_POST['guid']);
					
			if(!$query->execute())
				die($sqlfaultmsg);
		
			foreach($enhloadouts as $enh)
			{
				$enhname = base64_decode($enh['name']);
				
				$query = $dbc->prepare("INSERT INTO enhloadouts VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				$query->bind_param("iiissssssss", $_POST['guid'], $enh['enhid'], $enh['type'], $enhname, $enh['item'], $enh['slot0'], $enh['slot1'], $enh['slot2'], $enh['slot3'], $enh['slot4'], $enh['slot5']);
						
				if(!$query->execute())
					die($sqlfaultmsg);			
			}
		}
		
		$vehloadouts = json_decode($_POST['veh'], true);
		
		if(sizeof($vehloadouts) > 0)
		{
			$query = $dbc->prepare("DELETE FROM vehicleloadouts WHERE guid = ?");
			$query->bind_param("i", $_POST['guid']);
					
			if(!$query->execute())
				die($sqlfaultmsg);
		
			foreach($vehloadouts as $veh)
			{
				$vehname = base64_decode($veh['name']);
				
				$query = $dbc->prepare("INSERT INTO vehicleloadouts VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				$query->bind_param("iisssssssssssssssss", $_POST['guid'], $veh['vehid'], $veh['vdata'], $vehname, $veh['slot0'], $veh['slot1'], $veh['slot2'], $veh['slot3'], $veh['slot4'], $veh['slot5'], $veh['slot6'], $veh['slot7'], $veh['enh0'], $veh['enh1'], $veh['enh2'], $veh['enh3'], $veh['enh4'], $veh['enh5'], $veh['mask']);
						
				if(!$query->execute())
					die($sqlfaultmsg);			
			}
		}		
			
		echo("STATUS\tOK\t" . $_POST['guid']);
	}
}
else if(isset($_POST['registerlogin'])) // used in register.php
{
	if(!isset($_POST['tbPassword']) || isset($_POST['tbPassword']) && strlen($_POST['tbPassword']) < 8)
		die("Error: Password too short.");

	if(!isset($_POST['tbLoginName']) || isset($_POST['tbLoginName']) && strlen($_POST['tbLoginName']) < 3)
		die("Error: Server name too short.");
		
	$dbc = newSQL("md3");
	
	$query = $dbc->prepare("SELECT serverlogin FROM servers WHERE serverlogin = ?");
	$query->bind_param("s", $_POST['tbLoginName']);
	
	if(!$query->execute())
		die($sqlfaultmsg);
		
	$query->bind_result($iptest);
	$result = $query->fetch(); 

	if($result)
		die("Error: Server already registered.");
	
	$serverid = generateServerID();
	$password = generatePassword($_POST['tbLoginName'], $_POST['tbPassword']);
	
	$query = $dbc->prepare("INSERT INTO servers VALUES (?, ?, ?, ?, ?, 1)");
	$query->bind_param("sssss", $serverid, $_POST['tbLoginName'], $password, $_POST['tbServerName'], $_POST['tbIPAddress']);
	
	if(!$query->execute())
		die($sqlfaultmsg);
		
	echo("Success: {$serverid}");
}
else if(isset($_GET['testlogin'])) // used in register.php
{
	$passout = generatePassword($_GET['uid'], $_GET['pwd']);
		
	echo($passout);
}
else
	echo($loginfailmsg);
?>